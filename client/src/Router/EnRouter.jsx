import React, { Component, Fragment } from "react";
import Subscription from "../components/Pages/Subscription";
import Store from "../components/Pages/Store";
import { Route, Switch } from "react-router-dom";
import Sidebar from "../components/Layout/sidebar";

import SingleApp from "../components/Pages/single page/app";
import Contact from "../components/Pages/Contact";
import View from "../components/Pages/Profile/View";
import Edit from "../components/Pages/Profile/Edit";
import Orders from "../components/Pages/Profile/Orders";
import ForgotPassword from "../components/Pages/ForgotPassword";
import Login from "../components/Pages/Auth/Login/Login";
import Register from "../components/Pages/Auth/Register/Register";
import Checkout from "../components/Pages/checkout/Checkout";
import Paymentsuccess from "../components/extrapages/Paymentsuccess";
import verificationLink from "../components/extrapages/Verificationlink/Verificationlink";
import confirmationLink from "../components/extrapages/Confirmationlink/Confirmationlink";
import resetPasswordLink from "../components/extrapages/Verificationlink/ResetPasswordLink";
import resetPasswordSuccess from "../components/extrapages/Verificationlink/ResetPasswordSuccess";
import resetPassword from "../components/Pages/ResetPassword";
import Invoice from "../components/Pages/Profile/invoice";
import Paymentcancelled from "../components/extrapages/Paymentfailed";
import Aboutus from "../components/extrapages/Aboutus";
import PrivacyPolicy from "../components/extrapages/PrivacyPolicy";
import CacelationRefundPolicy from "../components/extrapages/CacelationRefundPolicy";
import TermsService from "../components/extrapages/TermsService";
import BankTransferSuccess from "../components/extrapages/BankTransferSuccess";
import AppType from "../components/Pages/AppType";
import ConfirmOrderSuccess from "../components/extrapages/ConfirmOrderSuccess";
import CallBackConfirmation from "../components/extrapages/CallBackConfirmation";
import ResumeSubmitted from "../components/extrapages/ResumeSubmitConfirmation";
import PaymentRedirect from "../components/Pages/PaymentForm";
import "../assets/css/animate.css";
import "../index.css";
import ApplicationRequest from "../components/Pages/ApplicationRequest";
import DemoRequests from "../components/Pages/DemoRequests";
import DemoRequestsSucess from "../components/extrapages/DemoRequestConfirmation";
import PartnerConfirmation from "../components/extrapages/PartnerConfirmationSucess";
import Faqs from "../components/extrapages/Faq";
// import EcommMarket from "../components/extrapages/ecommMarket";
import PageNotFound from "../components/extrapages/404NotFound";
import BecomeOurPartner from "../components/Pages/BecomeOurPartner";
import Careers from "../components/Pages/careers/careers";
import ApplyForJob from "../components/Pages/careers/applyJob";
import HomeNew from "../components/Pages/HomeNew";
import EcommerceLandingPage from "../components/Pages/EcommerceLandingPage";
import BookingLandingPage from "../components/Pages/BookingLandingPage";
import MobileApp from "../components/MobileAppView/MobileAppPage";
import eventMeetup from "../components/Pages/Meetup";
import Clientele from "../components/Pages/Clientele";
import CheckoutJourney from "../components/Pages/CheckoutJourney/CheckoutJourney";
import FacilityManagement from "../components/Pages/FacilityManagement"; 
import RestaurantApp from "../components/Pages/RestaurentLandingPage"; 
import RestaurantAppDemo from "../components/Pages/RestaurentLandingPageDemo"; 

export class EnRouter extends Component {
  render() {
    return (
      <Fragment>
        <Switch>
          {/* <Route path="/home" component={HomeNew} exact /> */}
          <Route
            path="/multi-vendor-marketplace"
            component={EcommerceLandingPage}
            exact
          />
          <Route
            path="/facility-management-application"
            component={FacilityManagement}
            exact
          />
          <Route
            path="/restaurant-application"
            component={RestaurantApp}
            exact
          />
          <Route
            path="/restaurant-application-demo"
            component={RestaurantAppDemo}
            exact
          />
          <Route path="/bookingsolution" component={BookingLandingPage} exact />
          <Route path="/Login" component={Login} exact />
          <Route path="/SignUp" component={Register} exact />
          <Route path="/factory" component={Store} exact />
          <Route path="/factory/:type" component={Store} exact />
          <Route path="/readymade-apps/:slug/:id" component={SingleApp} exact />
          <Route path="/pricing" component={Subscription} exact />
          <Route
            path="/readymade-apps/:slug/:id/checkout/:price"
            component={Checkout}
          />
          {/* <Route path="/support" component={Contact} exact /> */}
          <Route path="/contactUs" component={Contact} exact />
          <Route path="/my-profile" component={View} exact />
          <Route path="/edit-profile" component={Edit} exact />
          <Route path="/my-orders" component={Orders} exact />
          {/* <Route path="/my-payments" component={Payments} exact /> */}
          <Route path="/Paymentsuccess/:OrderID" component={Paymentsuccess} />
          <Route
            path="/Paymentcancelled/:OrderID"
            component={Paymentcancelled}
          />
          <Route path="/verifyaccount" component={verificationLink} />
          <Route path="/confirmation" component={confirmationLink} />
          <Route path="/forgot-password" component={ForgotPassword} exact />
          <Route path="/forgot-password/token" component={resetPasswordLink} />
          <Route
            path="/reset-password/success"
            component={resetPasswordSuccess}
            exact
          />
          <Route
            path="/reset-password/:token"
            component={resetPassword}
            exact
          />
          <Route path="/my-orders/:invoice" component={Invoice} />
          <Route path="/cms/admin/login" component={Login} />
          <Route path="/about-us" component={Aboutus} exact />
          <Route path="/privacy-policy" component={PrivacyPolicy} exact />
          <Route
            path="/cancellation-and-refund-policy"
            component={CacelationRefundPolicy}
            exact
          />
          <Route path="/terms-service" component={TermsService} exact />
          <Route
            path="/bank-transfer-success"
            component={BankTransferSuccess}
            exact
          />
          <Route
            path="/confirm-order-success"
            component={ConfirmOrderSuccess}
            exact
          />
          {/* <Route path="/industries/:industry" component={Industries} exact /> */}
          <Route path="/type/:type" component={AppType} exact />
          <Route
            path="/contactUs/call-back-confirmation"
            component={CallBackConfirmation}
            exact
          />
          <Route path="/PaymentRedirect" component={PaymentRedirect} exact />
          <Route path="/app-requests" component={ApplicationRequest} exact />
          <Route path="/demo-requests" component={DemoRequests} exact />
          <Route path="/faqs" component={Faqs} exact />
          {/* <Route
            path="/mobile-application-development"
            component={EcommMarket}
            exact
          /> */}
          <Route
            path="/demo-requests/success"
            component={DemoRequestsSucess}
            exact
          />
          <Route
            path="/become-our-partner"
            component={BecomeOurPartner}
            exact
          />
          <Route path="/careers" component={Careers} exact />
          <Route path="/career/apply/:id" component={ApplyForJob} exact />
          <Route
            path="/career/apply/resume/sumbitted"
            component={ResumeSubmitted}
            exact
          />

          <Route
            path="/become-our-partner/sumbitted"
            component={PartnerConfirmation}
            exact
          />
          <Route path="/sri-lanka/meetup" component={eventMeetup} exact />
          <Route
            path="/events/sumbitted"
            component={PartnerConfirmation}
            exact
          />
          <Route path="/clientele" component={Clientele} exact />
          <Route path="/mobileapp" component={MobileApp} exact />
          <Route path="/journey" component={CheckoutJourney} exact />
          <Route component={PageNotFound} />
        </Switch>
      </Fragment>
    );
  }
}

export default EnRouter;
