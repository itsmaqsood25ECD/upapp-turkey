import { ALL_CUSTOMER_SUCCESS, ALL_CUSTOMER_FAIL, GET_CUSTOMER_SUCCESS, GET_CUSTOMER_FAIL } from "../../actions/constants";

const initialState = {
  Customers: [],
  loading: true,
  Customer:{}
};

export default function (state = initialState, action) {
  const { type, payload } = action;
  switch (type) {
    case ALL_CUSTOMER_SUCCESS:
      return {
        ...state,
        Customers: payload.result,
        loading: false
      };
    case GET_CUSTOMER_SUCCESS:
      return {
        ...state,
        Customer: payload.result,
        loading: false
      };
  
    default:

      return {
        state
      }
  }
}