import {
  LOAD_JOB_SUCCESS,
  GET_JOB_FETCH_SUCCESS,
  LOAD_JOB_FAIL
} from "../../actions/constants";

const initialState = {
  Jobs: null,
  Job: null,
  loading: true
};

export default function(state = initialState, action) {
  const { type, payload, edit } = action;
  switch (type) {
    case LOAD_JOB_SUCCESS:
      return {
        ...state,
        Jobs: payload.result
      };
    case GET_JOB_FETCH_SUCCESS:
      return {
        ...state,
        Job: payload.result
      };
    case LOAD_JOB_FAIL:
      return {
        ...state,
        Jobs: null,
        Job: null
      };
    default:
      return state;
  }
}
