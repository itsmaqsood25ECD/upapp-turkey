import {
  GET_BLOGS,
  GET_BLOG,
  BLOG_FAIL,
  BLOG_SUCCESS,
  GET_RECENT_BLOG,
  GET_BLOG_FAIL,
  BLOG_DELETE_FAIL,
  BLOG_DELETE_SUCCESS
} from "../../actions/constants";

const initialState = {
  Blogs: [],
  Blog: null,
  recentBlog: [],
  loading: true,
  refresh: false
};

export default function (state = initialState, action) {
  const { type, payload, edit } = action;
  switch (type) {
    case GET_BLOGS:
      return {
        ...state,
        Blogs: payload.result,
        loading: false,
        refresh: false
      };
    case BLOG_SUCCESS:
      return {
        ...state,
        refresh: true
      }
    case GET_BLOG:
      console.log(payload.result, "payload")
      return {
        ...state,
        Blog: payload.result,
        loading: false
      };
    case GET_RECENT_BLOG:
      return {
        ...state,
        recentBlog: payload.result,
        loading: false
      }
    case BLOG_FAIL:
      return {
        ...state,
        Blogs: [],
        Blog: [],
        recentBlog: null,
        loading: false,
        refresh: false
      };
    case GET_BLOG_FAIL:
      return {
        ...state,
        Blog: [],
        loading: false
      };
    case BLOG_DELETE_FAIL:
      return {
        ...state,
        loading: false
      }
    case BLOG_DELETE_SUCCESS:
      return {
        ...state,
        refresh: true
      }
    default:
      return state;
  }
}
