import {
	GET_ALL_ORDERS,
	ALL_ORDERS_ERROR,
	GET_CANCELLED_ORDERS
} from '../../actions/constants';

const initialState = {
	orders: [],
	loading: true,
	cancelledOrders: []
};

export default function(state = initialState, action) {
	const { type, payload } = action;
	switch (type) {
		case GET_ALL_ORDERS:
			return {
				...state,
				orders: payload,
				loading: false
			};
		case GET_CANCELLED_ORDERS:
			return {
				...state,
				cancelledOrders: payload,
				loading: false
			};
		case ALL_ORDERS_ERROR:
			return {
				...state,
				loading: false
			};
		default:
			return state;
	}
}
