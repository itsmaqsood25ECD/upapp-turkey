import {
	GET_COUPONS,
	COUPONS_ERROR,
	GET_COUPON_DETAILS,
	COUPON_SUCCESS
} from "../../actions/constants";

const initialState = {
	allcoupons: [],
	loading: true,
	isSubmitted: false
};

export default function(state = initialState, action) {
	const { type, payload } = action;
	switch (type) {
		case GET_COUPONS:
			return {
				...state,
				allcoupons: payload,
				loading: false
			};
		case COUPONS_ERROR:
			return {
				...state,
				loading: false
			};
		case GET_COUPON_DETAILS:
			return {
				...state,
				coupon_details: payload,
				loading: false
			};
		case COUPON_SUCCESS:
			return {
				...state,
				isSubmitted: true,
				loading: false
			};
		default:
			return state;
	}
}
