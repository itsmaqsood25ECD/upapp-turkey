import { GET_CONTACT } from "../../actions/constants";

const initialState = {
  // isSubmitted: null,
  loading: false,
  requests: []
};

export default function (state = initialState, action) {
  const { type, payload } = action;
  switch (type) {
    case GET_CONTACT:
      return {
        ...state,
        requests: payload.result,
        loading: false
      }
    default:
      return state;
  }
}
