import {
	GET_PAYMENTDETAILS,
	PROCEED_TO_PAY,
	PAYMENT_FAILED,
	GET_COUPON_DETAILS
} from "../actions/constants";

const initialState = {
	Product: null,
	loading: true,
	redirect: false,
	success: false
};

export default function (state = initialState, action) {
	const { type, payload } = action;
	switch (type) {
		case GET_PAYMENTDETAILS:
			return {
				...state,
				Product: payload,
				loading: false
			};
		case PROCEED_TO_PAY:
			return {
				...state,
				order: payload.result,
				loading: false,
				success: true
			};
		case GET_COUPON_DETAILS:
			return {
				...state,
				coupon_details: payload,
				loading: false
			};
		case PAYMENT_FAILED:
			return {
				...state,
				loading: false,
				url: false,
				success: false
			};
		default:
			return state;
	}
}
