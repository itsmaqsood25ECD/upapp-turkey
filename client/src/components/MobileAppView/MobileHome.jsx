import React, { useEffect, useState, Fragment } from "react";
import {
  Flex,
  WhiteSpace,
  SearchBar,
  TabBar,
  Carousel,
  WingBlank,
  NavBar,
  Icon,
  Grid,
  Steps,
  InputItem,
  Button,
  Drawer
} from "antd-mobile";
import "antd-mobile/dist/antd-mobile.css";
import "../../assets/css/mobile-ui.css";
import "react-perfect-scrollbar/dist/css/styles.css";
import PerfectScrollbar from "react-perfect-scrollbar";

const Step = Steps.Step;

const PlaceHolder = ({ className = "", ...restProps }) => (
  <div className={`${className} placeholder`} {...restProps}>
    Block
  </div>
);

const MobileHome = props => {
  const [state, setState] = useState({
    selectedTab: "blueTab",
    hidden: false,
    fullScreen: false,
    primary_color: "#333",
    secondry_color: "333",
    bg_color: "#fff",
    data: ["1", "2", "3"]
    // imgHeight: 176
  });

  const customIcon = [
    {
      cart: (
        <svg
          version="1.0"
          id="Layer_1"
          xmlns="http://www.w3.org/2000/svg"
          x="0px"
          y="0px"
          width="23.3px"
          height="22.2px"
          viewBox="0 0 23.3 22.2"
          enable-background="new 0 0 23.3 22.2"
        >
          <g>
            <path
              fill={state.primary_color}
              d="M0.4,1.4C0.3,1.7,0.4,2.1,1,2.3h3.6c0,0,0.6,0.3,1.1,1.8L8,14c0,0,0.4,1.9,2.4,1.8l6.9,0
 c0,0,2.3-0.5,2.9-2.2l2.4-5.8c0,0,1.1-2.4-1.8-3.3L7.8,4.4c0,0-0.4-3.4-2.9-3.9H1.6C1.1,0.5,0.6,0.9,0.4,1.4z"
            />
            <path
              fill="#FFFFFF"
              d="M8.1,6.5l1.9,7.3h7.2c0,0,1.1-0.4,1.5-1.6L21,7.4c0,0,0.3-0.9-0.3-0.9H8.1z"
            />
            <circle
              fill="none"
              stroke={state.primary_color}
              stroke-width="1.5"
              stroke-miterlimit="10"
              cx="9"
              cy="18.8"
              r="1.7"
            />
            <circle
              fill="none"
              stroke={state.primary_color}
              stroke-width="1.5"
              stroke-miterlimit="10"
              cx="18.6"
              cy="18.8"
              r="1.7"
            />
          </g>
        </svg>
      ),
      delivery: (
        <svg
          version="1.0"
          id="Layer_1"
          xmlns="http://www.w3.org/2000/svg"
          x="0px"
          y="0px"
          viewBox="0 0 19.9 25.6"
          enable-background="new 0 0 19.9 25.6"
        >
          <g>
            <path
              fill="none"
              stroke="#000000"
              stroke-width="1.5"
              stroke-miterlimit="10"
              d="M18.5,10.8c-0.3,5.8-7.2,13-8.5,13
		c-1.3,0-8-7.3-8.3-13c-0.3-5.6,3.8-9,8.4-9C14.8,1.7,18.9,5.2,18.5,10.8z"
            />
            <circle
              fill="none"
              stroke="#000000"
              stroke-width="1.5"
              stroke-miterlimit="10"
              cx="10.2"
              cy="10.1"
              r="3"
            />
          </g>
        </svg>
      ),
      Money: (
        <svg
          version="1.0"
          id="Layer_1"
          xmlns="http://www.w3.org/2000/svg"
          x="0px"
          y="0px"
          viewBox="0 0 20.2 23.7"
          enable-background="new 0 0 20.2 23.7"
        >
          <g>
            <g>
              <path
                d="M9.2,13.3c-1.6-0.4-2.7-1.1-3.5-1.9c-0.8-0.8-1.1-2-0.9-3.5c0.1-1.2,0.7-2.2,1.8-3C7.7,4.2,9,3.8,10.5,3.8
			c1.5,0,2.8,0.4,3.9,1.1s1.7,1.9,1.8,3.5h-3.1c-0.1-0.6-0.4-1-0.9-1.4s-1.1-0.5-1.7-0.5C10,6.5,9.4,6.7,8.9,7
			C8.5,7.2,8.2,7.6,8.1,8C7.9,8.6,8,9,8.1,9.3c0.3,0.5,0.9,1,1.9,1.3l2.4,0.6c2.9,0.7,4.3,2.3,4.3,4.8c0,1.5-0.6,2.7-1.8,3.6
			c-1.2,0.9-2.7,1.4-4.5,1.4c-1.6,0-3-0.5-4.2-1.5c-1.2-1-1.9-2.2-1.9-3.9h2.9c0.1,0.9,0.6,1.5,1.3,1.9c0.7,0.4,1.6,0.6,2.6,0.6
			c0.8-0.1,1.3-0.3,1.8-0.7s0.7-0.9,0.7-1.5c0-0.6-0.2-1-0.6-1.3c-0.4-0.3-1-0.6-1.6-0.7L9.2,13.3z"
              />
            </g>
            <g>
              <rect x="9.3" y="1.4" width="2.5" height="21.4" />
            </g>
          </g>
        </svg>
      ),
      flag: (
        <svg
          version="1.0"
          id="Layer_1"
          xmlns="http://www.w3.org/2000/svg"
          x="0px"
          y="0px"
          viewBox="0 0 18.2 22.4"
          enable-background="new 0 0 18.2 22.4"
        >
          <g>
            <rect
              x="1.5"
              y="3"
              stroke="#000000"
              stroke-miterlimit="10"
              width="7.9"
              height="14.4"
            />
            <path
              stroke="#000000"
              stroke-miterlimit="10"
              d="M1.3,21.4h1.9c0.1,0,0.2-0.1,0.2-0.2V1.1c0-0.1-0.1-0.2-0.2-0.2H1.3
		C1.2,0.9,1.1,1,1.1,1.1v20.1C1.1,21.3,1.2,21.4,1.3,21.4z"
            />
            <polygon
              fill="none"
              stroke="#000000"
              stroke-width="1.5"
              stroke-miterlimit="10"
              points="10.3,4.7 16.5,4.7 14.2,11.1 16.5,17.2 
		9.6,17.2 9.6,4.7 	"
            />
          </g>
        </svg>
      )
    }
  ];
  const CategoryIcon = [
    "https://upappfactory-static-images.s3.us-east-2.amazonaws.com/mobile-apps-showcase/categories_icon/Men/MensAccessories.png",
    "https://upappfactory-static-images.s3.us-east-2.amazonaws.com/mobile-apps-showcase/categories_icon/Men/MensClothing.png",
    "https://upappfactory-static-images.s3.us-east-2.amazonaws.com/mobile-apps-showcase/categories_icon/Men/MensInnerwear.png",
    "https://upappfactory-static-images.s3.us-east-2.amazonaws.com/mobile-apps-showcase/categories_icon/Men/MensJewellery.png",
    "https://upappfactory-static-images.s3.us-east-2.amazonaws.com/mobile-apps-showcase/categories_icon/Men/MensShoes.png",
    "https://upappfactory-static-images.s3.us-east-2.amazonaws.com/mobile-apps-showcase/categories_icon/Men/MensWatches.png"
  ];

  const data = [
    "https://upappfactory-static-images.s3.us-east-2.amazonaws.com/mobile-apps-showcase/banner/man.png",
    "https://www.figaromodels.com.my/en/wp-content/uploads/2018/04/homebanner02.jpg",
    "http://www.tauragietis.lt/includes/templates/broglia_mobile//images/banner.jpg"
  ];

  useEffect(() => {
    setState({
      ...state,
      primary_color: props.primary_color,
      secondry_color: props.secondry_color,
      bg_color: props.bg_color,
      logo_img: props.logo_img
    });
  }, [props]);

  // console.log(props);

  const renderContent = pageText => {
    return (
      <PerfectScrollbar>
        <div
          style={{
            backgroundColor: `${state.bg_color}`,
            height: "100%",
            textAlign: "center",
            color: state.primary_color
          }}
        >
          <div style={{ paddingTop: 60 }}>
            Clicked “{pageText}” tab， show “{pageText}” information
          </div>
          <a
            style={{
              display: "block",
              marginTop: 40,
              marginBottom: 20,
              color: state.secondry_color
            }}
            onClick={e => {
              e.preventDefault();
              setState({
                hidden: !state.hidden
              });
            }}
          >
            Click to show/hide tab-bar
            <br />
            <br />
            <br />
            <br />
            <br />
            <br />
            Click to show/hide tab-bar
            <br />
            <br />
            <br />
            Click to show/hide tab-bar
            <br />
            Click to show/hide tab-barbr
            <br />
          </a>
          <div style={{ height: "500px" }}>sdfdsfsdf</div>
          <a
            //   style={{ display: "block", marginBottom: 600, color: state.secondry_color }}
            onClick={e => {
              e.preventDefault();
              setState({
                ...state,
                fullScreen: !state.fullScreen
              });
            }}
          >
            Click to switch fullscreen
          </a>
        </div>
      </PerfectScrollbar>
    );
  };

  return (
    <div
      className="flex-container mobile-container"
      style={{
        background: `${state.bg_color}`
      }}
    >
      <div style={{ height: "568px" }}>
        <TabBar
          unselectedTintColor="#949494"
          tintColor={state.primary_color}
          barTintColor={state.bg_color}
          hidden={state.hidden}
        >
          <TabBar.Item
            // title="Home"
            key="Life"
            icon={
              <div className="tab-bar">
                <svg
                  version="1.0"
                  id="Layer_1"
                  xmlns="http://www.w3.org/2000/svg"
                  x="0px"
                  y="0px"
                  width="22.5px"
                  height="22.2px"
                  viewBox="0 0 22.5 22.2"
                  enable-background="new 0 0 22.5 22.2"
                >
                  <g>
                    <path
                      fill="#0F0F0F"
                      d="M2.5,11c1.1-0.9,5.9-4.7,7.9-6.4c0.6-0.4,1.3-0.4,1.9,0l7.3,6.3c0.3,0.3,0.8,0.4,1.3,0.3
		c0.7-0.2,1.6-0.7,1-2.2c-0.1-0.2-0.2-0.4-0.4-0.6c-3-2.6-5.9-5.1-8.9-7.7c-0.3-0.3-0.7-0.4-1.1-0.4h-0.8c-0.4,0-0.8,0.2-1.2,0.4
		C6.6,3.5,3.7,6.1,0.8,8.7C0.5,9,0.2,9.4,0.3,9.8c0,0.8,0.3,1.8,1.6,1.5C2.1,11.2,2.3,11.1,2.5,11z"
                    />
                    <path
                      fill="#100F10"
                      d="M5.9,12c-0.3-0.4-1.3-1.3-2.4,0c-0.1,0.1-0.1,0.2-0.1,0.3v7.4v0.3c0,0.9,0.7,1.6,1.6,1.6h11.9
		c1,0,1.8-0.8,1.8-1.8v-0.2v-7.5c0-0.1,0-0.2-0.1-0.2c-0.3-0.4-1.3-1.5-2.2,0c0,0.1-0.1,0.1-0.1,0.2l0,6.8c0,0.2-0.2,0.4-0.4,0.4
		l-9.6,0c-0.2,0-0.4-0.2-0.4-0.4l0-6.6C6,12.2,5.9,12.1,5.9,12z"
                    />
                    <circle
                      fill="#0E0D0D"
                      stroke="#0E0C0C"
                      stroke-width="1.4871"
                      stroke-miterlimit="10"
                      cx="11"
                      cy="12.2"
                      r="2.6"
                    />
                  </g>
                </svg>
              </div>
            }
            selectedIcon={
              <div className="tab-bar">
                <svg
                  version="1.0"
                  id="Layer_1"
                  xmlns="http://www.w3.org/2000/svg"
                  x="0px"
                  y="0px"
                  width="22.5px"
                  height="22.2px"
                  viewBox="0 0 22.5 22.2"
                  enable-background="new 0 0 22.5 22.2"
                >
                  <g>
                    <path
                      fill={state.primary_color}
                      d="M2.5,11c1.1-0.9,5.9-4.7,7.9-6.4c0.6-0.4,1.3-0.4,1.9,0l7.3,6.3c0.3,0.3,0.8,0.4,1.3,0.3
  c0.7-0.2,1.6-0.7,1-2.2c-0.1-0.2-0.2-0.4-0.4-0.6c-3-2.6-5.9-5.1-8.9-7.7c-0.3-0.3-0.7-0.4-1.1-0.4h-0.8c-0.4,0-0.8,0.2-1.2,0.4
  C6.6,3.5,3.7,6.1,0.8,8.7C0.5,9,0.2,9.4,0.3,9.8c0,0.8,0.3,1.8,1.6,1.5C2.1,11.2,2.3,11.1,2.5,11z"
                    />
                    <path
                      fill={state.primary_color}
                      d="M5.9,12c-0.3-0.4-1.3-1.3-2.4,0c-0.1,0.1-0.1,0.2-0.1,0.3v7.4v0.3c0,0.9,0.7,1.6,1.6,1.6h11.9
  c1,0,1.8-0.8,1.8-1.8v-0.2v-7.5c0-0.1,0-0.2-0.1-0.2c-0.3-0.4-1.3-1.5-2.2,0c0,0.1-0.1,0.1-0.1,0.2l0,6.8c0,0.2-0.2,0.4-0.4,0.4
  l-9.6,0c-0.2,0-0.4-0.2-0.4-0.4l0-6.6C6,12.2,5.9,12.1,5.9,12z"
                    />
                    <circle
                      fill={state.primary_color}
                      stroke={state.primary_color}
                      stroke-width="1.4871"
                      stroke-miterlimit="10"
                      cx="11"
                      cy="12.2"
                      r="2.6"
                    />
                  </g>
                </svg>
              </div>
            }
            selected={state.selectedTab === "blueTab"}
            // badge={1}
            onPress={() => {
              setState({
                ...state,
                selectedTab: "blueTab"
              });
            }}
            data-seed="logId"
          >
            {/* {renderContent("Life")} */}
            <PerfectScrollbar>
              <NavBar
                mode="light"
                icon={
                  <div style={{ width: "22px", height: "22px" }}>
                    <svg
                      version="1.0"
                      id="Layer_1"
                      xmlns="http://www.w3.org/2000/svg"
                      x="0px"
                      y="0px"
                      width="23.7px"
                      height="20.6px"
                      viewBox="0 0 23.7 20.6"
                      enable-background="new 0 0 23.7 20.6"
                    >
                      <g>
                        <g>
                          <path
                            fill={state.primary_color}
                            stroke={state.primary_color}
                            stroke-linecap="round"
                            stroke-miterlimit="10"
                            d="M2.3,1.4h19c0.8,0,1.4,0.6,1.4,1.4v0
			c0,0.8-0.6,1.4-1.4,1.4h-19C1.6,4.1,1,3.5,1,2.8v0C1,2,1.6,1.4,2.3,1.4z"
                          />
                        </g>
                        <g>
                          <path
                            fill={state.primary_color}
                            stroke={state.primary_color}
                            stroke-miterlimit="10"
                            d="M2.2,9.7h11.2c0.7,0,1.2,0.5,1.2,1.2v0c0,0.7-0.5,1.2-1.2,1.2
			H2.2C1.5,12,1,11.5,1,10.8v0C1,10.2,1.5,9.7,2.2,9.7z"
                          />
                        </g>
                        <g>
                          <path
                            fill={state.primary_color}
                            stroke={state.primary_color}
                            stroke-miterlimit="10"
                            d="M2.3,16.8h19c0.7,0,1.3,0.6,1.3,1.3v0c0,0.7-0.6,1.3-1.3,1.3h-19
			c-0.7,0-1.3-0.6-1.3-1.3v0C1,17.4,1.6,16.8,2.3,16.8z"
                          />
                        </g>
                      </g>
                    </svg>
                  </div>
                }
                // onLeftClick={() => console.log("onLeftClick")}
                rightContent={
                  [
                    // <Icon key="0" type="search" style={{ marginRight: "16px" }} />,
                    // <Icon key="1" type="ellipsis" />
                  ]
                }
              >
                <div
                  style={{ height: "40px", width: "100%", textAlign: "center" }}
                >
                  {state.logo_img === "" ? (
                    <div style={{ marginTop: "5px" }}>LOGO</div>
                  ) : (
                    <img
                      style={{ height: "35px", marginTop: "5px" }}
                      src={state.logo_img}
                      alt="LOGO"
                    />
                  )}
                </div>
              </NavBar>
              <br />

              <WingBlank>
                <Carousel
                  autoplay={false}
                  infinite
                  slideWidth={1}
                  // beforeChange={(from, to) =>
                  //   console.log(`slide from ${from} to ${to}`)
                  // }
                  // afterChange={index => console.log("slide to", index)}
                >
                  {data.map(val => (
                    <img
                      src={val}
                      alt=""
                      style={{ width: "100%", verticalAlign: "top" }}
                      onLoad={() => {
                        // fire window resize event to change height
                        window.dispatchEvent(new Event("resize"));
                        setState({ ...state, imgHeight: "auto" });
                      }}
                    />
                  ))}
                </Carousel>
              </WingBlank>
              <WingBlank style={{ margin: "15px auto" }}>
                <Carousel
                  autoplay={false}
                  infinite
                  slideWidth={0.25}
                  cellSpacing={5}
                  dots={false}
                  autoplay
                  // beforeChange={(from, to) =>
                  //   console.log(`slide from ${from} to ${to}`)
                  // }
                  // afterChange={index => console.log("slide to", index)}
                >
                  {CategoryIcon.map(val => (
                    <img
                      src={val}
                      alt=""
                      style={{ width: "65px", verticalAlign: "middle" }}
                      onLoad={() => {
                        // fire window resize event to change height
                        window.dispatchEvent(new Event("resize"));
                        setState({ ...state, imgHeight: "auto" });
                      }}
                    />
                  ))}
                </Carousel>
              </WingBlank>
              <div style={{ textAlign: "left" }}>
                <p className="left">New Arrivals</p>
                <p className="right">
                  See All{" "}
                  <Icon
                    style={{ width: "16px", height: "16px" }}
                    type="right"
                  />
                </p>
              </div>
              <Grid
                style={{ marginBottom: "100px" }}
                hasLine={false}
                itemStyle={{
                  height: "200px",
                  background: "#fff",
                  margin: "5px"
                }}
                data={data}
                columnNum={3}
                renderItem={data => (
                  <div style={{ padding: "0px" }}>
                    <div>
                      <img
                        src={data}
                        alt=""
                        style={{
                          width: "85px",
                          height: "100px",
                          objectFit: "cover"
                        }}
                      />
                    </div>
                    <div>
                      <p
                        className="brand-name"
                        style={{ color: `${state.secondry_color}` }}
                      >
                        Color Block Men Hooded Neck Yellow, White, Blue T-Shirt
                      </p>
                      <p className="brand-price">$ 1000</p>
                    </div>
                  </div>
                )}
              />
              <div style={{ textAlign: "left" }}>
                <p className="left">Best Deal</p>
                <p className="right">
                  See All{" "}
                  <Icon
                    style={{ width: "16px", height: "16px" }}
                    type="right"
                  />
                </p>
              </div>
              <Grid
                style={{ marginBottom: "100px" }}
                hasLine={false}
                itemStyle={{
                  height: "200px",
                  background: "#fff",
                  margin: "5px"
                }}
                data={data}
                columnNum={3}
                renderItem={data => (
                  <div style={{ padding: "0px" }}>
                    <div>
                      <img
                        src={data}
                        alt=""
                        style={{
                          width: "85px",
                          height: "100px",
                          objectFit: "cover"
                        }}
                      />
                    </div>
                    <div>
                      <p
                        className="brand-name"
                        style={{ color: `${state.secondry_color}` }}
                      >
                        Color Block Men Hooded Neck Yellow, White, Blue T-Shirt
                      </p>
                      <p className="brand-price">$ 1000</p>
                    </div>
                  </div>
                )}
              />
              <div style={{ marginBottom: "100px" }}></div>
            </PerfectScrollbar>
          </TabBar.Item>
          <TabBar.Item
            // title="category"
            key="category"
            icon={
              <div className="tab-bar">
                <svg
                  version="1.0"
                  id="Layer_1"
                  xmlns="http://www.w3.org/2000/svg"
                  x="0px"
                  y="0px"
                  width="25px"
                  height="24.8px"
                  viewBox="0 0 25 24.8"
                  enable-background="new 0 0 25 24.8"
                >
                  <g>
                    <path
                      fill="none"
                      stroke="#0B0707"
                      stroke-width="2"
                      stroke-miterlimit="10"
                      d="M20.2,10.5h-3.9c-1.4,0-2.5-1.1-2.5-2.5V4.8
		c0-1.4,1.1-2.5,2.5-2.5h3.9c1.4,0,2.5,1.1,2.5,2.5V8C22.7,9.3,21.6,10.5,20.2,10.5z"
                    />
                    <path
                      fill="none"
                      stroke="#0B0707"
                      stroke-width="2"
                      stroke-miterlimit="10"
                      d="M20.2,22.6h-3.9c-1.4,0-2.5-1.1-2.5-2.5V17
		c0-1.4,1.1-2.5,2.5-2.5h3.9c1.4,0,2.5,1.1,2.5,2.5v3.1C22.7,21.5,21.6,22.6,20.2,22.6z"
                    />
                    <path
                      fill="none"
                      stroke="#0B0707"
                      stroke-width="2"
                      stroke-miterlimit="10"
                      d="M8.3,22.6H4.4c-1.4,0-2.5-1.1-2.5-2.5V17
		c0-1.4,1.1-2.5,2.5-2.5h3.9c1.4,0,2.5,1.1,2.5,2.5v3.1C10.8,21.5,9.7,22.6,8.3,22.6z"
                    />
                    <path
                      fill="none"
                      stroke="#0B0707"
                      stroke-width="2"
                      stroke-miterlimit="10"
                      d="M8.3,10.5H4.4C3.1,10.5,2,9.3,2,8V4.8
		c0-1.4,1.1-2.5,2.5-2.5h3.9c1.4,0,2.5,1.1,2.5,2.5V8C10.8,9.3,9.7,10.5,8.3,10.5z"
                    />
                  </g>
                </svg>
              </div>
            }
            selectedIcon={
              <div className="tab-bar">
                <svg
                  version="1.0"
                  id="Layer_1"
                  xmlns="http://www.w3.org/2000/svg"
                  x="0px"
                  y="0px"
                  width="25px"
                  height="24.8px"
                  viewBox="0 0 25 24.8"
                  enable-background="new 0 0 25 24.8"
                >
                  <g>
                    <path
                      fill="none"
                      stroke={state.primary_color}
                      stroke-width="2"
                      stroke-miterlimit="10"
                      d="M20.2,10.5h-3.9c-1.4,0-2.5-1.1-2.5-2.5V4.8
		c0-1.4,1.1-2.5,2.5-2.5h3.9c1.4,0,2.5,1.1,2.5,2.5V8C22.7,9.3,21.6,10.5,20.2,10.5z"
                    />
                    <path
                      fill="none"
                      stroke={state.primary_color}
                      stroke-width="2"
                      stroke-miterlimit="10"
                      d="M20.2,22.6h-3.9c-1.4,0-2.5-1.1-2.5-2.5V17
		c0-1.4,1.1-2.5,2.5-2.5h3.9c1.4,0,2.5,1.1,2.5,2.5v3.1C22.7,21.5,21.6,22.6,20.2,22.6z"
                    />
                    <path
                      fill="none"
                      stroke={state.primary_color}
                      stroke-width="2"
                      stroke-miterlimit="10"
                      d="M8.3,22.6H4.4c-1.4,0-2.5-1.1-2.5-2.5V17
		c0-1.4,1.1-2.5,2.5-2.5h3.9c1.4,0,2.5,1.1,2.5,2.5v3.1C10.8,21.5,9.7,22.6,8.3,22.6z"
                    />
                    <path
                      fill="none"
                      stroke={state.primary_color}
                      stroke-width="2"
                      stroke-miterlimit="10"
                      d="M8.3,10.5H4.4C3.1,10.5,2,9.3,2,8V4.8
		c0-1.4,1.1-2.5,2.5-2.5h3.9c1.4,0,2.5,1.1,2.5,2.5V8C10.8,9.3,9.7,10.5,8.3,10.5z"
                    />
                  </g>
                </svg>
              </div>
            }
            selected={state.selectedTab === "orangeTab"}
            // badge={1}
            onPress={() => {
              setState({
                ...state,
                selectedTab: "orangeTab"
              });
            }}
            data-seed="logId5"
          >
            <PerfectScrollbar>
              <NavBar
                mode="light"
                icon={
                  <div style={{ width: "22px", height: "22px" }}>
                    <svg
                      version="1.0"
                      id="Layer_1"
                      xmlns="http://www.w3.org/2000/svg"
                      x="0px"
                      y="0px"
                      width="23.7px"
                      height="20.6px"
                      viewBox="0 0 23.7 20.6"
                      enable-background="new 0 0 23.7 20.6"
                    >
                      <g>
                        <g>
                          <path
                            fill={state.primary_color}
                            stroke={state.primary_color}
                            stroke-linecap="round"
                            stroke-miterlimit="10"
                            d="M2.3,1.4h19c0.8,0,1.4,0.6,1.4,1.4v0
			c0,0.8-0.6,1.4-1.4,1.4h-19C1.6,4.1,1,3.5,1,2.8v0C1,2,1.6,1.4,2.3,1.4z"
                          />
                        </g>
                        <g>
                          <path
                            fill={state.primary_color}
                            stroke={state.primary_color}
                            stroke-miterlimit="10"
                            d="M2.2,9.7h11.2c0.7,0,1.2,0.5,1.2,1.2v0c0,0.7-0.5,1.2-1.2,1.2
			H2.2C1.5,12,1,11.5,1,10.8v0C1,10.2,1.5,9.7,2.2,9.7z"
                          />
                        </g>
                        <g>
                          <path
                            fill={state.primary_color}
                            stroke={state.primary_color}
                            stroke-miterlimit="10"
                            d="M2.3,16.8h19c0.7,0,1.3,0.6,1.3,1.3v0c0,0.7-0.6,1.3-1.3,1.3h-19
			c-0.7,0-1.3-0.6-1.3-1.3v0C1,17.4,1.6,16.8,2.3,16.8z"
                          />
                        </g>
                      </g>
                    </svg>
                  </div>
                }
                // onLeftClick={() => console.log("onLeftClick")}
                rightContent={
                  [
                    // <Icon key="0" type="search" style={{ marginRight: "16px" }} />,
                    // <Icon key="1" type="ellipsis" />
                  ]
                }
              >
                <div
                  style={{ height: "40px", width: "100%", textAlign: "center" }}
                >
                  {state.logo_img === "" ? (
                    <div style={{ marginTop: "5px" }}>LOGO</div>
                  ) : (
                    <img
                      style={{ height: "35px", marginTop: "5px" }}
                      src={state.logo_img}
                      alt="LOGO"
                    />
                  )}
                </div>
              </NavBar>
              <br />
              <div style={{ padding: "15px" }}>
                <div
                  style={{
                    height: "200px"
                  }}
                  className="m-category-card"
                >
                  <div className="cat-box">
                    <p className="mobile-category-title-right">HAND BAGS</p>
                    <p className="mobile-category-quantity">2321 Products</p>
                  </div>
                </div>
                <br />
                <div
                  style={{
                    height: "200px"
                  }}
                  className="m-category-card-1"
                >
                  <div className="cat-box">
                    <p className="mobile-category-title-left">
                      Mens Collection
                    </p>
                    <p className="mobile-category-quantity-left">
                      1250 Products
                    </p>
                  </div>
                </div>
                <br />
                <div
                  style={{
                    height: "200px"
                  }}
                  className="m-category-card-2"
                >
                  <div className="cat-box">
                    <p className="mobile-category-title-right">
                      Women Collection
                    </p>
                    <p className="mobile-category-quantity">3053 Products</p>
                  </div>
                </div>
                <br />
                <div
                  style={{
                    height: "200px"
                  }}
                  className="m-category-card-3"
                >
                  <div className="cat-box">
                    <p className="mobile-category-title-left">
                      Kids Collection
                    </p>
                    <p className="mobile-category-quantity-left">
                      993 Products
                    </p>
                  </div>
                </div>
              </div>
            </PerfectScrollbar>
          </TabBar.Item>
          <TabBar.Item
            icon={
              <div className="tab-bar">
                <svg
                  version="1.0"
                  id="Layer_1"
                  xmlns="http://www.w3.org/2000/svg"
                  x="0px"
                  y="0px"
                  width="24.3px"
                  height="20.4px"
                  viewBox="0 0 24.3 20.4"
                  enable-background="new 0 0 24.3 20.4"
                >
                  <g>
                    <path
                      stroke="#0B0707"
                      stroke-width="0.5"
                      stroke-miterlimit="10"
                      d="M21.8,19.3l-7.2-4.7c-0.6-0.4-0.7-1.1-0.4-1.7l0,0
		c0.4-0.6,1.1-0.7,1.7-0.4l7.2,4.7c0.6,0.4,0.7,1.1,0.4,1.7l0,0C23.2,19.6,22.4,19.7,21.8,19.3z"
                    />
                    <circle
                      fill="none"
                      stroke="#0B0707"
                      stroke-width="2"
                      stroke-miterlimit="10"
                      cx="8.7"
                      cy="8.6"
                      r="7.3"
                    />
                  </g>
                </svg>
              </div>
            }
            selectedIcon={
              <div className="tab-bar">
                <svg
                  version="1.0"
                  id="Layer_1"
                  xmlns="http://www.w3.org/2000/svg"
                  x="0px"
                  y="0px"
                  width="24.3px"
                  height="20.4px"
                  viewBox="0 0 24.3 20.4"
                  enable-background="new 0 0 24.3 20.4"
                >
                  <g>
                    <path
                      stroke={state.primary_color}
                      stroke-width="0.5"
                      stroke-miterlimit="10"
                      d="M21.8,19.3l-7.2-4.7c-0.6-0.4-0.7-1.1-0.4-1.7l0,0
		c0.4-0.6,1.1-0.7,1.7-0.4l7.2,4.7c0.6,0.4,0.7,1.1,0.4,1.7l0,0C23.2,19.6,22.4,19.7,21.8,19.3z"
                    />
                    <circle
                      fill="none"
                      stroke={state.primary_color}
                      stroke-width="2"
                      stroke-miterlimit="10"
                      cx="8.7"
                      cy="8.6"
                      r="7.3"
                    />
                  </g>
                </svg>
              </div>
            }
            // title="My"
            key="my"
            selected={state.selectedTab === "yellowTab"}
            onPress={() => {
              setState({
                ...state,
                selectedTab: "yellowTab"
              });
            }}
          >
            <div style={{ padding: "15px" }}>
              <SearchBar
                className="mobile-mode-search-bar"
                placeholder="Search product by name"
                cancelText="Cancel"
              />
              <br />
              <p>Your Search Keyword did not match any product..</p>
            </div>
          </TabBar.Item>
          <TabBar.Item
            icon={
              <div className="tab-bar">
                <svg
                  version="1.0"
                  id="Layer_1"
                  xmlns="http://www.w3.org/2000/svg"
                  x="0px"
                  y="0px"
                  width="24.4px"
                  height="22.6px"
                  viewBox="0 0 24.4 22.6"
                  enable-background="new 0 0 24.4 22.6"
                >
                  <g>
                    <path
                      fill="#111010"
                      d="M0.5,1.3C0.3,1.7,0.4,2,1,2.3h3.8c0,0,0.7,0.3,1.1,1.8l2.4,10.4c0,0,0.5,2,2.6,1.8l7.2,0
             c0,0,2.4-0.5,3.1-2.3L23.8,8c0,0,1.2-2.5-1.8-3.5L8.2,4.5c0,0-0.4-3.5-3-4H1.7C1.1,0.4,0.6,0.8,0.5,1.3z"
                    />
                    <path
                      fill="none"
                      d="M8.5,6.6l2,7.6H18c0,0,1.1-0.4,1.6-1.7L22,7.6c0,0,0.3-1-0.3-1H8.5z"
                    />
                    <circle
                      fill="none"
                      stroke="#111010"
                      stroke-width="1.5"
                      stroke-miterlimit="10"
                      cx="9.4"
                      cy="19.5"
                      r="1.8"
                    />
                    <circle
                      fill="none"
                      stroke="#111010"
                      stroke-width="1.5"
                      stroke-miterlimit="10"
                      cx="19.5"
                      cy="19.5"
                      r="1.8"
                    />
                  </g>
                </svg>
              </div>
            }
            selectedIcon={
              <div className="tab-bar">
                <svg
                  version="1.0"
                  id="Layer_1"
                  xmlns="http://www.w3.org/2000/svg"
                  x="0px"
                  y="0px"
                  width="23.3px"
                  height="22.2px"
                  viewBox="0 0 23.3 22.2"
                  enable-background="new 0 0 23.3 22.2"
                >
                  <g>
                    <path
                      fill={state.primary_color}
                      d="M0.4,1.4C0.3,1.7,0.4,2.1,1,2.3h3.6c0,0,0.6,0.3,1.1,1.8L8,14c0,0,0.4,1.9,2.4,1.8l6.9,0
           c0,0,2.3-0.5,2.9-2.2l2.4-5.8c0,0,1.1-2.4-1.8-3.3L7.8,4.4c0,0-0.4-3.4-2.9-3.9H1.6C1.1,0.5,0.6,0.9,0.4,1.4z"
                    />
                    <path
                      fill="#FFFFFF"
                      d="M8.1,6.5l1.9,7.3h7.2c0,0,1.1-0.4,1.5-1.6L21,7.4c0,0,0.3-0.9-0.3-0.9H8.1z"
                    />
                    <circle
                      fill="none"
                      stroke={state.primary_color}
                      stroke-width="1.5"
                      stroke-miterlimit="10"
                      cx="9"
                      cy="18.8"
                      r="1.7"
                    />
                    <circle
                      fill="none"
                      stroke={state.primary_color}
                      stroke-width="1.5"
                      stroke-miterlimit="10"
                      cx="18.6"
                      cy="18.8"
                      r="1.7"
                    />
                  </g>
                </svg>
              </div>
            }
            // title="Koubei"
            key="Koubei"
            // badge={"new"}
            selected={state.selectedTab === "redTab"}
            onPress={() => {
              setState({
                ...state,
                selectedTab: "redTab"
              });
            }}
            data-seed="logId1"
          >
            <br />
            <br />
            <Steps direction="horizontal">
              <Step title="Cart" icon={customIcon[0].cart} />
              <Step title="Delivery" icon={customIcon[0].delivery} />
              <Step title="Payment" icon={customIcon[0].Money} />
              <Step title="Order" icon={customIcon[0].flag} />
            </Steps>

            <div style={{ textAlign: "left" }}>
              <p
                style={{
                  fontSize: "20px",
                  display: "inline",
                  fontWeight: "800",
                  paddingLeft: "15px"
                }}
              >
                Total Price:
              </p>
              <p
                style={{
                  float: "right",
                  fontSize: "20px",
                  paddingRight: "15px"
                }}
              >
                $175
              </p>
            </div>
            <hr style={{ maxWidth: "90%", opacity: "0.3" }} />
            <div style={{ display: "flex", width: "100%", padding: "15px" }}>
              <div>
                <div className="cart-product-img"></div>
              </div>
              <div style={{ paddingLeft: "10px", width: "160px" }}>
                <p style={{ fontSize: "18px", fontWeight: "800" }}>
                  Jumbo T-shirt M
                </p>
                <p
                  style={{
                    fontSize: "20px",
                    fontWeight: "800",
                    letterSpacing: "-1px"
                  }}
                >
                  $ 175.54
                </p>
              </div>
              <div style={{ width: "40px" }}>
                <svg
                  version="1.0"
                  id="Layer_1"
                  xmlns="http://www.w3.org/2000/svg"
                  x="0px"
                  y="0px"
                  viewBox="0 0 24.2 61.7"
                  enable-background="new 0 0 24.2 61.7"
                >
                  <path
                    fill="#F7F8FA"
                    stroke="#E5E5E5"
                    stroke-miterlimit="10"
                    d="M12.1,59.6L12.1,59.6c-4.7,0-8.5-3.8-8.5-8.5V9.9
	c0-4.7,3.8-8.5,8.5-8.5h0c4.7,0,8.5,3.8,8.5,8.5v41.3C20.6,55.8,16.8,59.6,12.1,59.6z"
                  />
                  <path
                    fill="#B9C4CD"
                    d="M11.6,8.9l-2.2,2.2c-0.3,0.3-0.1,0.7,0.3,0.7h4.2c0.4,0,0.5-0.4,0.3-0.7l-2-2.2C12.1,8.7,11.8,8.7,11.6,8.9z
	"
                  />
                  <path
                    fill="#B9C1C9"
                    d="M12.5,53.3l2.1-2.1c0.2-0.2,0.1-0.7-0.3-0.7l-4,0c-0.3,0-0.5,0.4-0.3,0.7l1.9,2.1
	C12.1,53.5,12.3,53.5,12.5,53.3z"
                  />
                  <g>
                    <path
                      d="M9.9,34.3v-0.5l0.6-0.6c1.5-1.4,2.2-2.2,2.2-3.1c0-0.6-0.3-1.1-1.2-1.1c-0.5,0-1,0.3-1.2,0.5L10,28.9
		c0.4-0.3,1-0.6,1.7-0.6c1.3,0,1.8,0.9,1.8,1.7c0,1.1-0.8,2-2,3.2L11,33.6v0h2.6v0.7H9.9z"
                    />
                  </g>
                  <g></g>
                  <g></g>
                  <g></g>
                  <g></g>
                  <g></g>
                  <g></g>
                </svg>
              </div>
            </div>
            <hr />
            <div style={{ textAlign: "left" }}>
              <p
                style={{
                  paddingLeft: "15px",
                  fontSize: "22px",
                  fontWeight: "800"
                }}
              >
                Coupon Code
              </p>
              <div style={{ display: "flex", padding: "0 15px" }}>
                <InputItem className="coupon-box" placeholder="Coupon Code" />
                <Button
                  style={{
                    maxWidth: "100px",
                    borderRadius: "25px",
                    background: "#e8e81b",
                    border: "none",
                    width: "100px",
                    margin: "0px 10px"
                  }}
                >
                  Apply{" "}
                </Button>
              </div>
            </div>
          </TabBar.Item>
          <TabBar.Item
            icon={
              <div className="tab-bar">
                <svg
                  version="1.0"
                  id="Layer_1"
                  xmlns="http://www.w3.org/2000/svg"
                  x="0px"
                  y="0px"
                  width="25.3px"
                  height="24.3px"
                  viewBox="0 0 25.3 24.3"
                  enable-background="new 0 0 25.3 24.3"
                >
                  <path
                    fill="none"
                    stroke="#0B0707"
                    stroke-width="2"
                    stroke-miterlimit="10"
                    d="M12.9,4.3c0.2-0.3,1.9-2.1,4.5-2
	c1.5,0.1,2.5,0.7,3,1.1c0,0,4.5,2.3,2.6,8.1c0,0-0.7,3.5-8,9.8c0,0-2,2.2-4.4-0.2c0,0-6.4-5.3-7.9-10.1c0,0-2.3-6.3,4.8-8.4
	c0.5-0.1,1.2-0.2,2-0.1C11.4,2.7,12.5,3.9,12.9,4.3z"
                  />
                </svg>
              </div>
            }
            selectedIcon={
              <div className="tab-bar">
                <svg
                  version="1.0"
                  id="Layer_1"
                  xmlns="http://www.w3.org/2000/svg"
                  x="0px"
                  y="0px"
                  width="25.3px"
                  height="24.3px"
                  viewBox="0 0 25.3 24.3"
                  enable-background="new 0 0 25.3 24.3"
                >
                  <path
                    fill="none"
                    stroke={state.primary_color}
                    stroke-width="2"
                    stroke-miterlimit="10"
                    d="M12.9,4.3c0.2-0.3,1.9-2.1,4.5-2
	c1.5,0.1,2.5,0.7,3,1.1c0,0,4.5,2.3,2.6,8.1c0,0-0.7,3.5-8,9.8c0,0-2,2.2-4.4-0.2c0,0-6.4-5.3-7.9-10.1c0,0-2.3-6.3,4.8-8.4
	c0.5-0.1,1.2-0.2,2-0.1C11.4,2.7,12.5,3.9,12.9,4.3z"
                  />
                </svg>
              </div>
            }
            // title="Friend"
            key="Friend"
            // dot
            selected={state.selectedTab === "greenTab"}
            onPress={() => {
              setState({
                ...state,
                selectedTab: "greenTab"
              });
            }}
          >
            <PerfectScrollbar>
              <NavBar
                mode="light"
                icon={
                  <div style={{ width: "22px", height: "22px" }}>
                    <svg
                      version="1.0"
                      id="Layer_1"
                      xmlns="http://www.w3.org/2000/svg"
                      x="0px"
                      y="0px"
                      width="23.7px"
                      height="20.6px"
                      viewBox="0 0 23.7 20.6"
                      enable-background="new 0 0 23.7 20.6"
                    >
                      <g>
                        <g>
                          <path
                            fill={state.primary_color}
                            stroke={state.primary_color}
                            stroke-linecap="round"
                            stroke-miterlimit="10"
                            d="M2.3,1.4h19c0.8,0,1.4,0.6,1.4,1.4v0
			c0,0.8-0.6,1.4-1.4,1.4h-19C1.6,4.1,1,3.5,1,2.8v0C1,2,1.6,1.4,2.3,1.4z"
                          />
                        </g>
                        <g>
                          <path
                            fill={state.primary_color}
                            stroke={state.primary_color}
                            stroke-miterlimit="10"
                            d="M2.2,9.7h11.2c0.7,0,1.2,0.5,1.2,1.2v0c0,0.7-0.5,1.2-1.2,1.2
			H2.2C1.5,12,1,11.5,1,10.8v0C1,10.2,1.5,9.7,2.2,9.7z"
                          />
                        </g>
                        <g>
                          <path
                            fill={state.primary_color}
                            stroke={state.primary_color}
                            stroke-miterlimit="10"
                            d="M2.3,16.8h19c0.7,0,1.3,0.6,1.3,1.3v0c0,0.7-0.6,1.3-1.3,1.3h-19
			c-0.7,0-1.3-0.6-1.3-1.3v0C1,17.4,1.6,16.8,2.3,16.8z"
                          />
                        </g>
                      </g>
                    </svg>
                  </div>
                }
                // onLeftClick={() => console.log("onLeftClick")}
                rightContent={
                  [
                    // <Icon key="0" type="search" style={{ marginRight: "16px" }} />,
                    // <Icon key="1" type="ellipsis" />
                  ]
                }
              >
                <div
                  style={{ height: "40px", width: "100%", textAlign: "center" }}
                >
                  {state.logo_img === "" ? (
                    <div style={{ marginTop: "5px" }}>LOGO</div>
                  ) : (
                    <img
                      style={{ height: "35px", marginTop: "5px" }}
                      src={state.logo_img}
                      alt="LOGO"
                    />
                  )}
                </div>
              </NavBar>
              <br />

              <div style={{ textAlign: "left" }}>
                <p
                  style={{
                    fontSize: "20px",
                    display: "inline",
                    fontWeight: "800",
                    paddingLeft: "15px"
                  }}
                >
                  Wishlist
                </p>
                <p
                  style={{
                    float: "right",
                    fontSize: "14px",
                    paddingRight: "15px",
                    color: `${state.primary_color}`
                  }}
                >
                  1 item
                </p>
              </div>

              <div style={{ display: "flex", width: "100%", padding: "15px" }}>
                <div>
                  <div className="cart-product-img"></div>
                </div>
                <div style={{ paddingLeft: "10px", width: "160px" }}>
                  <p style={{ fontSize: "18px", fontWeight: "800" }}>
                    Jumbo T-shirt M
                  </p>
                  <p
                    style={{
                      fontSize: "20px",
                      fontWeight: "800",
                      letterSpacing: "-1px",
                      color: `${state.primary_color}`
                    }}
                  >
                    $ 175.54
                  </p>
                </div>
              </div>
              <hr />
              <div
                style={{
                  display: "flex",
                  width: "100%",
                  position: "relative",
                  top: "228px"
                }}
              >
                <Button className="fav-cart-button" type="warning">
                  Clean All
                </Button>
                <Button className="fav-cart-button">Add all to cart</Button>
              </div>
            </PerfectScrollbar>
          </TabBar.Item>
        </TabBar>
      </div>
    </div>
  );
};

export default MobileHome;
