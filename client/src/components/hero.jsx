/* eslint react/prop-types: 0 */
import React, { Component } from "react";
import { Typography } from "antd";

const { Title } = Typography;

export default class Hero extends Component {
  render() {
    return (
      <div className="hero-bg">
        <Title className="hero-title fontSize-30" style={{ color: "#fff" }}>
          {this.props.Title}
        </Title>
      </div>
    );
  }
}
