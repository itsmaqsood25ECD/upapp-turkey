import React, { Fragment } from 'react'
import PropTypes from 'prop-types'
import { connect } from "react-redux"
import { Icon } from 'antd';

const Alert = ({ alerts }) => alerts !== null && alerts.length > 0 && alerts.map(alert =>
  <div className="ant-message"><span>
    <div className="ant-message-notice">
    <div key={alert.id} className={` alert-${alert.alertType} ant-message-notice-content animated bounceInTop`}>
  {alert.alertType ==='danger' ? <Icon style={{color:"#f44336ad"}} type="close-circle" /> : <Icon style={{color:"#00968882"}} type="check-circle" />} {" "} {alert.msg}
</div>
    </div>
   
    </span></div>
 
)

Alert.propTypes = {
  alerts: PropTypes.array.isRequired,
}

const mapStateToProps = state => ({
  alerts: state.alert
})

export default connect(mapStateToProps)(Alert)
