import React, { Component, Fragment } from "react";
import Hero from "../hero";
import { connect } from "react-redux";
import ScrollTop from "../ScrollTop";
import { Card, Typography, Table } from "antd";
import "../../assets/css/homeNew.css";
import { Helmet } from "react-helmet";
import withDirection, { withDirectionPropTypes, DIRECTIONS } from 'react-with-direction';
const columns = [
  {
    title: "Month",
    dataIndex: "Month",
    key: "Month"
  },
  {
    title: "Cost %",
    dataIndex: "Cost",
    key: "Cost"
  },
  {
    title: "Website",
    dataIndex: "Website",
    key: "Website"
  },
  {
    title: "Mobile App",
    dataIndex: "App",
    key: "App"
  },
  {
    title: "Mobile App + Website",
    dataIndex: "WebApp",
    key: "WebApp"
  }
];

const data = [
  {
    key: "1",
    Month: "1 - 3 Months",
    Cost: "NA",
    Website: "NA",
    App: "NA",
    WebApp: "NA"
  },
  {
    key: "2",
    Month: "4 - 6 Months",
    Cost: "80%",
    Website: "$ 240",
    App: "$ 400 ",
    WebApp: "$ 560"
  },
  {
    key: "3",
    Month: "7 - 9 Months",
    Cost: "60%",
    Website: "$ 180",
    App: "$ 300 ",
    WebApp: "$ 420"
  },
  {
    key: "4",
    Month: "10+ Months",
    Cost: "30%",
    Website: "$ 90",
    App: "$ 150 ",
    WebApp: "$ 210"
  }
];
const columnsAR = [
  {
    title: "الشهر",
    dataIndex: "Month",
    key: "Month"
  },
  {
    title: "التكلفة ٪	",
    dataIndex: "Cost",
    key: "Cost"
  },
  {
    title: "الموقع الإلكتروني",
    dataIndex: "Website",
    key: "Website"
  },
  {
    title: "تطبيق الهاتف",
    dataIndex: "App",
    key: "App"
  },
  {
    title: "تطبيق الهاتف + الموقع الإلكتروني",
    dataIndex: "WebApp",
    key: "WebApp"
  }
];
const dataAR = [
  {
    key: "1",
    Month: "3-1 شهور",
    Cost: "غير متواجد",
    Website: "غير متواجد",
    App: "غير متواجد",
    WebApp: "غير متواجد"
  },
  {
    key: "2",
    Month: "6-4 شهور",
    Cost: "80%",
    Website: "$ 240",
    App: "$ 400 ",
    WebApp: "$ 560"
  },
  {
    key: "3",
    Month: "9-7 شهور",
    Cost: "60%",
    Website: "$ 180",
    App: "$ 300 ",
    WebApp: "$ 420"
  },
  {
    key: "4",
    Month: "10+ شهور",
    Cost: "30%",
    Website: "$ 90",
    App: "$ 150 ",
    WebApp: "$ 210"
  }
];
const cancelationRefundPolicy = ({ direction }) => {



  return (
    <Fragment>
      <ScrollTop></ScrollTop>
      <ScrollTop></ScrollTop>
      <Helmet>
        <title>Terms and Conditions - upappfactory</title>
        <meta
          name="Terms and Conditions - upappfactory"
          content="Terms and Conditions"
        />
      </Helmet>
      {/* Arabic */}
      {direction === DIRECTIONS.RTL && <Fragment>
        <Hero Title=" شروط الإلغاء والإسترجاع"></Hero>
        <div className="UAF-container-fluid" style={{ padding: "5rem 1rem", textAlign: "justify" }}>
          <div className="Privacy-policy-container">
            <p>
              في أفق للتطبيقات نحن نبتكر تطبيقات مذهلة للهاتف المحمول مع الواجهة الخلفية، لتستطيع الإستمتاع بإستخدامها في حياتك المهنية والتجارية. حيث أن خطة العمل مصوغة بطريقة تلائمك في حالة لم تستطع تلبية توقعاتك.
          </p>
            <ul>
              <li className="crp-sub-heading-list">الإشتراك الشهري:</li>
              <p>
                إذا فضلت إختيار الإشتراك الشهري ثم قررت إلغاء الإشتراك، فسوف نلغي إشتراكك في نهاية الشهر حيث أنك قد قمت
                بدفع المبلغ لذلك الشهر مُقدماً في نفس تاريخ الشهر الذي بدأت به.

              </p>
              <li className="crp-sub-heading-list">الإشتراك السنوي:</li>
              <p>
                نحن في أفق للتطبيقات لا نجبرك على الإشتراك السنوي كشرط لإستخدام خدماتنا. نحن نفضّل إعطائك المرونة لإختيار ما يناسبك. في المقابل عند إلتزامك بالإشتراك السنوي مقدماً، سوف تحصل على خصم على الإشتراك السنوي المخفّض. إذا قررت إلغاء إشتراكك السنوي في أي وقت، فنحن نعمل على خطة كالتالي:

            </p>
              <ol>
                <li>
                  <p>
                    عدد الشهور التي إستخدمت فيها خدماتنا، نخصم هذه الشهور بنسبة 29.9 دولار لكل شهر + 25٪ رسوم بقية الأشهر.  مثال: عندما تشترك في الخطة السنوية التي قيمتها 359 دولار وبعدها تقرر إلغاء إشتراكك في الشهر السادس، فإنك سوف تدفع ) 6 أشهر x $29.9 = $179.4 )  + ( 25% من 6 أشهرx $29.9 = $44.85 )
                    فإذاً، كامل المبلغ المسترجع في بطاقة إئتمانك سيكون 134.75 دولار.

                </p>
                </li>
              </ol>
              <li className="crp-sub-heading-list">التجديد التلقائي:</li>
              <p>
                لضمان راحتك، فإن إشتراكك الشهري فقط سوف يتجدد تلقائياً إلى أن تقوم بإلغاء الإشتراك. في كل مرة قبل أن يتجدد إشتراكك، سنقوم بإرسال بريد في غضون خمسة أيام  نحدد فيه قيمة المبلغ المقتطع من بطاقة الإئتمان. وبالمثل، عند كل تجديد إشتراك سوف نقوم بإرسال إيصال عن طريق الإيميل نحدد فيه قيمة المبلغ المقتطع،  وأيضاً التاريخ والمبلغ الذي سوف يقتطع من التجديد الذي يليه.
            </p>
              <p>
                نحن نعلم أنه أحياناً قد ينسى الزبائن إلغاء إشتراك لا يعودوا بحاجة له، وينتبهوا لذلك بعد خصم رسوم الإشتراك. ولذلك فإنه بإمكانك إلغاء إشتراكك الشهري\السنوي حتى بعد مضي يومين على تجديد إشتراكك، وسوف نقوم بإكمال عملية الإلغاء وإرجاع المبلغ كاملاً.
            </p>
              <p>
                للإستفسار، الرجاء إرسال بريد إلكتروني إلى
                <a href="mailto:contactus@upapp.co"></a>
              &nbsp;contactus@upapp.co.
            </p>
              <li className="crp-sub-heading-list">
                إستثناء لسياسة الإسترجاع:
            </li>
              <p>
                يرجى الإنتباه بأنه عند إنتهاكك لشروط الأحكام، فإننا سنقوم بإيقاف دخولك لخدمات أفق للتطبيقات، ونتيجة لذلك لن نقوم بقبول أي طلب لإسترجاع المبلغ (كاملاً أونسبةً منه).
            </p>
              <li className="crp-sub-heading-list">
                تحويل الإشتراك من الباقة المستأجرة إلى الباقة الأساسية:
            </li>
              <p>
                <Table columns={columnsAR} pagination={false} dataSource={dataAR} className="uaf_crppage_artable" />
              </p>
            </ul>
          </div>
        </div>
      </Fragment>}

      {/* English */}
      {direction === DIRECTIONS.LTR && <Fragment>
        <Hero Title="Cancellation & Refund Policy"></Hero>
        <div className="UAF-container-fluid" style={{ padding: "5rem 1rem", textAlign: "justify" }}>
          <div className="Privacy-policy-container">
            <p>
              At UPapp we create awesome mobile apps & backend systems that you
              enjoy using for your business or professional life. The policy is
              framed for your convenience in a situation wherein we have failed to
              meet your expectations.
          </p>
            <ul>
              <li className="crp-sub-heading-list">Monthly Subscriptions:</li>
              <p>
                If you have opted for Monthly Subscription model and you decide to
                cancel your subscription, we will cancel your subscription at the
                end of the month as you would have already paid for that month in
                advance on the same date of the month which your first month began
              with.{" "}
              </p>
              <li className="crp-sub-heading-list">Annual Subscriptions:</li>
              <p>
                UPapp doesn't force you into an annual subscription as a condition
                to use our services. We prefer to give you the flexibility to
                choose. In exchange for you signing up for an annual up-front
                commitment, we offer you a significant discount over the
                already-low monthly subscription cost. If during the annual
                subscription you decide to cancel your subscription, we work on a
                very convenient method for you as follows:
            </p>
              <ol>
                <li>
                  <p>
                    Total number of months that you have utilized using our
                    services, we deduct those months at the rate of USD 29.9 per
                    month + 25% fee of the remaining months. For example, you
                    opted for our annual plan which is priced at USD 359 & you
                    decide to cancel the subscription in the 6th month. Then you
                    will pay (6 months x USD 29.9 = USD 179.4) + (25% of 6 months
                    x USD 29.9 = USD 44.85). So, the total amount that will be
                    refunded to your card would be USD 134.75
                </p>
                </li>
              </ol>
              <li className="crp-sub-heading-list">Auto-Renewal:</li>
              <p>
                For your convenience, only your monthly subscriptions will
                auto-renew until you cancel the service. Every time before your
                subscription auto-renews, we will send a mail specifying the
                amount that will be charged to your credit card at least 5 days in
                advance. Similarly, after each renewal we will send you a receipt
                via e-mail specifying the amount that has been deducted together
                with the next renewal date and the next renewal amount.
            </p>
              <p>
                We know that sometimes customers forget to cancel an account they
                no longer want until it is having already been charged. That's why
              you can cancel your monthly/annual subscription{" "}
                <strong>even 2 business days after your renewal date</strong>, and
              we will still process your cancellation and give you a{" "}
                <strong>FULL REFUND</strong>.
            </p>
              <p>
                For questions, please e-mail{" "}
                <a href="mailto:contactus@upapp.co"></a>
              contactus@upapp.co.
            </p>
              <li className="crp-sub-heading-list">
                Exception to our Refund Policy:
            </li>
              <p>
                Please note that we will not entertain a request for refund (FULL
                or PRO-RATED) when we have suspended or terminated your access to
                UPapp Services due to a violation of our Terms of Use.
            </p>
              <li className="crp-sub-heading-list">
                Switching Cost From Rental Plan to Standard Plan:
            </li>
              {/* <p>
              The Customer cannot switch from Rental to Standard Plan if they
              are in Rental Plan – They have to minimum use the rental plan for
              the first 3 months and only from the 4th month they can switch to
              Standard Plan. Add a nice table like shown below in the page for
              them to better understand the cost
            </p> */}
              <p>
                <Table columns={columns} pagination={false} dataSource={data} className="uaf_crppage_entable" />
              </p>
            </ul>
          </div>
        </div>
      </Fragment>}

    </Fragment>
  );
};



cancelationRefundPolicy.propTypes = {
  direction: withDirectionPropTypes.direction,
};

export default connect()(withDirection(cancelationRefundPolicy));
