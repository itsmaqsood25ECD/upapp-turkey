import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { Button } from 'antd';
import fullLogo from '../../../assets/img/brand/logo.png';
import './confirmationlink.css';

export default class confirmationLink extends Component {
	render() {
		return (
			<div className="verifyAccount">
				<div>
					<img src={fullLogo} alt="UP APP LOGO" style={{ maxWidth: '90px' }} />
				</div>
				<div class="msg">
					<p>
						Your email has been verified.
						{/* <i class="email-id">itsmaqsood25@gmail.com</i>  */}
					</p>
				</div>
				<Link to="/login">
					<Button
						type="primary"
						size="large"
						style={{
							height: '50px',
							width: '100%',
							maxWidth: '380px',
							fontSize: '18px'
						}}
					>
						Login
					</Button>
				</Link>
			</div>
		);
	}
}
