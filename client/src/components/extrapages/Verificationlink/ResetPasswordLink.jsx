import React, { Component } from "react";
import fullLogo from "../../../assets/img/brand/logo.png";
import "./verificationlink.css";

export default class resetPasswordLink extends Component {
	render() {
		return (
			<div className="verifyAccount">
				<div>
					<img src={fullLogo} alt="UP APP LOGO" style={{ maxWidth: "90px" }} />
				</div>
				<div class="msg">
					<p>
						Reset Link has been sent to your email
						{/* <i class="email-id">{this.email}</i> */}
					</p>
				</div>
			</div>
		);
	}
}
