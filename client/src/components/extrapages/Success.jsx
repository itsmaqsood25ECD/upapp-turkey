import React, { Component, Fragment } from 'react';
import { Link } from 'react-router-dom';
import { Result, Button } from 'antd';
import ScrollTop from '../ScrollTop';

export default class CallBackConfirmation extends Component {
	constructor(props) {
		super(props);
		this.orderString = `Order ${this.props.match.params.orderID} Placed successfully, our Support team will reach you soon.`;
		this.title = `Order Placed Successfully`;
	}
	render() {
		return (
			<Fragment>
				<ScrollTop />
				<Result
					style={{ paddingTop: '150px', height:"100vH" }}
					status="success"
					title={this.title}
					subTitle={this.orderString}
					extra={[
						<Link to="/">
							<Button type="primary" key="console">
								Go to Home Page
							</Button>
						</Link>
					]}
				/>
			</Fragment>
		);
	}
}
