import React, { Fragment, useEffect, useState } from "react";
import { connect } from "react-redux";
import ScrollTop from "../../ScrollTop";
import { Card, Carousel, Button, Typography, Table, Row, Col } from "antd";
import { Link, NavLink, Redirect } from "react-router-dom";
import "../../../assets/css/homeNew.css";
import "../../../assets/css/blog.css";
import PropTypes from "prop-types";
import { getAllBlog, getRecentBlog } from "../../../actions/admin/blog.management.action";
import moment from 'moment';
import BannerAnim, { Element } from 'rc-banner-anim';
import TweenOne from 'rc-tween-one';
import 'rc-banner-anim/assets/index.css';


const BlogPage = ({ getAllBlog, getRecentBlog, recentBlog, Blogs }) => {

  useEffect(() => {
    getAllBlog();
    getRecentBlog();


  }, []);
  console.log(Blogs);
  console.log("recent Blogs", recentBlog)

  const { Meta } = Card;
  const BgElement = Element.BgElement;
  return (
    <Fragment>
      <ScrollTop></ScrollTop>
      <BannerAnim prefixCls="blog-slider" type="across">
        {recentBlog && recentBlog.map((blog) =>
          <Element
            prefixCls="banner-user-elem"
            key="0"
          >
            <BgElement
              key="bg"
              className="bg"
              style={{
                backgroundImage: 'url(' + blog.featured_image + ')',
                backgroundRepeat: "no-repeat",
                width: "100%",
                backgroundSize: "cover",
                filter: 'brightness(0.6)'
              }}
            />

            <TweenOne className="banner-user-title title" animation={{ y: 30, opacity: 0, type: 'from' }}>
              {blog.title}
            </TweenOne>
            <TweenOne className="banner-user-text button"
              animation={{ y: 30, opacity: 0, type: 'from', delay: 100 }}
            >
              <NavLink to={`/blog/${blog.title.split(" ").join("-")}`}>
                <Button size={"large"} className="slider-get-started-btn uaf_allPageBtn_bg_Color">Read Full Article</Button>
              </NavLink>
            </TweenOne>
          </Element>
        )}
      </BannerAnim>

      <section className="UAF-container-fluid" style={{ marginTop: "3rem" }}>
        <Row gutter={{ xs: 8, sm: 16, md: 24, lg: 32 }}>
          <Col xs={{ span: 24 }} sm={{ span: 24 }} md={{ span: 24 }} lg={{ span: 16 }} xl={{ span: 16 }}>


            {/* <Carousel dots={false} autoplay>
              {recentBlog && recentBlog.map((blog) =>
                <div>
                  <Link to={`/blog`}>

                    <img style={{ height: "500px", width: "100%", objectFit: "cover", borderRadius: "2px" }} alt="example" src={blog.featured_image} />
                  </Link>
                  <div style={{ position: "relative", top: -80, background: "rgba(255, 255, 255, 0.8)", width: "100%", height: "80px", textAlign: "left", padding: "10px 20px" }}>
                    <p className="uaf_bp_card-desc-heading" style={{ color: "#000" }}>{blog.title}</p>
                    <p className="uaf_bp_card-desc"><span className="uaf_bp_card-desc-date">{moment(blog.date_addedd).format('ll')} | by</span><a href=""> {blog.author}</a></p>

                  </div>
                </div>
              )}
            </Carousel>
             */}
            <Row gutter={{ xs: 8, sm: 16, md: 24, lg: 32 }}>
              {Blogs && Blogs.map((blog) =>

                <Col xs={{ span: 24 }} sm={{ span: 24 }} md={{ span: 12 }} lg={{ span: 12 }} xl={{ span: 12 }} className="uaf_bp_card_gridCol">

                  <Link to={`/blog/${blog.title.split(" ").join("-")}`}>
                    <Card className="uaf_bp_card"
                      cover={<img style={{ height: "230px", width: "100%", objectFit: "cover" }} src={blog.featured_image} />}
                    >
                      <Meta description={<div>
                        <p className="uaf_bp_card-desc"><span className="uaf_bp_card-desc-date">{moment(blog.date_addedd).format('ll')} | by</span><a href="javascript:void(0)" > {blog.author}</a></p>
                        <p className="uaf_bp_card-desc-heading">{blog.title}</p>
                        {/* <p className="uaf_bp_card-desc-data">{renderHTML(blog.content)}</p> */}
                      </div>} />
                    </Card>
                  </Link>
                </Col>

              )
              }

            </Row>
          </Col>
          <Col xs={{ span: 24 }} sm={{ span: 24 }} md={{ span: 24 }} lg={{ span: 8 }} xl={{ span: 8 }}>
            <Row gutter={{ xs: 8, sm: 16, md: 24, lg: 0 }} style={{ padding: "1rem 0" }}>
              <p className="uaf_bp_rsbloglist-heading">Recent posts</p>
              {recentBlog && recentBlog.map((blog) =>
                <Col className="uaf_bp_rsbloglist" xs={{ span: 24 }} sm={{ span: 24 }} md={{ span: 12 }} lg={{ span: 24 }} xl={{ span: 24 }}>
                  <Row>
                    <Col span="6">
                      <Link to={`/blog/${blog.title.split(" ").join("-")}`}>
                        <img style={{ height: "80px", borderRadius: "3px", width: "100%", objectFit: "cover" }} src={blog.featured_image} alt="" />
                      </Link>
                    </Col>
                    <Col span="18">
                      <p className="uaf_bp_rsbloglist-ut">{blog.title}</p>
                      <p className="uaf_bp_rsbloglist-lt">Posted at {moment(blog.date_addedd).format('ll')}<br />By {blog.author}</p>
                    </Col>
                  </Row>
                </Col>

              )
              }
            </Row>
          </Col>
        </Row>
      </section>

    </Fragment>
  );
};

BlogPage.propTypes = {
  getAllBlog: PropTypes.func.isRequired,
  getRecentBlog: PropTypes.func.isRequired,
  Blogs: PropTypes.array.isRequired
};

const mapStateToProps = state => ({
  Blogs: state.BlogManagement.Blogs,
  recentBlog: state.BlogManagement.recentBlog
});

export default connect(mapStateToProps, {
  getAllBlog,
  getRecentBlog
})(BlogPage);

