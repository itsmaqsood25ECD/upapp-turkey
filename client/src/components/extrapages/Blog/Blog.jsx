import React, { Fragment, useEffect, useState } from "react";
import { connect } from "react-redux";
import ScrollTop from "../../ScrollTop";
import { Card, Button, Row, Col, Spin } from "antd";
import { Link, Redirect } from "react-router-dom";
import "../../../assets/css/homeNew.css";
import "../../../assets/css/blog.css";
import PropTypes from "prop-types";
import { getAllBlog, getRecentBlog, getBlog } from "../../../actions/admin/blog.management.action";
import parse from 'html-react-parser';
import moment from 'moment';
import {
  EmailShareButton,
  FacebookShareButton,
  InstapaperShareButton,
  LinkedinShareButton,
  PinterestShareButton,
  TelegramShareButton,
  TwitterShareButton,
  WhatsappShareButton,

  FacebookIcon,
  TwitterIcon,
  LinkedinIcon,
  PinterestIcon,
  TelegramIcon,
  WhatsappIcon,
  EmailIcon,
} from "react-share";

import { Helmet } from "react-helmet";

const Blog = ({match, getBlog, getRecentBlog, recentBlog, Blog,Blogs, loading }) => {

  const [state, setState] = useState({
    byUID:false,
    slug:''
  })
  const { byUID, slug} = state

  useEffect(() => {
    getAllBlog()
    getBlog(match.params.id);
    Blogs && Blogs.map(bl => {
      if(match.params.id === bl.UID){
        setState({
          ...state,
          byUID:true,
          slug:bl.slug
        })
      } else {
        console.log("else >>")
        getBlog(match.params.id);
      }
    })
    


    getRecentBlog();
  }, [match]);

  if (byUID) {
    return <Redirect to={`/blog`}></Redirect>;
  }
  // Blogs && Blogs.map(bl => {
  //   if(bl.id == match.params.id ){
  //     return <Redirect to={`/blog/${bl.slug}`}></Redirect>;
  //   } else {
  //     getBlog(match.params.id);
  //   }
  // })

  console.log(Blog);
  console.log("recent Blogs",recentBlog)

  console.log(Blog);
  console.log("recent Blogs", recentBlog)
  // const tt = Blog && Blog.title.split(" ").join("-");
  // console.log("Title >>", tt)
  
  const { Meta } = Card;
  return loading ? (
    <Fragment>
      <Spin>

      </Spin>
    </Fragment>
  ) : 
    <Fragment>
      <ScrollTop></ScrollTop>
      <Helmet>
          <title>{Blog && Blog.title}</title>
          <meta
            name="description"
            content={Blog && Blog.title}
          />
      <meta property="og:url" content={window.location.href} />
      <meta property="og:description" content={Blog && Blog.title} />
      <meta property="og:image" content={Blog && Blog.featured_image} />
      <meta property="og:type" content={Blog && Blog.title} />
        </Helmet>
      <section className="UAF-container-fluid" style={{ marginTop: "10rem" }}>
        <Row gutter={{ xs: 8, sm: 16, md: 24, lg: 32 }}>
            <Col xs={{ span: 24 }} sm={{ span: 24 }} md={{ span: 24 }} lg={{ span: 16 }} xl={{ span: 16 }}>
              <Row gutter={{ xs: 8, sm: 16, md: 24, lg: 32 }}>
              <p style={{fontSize:"30px",fontWeight:600,lineHeight:1.2,color:"#000"}}>{Blog && Blog.title}</p> 
              <p className="uaf_bp_card-desc"><span className="uaf_bp_card-desc-date">{moment(Blog && Blog.date_addedd).format('ll')} | by</span><a href="javascript:void(0)"> {Blog && Blog.author}</a></p>
              <div>
                  <img style={{width:"100%",height:"450px",objectFit:"cover"}} src={Blog && Blog.featured_image} />
              </div>
              <div style={{marginTop:"80px",marginBottom:"150px"}}>
              <p className="uaf_bp_card-desc-data">{parse(`${Blog && Blog.content}`)}</p>
              </div>
               </Row>
            </Col>
            <Col xs={{ span: 24 }} sm={{ span: 24 }} md={{ span: 24 }} lg={{ span: 8 }} xl={{ span: 8 }}>
              <Row gutter={{ xs: 8, sm: 16, md: 24, lg: 0 }} style={{ padding: "1rem 0" }}>
                <p className="uaf_bp_rsbloglist-heading">Recent posts</p>
                {recentBlog && recentBlog.map((blog) => 
                 <Col className="uaf_bp_rsbloglist" xs={{ span: 24 }} sm={{ span: 24 }} md={{ span: 12 }} lg={{ span: 24 }} xl={{ span: 24 }}>
                 <Row>
                   <Col span="6">
                       {console.log("blog >>",blog._id)}
                    <Link to={`/blog/${blog.title.split(" ").join("-")}`}>
                     <img style={{height:"80px",width:"100%",objectFit:"cover"}} src={blog.featured_image} alt="" />
                     </Link>
                   </Col>
                   <Col span="18">
                     <p className="uaf_bp_rsbloglist-ut">{blog.title}</p>
                <p className="uaf_bp_rsbloglist-lt">Posted at {moment(blog.date_addedd).format('ll')}<br />By {blog.author}</p>
                   </Col>
                 </Row>
               </Col>
              
                )
                }
               </Row>
               <Link to={`/blog`}>
               <Button type="primary" ghost block>View All Blogs</Button>
               </Link>

            </Col>
          </Row>
          <div>
          
           <div style={{display:"flex",marginBottom:"80px"}}>
          <FacebookShareButton
            url={window.location.href}
            className="share-button"
          >
            <FacebookIcon size={32} round />
            <p>Share</p>
          </FacebookShareButton>
          <LinkedinShareButton
            url={window.location.href}
            className="share-button"
          >
            <LinkedinIcon size={32} round />
            <p>Share</p>
          </LinkedinShareButton>
          <TwitterShareButton
            url={window.location.href}
            className="share-button"
          >
            <TwitterIcon size={32} round />
            <p>Share</p>
          </TwitterShareButton>
          <WhatsappShareButton
            url={window.location.href}
            className="share-button"
          >
            <WhatsappIcon size={32} round />
            <p>Share</p>
          </WhatsappShareButton>
          </div> 
          </div>
        </section>
    </Fragment>
};

Blog.propTypes = {
  getAllBlog: PropTypes.func.isRequired,
  getBlog: PropTypes.func.isRequired,
  getRecentBlog: PropTypes.func.isRequired,
  loading:PropTypes.bool.isRequired
  // Blogs: PropTypes.array.isRequired
};

const mapStateToProps = state => ({
  Blogs: state.BlogManagement.Blogs,
  Blog: state.BlogManagement.Blog,
  recentBlog: state.BlogManagement.recentBlog,
  loading:state.BlogManagement.loading
});

export default connect(mapStateToProps, {
  getAllBlog,
  getBlog,
  getRecentBlog
})(Blog);

