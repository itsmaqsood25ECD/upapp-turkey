import React, { Component, Fragment } from "react";
import { Link } from "react-router-dom";
import { Result, Button } from "antd";

export default class Paymentfailed extends Component {
	constructor(props) {
		super(props);
		this.orderString = `Order with order no:${this.props.match.params.OrderID} has been cancelled`;
		this.title = `Payment Cancelled!`;
	}
	render() {
		return (
			<Fragment>
				<Result
					style={{ paddingTop: "150px" }}
					title={this.title}
					subTitle={this.orderString}
					extra={[
						<Link to="/">
							<Button type="primary" key="console">
								Go to Home Page
							</Button>
						</Link>
					]}
				/>
			</Fragment>
		);
	}
}
