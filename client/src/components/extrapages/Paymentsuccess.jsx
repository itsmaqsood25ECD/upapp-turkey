import React, { Component, Fragment } from "react";
import { Link } from "react-router-dom";
import { Result, Button } from "antd";

export default class Paymentsuccess extends Component {
	constructor(props) {
		super(props);
		this.orderString = `Order Placed. Please save your order confirmation number : ${
			this.props.match.params.OrderID
		}`;
		this.title = `Successfully Purchased  Application!`;
	}
	render() {
		return (
			<Fragment>
				<Result
				    style={{paddingTop:"150px"}}
					status="success"
					title={this.title}
					subTitle={this.orderString}
					extra={[
						<Link to="/">
							<Button type="primary" key="console">
								Go to Home Page
							</Button>
						</Link>
					]}
				/>
			</Fragment>
		);
	}
}
