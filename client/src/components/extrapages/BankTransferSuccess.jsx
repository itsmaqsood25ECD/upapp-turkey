import React, { Component, Fragment } from 'react';
import { Link } from 'react-router-dom';
import { Result, Button } from 'antd';
import ScrollTop from '../../components/ScrollTop';

export default class BankTransferSuccess extends Component {
	constructor(props) {
		super(props);
		this.orderString = `We have emailed the bank details to your registered email address, please follow the instructions specified in the email and complete your purchase. We Thank You for your order with UP app factory.`;
		this.title = `Bank Details Mailed Successfully`;
	}
	render() {
		return (
			<Fragment>
				<ScrollTop />
				<Result
					style={{ paddingTop: '150px',maxWidth:"600px",margin:"0 auto" }}
					status="success"
					title={this.title}
					subTitle={this.orderString}
					extra={[
						<Link to="/">
							<Button type="primary" key="console">
								Go to Home Page
							</Button>
						</Link>
					]}
				/>
			</Fragment>
		);
	}
}
