import React, { Fragment, useState, useEffect } from "react";
import { connect } from "react-redux";
import ScrollTop from "../ScrollTop";
import Hero from "../../components/hero";
import 'react-phone-number-input/style.css';
import {Helmet} from "react-helmet";
import {
	Button,
	Typography,
	Form,
	Icon,
	Input,
	Row,
	Col,
	Select,
	Collapse,
	Affix
} from "antd";
import { contact } from "../../actions/contact.action";
import "antd/dist/antd.css";

const { Panel } = Collapse;
const { Title } = Typography;





const Faq = () => {
							


	return (
		<Fragment>
			<ScrollTop></ScrollTop>
			<Helmet>
			<title>FAQ - UPappfactory</title>
			<meta name="FAQ's " content="FAQ's" />
			</Helmet>

			<Hero Title="FAQ's"></Hero>
			<div style={{background:"rgb(250, 255, 255)"}}>
			<div className="faqs-bg"  style={{paddingBottom: "150px" }}>
				<Row gutter={16}>
				<div style={{ marginTop: "80px" }}>
							
						</div>
					<Col xs={24} lg={12}>
						

						<div
							style={{
								padding: "24",
								background: "#fff",
								margin: "30px 15px",
								boxShadow: "0px 0px 3px #ccc"
							}}
						>
							<Collapse bordered={false} defaultActiveKey={["1"]} accordion>
								<Panel header="How to setup my app?" key="1">
									Simply select the app that you would like to have for your
									business and subscribe for the plan (monthly or annual) and
									then UPapp Customer Support Team will be in touch with you to
									setup your app and system for that app. All apps will be posted on both iOS & Android within 10-12 working days
								</Panel>
								<Panel header="How do I sign up?" key="2">
									Simply fill in the details on UPapp Factor’s website and
									followthe process to complete the registration
								</Panel>
								<Panel header="Will someone help me?" key="3">
								You can always write to us at <a href="mailto:contactus@upapp.co"></a>contactus@upapp.co or simple select Call Back option on our Contact Us page
								</Panel>
								<Panel
									header="What can I change from the demos shown in UPapp or what Customer Sales Team showed?"
									key="4"
								>
									You can change the branding and colour themes from the demos of the apps shown on UPapp factory website. If by only looking at demos on UPapp factory website you are not convinced and would like to see a detailed demo of the apps and system, please contact our UPapp factory's Sales Team and we will be more than glad to give you a personalized demo before you make the purchase on our website.
								</Panel>
								<Panel
									header="What do I get from UPapp along with the apps, system to manage the apps?"
									key="5"
								>You get technical support for the system and apps; you also enjoy free hosting on the server for your apps and admin panel and our UPall factory Customer Support Team will always there to support you on any issue or understanding that you are seeking. Customer Support Team is available from Sunday to Thursday from 9am till 7pm (Oman Standard Time). If you have sent a message or email to Customer Support Team out of working hours or days, then they will contact you in the next business hours.
								</Panel>
								<Panel
									header="How many days does it take to have my app on iOS & Android Stores?"
									key="6"
								>
									It takes 10-12 business days for your apps and admin panel (system) to be ready. The time frame starts from the very day we receive all required materials from you. Also, note that 10-12 business days is exclusive of the time required by iTunes (for iOS apps) and Google Play (for Android apps) stores to approve your app and make the same Go Live.
								</Panel>
								<Panel
									header="Can I Customise the mobile apps more than what UPapp offers?"
									key="7"
								>
									Yes, you can but it comes with an additional charge. You can discuss the requires customization with UPapp factory's Sales Team and they will advise you with the additional charges for those customizations. You can then pay the same through an e-invoice generated to you which will also have the link for the payment. Once your payment for the discussed and agreed customization(s) is received, our technical team will start their effort in ensuring the customization(s) requested by you for your apps or admin panel (system).
									</Panel>
							</Collapse>
						</div>
					</Col>

					<Col xs={24} lg={12}>
					<div
							style={{
								padding: "24",
								background: "#fff",
								margin: "30px 15px",
								boxShadow: "0px 0px 3px #ccc"
							}}
						>

					<Collapse bordered={false} defaultActiveKey={["1"]} accordion>
								<Panel
									header="What support can I get once my app is live in the stores?"
									key="8"
								>
									All technical upkeep of the apps and admin panel along with
									your requests related to any issues that you might be facing
									with the apps.
								</Panel>
								<Panel header="Can I cancel my monthly subscription?" key="9">
									<p>
									Yes, you can, you can cancel your subscription at any point you would like to either it’s a monthly or annual subscription. If you cancel your monthly subscription, your subscription would end to the end of the date of the month on which you started your subscription. For example, you started your subscription on 10th of June and you would like to cancel your subscription on 25th of September, then your subscription would end on 10th of October and your apps will be removed from the stores along with the deactivation of your admin panel for those apps.
									</p>
									<p>
									If during the annual subscription you decide to cancel your subscription, we work on a very convenient method for you as follows:
									</p>
									<ol>
										<li>
										The total number of months that you have utilized using our services, we deduct those months at the rate of OMR 69 per month + 25% fee of the remaining months. For example, you opted for our annual plan which is priced at OMR 588 & you decide to cancel the subscription in the 6th month. Then you will pay (6 months X OMR 49 = OMR 294) + (25% of 6 months X OMR 49 = OMR 73.5). So, the total amount that will be refunded to your card would be OMR 220.500.
										</li>
									</ol>
									<p>
									Please read our Cancellation & Refund Policies for more details.
									</p>
								</Panel>
								<Panel
									header="Can I cancel my annual subscription ?"
									key="10"
								>
									Yes, you can and as discussed in the previous FAQ. Please read
									our Cancellation & Refund Policies for more details.
								</Panel>
								<Panel
									header="How Can I Claim refund?"
									key="11"
								>
									Yes, you will and as discussed in the previous FAQ. Please
									read our Cancellation & Refund Policies for more details.
								</Panel>
								<Panel
									header="how would UPapp know if I am facing any issues with the app.?"
									key="12"
								>
									UPapp factory’s Customer Support has a mega dashboard that suggests which app might be having any technical issue, we notice that ourselves and fix the same on the go. If we are running maintenance of our server or stores, we notify our customers at least 7 business days in advance.
								</Panel>
								<Panel
									header="How can I reach out to UPapp factory Customer Support Team?"
									key="14"
								>
									You can speak with UPapp factory's Customer Support Team through chat within our website, email us & click on Call Back and we will call you. Please see Contact Us on our website for more details. Please use your Customer ID while contacting UPapp factory’s Customer Support Team.
								</Panel>
								<Panel
									header="How data from the current system be migrated to the new UPapp factory?
									"
									key="14"
								>
									Not an issue, our Sales Team can work with you on the same and if your data is in structures excel format, we can help you migrate the same
								</Panel>
							</Collapse>
					</div>
					</Col>
				</Row>
			</div>
			</div>	
		</Fragment>
	);
};



Faq.propTypes = {

};

const mapStateToProps = state => ({

});

export default connect(
	mapStateToProps,
	{ }
)(Faq);
