import React, { Component, Fragment } from "react";
import Hero from "../hero";
import ScrollTop from "../ScrollTop";
import Title from "antd/lib/typography/Title";
import { Row, Col } from "antd";
import { Helmet } from "react-helmet";
import ourMisson from "../../assets/img/AboutUs/ourMission.png";
import ourVision from "../../assets/img/AboutUs/ourVision.png";
import customerFirst from "../../assets/img/AboutUs/customer-first.svg";
import innovationKey from "../../assets/img/AboutUs/innovation_is_key.svg";
import helpingToSucess from "../../assets/img/AboutUs/Helpingyoutosucceed.svg";
import PropTypes from "prop-types";
import { connect } from "react-redux";

import withDirection, { withDirectionPropTypes, DIRECTIONS } from 'react-with-direction';
import { useTranslation} from 'react-i18next';
import i18next from 'i18next';


  const Aboutus = ({
    direction
  }) => {

    const { t } = useTranslation();

    return (
      <Fragment>
        <ScrollTop></ScrollTop>
        <Helmet>
          <title>About Us - About UPappfactory</title>
          <meta
            name="We are UpApp Factory and we are here to create an amazing channel between your brand and your customers, we call it a state-of-the-art native mobile app on iOS & Android along with amazing backend system with immense features to work on."
            content="about on UPappfactory information About UPappfactory"
          />
        </Helmet>

        {/* <Hero Title="About us"></Hero> */}
        {direction === DIRECTIONS.LTR && <Fragment>
          <div className="lg-ltr">

        <div
          className="about-us-section"
          style={{ textAlign: "center", paddingTop: "50px" }}
        >
          <div>
            <Title className="light-text" level={1}>
              {t("AboutUS.AboutUS")}
            </Title>
            <div className="lg-ltr" style={{marginTop:"50px"}}>
              <p>
              {t("AboutUS.1")}
              </p>
              <p>
              {t("AboutUS.2")}
              </p>
              <p>
              {t("AboutUS.3")}
              </p>
              <p>
              {t("AboutUS.4")}
              </p>
              <p>
                Our team consists of hard-working technology professionals who
                are available to assist you on any UPapp factory queries. Email
                us on{" "}
                <a href="mailto:contactus@upapp.co">
                contactus@upapp.co
                </a>
              </p>
            </div>
          </div>
          <div></div>
        </div>
        <br />
        <br />
        <br />
        <Row type="flex" justify="space-around" align="middle">
          <Col xs={{ span: 24, order: 1 }} lg={{ span: 12, order: 1 }}>
            <img className="our-mission" src={ourMisson} alt="" />
          </Col>
          <Col xs={{ span: 24, order: 2 }} lg={{ span: 12, order: 2 }}>
            <div>
              <Title className="light-text text-center" level={1}>
              {t("AboutUS.ourMission.1")}
              </Title>
              <p className="mission-msg text-center">
              {t("AboutUS.ourMission.2")}
              </p>
            </div>
          </Col>
        </Row>
        <Row type="flex" justify="space-around" align="middle">
          <Col xs={{ span: 24, order: 2 }} lg={{ span: 12, order: 1 }}>
            <div>
              <Title className="light-text text-center" level={1}>
              {t("AboutUS.ourVision.1")}
              </Title>
              <p className="vision-msg text-center">
              {t("AboutUS.ourVision.2")}
              </p>
            </div>
          </Col>
          <Col xs={{ span: 24, order: 1 }} lg={{ span: 12, order: 2 }}>
            <img className="our-mission" src={ourVision} alt="" />
          </Col>
        </Row>
        <br />
        <br />
        <br />
        <div className="text-center">
          <Title className="light-text" level={1}>
          {t("AboutUS.OurValues")}
          </Title>
        </div>
        <br />
        <br />
        <br />
        <Row>
          <Col xs={24} lg={8}>
            <div className="upapp-value-container">
              <img src={customerFirst} alt="" />
              <p>
              {t("AboutUS.customerFirst")}
              </p>
            </div>
          </Col>
          <Col xs={24} lg={8}>
            <div className="upapp-value-container">
              <img src={innovationKey} alt="" />
              <p>
              {t("AboutUS.innovationKey")}
              </p>
            </div>
          </Col>
          <Col xs={24} lg={8}>
            <div className="upapp-value-container">
              <img src={helpingToSucess} alt="" />
              <p>
              {t("AboutUS.helpingToSucceed")}
              </p>
            </div>
          </Col>
        </Row>
        </div>
        </Fragment>
        }

        {direction === DIRECTIONS.RTL && <Fragment>
          <div className="lg-rtl">

        <div
          className="about-us-section"
          style={{ textAlign: "center", paddingTop: "50px" }}
        >
          <div>
            <Title className="light-text" level={1}>
              {t("AboutUS.AboutUS")}
            </Title>
            <div className="lg-rtl" style={{marginTop:"50px"}}>
              <p>
              {t("AboutUS.1")}
              </p>
              <p>
              {t("AboutUS.2")}
              </p>
              <p>
              {t("AboutUS.3")}
              </p>
              <p>
              {t("AboutUS.4")}
              </p>
              <p>
                Our team consists of hard-working technology professionals who
                are available to assist you on any UPapp factory queries. Email
                us on{" "}
                <a href="mailto:contactus@upapp.co">
                contactus@upapp.co
                </a>
              </p>
            </div>
          </div>
          <div></div>
        </div>
        <br />
        <br />
        <br />
        <Row type="flex" justify="space-around" align="middle">
          <Col xs={{ span: 24, order: 1 }} lg={{ span: 12, order: 1 }}>
            <img className="our-mission" src={ourMisson} alt="" />
          </Col>
          <Col xs={{ span: 24, order: 2 }} lg={{ span: 12, order: 2 }}>
            <div>
              <Title className="light-text text-center" level={1}>
              {t("AboutUS.ourMission.1")}
              </Title>
              <p className="mission-msg text-center">
              {t("AboutUS.ourMission.2")}
              </p>
            </div>
          </Col>
        </Row>
        <Row type="flex" justify="space-around" align="middle">
          <Col xs={{ span: 24, order: 2 }} lg={{ span: 12, order: 1 }}>
            <div>
              <Title className="light-text text-center" level={1}>
              {t("AboutUS.ourVision.1")}
              </Title>
              <p className="vision-msg text-center">
              {t("AboutUS.ourVision.2")}
              </p>
            </div>
          </Col>
          <Col xs={{ span: 24, order: 1 }} lg={{ span: 12, order: 2 }}>
            <img className="our-mission" src={ourVision} alt="" />
          </Col>
        </Row>
        <br />
        <br />
        <br />
        <div className="text-center">
          <Title className="light-text" level={1}>
          {t("AboutUS.OurValues")}
          </Title>
        </div>
        <br />
        <br />
        <br />
        <Row>
          <Col xs={24} lg={8}>
            <div className="upapp-value-container">
              <img src={customerFirst} alt="" />
              <p>
              {t("AboutUS.customerFirst")}
              </p>
            </div>
          </Col>
          <Col xs={24} lg={8}>
            <div className="upapp-value-container">
              <img src={innovationKey} alt="" />
              <p>
              {t("AboutUS.innovationKey")}
              </p>
            </div>
          </Col>
          <Col xs={24} lg={8}>
            <div className="upapp-value-container">
              <img src={helpingToSucess} alt="" />
              <p>
              {t("AboutUS.helpingToSucceed")}
              </p>
            </div>
          </Col>
        </Row>
        </div>
        </Fragment>
        }
        <div></div>
      </Fragment>
    
    );
}

const mapStateToProps = state => ({
 
});

export default connect(mapStateToProps, { })(withDirection(Aboutus));
