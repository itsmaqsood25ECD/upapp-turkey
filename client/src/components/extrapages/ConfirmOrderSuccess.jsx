import React, { Component, Fragment } from 'react';
import { Link } from 'react-router-dom';
import { Result, Button } from 'antd';
import ScrollTop from '../../components/ScrollTop';

export default class ConfirmOrderSuccess extends Component {
	constructor(props) {
		super(props);
		this.orderString = `You have confirmed the order Successfully.Our Sales team will get back to you as soon as possible.`;
		this.title = `Confrim Order`;
	}
	render() {
		return (
			<Fragment>
				<ScrollTop />
				<Result
					style={{ paddingTop: '120px' }}
					status="success"
					title={this.title}
					subTitle={this.orderString}
					extra={[
						<Link to="/">
							<Button type="primary" key="console">
								Go to Home Page
							</Button>
						</Link>
					]}
				/>
			</Fragment>
		);
	}
}
