/* eslint react/prop-types: 0 */
import React, { Component } from "react";
import { connect } from "react-redux";
import { Row, Col, Typography, Radio, Button, Icon, Tabs, Anchor, List, Divider, } from "antd";
import Walkthrugh from "./Walkthrugh";
import CurrencyFormat from "react-currency-format";
import base64 from "base-64";
import "../../assets/css/packagePrice.css";
import withDirection, { withDirectionPropTypes, DIRECTIONS } from 'react-with-direction';
import { withTranslation } from 'react-i18next';
import i18next from 'i18next';

const { Title } = Typography;
const { TabPane } = Tabs;
const { Link } = Anchor;


export class PackagePriceVerticleFacility extends Component {
  constructor(props) {
    super(props);
    this.state = {
      // Plan A
      MBDSPM: this.props.MBDSPM,
      MADSPM: this.props.MADSPM,
      MBDSPY: this.props.MBDSPY,
      MADSPY: this.props.MADSPY,
      Duration: this.props.duration,
      currency: this.props.currency,
      MBDSP: this.props.MBDSPY,
      MADSP: this.props.MADSPY,
      PackType: this.props.PackType,
      SLUG: this.props.SLUG,
      ID: this.props.ID,

      //plan B
      MFPP: this.props.MFPP,

      // Setup Charge
      MSC: this.props.MSC
    };
  }



  componentWillReceiveProps(nextApp) {
    this.setState({
      // Plan B
      MFPP: nextApp.MFPP,
      // Plan A
      MBDSPM: nextApp.MBDSPM,
      MADSPM: nextApp.MADSPM,
      MBDSPY: nextApp.MBDSPY,
      MADSPY: nextApp.MADSPY,
      Duration: nextApp.duration,
      currency: nextApp.currency,
      PackType: nextApp.PackType,
      MBDSP: nextApp.MBDSPY,
      MADSP: nextApp.MADSPY,
      // Setup Charge
      MSC: nextApp.MSC
    });
  }

  handleChange = e => {
    if (e.target.value === "Year") {
      this.setState({
        MBDSP: this.state.MBDSPY,
        MADSP: this.state.MADSPY,

        PackType: "Annually"
      });
    } else if (e.target.value === "Month") {
      this.setState({
        MBDSP: this.state.MBDSPM,
        MADSP: this.state.MADSPM,

        PackType: "Monthly"
      });
    }
  };

  render() {
    const { t } = this.props;
    const currency = this.props.currency;
    const MSC = this.props.MSC;

    const featuresList = [
      {
        title: `${t("restaurentApp.UnlimitedOrders")}`,
      },
      {
        title: `${t("restaurentApp.UnlimitedBranches")}`
      },
      {
        title: `${t("restaurentApp.UnlimitedMenu")}`
      },
      {
        title: `${t("restaurentApp.iosAndroid")}`
      },
      {
        title: `${t("restaurentApp.PaymentGatewaty")}`
      },
      {
        title: `${t("restaurentApp.LoyalityProgram")}`
      },
      {
        title: `${t("restaurentApp.TableReservation")}`
      },
      {
        title: `${t("restaurentApp.CouponsPromotions")}`
      },
      {
        title: `${t("restaurentApp.DeliveryOptions")}`
      },
      {
        title: `${t("restaurentApp.ModesofPaymentOptions")}`
      }
    ]

    return (

      <div className="package-bg">
        <div style={{ textAlign: "center" }}>
          <Title level={3}>{t("restaurentApp.FeaturesIncluded")}</Title>
        </div>
        <Row
          gutter={16}
          type="flex"
          justify="center"
          align="middle"
          style={{ margin: "0px 0px", border: "0px solid #eee" }}
        >
          <Col lg={24} xs={24} className="">
            <div
              className="V-Price-Switcher"
              style={{
                textAlign: "center",
                marginTop: "15px",
                marginBottom: "15px"
              }}
            >
            </div>
          </Col>
          <Col xs={24}>
            <Row gutter={16}>
              <div  className="uaf_pricingVertical_outer">
                <div className="priceing-card-horizontal" style={{ paddingTop: "60px" }}>
                  <List
                    grid={{
                      gutter: 16,
                      xs: 1,
                      sm: 2,
                      md: 4,
                      lg: 4,
                      xl: 4,
                      xxl: 3,
                    }}
                    dataSource={featuresList}
                    renderItem={item => (
                      <List.Item style={{ fontSize: "14px" }}>
                        <Icon style={{ color: "green", fontSize: "20px" }} type="check" />  {item.title}
                      </List.Item>
                    )}
                  />
                  <Divider style={{ fontSize: "25px", marginTop: "35px", marginBottom: "35px" }} >{t('pricing.ourPricing')}</Divider>
                  <p
                    className="text-demo-sub-text lg-ltr"
                    style={{
                        fontSize: "17px",
                        lineHeight: "20px",
                        fontWeight: 500,
                        maxWidth: "100%",
                        textAlign: 'center',
                        marginBottom: '25px'
                    }}
                    >
                  <div>
                  <p>For Special Pricing with free hosting & technical Support
                  Contact Sales</p>
                  </div>
                  </p>
                  <div style={{ textAlign: "center", width: "100%", marginBottom: "30px", marginTop: "20px" }}>
                    <Anchor style={{ margin: "0 auto" }}>
                      <Link
                        href="/contactUs#GetInTouchForm"
                        title={
                          <Button className="UAFprimaryButton1" type="primary">{t('General.ContactSales')}</Button>
                        }
                      ></Link>
                    </Anchor>
                  </div>

                </div>
              </div>
            </Row>
          </Col>
        </Row>
      </div>

    );
  }
}

export default withTranslation()(PackagePriceVerticleFacility);
