/* eslint react/prop-types: 0 */
import React, { Component } from "react";
import { connect } from "react-redux";
import "../../assets/css/packagePrice.css";
import { Row, Col, Typography, Radio, Button, Icon } from "antd";
import Walkthrugh from "./Walkthrugh";
import CurrencyFormat from "react-currency-format";
import base64 from "base-64";
import MobileApp from "../../assets/img/PackIcon/mobileApp.svg";
import WebApp from "../../assets/img/PackIcon/WebApp.svg";
import Website from "../../assets/img/PackIcon/Website.svg";
import utf8 from "utf8";

const { Title } = Typography;
export class PackagePriceVerticle extends Component {
  constructor(props) {
    super(props);
    this.state = {
      MBDSPM: this.props.MBDSPM,
      MADSPM: this.props.MADSPM,
      MBDSPY: this.props.MBDSPY,
      MADSPY: this.props.MADSPY,
      WBDSPM: this.props.WBDSPM,
      WADSPM: this.props.WADSPM,
      WBDSPY: this.props.WBDSPY,
      WADSPY: this.props.WADSPY,
      WABDSPM: this.props.WABDSPM,
      WAADSPM: this.props.WAADSPM,
      WABDSPY: this.props.WABDSPY,
      WAADSPY: this.props.WAADSPY,
      Duration: this.props.duration,
      currency: this.props.currency,
      MBDSP: this.props.MBDSPY,
      MADSP: this.props.MADSPY,
      WBDSP: this.props.WBDSPY,
      WADSP: this.props.WADSPY,
      WABDSP: this.props.WABDSPY,
      WAADSP: this.props.WAADSPY,
      PackType: this.props.PackType,
      SLUG: this.props.SLUG,
      ID: this.props.ID,
      MDP: "0",
      YDP: "0",
      DP: (
        ((+this.props.BDSPY - +this.props.ADSPY) / +this.props.BDSPY) *
        100
      ).toFixed(1)
    };
  }

  componentWillReceiveProps(nextApp) {
    this.setState({
      MBDSPM: nextApp.MBDSPM,
      MADSPM: nextApp.MADSPM,
      MBDSPY: nextApp.MBDSPY,
      MADSPY: nextApp.MADSPY,
      WBDSPM: nextApp.WBDSPM,
      WADSPM: nextApp.WADSPM,
      WBDSPY: nextApp.WBDSPY,
      WADSPY: nextApp.WADSPY,
      WABDSPM: nextApp.WABDSPM,
      WAADSPM: nextApp.WAADSPM,
      WABDSPY: nextApp.WABDSPY,
      WAADSPY: nextApp.WAADSPY,
      Duration: nextApp.duration,
      currency: nextApp.currency,
      MBDSP: nextApp.MBDSPY,
      MADSP: nextApp.MADSPY,
      WBDSP: nextApp.WBDSPY,
      WADSP: nextApp.WADSPY,
      WABDSP: nextApp.WABDSPY,
      WAADSP: nextApp.WAADSPY,
      PackType: nextApp.PackType,
      SLUG: nextApp.SLUG,
      ID: nextApp.ID
    });
  }

  handleChange = e => {
    if (e.target.value === "Year") {
      this.setState({
        MBDSP: this.state.MBDSPY,
        MADSP: this.state.MADSPY,
        WBDSP: this.state.WBDSPY,
        WADSP: this.state.WADSPY,
        WABDSP: this.state.WABDSPY,
        WAADSP: this.state.WAADSPY,
        PackType: "Annually"
      });
    } else if (e.target.value === "Month") {
      this.setState({
        MBDSP: this.state.MBDSPM,
        MADSP: this.state.MADSPM,
        WBDSP: this.state.WBDSPM,
        WADSP: this.state.WADSPM,
        WABDSP: this.state.WABDSPM,
        WAADSP: this.state.WAADSPM,
        PackType: "Monthly"
      });
    }
  };

  render() {
    const currency = this.props.currency;
    return (
      <div className="package-bg">
        {/* <RequestForDemo /> */}

        <div style={{ textAlign: "center" }}>
          <Title level="3">Boost Your Business</Title>

          <div className="price-verticle-tagline">
            No Commitment Necessary
            <br />
            Change or cancel Your Subscription Anytime
          </div>

          {/* <Text>Flexible plans and pricing</Text> */}
        </div>
        <Row
          gutter={16}
          type="flex"
          justify="center"
          align="middle"
          style={{ margin: "0px 0px", border: "0px solid #eee" }}
        >
          <Col lg={24} xs={24} className="">
            <div
              className="V-Price-Switcher"
              style={{
                textAlign: "center",
                marginTop: "15px",
                marginBottom: "15px"
              }}
            >
              <Radio.Group
                defaultValue="Year"
                buttonStyle="solid"
                onChange={this.handleChange}
                defaultValue="Year"
                size="large"
                className="Pack-chooser-container"
                shape="circle"
              >
                <Radio.Button className="year" value="Year">
                  Yearly
                </Radio.Button>
                <Radio.Button className="month" value="Month">
                  Monthly
                </Radio.Button>
              </Radio.Group>
            </div>
          </Col>
          <Col xs={24}>
            <Row gutter={16}>
              <div className="uaf_pricingVertical_outer">
                {/* Website Package */}
                <Col xs={24} lg={8}>
                  <div className="pack-card Basic">
                    <div className="package-img-cont">
                      <img
                        className="package-img"
                        src={Website}
                        alt=""
                        style={{ width: "200px" }}
                      />
                    </div>
                    <div style={{ textAlign: "center" }}>
                      <Title className="package-title" level={3}>
                        Basic
                      </Title>
                    </div>
                    <div
                      style={{
                        textAlign: "center",
                        margin: "10px auto",
                        fontSize: "14px"
                      }}
                    >
                      <div
                        style={{
                          maxWidth: "230px",
                          borderBottom: "0.5px solid #ddd",
                          margin: "0px auto",
                          lineHeight: "2"
                        }}
                      >
                        eCommerce Website Included
                      </div>
                      <div
                        style={{
                          maxWidth: "150px",
                          margin: "0px auto",
                          lineHeight: "2"
                        }}
                      >
                        All Features Included.
                      </div>

                      <div></div>
                    </div>
                    <Title
                      level={2}
                      class="light-text"
                      style={{
                        marginTop: "40px",
                        marginBottom: "20px",
                        textAlign: "center"
                      }}
                    >
                      <div className="before-discount-price">
                        {currency}&nbsp;
                        <CurrencyFormat
                          value={Number(this.state.WBDSP)}
                          displayType={"text"}
                          thousandSeparator={true}
                        />
                      </div>
                      &nbsp;
                      <span className="PriceNum">
                        <span className="currency">{currency}</span>
                        <CurrencyFormat
                          value={Number(this.state.WADSP)}
                          displayType={"text"}
                          thousandSeparator={true}
                        />
                        <sup className="sup-star">*</sup>
                        <span></span>
                        <span className="per-month-tag">/Mo</span>
                      </span>
                      {/* <span className="Discount-V">
							{Number(this.state.DP)}%
							<span style={{ whiteSpace: "pre-line" }}>Off</span>
						</span> */}
                    </Title>
                    <div style={{ textAlign: "center" }}>
                      <Walkthrugh
                        PLink={`/readymade-apps/${this.state.SLUG}/${
                          this.state.ID
                        }/checkout/${base64.encode(Number(this.state.WADSP))}`}
                      ></Walkthrugh>
                      <div className="one-time-setup-tag">
                        *one-time minimum set up charges $500 will be apply for
                        year one only
                      </div>
                    </div>
                  </div>
                </Col>
                {/* End Website Package */}

                {/* Web App Package */}
                <Col xs={24} lg={8}>
                  <div
                    className="pack-card ultra"
                    style={{ marginTop: "-10px" }}
                  >
                    <div className="package-img-cont">
                      <img className="package-img" src={WebApp} alt="" />
                    </div>

                    <div style={{ textAlign: "center" }}>
                      <Title className="package-title" level={3}>
                        Ultra
                      </Title>
                    </div>
                    <div
                      style={{
                        textAlign: "center",
                        margin: "10px auto",
                        fontSize: "14px"
                      }}
                    >
                      <div
                        style={{
                          maxWidth: "230px",
                          borderBottom: "0.5px solid #ddd",
                          margin: "0px auto",
                          lineHeight: "2"
                        }}
                      >
                        Android | iPhone App Included
                      </div>
                      <div
                        style={{
                          maxWidth: "230px",
                          borderBottom: "0.5px solid #ddd",
                          margin: "0px auto",
                          lineHeight: "2"
                        }}
                      >
                        eCommerce Website Included
                      </div>
                      <div
                        style={{
                          maxWidth: "150px",
                          margin: "0px auto",
                          lineHeight: "2"
                        }}
                      >
                        All Features Included.
                      </div>
                      <div></div>
                    </div>
                    <Title
                      level={2}
                      class="light-text"
                      style={{
                        marginTop: "40px",
                        marginBottom: "20px",
                        textAlign: "center"
                      }}
                    >
                      <div className="before-discount-price">
                        {currency}&nbsp;
                        <CurrencyFormat
                          value={Number(this.state.WABDSP)}
                          displayType={"text"}
                          thousandSeparator={true}
                        />
                      </div>
                      &nbsp;
                      <span className="PriceNum">
                        <span className="currency">{currency}</span>
                        <CurrencyFormat
                          value={Number(this.state.WAADSP)}
                          displayType={"text"}
                          thousandSeparator={true}
                        />
                        <sup className="sup-star">*</sup>
                        <span></span>
                        <span className="per-month-tag">/Mo</span>
                      </span>
                      {/* <span className="Discount-V">
							{Number(this.state.DP)}%
							<span style={{ whiteSpace: "pre-line" }}>Off</span>
						</span> */}
                    </Title>
                    <div style={{ textAlign: "center" }}>
                      <Walkthrugh
                        PLink={`/readymade-apps/${this.state.SLUG}/${
                          this.state.ID
                        }/checkout/${base64.encode(Number(this.state.WAADSP))}`}
                      ></Walkthrugh>
                      <div className="one-time-setup-tag">
                        *one-time minimum set up charges $500 will be apply for
                        year one only
                      </div>
                    </div>
                  </div>
                </Col>
                {/* End Web App Package */}
                {/* Mobile App Package */}
                <Col xs={24} lg={8}>
                  <div className="pack-card Premium">
                    <div className="package-img-cont">
                      <img className="package-img" src={MobileApp} alt="" />
                    </div>
                    <div style={{ textAlign: "center" }}>
                      <Title className="package-title" level={3}>
                        Premium
                      </Title>
                    </div>
                    <div
                      style={{
                        textAlign: "center",
                        margin: "10px auto",
                        fontSize: "14px"
                      }}
                    >
                      <div
                        style={{
                          maxWidth: "230px",
                          borderBottom: "0.5px solid #ddd",
                          margin: "0px auto",
                          lineHeight: "2"
                        }}
                      >
                        Android | iPhone App Included
                      </div>
                      <div
                        style={{
                          maxWidth: "150px",
                          margin: "0px auto",
                          lineHeight: "2"
                        }}
                      >
                        All Features Included.
                      </div>
                      <div></div>
                    </div>

                    <Title
                      level={2}
                      class="light-text"
                      style={{
                        marginTop: "40px",
                        marginBottom: "20px",
                        textAlign: "center"
                      }}
                    >
                      <div className="before-discount-price">
                        {currency}&nbsp;
                        <CurrencyFormat
                          value={Number(this.state.MBDSP)}
                          displayType={"text"}
                          thousandSeparator={true}
                        />
                      </div>
                      &nbsp;
                      <span className="PriceNum">
                        <span className="currency">{currency}</span>
                        <CurrencyFormat
                          value={Number(this.state.MADSP)}
                          displayType={"text"}
                          thousandSeparator={true}
                        />
                        <sup className="sup-star">*</sup>
                        <span></span>
                        <span className="per-month-tag">/Mo</span>
                      </span>
                      {/* <span className="Discount-V">
							{Number(this.state.DP)}%
							<span style={{ whiteSpace: "pre-line" }}>Off</span>
						</span> */}
                    </Title>
                    <div style={{ textAlign: "center" }}>
                      <Walkthrugh
                        PLink={`/readymade-apps/${this.state.SLUG}/${
                          this.state.ID
                        }/checkout/${base64.encode(Number(this.state.MADSP))}`}
                      ></Walkthrugh>
                      <div className="one-time-setup-tag">
                        *one-time minimum set up charges $500 will be apply for
                        year one only
                      </div>
                    </div>
                  </div>
                </Col>
                {/* End Mobile App Package */}
              </div>
            </Row>
          </Col>
        </Row>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  user: state.auth.user
});

const mapDispatchToProps = {};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(PackagePriceVerticle);
