/* eslint react/prop-types: 0 */
import React, { useState, useEffect, Fragment } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { Button, Typography, Row, Col, Icon, Carousel, List, Spin, Input, Modal, Form, Tabs } from "antd";
import "rc-banner-anim/assets/index.css";
import ScrollTop from "../../ScrollTop";
import ReactGA from "react-ga";
import { Link } from "react-router-dom";
import WebsiteThemes from "../../Pages/WebsiteThemes";
import WebsiteThemesTemp from "../../Pages/WebsiteThemesTemp";
import "antd/dist/antd.css";
import "../../../assets/css/custom.css";
import "../../../assets/css/home.css";
import "../../../assets/css/homeNew.css";
import "react-animated-slider/build/horizontal.css";

import OneStopEcommerceSolution from "../../../assets/img/eCommerce Page/OneStopEcommerceSolution.svg";
import CustomizedApps from "../../../assets/img/Home Page images/CustomizedApps.svg";
import AndroidIosAppIncluded from "../../../assets/img/eCommerce Page/Android&IosAppIncluded.svg";
import SecureAGILE from "../../../assets/img/eCommerce Page/SecureAGILE.svg";
import MultiVendor from "../../../assets/img/eCommerce Page/MultiVendor.svg";
import ResponsiveWebsite from "../../../assets/img/eCommerce Page/ResponsiveWebsite.svg";

// Benifits

// Key Benifits
import ReadyToLaunch from "../../../assets/img/eCommerce Page/KeyBenifits/ReadyToLaunch.svg";
import NotificationsSMSEmail from "../../../assets/img/eCommerce Page/KeyBenifits/NotificationsSMSEmail.svg";
import SecurityisParamount from "../../../assets/img/eCommerce Page/KeyBenifits/SecurityisParamount.svg";
import AcceptPaymentsOnline from "../../../assets/img/eCommerce Page/KeyBenifits/AcceptPaymentsOnline.svg";
import IntegrationAPI from "../../../assets/img/eCommerce Page/KeyBenifits/Integration&API.svg";
import CustomFeatures from "../../../assets/img/eCommerce Page/KeyBenifits/CustomFeatures.svg";
// End Key Benifits

//Business Benifits
import AutomaticAppSystemUpgrade from "../../../assets/img/eCommerce Page/Business Benifits/AutomaticAppSystemUpgrade.svg";
import Customization from "../../../assets/img/eCommerce Page/Business Benifits/Customization.svg";
import FastestLoadingTime from "../../../assets/img/eCommerce Page/Business Benifits/FastestLoadingTime.svg";
import SocialMediaIntegration from "../../../assets/img/eCommerce Page/Business Benifits/SocialMediaIntegration.svg";
import FreeAppStoreFeeSpace from "../../../assets/img/eCommerce Page/Business Benifits/FreeAppStoreFee&Space.svg";
import ProductsPromotions from "../../../assets/img/eCommerce Page/Business Benifits/Products&Promotions.svg";

import UnlimitedTransactions from "../../../assets/img/eCommerce Page/Client Benifits/UnlimitedTransactions.svg";
import Reviews from "../../../assets/img/eCommerce Page/Client Benifits/Reviews.svg";
// Client Benifits

import MultiLanguageApp from "../../../assets/img/eCommerce Page/Client Benifits/MultiLanguageApp.svg";

import FreeServerSpace from "../../../assets/img/eCommerce Page/Client Benifits/FreeServerSpace.svg";

// End of Client Benifits

// End Business Benifits
import MobileAppView from "../../MobileAppView/MobileAppPage";
import PackagePriceVerticleEcommerce from "../PackagePriceVerticleEcommerce";
import ContactUsForm from "../ContactUsForm";
import { store } from "../../../store/store";
import { requestForCustomisedApp } from "../../../actions/contact.action";
import { loadApplication } from "../../../actions/application.action";
import { setAlert } from "../../../actions/alert.action.jsx";
import { Helmet } from "react-helmet";
import IndustriesWeServe from "../IndustriesWeServe";
import HomePageBanners from "../HomePageBanners";
import withDirection, { withDirectionPropTypes, DIRECTIONS } from 'react-with-direction';
import { useTranslation } from 'react-i18next';
import i18next from 'i18next';

const { TabPane } = Tabs;
const { TextArea } = Input;
const { Title, Text } = Typography;
let filteredApps;
const recaptchaRef = React.createRef();

const HomeNew = ({
  loadApplication,
  Applications,
  loading,
  requestForCustomisedApp,
  setAlert,
  form: { getFieldDecorator, validateFields, setFieldsValue },
  direction,
  user
}) => {

  const { t } = useTranslation();


  const [appDetails, setAppData] = useState({
    apps: [],
    selectApplication: "",
    currency: "",
    // Booking
    bookingMobileBDSPM: "",
    bookingMobileADSPM: "",
    bookingMobileBDSPY: "",
    bookingMobileADSPY: "",
    bookingWebsiteBDSPM: "",
    bookingWebsiteADSPM: "",
    bookingWebsiteBDSPY: "",
    bookingWebsiteADSPY: "",
    bookingWebAppBDSPM: "",
    bookingWebAppADSPM: "",
    bookingWebAppBDSPY: "",
    bookingWebAppADSPY: "",
    // EcommcercenewBDSPM: '',
    newADSPM: "",
    newBDSPY: "",
    newADSPY: "",
    newMobileBDSPM: "",
    newMobileADSPM: "",
    newMobileBDSPY: "",
    newMobileADSPY: "",
    newWebsiteBDSPM: "",
    newWebsiteADSPM: "",
    newWebsiteBDSPY: "",
    newWebsiteADSPY: "",
    newWebAppBDSPM: "",
    newWebAppADSPM: "",
    newWebAppBDSPY: "",
    newWebAppADSPY: "",
    country: "",

    // Plan B
    WFPP: "",
    WAFPP: "",
    MFPP: ""
  });
  const [formData, setFormData] = useState({});

  const [state, setState] = useState({
    visible: false,
    modalVisible: false,
    userlocation: "",
    HumanVarification: false
  });
  const { HumanVarification } = state;



  const {
    apps,
    selectApplication,
    country,
    currency,
    newMobileBDSPM,
    newMobileADSPM,
    newMobileBDSPY,
    newMobileADSPY,
    newWebsiteBDSPM,
    newWebsiteADSPM,
    newWebsiteBDSPY,
    newWebsiteADSPY,
    newWebAppBDSPM,
    newWebAppADSPM,
    newWebAppBDSPY,
    newWebAppADSPY,
    WFPP,
    WAFPP,
    MFPP,
    // setup charge
    WSC,
    WMSC,
    MSC
  } = appDetails;

  let ecommApps, bookingApps;

  // store.dispatch(loadUser());
  // store.dispatch(loadApplication());
  // store.dispatch(getLocation());

  useEffect(() => {

    setAppData({
      ...appDetails,
      apps:
        Applications &&
        Applications.map(arg => {
          return arg;
        }),
      country: sessionStorage.getItem("country"),
      currency: sessionStorage.getItem("currency_sign"),
      // Website
      newWebsiteBDSPM: Math.round(
        29.9 * sessionStorage.getItem("currency_rate")
      ),
      newWebsiteADSPM: Math.round(
        19.9 * sessionStorage.getItem("currency_rate")
      ),
      newWebsiteBDSPY: Math.round(
        29.9 * sessionStorage.getItem("currency_rate")
      ),
      newWebsiteADSPY: Math.round(
        9.9 * sessionStorage.getItem("currency_rate")
      ),
      // Web App
      newWebAppBDSPM: Math.round(
        49.9 * sessionStorage.getItem("currency_rate")
      ),
      newWebAppADSPM: Math.round(
        39.9 * sessionStorage.getItem("currency_rate")
      ),
      newWebAppBDSPY: Math.round(
        49.9 * sessionStorage.getItem("currency_rate")
      ),
      newWebAppADSPY: Math.round(
        29.9 * sessionStorage.getItem("currency_rate")
      ),
      // Mobile
      newMobileBDSPM: Math.round(
        39.9 * sessionStorage.getItem("currency_rate")
      ),
      newMobileADSPM: Math.round(
        29.9 * sessionStorage.getItem("currency_rate")
      ),
      newMobileBDSPY: Math.round(
        39.9 * sessionStorage.getItem("currency_rate")
      ),
      newMobileADSPY: Math.round(
        19.9 * sessionStorage.getItem("currency_rate")
      ),
      //  Plan B

      WFPP: Math.round(59.9 * sessionStorage.getItem("currency_rate")),
      MFPP: Math.round(89.9 * sessionStorage.getItem("currency_rate")),
      WAFPP: Math.round(119.9 * sessionStorage.getItem("currency_rate")),
      // setup charge
      WSC: Math.round(500 * sessionStorage.getItem("currency_rate")),
      WMSC: Math.round(1200 * sessionStorage.getItem("currency_rate")),
      MSC: Math.round(800 * sessionStorage.getItem("currency_rate"))
    });
  }, [Applications, country, currency]);

  ecommApps =
    Applications &&
    Applications.filter(myapp => {
      return myapp.fields && myapp.fields.type.toLowerCase() === "ecommerce";
    });

  bookingApps =
    Applications &&
    Applications.filter(myapp => {
      return myapp.fields && myapp.fields.type.toLowerCase() === "booking";
    });
  // console.log(Applications, ecommApps, bookingApps);

  if (selectApplication === "" || selectApplication === undefined) {
    filteredApps = apps && apps[0];
  } else {
    let data =
      apps &&
      apps.filter(myapp => {
        return (
          (myapp.fields &&
            myapp.fields.name
              .toLowerCase()
              .includes(
                selectApplication && selectApplication.toLocaleLowerCase()
              )) ||
          (myapp.fields &&
            myapp.fields.type
              .toLowerCase()
              .includes(
                selectApplication && selectApplication.toLocaleLowerCase()
              ))
        );
      });
    filteredApps = data && data[0];
  }

  const KeyBenifits = [
    {
      icon: ReadyToLaunch,
      name: `${t("ecommercePage.ReadyToLaunch")}`
    },
    {
      icon: NotificationsSMSEmail,
      name: `${t("ecommercePage.PushNotifications")}`
    },
    {
      icon: SecurityisParamount,
      name: `${t("ecommercePage.HighlySecurePlatform")}`
    },
    {
      icon: AcceptPaymentsOnline,
      name: `${t("ecommercePage.AcceptPaymentOnline")}`
    },
    // {
    //   icon: IntegrationAPI,
    //   name: `${t("ecommercePage.Integration&API")}`
    // },

    // {
    //   icon: AutomaticAppSystemUpgrade,
    //   name:`${t("ecommercePage.Automaticappsystemupgrade")}`
    // },

    {
      icon: FastestLoadingTime,
      name: `${t("ecommercePage.QuickLoadTime")}`
    },
    // {
    //   icon: SocialMediaIntegration,
    //   name: `${t("ecommercePage.SocialMediaIntegration")}` 
    // },
    {
      icon: FreeAppStoreFeeSpace,
      name: `${t("ecommercePage.Noadditionalservercost")}`
    },
    {
      icon: ProductsPromotions,
      name: `${t("ecommercePage.MultiplePromotions")}`
    },
    {
      icon: UnlimitedTransactions,
      name: `${t("ecommercePage.UnlimitedTransactions")}`
    },
    {
      icon: Reviews,
      name: `${t("ecommercePage.Reviews")}`
    }

  ];

  return Applications === null || loading ? (
    <Fragment>
      <Spin></Spin>
    </Fragment>
  ) : (
      <div>
        <ScrollTop />
        <Helmet>
          <title>UPapp factory - Buy Ready Made Apps</title>
          <meta
            name="Readymade eCommerce Mobile Apps for Android and iOS Platforms | UPapp factory"
            content="Build native mobile ecommerce app (android & iOS) for your eCommerce store. Readymade ecommerce app on all platforms. we help you by developing a feature-rich mobile app solution depends on your business requirements."
          />
        </Helmet>
        <section
          className="UAF-container-fluid"
          style={{
            background: "#fff",
            padding: "40px 15px",
            maxWidth: "1200px",
            margin: "100px auto 180px auto"
          }}
        >
          <Row type="flex">
            <Col md={{ span: 12, order: 1 }} lg={{ span: 12, order: 1 }} xs={{ span: 24, order: 2 }}>
              {direction === DIRECTIONS.LTR && <Fragment>
                <div className="tagline-heading lg-ltr" style={{ padding: "auto 15px", }}>
                  <Title level={1} className="texty-demo" style={{}}>
                    {t("ecommercePage.readytolaunch")}
                    <br /> {t("ecommercePage.e-CommerceBusiness")}
                  </Title>
                  <Title
                    level={1}
                    className="text-demo-sub-text"
                    style={{
                      fontSize: "18px",
                      lineHeight: "20px",
                      maxWidth: "480px",
                      fontWeight: "400"
                    }}
                  >
                    {t("ecommercePage.subtagline")}
                  </Title>

                  {/* <Link to="/factory">
                    <Button size={"large"} className="slider-get-started-btn uaf_allPageBtn_bg_Color">
                      {t("General.GetStarted")}
                    </Button>
                  </Link> */}

                  <a href="#plan_single_vendor">
                    <Button size={"large"} className="slider-get-started-btn uaf_allPageBtn_bg_Color">
                      {t("General.GetStarted")}
                    </Button>
                  </a>
                </div>
              </Fragment>
              }

              {direction === DIRECTIONS.RTL && <Fragment>
                <div className="tagline-heading" style={{ padding: "auto 15px" }}>
                  <Title level={1} className="texty-demo rtl" style={{}}>
                    {t("ecommercePage.readytolaunch")}
                    <br /> {t("ecommercePage.e-CommerceBusiness")}
                  </Title>
                  <Title
                    level={1}
                    className="text-demo-sub-text rtl"
                    style={{
                      fontSize: "18px",
                      lineHeight: "20px",
                      maxWidth: "480px",
                      fontWeight: "400"
                    }}
                  >
                    {t("ecommercePage.subtagline")}
                  </Title>

                  {/* <Link to="/factory">
                    <Button size={"large"} className="slider-get-started-btn float-right uaf_allPageBtn_bg_Color">
                      {t("General.GetStarted")}
                    </Button>
                  </Link> */}
                  <a href="#plan_single_vendor">
                    <Button size={"large"} className="slider-get-started-btn uaf_allPageBtn_bg_Color">
                      {t("General.GetStarted")}
                    </Button>
                  </a>
                </div>
              </Fragment>
              }


            </Col>
            <Col md={{ span: 12, order: 2 }} lg={{ span: 12, order: 2 }} xs={{ span: 24, order: 1 }}>
              <div className="home-feature-img main">
                <img
                  style={{ maxWidth: "200px" }}
                  src={OneStopEcommerceSolution}
                  alt="image with animation for upapp"
                />
              </div>
            </Col>
          </Row>
        </section>
        <section
          className="UAF-container-fluid"
          style={{
            background: "#fff",
            padding: 0,
            maxWidth: "1200px",
            margin: "0 auto 80px auto",
            textAlign: "center"
          }}
        >
          <p style={{ fontSize: "45px", marginBottom: "0px" }}>{t("ecommercePage.KeyFeatures")}</p>

          <Row type="flex" justify="center">
            {KeyBenifits.map(index => {
              return (
                <Col xs={12} sm={6} lg={4}>
                  <div
                    style={{
                      width: "100%",
                      maxWidth: "180px",
                      height: "130px",
                      margin: "30px auto"
                    }}
                  >
                    <img
                      style={{ width: "100px", height: "100px" }}
                      src={index.icon}
                      alt=""
                    />
                    <p>{index.name}</p>
                  </div>
                </Col>
              );
            })}
          </Row>
        </section>

        {/* <section
        className="UAF-container-fluid"
        style={{
          margin: "0px auto 60px auto"
        }}
      >
        <div
          style={{
            maxWidth: "1200px",
            margin: "0 auto",
            textAlign: "center",
            padding: "25px"
          }}
        >
          <Row type="flex">
            <Col lg={{ span: 12, order: 1 }} xs={{ span: 24, order: 3 }}>
              <div
                className="tagline-heading sub-points"
                style={{ padding: "auto 15px" , marginTop:"-50px"}}
              >
                <Title level={1} className="texty-demo lg-ltr" style={{}}>
                  eCommerce Marketplace
                  <br />
                </Title>
                <Title
                  level={1}
                  className="text-demo-sub-text lg-ltr uaf_Title_Desc1">
                  Multi-Vendor is standalone eCommerce platform that allows you
                  to create an online marketplace. In Multi-Vendor, independent
                  vendors can sell their products through a single storefront.
                </Title>

                <Link
                  to="/factory/booking
              
              "
                >
                  <Button size={"large"} className="slider-get-started-btn lg-float-left uaf_allPageBtn_bg_Color">
                     {t("General.GetStarted")}
                  </Button>
                </Link>
              </div>
            </Col>
            <Col lg={{ span: 12, order: 3 }} xs={{ span: 24, order: 1 }}>
              <div className="home-feature-img">
                <img
                  style={{ maxWidth: "200px" }}
                  src={MultiVendor}
                  alt="image with animation for upapp"
                />
              </div>
            </Col>
          </Row>
        </div>
      </section> */}

        <section
          className="UAF-container-fluid"
          style={{
            background: "#fff",
            padding: 0,
            maxWidth: "1200px",
            margin: "0px auto 80px auto"
          }}
        >
          <Row type="flex">
            <Col md={{ span: 12, order: 2 }} lg={{ span: 12, order: 2 }} xs={{ span: 24, order: 2 }}>
              {direction === DIRECTIONS.LTR && <Fragment>
                <div
                  className="tagline-heading sub-points lg-rtl"
                  style={{ padding: "auto 15px", }}
                >
                  <Title level={1} className="texty-demo lg-rtl" style={{}}>
                    {t("ecommercePage.layer3.title1")} <br /> {t("ecommercePage.layer3.title2")}
                  </Title>
                  <Title
                    level={1}
                    className="text-demo-sub-text main lg-float-right lg-rtl"
                    style={{
                      fontSize: "18px",
                      lineHeight: "20px",
                      fontWeight: "400",
                      maxWidth: "480px"
                    }}
                  >
                    {t("ecommercePage.layer3.desc")}
                  </Title>

                  {/* <Link to="/factory">
                    <Button size={"large"} className="slider-get-started-btn lg-float-right uaf_allPageBtn_bg_Color">
                      {t("General.GetStarted")}
                    </Button>
                  </Link> */}
                  <a href="#plan_single_vendor">
                    <Button size={"large"} className="slider-get-started-btn lg-float-right uaf_allPageBtn_bg_Color">
                      {t("General.GetStarted")}
                    </Button>
                  </a>
                </div>
              </Fragment>
              }
              {direction === DIRECTIONS.RTL && <Fragment>
                <div
                  className="tagline-heading sub-points lg-rtl lg-p-ltr-15"
                  style={{ padding: "auto 15px", }}
                >
                  <Title level={1} className="texty-demo " style={{}}>
                    {t("ecommercePage.layer3.title1")} <br /> {t("ecommercePage.layer3.title2")}
                  </Title>
                  <Title
                    level={1}
                    className="text-demo-sub-text lg-rtl"
                    style={{
                      fontSize: "18px",
                      lineHeight: "20px",
                      fontWeight: "400",
                      maxWidth: "480px"
                    }}
                  >
                    {t("ecommercePage.layer3.desc")}
                  </Title>

                  {/* <Link to="/factory">
                    <Button size={"large"} className="slider-get-started-btn main lg-float-right uaf_allPageBtn_bg_Color">
                      {t("General.GetStarted")}
                    </Button>
                  </Link> */}
                  <a href="#plan_single_vendor">
                    <Button size={"large"} className="slider-get-started-btn lg-float-right uaf_allPageBtn_bg_Color">
                      {t("General.GetStarted")}
                    </Button>
                  </a>
                </div>
              </Fragment>
              }
            </Col>
            <Col md={{ span: 12, order: 1 }} lg={{ span: 12, order: 1 }} xs={{ span: 24, order: 1 }}>
              <div className="home-feature-img main">
                <img
                  style={{ maxWidth: "200px" }}
                  src={SecureAGILE}
                  alt="image with animation for upapp"
                />
              </div>
            </Col>
          </Row>
        </section>

        <section
          className="UAF-container-fluid"
          style={{
            margin: "80px auto 0px auto"
          }}
        >
          <div
            style={{
              maxWidth: "1200px",
              margin: "0 auto",
              textAlign: "center",
              padding: "25px"
            }}
          >
            {/* <p style={{ fontSize: "45px", marginBottom: "0px" }}>
            We offer what you need
          </p> */}

            <Row type="flex">
              <Col md={{ span: 12, order: 1 }} lg={{ span: 12, order: 1 }} xs={{ span: 24, order: 3 }}>
                {direction === DIRECTIONS.LTR && <Fragment>
                  <div
                    className="tagline-heading sub-points"
                    style={{ padding: "auto 15px", }}
                  >
                    <Title level={1} className="texty-demo lg-ltr">
                      {t("ecommercePage.layer4.title1")}
                    </Title>
                    <Title
                      style={{
                        fontSize: "18px",
                        lineHeight: "20px",
                        fontWeight: "400",
                        maxWidth: "480px"
                      }}
                      level={1}
                      className="text-demo-sub-text lg-ltr uaf_Title_Desc1">
                      {t("ecommercePage.layer4.desc")}
                    </Title>

                    {/* <Link to="/factory">
                      <Button size={"large"} className="slider-get-started-btn lg-float-left uaf_allPageBtn_bg_Color">
                        {t("General.GetStarted")}
                      </Button>
                    </Link> */}
                    <a href="#plan_single_vendor">
                      <Button size={"large"} className="slider-get-started-btn lg-float-left uaf_allPageBtn_bg_Color">
                        {t("General.GetStarted")}
                      </Button>
                    </a>
                  </div>
                </Fragment>
                }
                {direction === DIRECTIONS.RTL && <Fragment>
                  <div
                    className="tagline-heading sub-points lg-rtl"
                    style={{ padding: "auto 15px", }}
                  >
                    <Title level={1} className="texty-demo lg-rtl">
                      {t("ecommercePage.layer4.title1")}
                    </Title>
                    <Title
                      level={1}
                      style={{
                        fontSize: "18px",
                        lineHeight: "20px",
                        fontWeight: "400",
                        maxWidth: "480px"
                      }}
                      className="text-demo-sub-text main lg-rtl lg-float-right uaf_Title_Desc1 text-demo-sub-text">
                      {t("ecommercePage.layer4.desc")}
                    </Title>

                    {/* <Link to="/factory">
                      <Button size={"large"} className="slider-get-started-btn lg-float-right uaf_allPageBtn_bg_Color">
                        {t("General.GetStarted")}
                      </Button>
                    </Link> */}
                    <a href="#plan_single_vendor">
                      <Button size={"large"} className="slider-get-started-btn lg-float-left uaf_allPageBtn_bg_Color">
                        {t("General.GetStarted")}
                      </Button>
                    </a>
                  </div>
                </Fragment>
                }
              </Col>
              <Col md={{ span: 12, order: 3 }} lg={{ span: 12, order: 3 }} xs={{ span: 24, order: 1 }}>
                <div className="home-feature-img">
                  <img
                    style={{ maxWidth: "200px" }}
                    src={AndroidIosAppIncluded}
                    alt="image with animation for upapp"
                  />
                </div>
              </Col>
            </Row>
          </div>
        </section>

        <section
          className="UAF-container-fluid mainSection_Outer_Counter">
          <Row type="flex">
            <Col md={{ span: 12, order: 2 }} lg={{ span: 12, order: 2 }} xs={{ span: 24, order: 2 }}>
              {direction === DIRECTIONS.LTR && <Fragment>
                <div
                  className="tagline-heading sub-points lg-rtl"
                  style={{ padding: "auto 15px", }}
                >
                  <Title level={1} className="texty-demo lg-rtl" style={{}}>
                    {t("ecommercePage.layer5.title1")} <br />  {t("ecommercePage.layer5.title2")}
                  </Title>
                  <Title
                    level={1}
                    className="text-demo-sub-text main lg-rtl lg-float-right"
                    style={{
                      fontSize: "18px",
                      lineHeight: "20px",
                      fontWeight: "400",
                      maxWidth: "480px"
                    }}
                  >
                    {t("ecommercePage.layer5.desc")}
                  </Title>
                  {/* <Link to="/factory">
                    <Button size={"large"} className="slider-get-started-btn lg-float-right uaf_allPageBtn_bg_Color">
                      {t("General.GetStarted")}
                    </Button>
                  </Link> */}
                  <a href="#plan_single_vendor">
                    <Button size={"large"} className="slider-get-started-btn lg-float-right uaf_allPageBtn_bg_Color">
                      {t("General.GetStarted")}
                    </Button>
                  </a>
                </div>
              </Fragment>
              }
              {direction === DIRECTIONS.RTL && <Fragment>
                <div
                  className="tagline-heading sub-points lg-rtl lg-p-rtl-15"
                  style={{ padding: "auto 15px", }}
                >
                  <Title level={1} className="texty-demo" style={{}}>
                    {t("ecommercePage.layer5.title1")} <br />  {t("ecommercePage.layer5.title2")}
                  </Title>
                  <Title
                    level={1}
                    className="text-demo-sub-text"
                    style={{
                      fontSize: "18px",
                      lineHeight: "20px",
                      fontWeight: "400",
                      maxWidth: "480px"
                    }}
                  >
                    {t("ecommercePage.layer5.desc")}
                  </Title>

                  {/* <Link to="/factory">
                    <Button size={"large"} className="slider-get-started-btn main  lg-float-right uaf_allPageBtn_bg_Color">
                      {t("General.GetStarted")}
                    </Button>
                  </Link> */}
                  <a href="#plan_single_vendor">
                    <Button size={"large"} className="slider-get-started-btn lg-float-right uaf_allPageBtn_bg_Color">
                      {t("General.GetStarted")}
                    </Button>
                  </a>
                </div>
              </Fragment>
              }
            </Col>
            <Col md={{ span: 12, order: 1 }} lg={{ span: 12, order: 1 }} xs={{ span: 24, order: 1 }}>
              <div className="home-feature-img main">
                <img
                  style={{ maxWidth: "200px" }}
                  src={ResponsiveWebsite}
                  alt="image with animation for upapp"
                />
              </div>
            </Col>
          </Row>
        </section>

        <section

          style={{
            background: "#fff",
            padding: 0,
            textAlign: "center",
            margin: "150px auto 0px auto"
          }}
        >
          <div
            style={{
              margin: "55px auto"
            }}
            className="single-app-heading-text"
          >
            {t("ecommercePage.ChhoseDemo")}
          </div>
          <WebsiteThemesTemp />
        </section>

        <section
          className="UAF-container-fluid"
          style={{
            background: "#fff",
            padding: 0,
            maxWidth: "1200px",
            textAlign: "center",
            margin: "150px auto 50px auto"
          }}
        >
          <MobileAppView />
        </section>


        <section className="UAF-container-fluid" id="plan_single_vendor" style={{ marginBottom: "6rem" }}>
          <PackagePriceVerticleEcommerce
            currency={currency}
            MBDSPM={newMobileBDSPM}
            MADSPM={newMobileADSPM}
            MBDSPY={newMobileBDSPY}
            MADSPY={newMobileADSPY}
            WBDSPM={newWebsiteBDSPM}
            WADSPM={newWebsiteADSPM}
            WBDSPY={newWebsiteBDSPY}
            WADSPY={newWebsiteADSPY}
            WABDSPM={newWebAppBDSPM}
            WAADSPM={newWebAppADSPM}
            WABDSPY={newWebAppBDSPY}
            WAADSPY={newWebAppADSPY}
            WFPP={WFPP}
            MFPP={MFPP}
            WAFPP={WAFPP}
            duration="Mo"
            SLUG={filteredApps && filteredApps.fields && filteredApps.fields.slug}
            ID={filteredApps && filteredApps.fields && filteredApps.UID}
            PackType="Annually"
            WSC={WSC}
            WMSC={WMSC}
            MSC={MSC}
            USER={user}
          />
        </section>
        {/* <section
          className="UAF-container-fluid"
          style={{
            background: "#fff",
            padding: 0,
            maxWidth: "1200px",
            textAlign: "center",
            margin: "100px auto 50px auto"
          }}
        >
          <div
            style={{
              textAlign: "center"
            }}
          >
            <p
              style={{
                fontSize: "45px",
                fontWeight: "600",
                textAlign: "center",
                marginBottom: "0px"
              }}
            >
              {t("ecommercePage.launchYourMarket")}
            </p>
            <Link to="/factory">
              <Button size={"large"} className="slider-get-started-btn uaf_allPageBtn_bg_Color">
                {t("General.GetStarted")}
              </Button>
            </Link>
          </div>
        </section> */}
        {/* <ContactUsForm /> */}
      </div>
    );
};

const HomeRFC = Form.create({ name: "Request_for_customize" })(HomeNew);

HomeRFC.propTypes = {
  loadApplication: PropTypes.func.isRequired,
  user: PropTypes.object.isRequired,
  requestForCustomisedApp: PropTypes.func.isRequired,
  Applications: PropTypes.array.isRequired,
  loading: PropTypes.bool,
  direction: withDirectionPropTypes.direction,
};

const mapStateToProps = state => ({
  user: state.auth.user,
  Applications: state.application.Applications,
  loading: state.application.loading
});

export default connect(mapStateToProps, {
  loadApplication,
  requestForCustomisedApp,
  setAlert
})(withDirection(HomeRFC));
