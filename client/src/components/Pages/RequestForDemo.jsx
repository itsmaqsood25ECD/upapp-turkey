/* eslint react/prop-types: 0 */
import React, { useEffect, useState, Fragment } from "react";
import PropTypes from "prop-types";
import { CountryDropdown } from "react-country-region-selector";
import PhoneInput from "react-phone-number-input";
import "react-phone-number-input/style.css";
import SmartInput from "react-phone-number-input/smart-input";
import { connect } from "react-redux";
import moment from "moment";
import {
  Icon,
  Button,
  Typography,
  Modal,
  Input,
  Row,
  Col,
  Form,
  Select,
  DatePicker,
  Menu,
  Anchor
} from "antd";
import { requestDemo } from "../../actions/contact.action";
import { loadApplication } from "../../actions/application.action";

import { useTranslation} from 'react-i18next';
import i18next from 'i18next';

const { Link } = Anchor;
const { Text } = Typography;
const { Option, OptGroup } = Select;
const dateFormatList = ["DD/MM/YYYY", "DD/MM/YY"];

const RequestForDemo = ({
  requestDemo,
  form: { getFieldDecorator, setFieldsValue, validateFieldsAndScroll },
  Applications,
}, props) => {

  const { t } = useTranslation();

  const [appDetails, setAppData] = useState({
    apps: [],
    selectApplication: ""
  });

  useEffect(() => {
    if (Applications) {
      setAppData({
        ...appDetails,
        apps:
          Applications &&
          Applications.map(arg => {
            return arg;
          })
      });
    }
  }, [Applications]);
  const { apps } = appDetails;

  const handleSelect = value => {
    setFormData({ ...formData, interested_in: value });
  };

  const handleSelectCsize = value => {
    setFormData({ ...formData, csize: value });
  };

  const ecommApps =
    apps &&
    apps.filter(myapp => {
      return myapp.fields && myapp.fields.type.toLowerCase() === "ecommerce";
    });

  const bookingApps =
    apps &&
    apps.filter(myapp => {
      return myapp.fields && myapp.fields.type.toLowerCase() === "booking";
    });

  //  Request For Demo

  const selectCountry = val => {
    setFormData({
      ...formData,
      country: val
    });
  };

  const [modal, setModal] = useState({
    visible: false
  });

  const [formData, setFormData] = useState({
    type: "demo"
  });
  const demoRequest = () => {
    showModal();
  };

  const showModal = () => {
    setModal({
      visible: true
    });
  };

  const handleOk = e => {
    validateFieldsAndScroll((err, values) => {
      if (!err) {
        setModal({
          visible: false
        });
        console.log(formData);
        requestDemo(formData);
        Modal.success({
          title: "Demo Request Successful",
          content:
            "Your request to have a demo with UP app factory’s demo team is now received and our pre-sales consultant will be in touch with you soon."
        });
      }
    });
  };

  const handleCancel = e => {
    setModal({
      visible: false
    });
  };

  const onPhoneChange = e => {
    setFormData({
      ...formData,
      mobile: e
    });
  };
  const onChange = e =>
    setFormData({ ...formData, [e.target.name]: e.target.value });

  const onDateChange = e => {
    setFormData({ ...formData, prefered_date: e._d });
  };

  const disabledDate = current => {
    // Can not select days before today and today
    return current && current < moment().endOf("day");
  };

  // Request for Demo

  return (
    <Fragment>
      <Anchor style={{padding:"0px"}}>
        <Link
          href="/contactUs#GetInTouchForm"
          style={{padding:"0px"}}
          title={
            <Button
              type="primary"
              className="D-requestForDemoButton"
              size="large"
            >
              {t('header.requestfordemo')}
            </Button>
          }
        ></Link>
      </Anchor>
      <Menu
        mode="horizontal"
        theme="light"
        style={{ float: "right", width: "80px" }}
        className="m-request-for-demo-menu"
      >
        <Menu.Item
          className="M-requestForDemoButton"
          style={{ padding: "0px", float: "right", marginTop: "8px" }}
          key="demo1"
        >
          {/* <Icon type="tablet" style={{fontSize:"30px"}} className="m-menu-icon" />
						<Ionicons.IoMdCrop className="m-menu-icon" />
						<div className="m-menu-title">Request for Demo</div> */}
          <Anchor style={{padding:"0px"}}>
            <Link
              href="/contactUs"
              style={{padding:"0px"}}
              title={
                <Button
                  type="primary"
                  size="large"
                  style={{ padding: "5px", margin: "0px 5px" }}
                  ghost
                >
                 {t('header.demo')}
                </Button>
              }
            ></Link>
          </Anchor>
        </Menu.Item>
      </Menu>
      <Modal
        centered={true}
        title="Request Demo"
        visible={modal.visible}
        onOk={handleOk}
        onCancel={handleCancel}
      >
        <Form>
          <Row gutter={16}>
            <Col xs={24} lg={24}>
              <Form.Item>
                {getFieldDecorator("name", {
                  rules: [
                    {
                      required: true,
                      message: "Please type your name"
                    }
                  ]
                })(
                  <Input
                    placeholder="Name"
                    name="name"
                    onChange={e => onChange(e)}
                    prefix={
                      <Icon type="user" style={{ color: "rgba(0,0,0,.25)" }} />
                    }
                  />
                )}
              </Form.Item>
            </Col>
            <Col xs={24} lg={24}>
              <Form.Item>
                {getFieldDecorator("email", {
                  rules: [
                    {
                      required: true,
                      message: "Please type your email"
                    }
                  ]
                })(
                  <Input
                    placeholder="Email"
                    name="email"
                    onChange={e => onChange(e)}
                    prefix={
                      <Icon type="mail" style={{ color: "rgba(0,0,0,.25)" }} />
                    }
                  />
                )}
              </Form.Item>
            </Col>
            {/* <Col xs={24} lg={12}>
						<Form.Item>
							{getFieldDecorator('company', {
								rules: [
									{
										required: true,
										message: 'Please type your comapany name'
									}
								]
							})(
								<Input
									placeholder="Company"
									name="company"
									onChange={e => onChange(e)}
									prefix={
										<Icon type="bank" style={{ color: 'rgba(0,0,0,.25)' }} />
									}
								/>
							)}
						</Form.Item>
					</Col>
					<Col xs={24} lg={12}>
						<Form.Item>
							{getFieldDecorator('designation', {
								rules: [
									{
										required: true,
										message: 'Please type your designation'
									}
								]
							})(
								<Input
									placeholder="Designation"
									name="designation"
									onChange={e => onChange(e)}
									prefix={
										<Icon
											type="laptop"
											style={{ color: 'rgba(0,0,0,.25)' }}
										/>
									}
								/>
							)}
						</Form.Item>
					</Col>

					<Col xs={24} lg={12}>
						<Form.Item>
							{getFieldDecorator('csize', {
								rules: [
									{
										required: true,
										message: 'Please type your company size'
									}
								]
							})(
								<Select
									placeholder="Company Size"
									style={{ marginTop: '10px' }}
									onChange={handleSelectCsize}
								>
									<Option key="CompanySize" value="Company Size">
										Company Size
										</Option>
									<Option key="1to10" value="1 - 10 Employees">
										1 - 10 Employees
										</Option>
									<Option key="10to50" value="10 - 50 Employees">
										10 - 50 Employees
										</Option>
									<Option key="50to100" value="50 - 100 Employees">
										50 - 100 Employees
										</Option>
									<Option key="100more" value="100+ Employees">
										100+ Employees
										</Option>
								</Select>
							)}
						</Form.Item>
					</Col> */}
            <Col xs={24} lg={24}>
              <Form.Item>
                {getFieldDecorator("country", {
                  rules: [
                    {
                      required: true,
                      message: "Please type your country"
                    }
                  ]
                })(
                  <CountryDropdown
                    blacklist={[
                      "AF",
                      "AO",
                      "DJ",
                      "GQ",
                      "ER",
                      "GA",
                      "IR",
                      "KG",
                      "LY",
                      "MD",
                      "NP",
                      "NG",
                      "ST",
                      "SL",
                      "SD",
                      "SY",
                      "SR",
                      "TM",
                      "VE",
                      "ZW",
                      "IL"
                    ]}
                    name="country"
                    valueType="short"
                    style={{ marginTop: "12px" }}
                    onChange={val => selectCountry(val)}
                  />
                )}
              </Form.Item>
            </Col>
            <Col xs={24} lg={24}>
              <Form.Item>
                {getFieldDecorator("mobile", {
                  rules: [
                    {
                      required: true,
                      message: "Please input your mobile number!"
                    }
                  ]
                })(
                  <PhoneInput
                    style={{ marginTop: "10px" }}
                    name="mobile"
                    inputComponent={SmartInput}
                    placeholder="Enter phone number"
                    onChange={e => onPhoneChange(e)}
                  />
                )}
              </Form.Item>
            </Col>
            <Col xs={24} lg={24}>
              <Form.Item hasFeedback>
                {getFieldDecorator("interested_in", {
                  rules: [
                    {
                      required: true,
                      message: "Please select the app you are interested in "
                    }
                  ]
                })(
                  <Select
                    placeholder="Choose the Category you are intrested in"
                    style={{ marginTop: "10px" }}
                    onChange={handleSelect}
                  >
                    <Option key="ecommerce" value="eCommerce App">
                      eCommerce Application
                    </Option>
                    <Option key="booking" value="Booking App">
                      Booking Application
                    </Option>
                  </Select>
                )}
              </Form.Item>
            </Col>
            {/* <Col xs={24}>
							<div style={{ marginBottom: '10px' }}>Prefered Date</div>
							<Form.Item>
								{getFieldDecorator('prefered_date', {
									rules: [
										{
											required: true,
											message: 'Please choose your prefered date for demo'
										}
									]
								})(
									<DatePicker
										showTime
										onChange={e => onDateChange(e)}
										format={dateFormatList}
										disabledDate={disabledDate}
										name="prefered_date"
										style={{ width: '100%' }}
									/>
								)}
							</Form.Item>
						</Col>*/}
          </Row>
        </Form>
      </Modal>
    </Fragment>
  );
};

const RFD = Form.create({ name: "Request_Demo" })(RequestForDemo);
RFD.propTypes = {
  requestDemo: PropTypes.func.isRequired,
  loadApplication: PropTypes.func.isRequired,
  Applications: PropTypes.array.isRequired,
  loading: PropTypes.bool
};

const mapStateToProps = state => ({
  Applications: state.application.Applications,
  loading: state.application.loading
});

const mapDispatchToProps = {};

export default connect(mapStateToProps, { requestDemo, loadApplication })(RFD);
