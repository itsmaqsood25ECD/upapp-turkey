/* eslint react/prop-types: 0 */
import React, { useState, useEffect, Fragment } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import {
  Button,
  Typography,
  Row,
  Col,
  Icon,
  Carousel,
  List,
  Spin,
  Input,
  Modal,
  Form,
  Tabs
} from "antd";
import "rc-banner-anim/assets/index.css";
import ScrollTop from "../ScrollTop";
import ReactGA from "react-ga";
import ReCAPTCHA from "react-google-recaptcha";
import { CountryDropdown } from "react-country-region-selector";
import PhoneInput from "react-phone-number-input";
import SmartInput from "react-phone-number-input/smart-input";
import { Link } from "react-router-dom";
import "antd/dist/antd.css";
import "../../assets/css/custom.css";
import "../../assets/css/home.css";
import "../../assets/css/homeNew.css";
import "react-animated-slider/build/horizontal.css";

import OneStopEcommerceSolution from "../../assets/img/eCommerce Page/OneStopEcommerceSolution.svg";
import CustomizedApps from "../../assets/img/Home Page images/CustomizedApps.svg";
import AndroidIosAppIncluded from "../../assets/img/eCommerce Page/Android&IosAppIncluded.svg";
import SecureAGILE from "../../assets/img/eCommerce Page/SecureAGILE.svg";
import MultiVendor from "../../assets/img/eCommerce Page/MultiVendor.svg";
import ResponsiveWebsite from "../../assets/img/eCommerce Page/ResponsiveWebsite.svg";

// Benifits

// Key Benifits
import ReadyToLaunch from "../../assets/img/eCommerce Page/KeyBenifits/ReadyToLaunch.svg";
import NotificationsSMSEmail from "../../assets/img/eCommerce Page/KeyBenifits/NotificationsSMSEmail.svg";
import SecurityisParamount from "../../assets/img/eCommerce Page/KeyBenifits/SecurityisParamount.svg";
import AcceptPaymentsOnline from "../../assets/img/eCommerce Page/KeyBenifits/AcceptPaymentsOnline.svg";
import IntegrationAPI from "../../assets/img/eCommerce Page/KeyBenifits/Integration&API.svg";
import CustomFeatures from "../../assets/img/eCommerce Page/KeyBenifits/CustomFeatures.svg";
// End Key Benifits

//Business Benifits
import AutomaticAppSystemUpgrade from "../../assets/img/eCommerce Page/Business Benifits/AutomaticAppSystemUpgrade.svg";
import Customization from "../../assets/img/eCommerce Page/Business Benifits/Customization.svg";
import FastestLoadingTime from "../../assets/img/eCommerce Page/Business Benifits/FastestLoadingTime.svg";
import SocialMediaIntegration from "../../assets/img/eCommerce Page/Business Benifits/SocialMediaIntegration.svg";
import FreeAppStoreFeeSpace from "../../assets/img/eCommerce Page/Business Benifits/FreeAppStoreFee&Space.svg";
import ProductsPromotions from "../../assets/img/eCommerce Page/Business Benifits/Products&Promotions.svg";

// Client Benifits

import MultiLanguageApp from "../../assets/img/eCommerce Page/Client Benifits/MultiLanguageApp.svg";
import UnlimitedTransactions from "../../assets/img/eCommerce Page/Client Benifits/UnlimitedTransactions.svg";
import Reviews from "../../assets/img/eCommerce Page/Client Benifits/Reviews.svg";
import FreeServerSpace from "../../assets/img/eCommerce Page/Client Benifits/FreeServerSpace.svg";

// End of Client Benifits

// End Business Benifits
import MobileAppView from "../MobileAppView/MobileAppPage";
import PackagePriceVerticleEcommerce from "./PackagePriceVerticleEcommerce";
import { store } from "../../store/store";
import { requestForCustomisedApp } from "../../actions/contact.action";
import { loadApplication } from "../../actions/application.action";
import { setAlert } from "../../actions/alert.action.jsx";
import { Helmet } from "react-helmet";
import IndustriesWeServe from "./IndustriesWeServe";
import HomePageBanners from "./HomePageBanners";

const { TabPane } = Tabs;
const { TextArea } = Input;
const { Title, Text } = Typography;
let filteredApps;
const recaptchaRef = React.createRef();

const BookingLandingPage = ({
  loadApplication,
  Applications,
  loading,
  requestForCustomisedApp,
  setAlert,
  form: { getFieldDecorator, validateFields, setFieldsValue }
}) => {
  const [appDetails, setAppData] = useState({
    apps: [],
    selectApplication: "",
    currency: "",
    // Booking
    bookingMobileBDSPM: "",
    bookingMobileADSPM: "",
    bookingMobileBDSPY: "",
    bookingMobileADSPY: "",
    bookingWebsiteBDSPM: "",
    bookingWebsiteADSPM: "",
    bookingWebsiteBDSPY: "",
    bookingWebsiteADSPY: "",
    bookingWebAppBDSPM: "",
    bookingWebAppADSPM: "",
    bookingWebAppBDSPY: "",
    bookingWebAppADSPY: "",
    // EcommcercenewBDSPM: '',
    newADSPM: "",
    newBDSPY: "",
    newADSPY: "",
    newMobileBDSPM: "",
    newMobileADSPM: "",
    newMobileBDSPY: "",
    newMobileADSPY: "",
    newWebsiteBDSPM: "",
    newWebsiteADSPM: "",
    newWebsiteBDSPY: "",
    newWebsiteADSPY: "",
    newWebAppBDSPM: "",
    newWebAppADSPM: "",
    newWebAppBDSPY: "",
    newWebAppADSPY: "",
    country: "",

    // Plan B
    WFPP: "",
    WAFPP: "",
    MFPP: ""
  });
  const [formData, setFormData] = useState({});

  const [state, setState] = useState({
    visible: false,
    modalVisible: false,
    userlocation: "",
    HumanVarification: false
  });
  const { HumanVarification } = state;

  const {
    apps,
    selectApplication,
    country,
    currency,
    newMobileBDSPM,
    newMobileADSPM,
    newMobileBDSPY,
    newMobileADSPY,
    newWebsiteBDSPM,
    newWebsiteADSPM,
    newWebsiteBDSPY,
    newWebsiteADSPY,
    newWebAppBDSPM,
    newWebAppADSPM,
    newWebAppBDSPY,
    newWebAppADSPY,
    WFPP,
    WAFPP,
    MFPP
  } = appDetails;

  let ecommApps, bookingApps;

  useEffect(() => {
    if (sessionStorage.getItem("currency") === "KWD") {
      setAppData({
        ...appDetails,
        apps:
          Applications &&
          Applications.map(arg => {
            return arg;
          }),
        country: sessionStorage.getItem("country"),
        currency: "KWD",
        // Website
        newWebsiteBDSPM: Math.round(9),
        newWebsiteADSPM: Math.round(6),
        newWebsiteBDSPY: Math.round(9),
        newWebsiteADSPY: Math.round(3),
        // Web App
        newWebAppBDSPM: Math.round(15),
        newWebAppADSPM: Math.round(12),
        newWebAppBDSPY: Math.round(15),
        newWebAppADSPY: Math.round(9),
        // Mobile
        newMobileBDSPM: Math.round(12),
        newMobileADSPM: Math.round(9),
        newMobileBDSPY: Math.round(12),
        newMobileADSPY: Math.round(6),
        //  Plan B

        WFPP: Math.round(18.17),
        MFPP: Math.round(27.28),
        WAFPP: Math.round(36.38)
      });
    } else if (sessionStorage.getItem("currency") === "AED") {
      setAppData({
        ...appDetails,
        apps:
          Applications &&
          Applications.map(arg => {
            return arg;
          }),
        country: sessionStorage.getItem("country"),
        currency: "AED",
        // Website
        newWebsiteBDSPM: Math.round(110),
        newWebsiteADSPM: Math.round(73),
        newWebsiteBDSPY: Math.round(110),
        newWebsiteADSPY: Math.round(36),
        // Web App
        newWebAppBDSPM: Math.round(183),
        newWebAppADSPM: Math.round(146),
        newWebAppBDSPY: Math.round(183),
        newWebAppADSPY: Math.round(183),
        // Mobile
        newMobileBDSPM: Math.round(146),
        newMobileADSPM: Math.round(110),
        newMobileBDSPY: Math.round(146),
        newMobileADSPY: Math.round(73),
        //  Plan B

        WFPP: Math.round(220.02),
        MFPP: Math.round(330.22),
        WAFPP: Math.round(440.42)
      });
    } else if (sessionStorage.getItem("currency") === "SGD") {
      setAppData({
        ...appDetails,
        apps:
          Applications &&
          Applications.map(arg => {
            return arg;
          }),
        country: sessionStorage.getItem("country"),
        currency: "SGD",
        // Website
        newWebsiteBDSPM: Math.round(41),
        newWebsiteADSPM: Math.round(27),
        newWebsiteBDSPY: Math.round(41),
        newWebsiteADSPY: Math.round(13),
        // Web App
        newWebAppBDSPM: Math.round(68),
        newWebAppADSPM: Math.round(54),
        newWebAppBDSPY: Math.round(68),
        newWebAppADSPY: Math.round(41),
        // Mobile
        newMobileBDSPM: Math.round(54),
        newMobileADSPM: Math.round(41),
        newMobileBDSPY: Math.round(54),
        newMobileADSPY: Math.round(27),
        //  Plan B

        WFPP: Math.round(80.71),
        MFPP: Math.round(121.13),
        WAFPP: Math.round(161.55)
      });
    } else if (sessionStorage.getItem("currency") === "OMR") {
      setAppData({
        ...appDetails,
        apps:
          Applications &&
          Applications.map(arg => {
            return arg;
          }),
        country: sessionStorage.getItem("country"),
        currency: "OMR",
        // Website
        newWebsiteBDSPM: Math.round(12),
        newWebsiteADSPM: Math.round(8),
        newWebsiteBDSPY: Math.round(12),
        newWebsiteADSPY: Math.round(4),
        // Web App
        newWebAppBDSPM: Math.round(19),
        newWebAppADSPM: Math.round(16),
        newWebAppBDSPY: Math.round(19),
        newWebAppADSPY: Math.round(12),
        // Mobile
        newMobileBDSPM: Math.round(16),
        newMobileADSPM: Math.round(12),
        newMobileBDSPY: Math.round(16),
        newMobileADSPY: Math.round(6),
        //  Plan B

        WFPP: Math.round(23.06),
        MFPP: Math.round(34.61),
        WAFPP: Math.round(46.16)
      });
    } else if (sessionStorage.getItem("currency") === "QAR") {
      setAppData({
        ...appDetails,
        apps:
          Applications &&
          Applications.map(arg => {
            return arg;
          }),
        country: sessionStorage.getItem("country"),
        currency: "QAR",
        // Website
        newWebsiteBDSPM: Math.round(109),
        newWebsiteADSPM: Math.round(72),
        newWebsiteBDSPY: Math.round(109),
        newWebsiteADSPY: Math.round(36),
        // Web App
        newWebAppBDSPM: Math.round(182),
        newWebAppADSPM: Math.round(145),
        newWebAppBDSPY: Math.round(182),
        newWebAppADSPY: Math.round(109),
        // Mobile
        newMobileBDSPM: Math.round(145),
        newMobileADSPM: Math.round(109),
        newMobileBDSPY: Math.round(145),
        newMobileADSPY: Math.round(72),
        //  Plan B

        WFPP: Math.round(218.1),
        MFPP: Math.round(327.33),
        WAFPP: Math.round(436.56)
      });
    } else if (sessionStorage.getItem("currency") === "BHD") {
      setAppData({
        ...appDetails,
        apps:
          Applications &&
          Applications.map(arg => {
            return arg;
          }),
        country: sessionStorage.getItem("country"),
        currency: "BD",
        // Website
        newWebsiteBDSPM: Math.round(11),
        newWebsiteADSPM: Math.round(8),
        newWebsiteBDSPY: Math.round(11),
        newWebsiteADSPY: Math.round(4),
        // Web App
        newWebAppBDSPM: Math.round(19),
        newWebAppADSPM: Math.round(15),
        newWebAppBDSPY: Math.round(19),
        newWebAppADSPY: Math.round(11),
        // Mobile
        newMobileBDSPM: Math.round(15),
        newMobileADSPM: Math.round(11),
        newMobileBDSPY: Math.round(15),
        newMobileADSPY: Math.round(8),
        //  Plan B

        WFPP: Math.round(22.58),
        MFPP: Math.round(33.89),
        WAFPP: Math.round(45.2)
      });
    } else if (sessionStorage.getItem("currency") === "MYR") {
      setAppData({
        ...appDetails,
        apps:
          Applications &&
          Applications.map(arg => {
            return arg;
          }),
        country: sessionStorage.getItem("country"),
        currency: "MYR",
        // Website
        newWebsiteBDSPM: Math.round(123),
        newWebsiteADSPM: Math.round(82),
        newWebsiteBDSPY: Math.round(123),
        newWebsiteADSPY: Math.round(41),
        // Web App
        newWebAppBDSPM: Math.round(206),
        newWebAppADSPM: Math.round(165),
        newWebAppBDSPY: Math.round(206),
        newWebAppADSPY: Math.round(123),
        // Mobile
        newMobileBDSPM: Math.round(165),
        newMobileADSPM: Math.round(123),
        newMobileBDSPY: Math.round(165),
        newMobileADSPY: Math.round(82),
        //  Plan B

        WFPP: Math.round(242.9),
        MFPP: Math.round(364.55),
        WAFPP: Math.round(486.21)
      });
    } else if (sessionStorage.getItem("currency") === "LKR") {
      setAppData({
        ...appDetails,
        apps:
          Applications &&
          Applications.map(arg => {
            return arg;
          }),
        country: sessionStorage.getItem("country"),
        currency: "LKR",
        // Website
        newWebsiteBDSPM: Math.round(5413),
        newWebsiteADSPM: Math.round(3603),
        newWebsiteBDSPY: Math.round(5413),
        newWebsiteADSPY: Math.round(1792),
        // Web App
        newWebAppBDSPM: Math.round(9034),
        newWebAppADSPM: Math.round(7224),
        newWebAppBDSPY: Math.round(9034),
        newWebAppADSPY: Math.round(5413),
        // Mobile
        newMobileBDSPM: Math.round(7224),
        newMobileADSPM: Math.round(5413),
        newMobileBDSPY: Math.round(7224),
        newMobileADSPY: Math.round(3603),
        //  Plan B

        WFPP: Math.round(10856.49),
        MFPP: Math.round(16293.79),
        WAFPP: Math.round(21731.1)
      });
    } else if (sessionStorage.getItem("currency") === "EGP") {
      setAppData({
        ...appDetails,
        apps:
          Applications &&
          Applications.map(arg => {
            return arg;
          }),
        country: sessionStorage.getItem("country"),
        currency: "EGP",
        // Website
        newWebsiteBDSPM: Math.round(482),
        newWebsiteADSPM: Math.round(321),
        newWebsiteBDSPY: Math.round(482),
        newWebsiteADSPY: Math.round(160),
        // Web App
        newWebAppBDSPM: Math.round(804),
        newWebAppADSPM: Math.round(643),
        newWebAppBDSPY: Math.round(804),
        newWebAppADSPY: Math.round(482),
        // Mobile
        newMobileBDSPM: Math.round(643),
        newMobileADSPM: Math.round(482),
        newMobileBDSPY: Math.round(643),
        newMobileADSPY: Math.round(321),
        //  Plan B

        WFPP: Math.round(933.98),
        MFPP: Math.round(1408.88),
        WAFPP: Math.round(1883.78)
      });
    } else if (sessionStorage.getItem("currency") === "SAR") {
      setAppData({
        ...appDetails,
        apps:
          Applications &&
          Applications.map(arg => {
            return arg;
          }),
        country: sessionStorage.getItem("country"),
        currency: "SAR",
        // Website
        newWebsiteBDSPM: Math.round(112),
        newWebsiteADSPM: Math.round(75),
        newWebsiteBDSPY: Math.round(112),
        newWebsiteADSPY: Math.round(37),
        // Web App
        newWebAppBDSPM: Math.round(187),
        newWebAppADSPM: Math.round(150),
        newWebAppBDSPY: Math.round(187),
        newWebAppADSPY: Math.round(112),
        // Mobile
        newMobileBDSPM: Math.round(150),
        newMobileADSPM: Math.round(112),
        newMobileBDSPY: Math.round(150),
        newMobileADSPY: Math.round(75),
        //  Plan B

        WFPP: Math.round(948.12),
        MFPP: Math.round(1422.98),
        WAFPP: Math.round(1897.83)
      });
    } else if (sessionStorage.getItem("currency") === "JOD") {
      setAppData({
        ...appDetails,
        apps:
          Applications &&
          Applications.map(arg => {
            return arg;
          }),
        country: sessionStorage.getItem("country"),
        currency: "JOD",
        // Website
        newWebsiteBDSPM: Math.round(21),
        newWebsiteADSPM: Math.round(14),
        newWebsiteBDSPY: Math.round(21),
        newWebsiteADSPY: Math.round(7),
        // Web App
        newWebAppBDSPM: Math.round(35),
        newWebAppADSPM: Math.round(28),
        newWebAppBDSPY: Math.round(35),
        newWebAppADSPY: Math.round(21),
        // Mobile
        newMobileBDSPM: Math.round(28),
        newMobileADSPM: Math.round(21),
        newMobileBDSPY: Math.round(28),
        newMobileADSPY: Math.round(14),
        //  Plan B

        WFPP: Math.round(42.47),
        MFPP: Math.round(63.74),
        WAFPP: Math.round(85.01)
      });
    } else if (sessionStorage.getItem("currency") === "KYD") {
      const currency_rate = 71;
      setAppData({
        ...appDetails,
        apps:
          Applications &&
          Applications.map(arg => {
            return arg;
          }),
        country: sessionStorage.getItem("country"),
        currency: "KYD",
        // Website
        newWebsiteBDSPM: Math.round(25),
        newWebsiteADSPM: Math.round(17),
        newWebsiteBDSPY: Math.round(25),
        newWebsiteADSPY: Math.round(8),
        // Web App
        newWebAppBDSPM: Math.round(41),
        newWebAppADSPM: Math.round(33),
        newWebAppBDSPY: Math.round(41),
        newWebAppADSPY: Math.round(25),
        // Mobile
        newMobileBDSPM: Math.round(33),
        newMobileADSPM: Math.round(25),
        newMobileBDSPY: Math.round(33),
        newMobileADSPY: Math.round(17),
        //  Plan B

        WFPP: Math.round(49.15),
        MFPP: Math.round(74.14),
        WAFPP: Math.round(99.14)
      });
    } else if (sessionStorage.getItem("currency") === "USD") {
      setAppData({
        ...appDetails,
        apps:
          Applications &&
          Applications.map(arg => {
            return arg;
          }),
        country: sessionStorage.getItem("country"),
        currency: "USD",
        // Website
        newWebsiteBDSPM: 29.9,
        newWebsiteADSPM: 19.9,
        newWebsiteBDSPY: 29.9,
        newWebsiteADSPY: 9.9,
        // Web App
        newWebAppBDSPM: 49.9,
        newWebAppADSPM: 39.9,
        newWebAppBDSPY: 49.9,
        newWebAppADSPY: 29.9,
        // Mobile
        newMobileBDSPM: 39.9,
        newMobileADSPM: 29.9,
        newMobileBDSPY: 39.9,
        newMobileADSPY: 19.9,
        //  Plan B

        WFPP: 59.9,
        MFPP: 89.9,
        WAFPP: 119.9
      });
    } else {
      setAppData({
        ...appDetails,
        apps:
          Applications &&
          Applications.map(arg => {
            return arg;
          }),
        country: sessionStorage.getItem("country"),
        currency: sessionStorage.getItem("currency_sign"),
        // Website
        newWebsiteBDSPM: Math.round(
          29.9 * sessionStorage.getItem("currency_rate")
        ),
        newWebsiteADSPM: Math.round(
          19.9 * sessionStorage.getItem("currency_rate")
        ),
        newWebsiteBDSPY: Math.round(
          29.9 * sessionStorage.getItem("currency_rate")
        ),
        newWebsiteADSPY: Math.round(
          9.9 * sessionStorage.getItem("currency_rate")
        ),
        // Web App
        newWebAppBDSPM: Math.round(
          49.9 * sessionStorage.getItem("currency_rate")
        ),
        newWebAppADSPM: Math.round(
          39.9 * sessionStorage.getItem("currency_rate")
        ),
        newWebAppBDSPY: Math.round(
          49.9 * sessionStorage.getItem("currency_rate")
        ),
        newWebAppADSPY: Math.round(
          29.9 * sessionStorage.getItem("currency_rate")
        ),
        // Mobile
        newMobileBDSPM: Math.round(
          39.9 * sessionStorage.getItem("currency_rate")
        ),
        newMobileADSPM: Math.round(
          29.9 * sessionStorage.getItem("currency_rate")
        ),
        newMobileBDSPY: Math.round(
          39.9 * sessionStorage.getItem("currency_rate")
        ),
        newMobileADSPY: Math.round(
          19.9 * sessionStorage.getItem("currency_rate")
        ),
        //  Plan B

        WFPP: Math.round(59.9 * sessionStorage.getItem("currency_rate")),
        MFPP: Math.round(89.9 * sessionStorage.getItem("currency_rate")),
        WAFPP: Math.round(119.9 * sessionStorage.getItem("currency_rate"))
      });
    }
  }, [Applications, country, currency]);

  ecommApps =
    Applications &&
    Applications.filter(myapp => {
      return myapp.fields && myapp.fields.type.toLowerCase() === "ecommerce";
    });

  bookingApps =
    Applications &&
    Applications.filter(myapp => {
      return myapp.fields && myapp.fields.type.toLowerCase() === "booking";
    });
  console.log(Applications, ecommApps, bookingApps);

  if (selectApplication === "" || selectApplication === undefined) {
    filteredApps = apps && apps[0];
  } else {
    let data =
      apps &&
      apps.filter(myapp => {
        return (
          (myapp.fields &&
            myapp.fields.name
              .toLowerCase()
              .includes(
                selectApplication && selectApplication.toLocaleLowerCase()
              )) ||
          (myapp.fields &&
            myapp.fields.type
              .toLowerCase()
              .includes(
                selectApplication && selectApplication.toLocaleLowerCase()
              ))
        );
      });
    filteredApps = data && data[0];
  }

  const KeyBenifits = [
    {
      icon: ReadyToLaunch,
      name: "Ready To Launch"
    },
    {
      icon: NotificationsSMSEmail,
      name: "Push Notifications"
    },
    {
      icon: SecurityisParamount,
      name: "Highly Secure Platform"
    },
    {
      icon: AcceptPaymentsOnline,
      name: "Accept online Payments"
    },
    {
      icon: IntegrationAPI,
      name: "Integration & API"
    },
    {
      icon: CustomFeatures,
      name: "Custom Features"
    }
  ];

  const BusinessBenifits = [
    {
      icon: AutomaticAppSystemUpgrade,
      name: "Automatic app system upgrade"
    },
    {
      icon: Customization,
      name: "Customization"
    },
    {
      icon: FastestLoadingTime,
      name: "Quick Load Time"
    },
    {
      icon: SocialMediaIntegration,
      name: "Social Media Integration"
    },
    {
      icon: FreeAppStoreFeeSpace,
      name: "Free App Store Fee & Space"
    },
    {
      icon: ProductsPromotions,
      name: "Multiple Pomotions"
    }
  ];

  const ClientBenifits = [
    {
      icon: MultiLanguageApp,
      name: "Multi Language App"
    },
    {
      icon: UnlimitedTransactions,
      name: "Unlimited Transactions"
    },
    {
      icon: Reviews,
      name: "Reviews"
    },
    {
      icon: FreeServerSpace,
      name: "No Additional Server Cost"
    },
    {
      icon: SocialMediaIntegration,
      name: "Social Media Integration"
    },
    {
      icon: ProductsPromotions,
      name: "Multiple Pomotions"
    }
  ];

  return Applications === null || loading ? (
    <Fragment>
      <Spin></Spin>
    </Fragment>
  ) : (
      <div>
        <ScrollTop />
        <Helmet>
          <title>UPapp factory - Buy Ready Made Apps</title>
          <meta
            name="Subscribe your ready made apps for your business. Start your eCommerce business in a minute. Just choose the product which suits best to your work and you are all done."
            content="Buy Ready Made eCommerce App Business Application Work Purchase"
          />
        </Helmet>
        <section
          style={{
            background: "#fff",
            padding: 0,
            maxWidth: "1200px",
            margin: "100px auto 180px auto"
          }}
        >
          <Row type="flex">
            <Col lg={{ span: 12, order: 1 }} xs={{ span: 24, order: 2 }}>
              <div className="tagline-heading" style={{ padding: "auto 15px" }}>
                <Title level={1} className="texty-demo" style={{}}>
                  Ready to launch
                <br /> e-Commerce Business
              </Title>
                <Title
                  level={1}
                  className="text-demo-sub-text"
                  style={{
                    fontSize: "18px",
                    lineHeight: "20px",
                    maxWidth: "480px",
                    fontWeight: "400"
                  }}
                >
                  Launch your eCommerce business with multivendor/single store
                  eCommerce website & mobile app (android and iOS) in 7-12
                  business days – state-of-the-art technology, platform, security,
                  hosting, technical support included.
              </Title>

                <Link to="/factory">
                  <Button size={"large"} className="slider-get-started-btn uaf_allPageBtn_bg_Color">Get Started</Button>
                </Link>
              </div>
            </Col>
            <Col lg={{ span: 12, order: 2 }} xs={{ span: 24, order: 1 }}>
              <div className="home-feature-img main">
                <img
                  style={{ maxWidth: "200px" }}
                  src={OneStopEcommerceSolution}
                  alt="image with animation for upapp"
                />
              </div>
            </Col>
          </Row>
        </section>
        <section
          style={{
            background: "#fff",
            padding: 0,
            maxWidth: "1200px",
            margin: "0 auto 80px auto"
          }}
        >
          <Tabs
            style={{
              margin: "0 auto 50px auto"
            }}
            className="benifits-tabs"
            defaultActiveKey="1"
          >
            <TabPane
              tab={<span className="benifit-tabs-tab">Key Benefits</span>}
              key="1"
            >
              <Row>
                {KeyBenifits.map(index => {
                  return (
                    <Col xs={24} lg={8}>
                      <div style={{ margin: "30px auto" }}>
                        <img
                          style={{ width: "100px", height: "100px" }}
                          src={index.icon}
                          alt=""
                        />
                        <p>{index.name}</p>
                      </div>
                    </Col>
                  );
                })}
              </Row>
            </TabPane>
            <TabPane
              tab={<span className="benifit-tabs-tab">Business Benefits</span>}
              key="2"
            >
              <Row>
                {BusinessBenifits.map(index => {
                  return (
                    <Col xs={24} lg={8}>
                      <div style={{ margin: "30px auto" }}>
                        <img
                          style={{ width: "100px", height: "100px" }}
                          src={index.icon}
                          alt=""
                        />
                        <p style={{ marginTop: "15px" }}>{index.name}</p>
                      </div>
                    </Col>
                  );
                })}
              </Row>
            </TabPane>
            <TabPane
              tab={
                <span className="benifit-tabs-tab">Your Customer Benefits</span>
              }
              key="3"
            >
              <Row>
                {ClientBenifits.map(index => {
                  return (
                    <Col xs={24} lg={8}>
                      <div style={{ margin: "30px auto" }}>
                        <img
                          style={{ width: "100px", height: "100px" }}
                          src={index.icon}
                          alt=""
                        />
                        <p style={{ marginTop: "15px", fontSize: "18px" }}>
                          {index.name}
                        </p>
                      </div>
                    </Col>
                  );
                })}
              </Row>
            </TabPane>
          </Tabs>
        </section>

        <section
          style={{
            margin: "100px auto 60px auto"
          }}
        >
          <div
            style={{
              maxWidth: "1200px",
              margin: "0 auto",
              textAlign: "center",
              padding: "25px"
            }}
          >
            <Row type="flex">
              <Col lg={{ span: 12, order: 1 }} xs={{ span: 24, order: 3 }}>
                <div className="tagline-heading" style={{ padding: "auto 15px" }}>
                  <Title level={1} className="texty-demo lg-ltr" style={{}}>
                    Multi-Vendor Marketplace
                  <br />
                  </Title>
                  <Title
                    level={1}
                    className="text-demo-sub-text lg-ltr uaf_Title_Desc1">
                    Multi-Vendor is standalone eCommerce platform that allows you
                    to create an online marketplace. In Multi-Vendor, independent
                    vendors can sell their products through a single storefront.
                </Title>

                  <Link
                    to="/factory/booking
              
              "
                  >
                    <Button size={"large"} className="slider-get-started-btn lg-float-left uaf_allPageBtn_bg_Color">Get Started </Button>
                  </Link>
                </div>
              </Col>
              <Col lg={{ span: 12, order: 3 }} xs={{ span: 24, order: 1 }}>
                <div className="home-feature-img">
                  <img
                    style={{ maxWidth: "200px" }}
                    src={MultiVendor}
                    alt="image with animation for upapp"
                  />
                </div>
              </Col>
            </Row>
          </div>
        </section>

        <section className="mainSection_Outer_Counter">
          <Row type="flex">
            <Col lg={{ span: 12, order: 2 }} xs={{ span: 24, order: 2 }}>
              <div
                className="tagline-heading lg-rtl"
                style={{ padding: "auto 15px" }}
              >
                <Title level={1} className="texty-demo lg-rtl" style={{}}>
                  Secure, Agile & <br /> Innovative Apps
              </Title>
                <Title
                  level={1}
                  className="text-demo-sub-text main lg-float-right"
                  style={{
                    fontSize: "18px",
                    lineHeight: "20px",
                    fontWeight: "400",
                    maxWidth: "480px"
                  }}
                >
                  We offer highly secure apps with regular technical upgrades that
                  keeps your business stable and up all the time.
              </Title>

                <Link to="/factory">
                  <Button size={"large"} className="slider-get-started-btn uaf_allPageBtn_bg_Color">Get Started</Button>
                </Link>
              </div>
            </Col>
            <Col lg={{ span: 12, order: 1 }} xs={{ span: 24, order: 1 }}>
              <div className="home-feature-img main">
                <img
                  style={{ maxWidth: "200px" }}
                  src={SecureAGILE}
                  alt="image with animation for upapp"
                />
              </div>
            </Col>
          </Row>
        </section>

        <section
          style={{
            margin: "80px auto 0px auto"
          }}
        >
          <div
            style={{
              maxWidth: "1200px",
              margin: "0 auto",
              textAlign: "center",
              padding: "25px"
            }}
          >
            {/* <p style={{ fontSize: "45px", marginBottom: "0px" }}>
            We offer what you need
          </p> */}

            <Row type="flex">
              <Col lg={{ span: 12, order: 1 }} xs={{ span: 24, order: 3 }}>
                <div className="tagline-heading" style={{ padding: "auto 15px" }}>
                  <Title level={1} className="texty-demo lg-ltr" style={{}}>
                    Android & IOS App
                  <br /> Ready
                </Title>
                  <Title
                    level={1}
                    className="text-demo-sub-text lg-ltr uaf_Title_Desc1">
                    Get native mobile apps for Android & iOS, easy to launch in
                    the App stores along with web-app which can be browsed through
                    all famous browsers in both desktop and mobile view
                </Title>

                  <Link to="/factory/booking">
                    <Button size={"large"} className="slider-get-started-btn lg-float-left uaf_allPageBtn_bg_Color">Get Started</Button>
                  </Link>
                </div>
              </Col>
              <Col lg={{ span: 12, order: 3 }} xs={{ span: 24, order: 1 }}>
                <div className="home-feature-img">
                  <img
                    style={{ maxWidth: "200px" }}
                    src={AndroidIosAppIncluded}
                    alt="image with animation for upapp"
                  />
                </div>
              </Col>
            </Row>
          </div>
        </section>

        <section className="mainSection_Outer_Counter">
          <Row type="flex">
            <Col lg={{ span: 12, order: 2 }} xs={{ span: 24, order: 2 }}>
              <div
                className="tagline-heading lg-rtl"
                style={{ padding: "auto 15px" }}
              >
                <Title level={1} className="texty-demo lg-rtl" style={{}}>
                  Fully Responsive & <br /> Modern Website
              </Title>
                <Title
                  level={1}
                  className="text-demo-sub-text main lg-float-right"
                  style={{
                    fontSize: "18px",
                    lineHeight: "20px",
                    fontWeight: "400",
                    maxWidth: "480px"
                  }}
                >
                  Increase your reach to your customer, anytime, anywhere, on any
                  device. we have ready to launch fully responsive e-commerce
                  website for your business.
              </Title>

                <Link to="/factory">
                  <Button size={"large"} className="slider-get-started-btn uaf_allPageBtn_bg_Color">Get Started</Button>
                </Link>
              </div>
            </Col>
            <Col lg={{ span: 12, order: 1 }} xs={{ span: 24, order: 1 }}>
              <div className="home-feature-img main">
                <img
                  style={{ maxWidth: "200px" }}
                  src={ResponsiveWebsite}
                  alt="image with animation for upapp"
                />
              </div>
            </Col>
          </Row>
        </section>

        <section
          style={{
            background: "#fff",
            padding: 0,
            maxWidth: "1200px",
            textAlign: "center",
            margin: "150px auto 50px auto"
          }}
        >
          <MobileAppView />
        </section>

        <section>
          <PackagePriceVerticleEcommerce
            currency={currency}
            MBDSPM={newMobileBDSPM}
            MADSPM={newMobileADSPM}
            MBDSPY={newMobileBDSPY}
            MADSPY={newMobileADSPY}
            WBDSPM={newWebsiteBDSPM}
            WADSPM={newWebsiteADSPM}
            WBDSPY={newWebsiteBDSPY}
            WADSPY={newWebsiteADSPY}
            WABDSPM={newWebAppBDSPM}
            WAADSPM={newWebAppADSPM}
            WABDSPY={newWebAppBDSPY}
            WAADSPY={newWebAppADSPY}
            WFPP={WFPP}
            MFPP={MFPP}
            WAFPP={WAFPP}
            duration="Mo"
            SLUG={filteredApps && filteredApps.fields && filteredApps.fields.slug}
            ID={filteredApps && filteredApps.fields && filteredApps.UID}
            PackType="Annually"
          />
        </section>
        <section
          style={{
            background: "#fff",
            padding: 0,
            maxWidth: "1200px",
            textAlign: "center",
            margin: "100px auto 50px auto"
          }}
        >
          <div
            style={{
              textAlign: "center"
            }}
          >
            <p
              style={{
                fontSize: "45px",
                fontWeight: "600",
                textAlign: "center",
                marginBottom: "0px"
              }}
            >
              Launch Your Marketplace & Apps Now
          </p>
            <Link to="/factory">
              <Button size={"large"} className="slider-get-started-btn uaf_allPageBtn_bg_Color">Get Started</Button>
            </Link>
          </div>
        </section>
      </div>
    );
};

const BookingRFC = Form.create({ name: "Request_for_customize" })(
  BookingLandingPage
);

BookingRFC.propTypes = {
  loadApplication: PropTypes.func.isRequired,
  requestForCustomisedApp: PropTypes.func.isRequired,
  Applications: PropTypes.array.isRequired,
  loading: PropTypes.bool
};

const mapStateToProps = state => ({
  Applications: state.application.Applications,
  loading: state.application.loading
});

export default connect(mapStateToProps, {
  loadApplication,
  requestForCustomisedApp,
  setAlert
})(BookingRFC);
