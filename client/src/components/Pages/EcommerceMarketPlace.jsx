/* eslint react/prop-types: 0 */
import React, { useState, useEffect, Fragment } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { Button, Typography, Row, Col, Spin, Form, Anchor } from "antd";
import "rc-banner-anim/assets/index.css";
import ScrollTop from "../ScrollTop";
import { NavLink } from "react-router-dom";
import "antd/dist/antd.css";
import "../../assets/css/custom.css";
import "../../assets/css/home.css";
import "../../assets/css/marketplace.css";
import "../../assets/css/homeNew.css";
import "react-animated-slider/build/horizontal.css";
import SSsliderFacility from "./single page/SSsliderFacility";
import WebsiteDemo from "./RestaurantAppPages/websitedemo"
import ContactUsForm from './ContactUsForm'


import withDirection, { withDirectionPropTypes, DIRECTIONS } from 'react-with-direction';
import { loadApplication } from "../../actions/application.action";
import { setAlert } from "../../actions/alert.action.jsx";
import { requestForCustomisedApp } from "../../actions/contact.action";
// images
import AndroidIosAppIncluded from "../../assets/img/eCommerce Page/Android&IosAppIncluded.svg";
import { useTranslation } from 'react-i18next';
import MultivendorMarketBuild from "./MultivendorMarketBuild";
// import PackagePriceVerticleRestaurant from "./PackagePriceVerticleRestaurant";
import PackagePriceVerticleMarketPlace from "./PackagePriceVerticleMarketPlace";
import { Helmet } from "react-helmet";
const { Title } = Typography;
let filteredApps;
const { Link } = Anchor;

const RestaurentLandingPage = ({ Applications, loading, direction }) => {

  const { t } = useTranslation();

  const [appDetails, setAppData] = useState({
    apps: [],
    selectApplication: "",
    currency: "",
    // Plan B
    WFPP: "",
    WAFPP: "",
    MFPP: ""
  });

  const {
    country,
    currency,
    MFPP,
  } = appDetails;

  useEffect(() => {
    setAppData({
      ...appDetails,
      country: sessionStorage.getItem("country"),
      currency: sessionStorage.getItem("currency_sign"),
      MFPP: Math.round(4900 * sessionStorage.getItem("currency_rate")),
    });

  }, [Applications, country, currency]);



  const mstore_Screenshot = [
    {
      img: "https://upappfactory-static-images.s3.us-east-2.amazonaws.com/marketplace/home.png",
      name: "Home Page"
    },
    {
      img: "https://upappfactory-static-images.s3.us-east-2.amazonaws.com/marketplace/category.png",
      name: "Categories Page"
    },
    {
      img: "https://upappfactory-static-images.s3.us-east-2.amazonaws.com/marketplace/categorylist.png",
      name: "Categories list View"
    },
    {
      img: "https://upappfactory-static-images.s3.us-east-2.amazonaws.com/marketplace/subcategory.png",
      name: "Sub Categories"
    },
    {
      img: "https://upappfactory-static-images.s3.us-east-2.amazonaws.com/marketplace/search.png",
      name: "Search Page"
    },
    {
      img: "https://upappfactory-static-images.s3.us-east-2.amazonaws.com/marketplace/searchresult.png",
      name: "Search Results"
    },
    {
      img: "https://upappfactory-static-images.s3.us-east-2.amazonaws.com/marketplace/cart.png",
      name: "Cart Page"
    },
    {
      img: "https://upappfactory-static-images.s3.us-east-2.amazonaws.com/marketplace/checkout.png",
      name: "Checkout Page"
    }
  ];

  const keyFeatures = [
    {
      icon: "https://upappfactory-static-images.s3.us-east-2.amazonaws.com/marketplace/microsite.svg",
      title: `${t("MarketPlacePage.keyfunction.Icons.1")}`
    },
    {
      icon: "https://upappfactory-static-images.s3.us-east-2.amazonaws.com/marketplace/catalog.svg",
      title: `${t("MarketPlacePage.keyfunction.Icons.2")}`
    },
    {
      icon: "https://upappfactory-static-images.s3.us-east-2.amazonaws.com/marketplace/rating.svg",
      title: `${t("MarketPlacePage.keyfunction.Icons.3")}`
    },
    {
      icon: "https://upappfactory-static-images.s3.us-east-2.amazonaws.com/marketplace/commission.svg",
      title: `${t("MarketPlacePage.keyfunction.Icons.4")}`
    },
    {
      icon: "https://upappfactory-static-images.s3.us-east-2.amazonaws.com/marketplace/transportation.svg",
      title: `${t("MarketPlacePage.keyfunction.Icons.5")}`
    },
    {
      icon: "https://upappfactory-static-images.s3.us-east-2.amazonaws.com/marketplace/advancedreports.svg",
      title: `${t("MarketPlacePage.keyfunction.Icons.6")}`
    },
    {
      icon: "https://upappfactory-static-images.s3.us-east-2.amazonaws.com/marketplace/multitheme.svg",
      title: `${t("MarketPlacePage.keyfunction.Icons.7")}`
    },
    {
      icon: "https://upappfactory-static-images.s3.us-east-2.amazonaws.com/marketplace/arabic.svg",
      title: `${t("MarketPlacePage.keyfunction.Icons.8")}`
    },
    {
      icon: "https://upappfactory-static-images.s3.us-east-2.amazonaws.com/marketplace/mobileapp.svg",
      title: `${t("MarketPlacePage.keyfunction.Icons.9")}`
    },
    {
      icon: "https://upappfactory-static-images.s3.us-east-2.amazonaws.com/marketplace/support.svg",
      title: `${t("MarketPlacePage.keyfunction.Icons.10")}`
    },
    {
      icon: "https://upappfactory-static-images.s3.us-east-2.amazonaws.com/marketplace/responsive.svg",
      title: `${t("MarketPlacePage.keyfunction.Icons.11")}`
    },
    {
      icon: "https://upappfactory-static-images.s3.us-east-2.amazonaws.com/marketplace/payment.svg",
      title: `${t("MarketPlacePage.keyfunction.Icons.12")}`
    }
  ];



  useEffect(() => {
    if (sessionStorage.getItem("currency") === "USD") {
      setAppData({
        ...appDetails,
        country: sessionStorage.getItem("country"),
        currency: "USD",
        //  Plan B
        MFPP: 4900
      });
    } else {
      setAppData({
        ...appDetails,
        country: sessionStorage.getItem("country"),
        currency: sessionStorage.getItem("currency_sign"),
        //  Plan B
        MFPP: Math.round(4900 * sessionStorage.getItem("currency_rate"))
      });
    }
  }, [country, currency]);


  return (
    <div>
      <ScrollTop />
      <Helmet>
        {/* <title>UPapp factory - eCommerce Multi Vendor Platform</title> */}
        <title>Create eCommerce Marketplace | Best Online Shopping Solution | Upapp factory</title>
        <meta name="description" content="Start your own online marketplace business and earn commission. A Multi-Vendor eCommerce Platform for Sellers to sell their products under One Hub." />
        <meta name="keywords" content="Multivendor Marketplace Website Development, Create eCommerce Marketplace, Online Shopping Marketplace Software" />
      </Helmet>
      <section
        className="UAF-container-fluid marketplace-page-space-adjust1"
        style={{
          background: "#fff",
          padding: 0,
          maxWidth: "1200px",
          margin: "100px auto 180px auto"
        }}
      >
        {direction === DIRECTIONS.RTL &&
          <Fragment>
            <Row type="flex" gutter={50}>
              <Col md={{ span: 12, order: 1 }} lg={{ span: 12, order: 1 }} xs={{ span: 24, order: 2 }}>
                <div className="tagline-heading landingheroarea" style={{ padding: "auto 15px" }}>
                  <Title level={1} className="texty-demo marketplace-page-title-adjust1 rtl" >
                    {t("MarketPlacePage.layer1.title")}
                  </Title>
                  <Title
                    level={1}
                    className="text-demo-sub-text rtl"
                    style={{
                      fontSize: "18px",
                      lineHeight: "20px",
                      fontWeight: 400,
                      maxWidth: "100%"
                    }}
                  >
                    {t("MarketPlacePage.layer1.desc")}
                  </Title>
                  <Anchor>
                    <Link href="/contactUs#GetInTouchForm"
                      title={
                        <Button size={"large"} className="slider-get-started-btn float-right uaf_allPageBtn_bg_Color">
                          {t("General.GetStarted")}
                        </Button>}>
                    </Link>
                  </Anchor>
                </div>
              </Col>
              <Col md={{ span: 12, order: 2 }} lg={{ span: 12, order: 2 }} xs={{ span: 24, order: 1 }}>
                <div className="home-feature-img main marketPlace-page-images-left">
                  <img
                    style={{ marginTop: "-100px", width: "70%" }}
                    src="https://upappfactory-static-images.s3.us-east-2.amazonaws.com/marketplace/landing.png"
                    alt="Restaurant Application"
                  />
                </div>
              </Col>
            </Row>
          </Fragment>
        }
        {direction === DIRECTIONS.LTR &&
          <Fragment>
            <Row type="flex" gutter={50}>
              <Col md={{ span: 12, order: 1 }} lg={{ span: 12, order: 1 }} xs={{ span: 24, order: 2 }}>
                <div className="tagline-heading landingheroarea" style={{ padding: "auto 15px" }}>
                  <Title level={1} className="texty-demo marketplace-page-title-adjust1" >
                    {t("MarketPlacePage.layer1.title")}
                  </Title>
                  <Title
                    level={1}
                    className="text-demo-sub-text"
                    style={{
                      fontSize: "18px",
                      lineHeight: "20px",
                      fontWeight: 400,
                      maxWidth: "100%"
                    }}
                  >
                    {t("MarketPlacePage.layer1.desc")}
                  </Title>
                  <Anchor>
                    <Link href="/contactUs#GetInTouchForm"
                      title={<Button size={"large"} className="slider-get-started-btn uaf_allPageBtn_bg_Color">{t("General.GetStarted")}</Button>}>
                    </Link>
                  </Anchor>
                </div>
              </Col>
              <Col md={{ span: 12, order: 2 }} lg={{ span: 12, order: 2 }} xs={{ span: 24, order: 1 }}>
                <div className="home-feature-img main marketPlace-page-images-right">
                  <img
                    style={{ marginTop: "-100px", width: "70%" }}
                    src="https://upappfactory-static-images.s3.us-east-2.amazonaws.com/marketplace/landing.png"
                    alt="Restaurant Application"
                  />
                </div>
              </Col>
            </Row>
          </Fragment>
        }
      </section>

      {/* ==========================
            Start Services Section
          ========================== */}
      {/* <section className="UAF-container-fluid"
        style={{
          background: "#fff",
          padding: 0,
          maxWidth: "1200px",
          margin: "0 auto 80px auto",
          textAlign: "center"
        }}>
        <p style={{ fontSize: "45px", marginBottom: "30px" }}>Key Value Propositions</p>
        <Row gutter={{ xs: 8, sm: 16, md: 24, lg: 32 }} className="servicesMainBody">
          {KeyServices.map(index => {
            return (
              <Col span={8} className="servicesCard">
                <div style={{
                  width: "100%",
                  margin: "30px auto"
                }}>
                  <img style={{ width: "130px", height: "130px" }} src={index.icon} alt="" />
                  <h4>{index.title}</h4>
                  <p style={{
                    fontSize: "18px",
                    lineHeight: "20px",
                    fontWeight: 400,
                    maxWidth: "100%",
                    textAlign: "left"
                  }}>{index.info}</p>
                </div>
              </Col>
            );
          })}
        </Row>
      </section>
   */}
      {/* ==========================
           End Services Section
      ========================== */}

      {/* ==========================
            Start Functionalities Section
      ========================== */}
      <section
        className="UAF-container-fluid marketplace-page-space-adjust2"
        style={{
          background: "#fff",
          padding: "50px 0",
          paddingBottom: 0,
          maxWidth: "1200px",
          margin: "100px auto 80px auto",
          textAlign: "center"
        }}
      >
        {/* <p style={{ fontSize: "45px", marginBottom: "30px" }}>MARKETPLACE – KEY FUNCTIONALITIES</p> */}
        <p style={{ fontSize: "45px", marginBottom: "30px" }}>{t("MarketPlacePage.keyfunction.keyfunction")}</p>
        <Row type="flex" justify="space-around" align="middle">
          {keyFeatures.map(index => {
            return (
              <Col xs={12} sm={6} lg={4}>
                <div
                  style={{
                    width: "100%",
                    maxWidth: "180px",
                    height: "130px",
                    margin: "30px auto"
                  }}
                >
                  <img
                    style={{ width: "100px", height: "100px", padding: "0.8rem" }}
                    src={index.icon}
                    alt=""
                  />
                  <p>{index.title}</p>
                </div>
              </Col>
            );
          })}
        </Row>
      </section>

      {/* ==========================
           End Functionalitis Section
      ========================== */}



      {/* ==========================
            Start Services Section
          ========================== */}

      <section
        className="UAF-container-fluid marketplace-page-space-adjust3"
        style={{
          margin: "50px auto 50px"
        }}
      >
        <div
          style={{
            maxWidth: "1200px",
            margin: "0 auto",
            textAlign: "center",
            padding: "25px"
          }}
        >
          {direction === DIRECTIONS.RTL && <Fragment>
            <Row type="flex" justify="center" align="top" gutter={30} style={{ alignItems: 'center' }}>
              <Col md={{ span: 12, order: 2 }} lg={{ span: 12, order: 2 }} xs={{ span: 24, order: 2 }}>

                <div
                  className="tagline-heading tagline-body sub-points lg-rtl"
                  style={{ padding: "auto 15px" }}
                >
                  <Title level={1} className="texty-demo lg-rtl" >
                    {t("MarketPlacePage.layer2.title")}
                    <br />
                  </Title>
                  <p
                    className="text-demo-sub-text lg-rtl lg-float-right"
                    style={{
                      fontSize: "18px",
                      lineHeight: "20px",
                      fontWeight: 400,
                      maxWidth: "100%"
                    }}
                  >
                    {t("MarketPlacePage.layer2.desc")}
                  </p>

                  <Anchor>
                    <Link href="/contactUs#GetInTouchForm" title={
                      <Button
                        // type="dashed"
                        size={"large"}
                        style={{
                          background: "#4FBFDD",
                          color: "#fff",
                          marginTop: "0px"
                        }}
                        className="slider-get-started-btn lg-float-right"
                      >
                        {t("General.GetStarted")}
                      </Button>
                    }>

                    </Link>
                  </Anchor>
                </div>

              </Col>
              <Col md={{ span: 12, order: 1 }} lg={{ span: 12, order: 1 }} xs={{ span: 24, order: 1 }}>
                <div className="home-feature-img feature-img-body marketPlace-page-images-right">
                  <img
                    src="https://upappfactory-static-images.s3.us-east-2.amazonaws.com/marketplace/caterglobalaudience.svg"
                    alt="Deliver food faster"
                  />
                </div>
              </Col>
            </Row>
          </Fragment>}
          {direction === DIRECTIONS.LTR && <Fragment>
            <Row type="flex" justify="center" align="top" gutter={30} style={{ alignItems: 'center' }}>
              <Col md={{ span: 12, order: 2 }} lg={{ span: 12, order: 2 }} xs={{ span: 24, order: 2 }}>

                <div
                  className="tagline-heading tagline-body sub-points lg-rtl"
                  style={{ padding: "auto 15px" }}
                >
                  <Title level={1} className="texty-demo lg-rtl" >
                    {t("MarketPlacePage.layer2.title")}
                    <br />
                  </Title>
                  <p
                    className="text-demo-sub-text lg-rtl lg-float-right"
                    style={{
                      fontSize: "18px",
                      lineHeight: "20px",
                      fontWeight: 400,
                      maxWidth: "100%"
                    }}
                  >
                    {t("MarketPlacePage.layer2.desc")}
                  </p>

                  <Anchor>
                    <Link href="/contactUs#GetInTouchForm" title={
                      <Button
                        // type="dashed"
                        size={"large"}
                        style={{
                          background: "#4FBFDD",
                          color: "#fff",
                          marginTop: "0px"
                        }}
                        className="slider-get-started-btn lg-float-right"
                      >
                        {t("General.GetStarted")}
                      </Button>
                    }>

                    </Link>
                  </Anchor>
                </div>

              </Col>
              <Col md={{ span: 12, order: 1 }} lg={{ span: 12, order: 1 }} xs={{ span: 24, order: 1 }}>
                <div className="home-feature-img feature-img-body marketPlace-page-images-left">
                  <img
                    src="https://upappfactory-static-images.s3.us-east-2.amazonaws.com/marketplace/caterglobalaudience.svg"
                    alt="Deliver food faster"
                  />
                </div>
              </Col>
            </Row>
          </Fragment>}
        </div>
      </section>

      <section
        className="UAF-container-fluid marketplace-page-space-adjust3"
        style={{
          margin: "50px auto 50px"
        }}
      >
        <div
          style={{
            maxWidth: "1200px",
            margin: "0 auto",
            textAlign: "center",
            padding: "25px"
          }}
        >
          {direction === DIRECTIONS.LTR && <Fragment>
            <Row type="flex" justify="center" align="top" style={{ alignItems: 'center' }}>
              <Col md={{ span: 12, order: 1 }} lg={{ span: 12, order: 1 }} xs={{ span: 24, order: 2 }}>
                <div
                  className="tagline-heading tagline-body sub-points"
                  style={{ padding: "auto 15px" }}
                >
                  <Title level={1} className="texty-demo lg-ltr" >

                    {t("MarketPlacePage.layer3.title")}
                  </Title>
                  <p
                    className="text-demo-sub-text lg-ltr"
                    style={{
                      fontSize: "18px",
                      lineHeight: "20px",
                      fontWeight: 400,
                      maxWidth: "100%"
                    }}
                  >
                    {t("MarketPlacePage.layer3.desc")}

                  </p>

                  <Anchor>
                    <Link href="/contactUs#GetInTouchForm"
                      title={
                        <Button
                          // type="dashed"
                          size={"large"}
                          style={{
                            background: "#4FBFDD",
                            color: "#fff",
                            marginTop: "20px"
                          }}
                          className="slider-get-started-btn lg-float-left"
                        >
                          {t("General.GetStarted")}
                        </Button>
                      }>

                    </Link>
                  </Anchor>
                </div>
              </Col>
              <Col md={{ span: 12, order: 2 }} lg={{ span: 12, order: 2 }} xs={{ span: 24, order: 1 }}>
                <div className="home-feature-img feature-img-body marketPlace-page-images-right">
                  <img
                    className="marketPlace-page-images"
                    src="https://upappfactory-static-images.s3.us-east-2.amazonaws.com/marketplace/scalability.svg"
                    alt="image with animation for upapp"
                  />
                </div>
              </Col>
            </Row>
          </Fragment>}
          {direction === DIRECTIONS.RTL && <Fragment>
            <Row type="flex" justify="center" align="top" style={{ alignItems: 'center' }}>
              <Col md={{ span: 12, order: 1 }} lg={{ span: 12, order: 1 }} xs={{ span: 24, order: 2 }}>
                <div
                  className="tagline-heading tagline-body sub-points"
                  style={{ padding: "auto 15px" }}
                >
                  <Title level={1} className="texty-demo rtl" >

                    {t("MarketPlacePage.layer3.title")}
                  </Title>
                  <p
                    className="text-demo-sub-text rtl"
                    style={{
                      fontSize: "18px",
                      lineHeight: "20px",
                      fontWeight: 400,
                      maxWidth: "100%"
                    }}
                  >
                    {t("MarketPlacePage.layer3.desc")}

                  </p>

                  <Anchor>
                    <Link href="/contactUs#GetInTouchForm" title={
                      <Button
                        // type="dashed"
                        size={"large"}
                        style={{
                          background: "#4FBFDD",
                          color: "#fff",
                          marginTop: "20px"
                        }}
                        className="slider-get-started-btn lg-float-right "
                      >
                        {t("General.GetStarted")}
                      </Button>
                    }>

                    </Link>
                  </Anchor>
                </div>
              </Col>
              <Col md={{ span: 12, order: 2 }} lg={{ span: 12, order: 2 }} xs={{ span: 24, order: 1 }}>
                <div className="home-feature-img feature-img-body marketPlace-page-images-left">
                  <img
                    className="marketPlace-page-images"
                    src="https://upappfactory-static-images.s3.us-east-2.amazonaws.com/marketplace/scalability.svg"
                    alt="image with animation for upapp"
                  />
                </div>
              </Col>
            </Row>
          </Fragment>}
        </div>
      </section>

      <section
        className="UAF-container-fluid marketplace-page-space-adjust3"
        style={{
          margin: "50px auto 50px"
        }}
      >
        <div
          style={{
            maxWidth: "1200px",
            margin: "0 auto",
            textAlign: "center",
            padding: "25px"
          }}
        >
          {direction === DIRECTIONS.RTL && <Fragment>
            <Row type="flex" justify="center" align="top" gutter={30} style={{ alignItems: 'center' }}>
              <Col md={{ span: 12, order: 2 }} lg={{ span: 12, order: 2 }} xs={{ span: 24, order: 2 }}>
                <div
                  className="tagline-heading tagline-body sub-points lg-rtl"
                  style={{ padding: "auto 15px" }}
                >
                  <Title level={1} className="texty-demo lg-rtl" >
                    {t("MarketPlacePage.layer4.title")}

                    <br />
                  </Title>
                  <p
                    className="text-demo-sub-text lg-rtl lg-float-right"
                    style={{
                      fontSize: "18px",
                      lineHeight: "20px",
                      fontWeight: 400,
                      maxWidth: "100%"
                    }}
                  >{t("MarketPlacePage.layer4.desc")}

                  </p>

                  <Anchor>
                    <Link href="/contactUs#GetInTouchForm" title={
                      <Button
                        // type="dashed"
                        size={"large"}
                        style={{
                          background: "#4FBFDD",
                          color: "#fff",
                          marginTop: "0px"
                        }}
                        className="slider-get-started-btn lg-float-right"
                      >
                        {t("General.GetStarted")}
                      </Button>
                    }>

                    </Link>
                  </Anchor>
                </div>
              </Col>
              <Col md={{ span: 12, order: 1 }} lg={{ span: 12, order: 1 }} xs={{ span: 24, order: 1 }}>
                <div className="home-feature-img feature-img-body marketPlace-page-images-right">
                  <img
                    className="marketPlace-page-images"
                    src="https://upappfactory-static-images.s3.us-east-2.amazonaws.com/marketplace/constantrevenuesystem.svg"
                    alt="Deliver food faster"
                  />
                </div>
              </Col>
            </Row>
          </Fragment>}
          {direction === DIRECTIONS.LTR && <Fragment>
            <Row type="flex" justify="center" align="top" gutter={30} style={{ alignItems: 'center' }}>
              <Col md={{ span: 12, order: 2 }} lg={{ span: 12, order: 2 }} xs={{ span: 24, order: 2 }}>
                <div
                  className="tagline-heading tagline-body sub-points lg-rtl"
                  style={{ padding: "auto 15px" }}
                >
                  <Title level={1} className="texty-demo lg-rtl" >
                    {t("MarketPlacePage.layer4.title")}

                    <br />
                  </Title>
                  <p
                    className="text-demo-sub-text lg-rtl lg-float-right"
                    style={{
                      fontSize: "18px",
                      lineHeight: "20px",
                      fontWeight: 400,
                      maxWidth: "100%"
                    }}
                  >{t("MarketPlacePage.layer4.desc")}

                  </p>

                  <Anchor>
                    <Link href="/contactUs#GetInTouchForm" title={
                      <Button
                        // type="dashed"
                        size={"large"}
                        style={{
                          background: "#4FBFDD",
                          color: "#fff",
                          marginTop: "0px"
                        }}
                        className="slider-get-started-btn lg-float-right"
                      >
                        {t("General.GetStarted")}
                      </Button>
                    }>

                    </Link>
                  </Anchor>
                </div>
              </Col>
              <Col md={{ span: 12, order: 1 }} lg={{ span: 12, order: 1 }} xs={{ span: 24, order: 1 }}>
                <div className="home-feature-img feature-img-body marketPlace-page-images-left">
                  <img
                    className="marketPlace-page-images"
                    src="https://upappfactory-static-images.s3.us-east-2.amazonaws.com/marketplace/constantrevenuesystem.svg"
                    alt="Deliver food faster"
                  />
                </div>
              </Col>
            </Row>
          </Fragment>}
        </div>
      </section>

      <section
        className="UAF-container-fluid marketplace-page-space-adjust3"
        style={{
          margin: "50px auto 50px"
        }}
      >
        <div
          style={{
            maxWidth: "1200px",
            margin: "0 auto",
            textAlign: "center",
            padding: "25px"
          }}
        >
          {direction === DIRECTIONS.RTL && <Fragment>
            <Row type="flex" justify="center" align="top" style={{ alignItems: 'center' }}>
              <Col md={{ span: 12, order: 1 }} lg={{ span: 12, order: 1 }} xs={{ span: 24, order: 2 }}>
                <div
                  className="tagline-heading tagline-body sub-points"
                  style={{ padding: "auto 15px" }}
                >
                  <Title level={1} className="texty-demo rtl" >

                    {t("MarketPlacePage.layer5.title")}
                  </Title>
                  <p
                    className="text-demo-sub-text rtl"
                    style={{
                      fontSize: "18px",
                      lineHeight: "20px",
                      fontWeight: 400,
                      maxWidth: "100%"
                    }}
                  >
                    {t("MarketPlacePage.layer5.desc")}

                  </p>

                  <Anchor>
                    <Link href="/contactUs#GetInTouchForm" title={
                      <Button
                        // type="dashed"
                        size={"large"}
                        style={{
                          background: "#4FBFDD",
                          color: "#fff",
                          marginTop: "20px"
                        }}
                        className="slider-get-started-btn lg-float-right"
                      >
                        {t("General.GetStarted")}
                      </Button>
                    }>

                    </Link>
                  </Anchor>
                </div>
              </Col>
              <Col md={{ span: 12, order: 2 }} lg={{ span: 12, order: 2 }} xs={{ span: 24, order: 1 }}>
                <div className="home-feature-img feature-img-body marketPlace-page-images-left">
                  <img
                    className="marketPlace-page-images"
                    src="https://upappfactory-static-images.s3.us-east-2.amazonaws.com/marketplace/customersatisfaction.svg"
                    alt="image with animation for upapp"
                  />
                </div>
              </Col>
            </Row>
          </Fragment>}
          {direction === DIRECTIONS.LTR && <Fragment>
            <Row type="flex" justify="center" align="top" style={{ alignItems: 'center' }}>
              <Col md={{ span: 12, order: 1 }} lg={{ span: 12, order: 1 }} xs={{ span: 24, order: 2 }}>
                <div
                  className="tagline-heading tagline-body sub-points"
                  style={{ padding: "auto 15px" }}
                >
                  <Title level={1} className="texty-demo lg-ltr" >

                    {t("MarketPlacePage.layer5.title")}
                  </Title>
                  <p
                    className="text-demo-sub-text lg-ltr"
                    style={{
                      fontSize: "18px",
                      lineHeight: "20px",
                      fontWeight: 400,
                      maxWidth: "100%"
                    }}
                  >
                    {t("MarketPlacePage.layer5.desc")}

                  </p>

                  <Anchor>
                    <Link href="/contactUs#GetInTouchForm" title={
                      <Button
                        // type="dashed"
                        size={"large"}
                        style={{
                          background: "#4FBFDD",
                          color: "#fff",
                          marginTop: "20px"
                        }}
                        className="slider-get-started-btn lg-float-left"
                      >
                        {t("General.GetStarted")}
                      </Button>
                    }>

                    </Link>
                  </Anchor>
                </div>
              </Col>
              <Col md={{ span: 12, order: 2 }} lg={{ span: 12, order: 2 }} xs={{ span: 24, order: 1 }}>
                <div className="home-feature-img feature-img-body marketPlace-page-images-right">
                  <img
                    className="marketPlace-page-images"
                    src="https://upappfactory-static-images.s3.us-east-2.amazonaws.com/marketplace/customersatisfaction.svg"
                    alt="image with animation for upapp"
                  />
                </div>
              </Col>
            </Row>
          </Fragment>}
        </div>
      </section>

      <section
        className="UAF-container-fluid marketplace-page-space-adjust3"
        style={{
          margin: "50px auto 50px"
        }}
      >
        <div
          style={{
            maxWidth: "1200px",
            margin: "0 auto",
            textAlign: "center",
            padding: "25px"
          }}
        >{direction === DIRECTIONS.RTL && <Fragment>
          <Row type="flex" justify="center" align="top" gutter={30} style={{ alignItems: 'center' }}>
            <Col md={{ span: 12, order: 2 }} lg={{ span: 12, order: 2 }} xs={{ span: 24, order: 2 }}>
              <div
                className="tagline-heading tagline-body sub-points lg-rtl"
                style={{ padding: "auto 15px" }}
              >
                <Title level={1} className="texty-demo lg-rtl" >

                  {t("MarketPlacePage.layer6.title")}
                  <br />
                </Title>
                <p
                  className="text-demo-sub-text lg-rtl lg-float-right"
                  style={{
                    fontSize: "18px",
                    lineHeight: "20px",
                    fontWeight: 400,
                    maxWidth: "100%"
                  }}
                >
                  {t("MarketPlacePage.layer6.desc")}

                </p>

                <Anchor>
                  <Link href="/contactUs#GetInTouchForm" title={
                    <Button
                      // type="dashed"
                      size={"large"}
                      style={{
                        background: "#4FBFDD",
                        color: "#fff",
                        marginTop: "0px"
                      }}
                      className="slider-get-started-btn lg-float-right"
                    >
                      {t("General.GetStarted")}
                    </Button>
                  }>

                  </Link>
                </Anchor>
              </div>
            </Col>
            <Col md={{ span: 12, order: 1 }} lg={{ span: 12, order: 1 }} xs={{ span: 24, order: 1 }}>
              <div className="home-feature-img feature-img-body marketPlace-page-images-right">
                <img
                  className="marketPlace-page-images"
                  src="https://upappfactory-static-images.s3.us-east-2.amazonaws.com/marketplace/tracksales.svg"
                  alt="Deliver food faster"
                />
              </div>
            </Col>
          </Row>
        </Fragment>}
          {direction === DIRECTIONS.LTR && <Fragment>
            <Row type="flex" justify="center" align="top" gutter={30} style={{ alignItems: 'center' }}>
              <Col md={{ span: 12, order: 2 }} lg={{ span: 12, order: 2 }} xs={{ span: 24, order: 2 }}>
                <div
                  className="tagline-heading tagline-body sub-points lg-rtl"
                  style={{ padding: "auto 15px" }}
                >
                  <Title level={1} className="texty-demo lg-rtl" >

                    {t("MarketPlacePage.layer6.title")}
                    <br />
                  </Title>
                  <p
                    className="text-demo-sub-text lg-rtl lg-float-right"
                    style={{
                      fontSize: "18px",
                      lineHeight: "20px",
                      fontWeight: 400,
                      maxWidth: "100%"
                    }}
                  >
                    {t("MarketPlacePage.layer6.desc")}

                  </p>

                  <Anchor>
                    <Link href="/contactUs#GetInTouchForm" title={
                      <Button
                        // type="dashed"
                        size={"large"}
                        style={{
                          background: "#4FBFDD",
                          color: "#fff",
                          marginTop: "0px"
                        }}
                        className="slider-get-started-btn lg-float-right"
                      >
                        {t("General.GetStarted")}
                      </Button>
                    }>

                    </Link>
                  </Anchor>
                </div>
              </Col>
              <Col md={{ span: 12, order: 1 }} lg={{ span: 12, order: 1 }} xs={{ span: 24, order: 1 }}>
                <div className="home-feature-img feature-img-body marketPlace-page-images-left">
                  <img
                    className="marketPlace-page-images"
                    src="https://upappfactory-static-images.s3.us-east-2.amazonaws.com/marketplace/tracksales.svg"
                    alt="Deliver food faster"
                  />
                </div>
              </Col>
            </Row>
          </Fragment>}
        </div>
      </section>

      <section
        className="UAF-container-fluid marketplace-page-space-adjust3"
        style={{
          margin: "50px auto 50px"
        }}
      >
        <div
          style={{
            maxWidth: "1200px",
            margin: "0 auto",
            textAlign: "center",
            padding: "25px"
          }}
        >
          {direction === DIRECTIONS.RTL && <Fragment>
            <Row type="flex" justify="center" align="top" style={{ alignItems: 'center' }}>
              <Col md={{ span: 12, order: 1 }} lg={{ span: 12, order: 1 }} xs={{ span: 24, order: 2 }}>
                <div
                  className="tagline-heading tagline-body sub-points"
                  style={{ padding: "auto 15px" }}
                >
                  <Title level={1} className="texty-demo rtl" >

                    {t("MarketPlacePage.layer7.title")}
                  </Title>
                  <p
                    className="text-demo-sub-text rtl"
                    style={{
                      fontSize: "18px",
                      lineHeight: "20px",
                      fontWeight: 400,
                      maxWidth: "100%"
                    }}
                  >
                    {t("MarketPlacePage.layer7.desc.1")},<br />
                  • {t("MarketPlacePage.layer7.desc.2")}<br />
                  • {t("MarketPlacePage.layer7.desc.3")}<br />
                  • {t("MarketPlacePage.layer7.desc.4")}<br />
                  • {t("MarketPlacePage.layer7.desc.5")}

                  </p>

                  <Anchor>
                    <Link href="/contactUs#GetInTouchForm" title={
                      <Button
                        // type="dashed"
                        size={"large"}
                        style={{
                          background: "#4FBFDD",
                          color: "#fff",
                          marginTop: "20px"
                        }}
                        className="slider-get-started-btn lg-float-right"
                      >
                        {t("General.GetStarted")}
                      </Button>
                    }>

                    </Link>
                  </Anchor>
                </div>
              </Col>
              <Col md={{ span: 12, order: 2 }} lg={{ span: 12, order: 2 }} xs={{ span: 24, order: 1 }}>
                <div className="home-feature-img feature-img-body marketPlace-page-images-left">
                  <img
                    className="marketPlace-page-images"
                    src="https://upappfactory-static-images.s3.us-east-2.amazonaws.com/marketplace/automatedbusiness.svg"
                    alt="image with animation for upapp"
                  />
                </div>
              </Col>
            </Row>
          </Fragment>}
          {direction === DIRECTIONS.LTR && <Fragment>
            <Row type="flex" justify="center" align="top" style={{ alignItems: 'center' }}>
              <Col md={{ span: 12, order: 1 }} lg={{ span: 12, order: 1 }} xs={{ span: 24, order: 2 }}>
                <div
                  className="tagline-heading tagline-body sub-points"
                  style={{ padding: "auto 15px" }}
                >
                  <Title level={1} className="texty-demo lg-ltr" >

                    {t("MarketPlacePage.layer7.title")}
                  </Title>
                  <p
                    className="text-demo-sub-text lg-ltr"
                    style={{
                      fontSize: "18px",
                      lineHeight: "20px",
                      fontWeight: 400,
                      maxWidth: "100%"
                    }}
                  >
                    {t("MarketPlacePage.layer7.desc.1")},<br />
                  • {t("MarketPlacePage.layer7.desc.2")}<br />
                  • {t("MarketPlacePage.layer7.desc.3")}<br />
                  • {t("MarketPlacePage.layer7.desc.4")}<br />
                  • {t("MarketPlacePage.layer7.desc.5")}

                  </p>

                  <Anchor>
                    <Link href="/contactUs#GetInTouchForm" title={
                      <Button
                        // type="dashed"
                        size={"large"}
                        style={{
                          background: "#4FBFDD",
                          color: "#fff",
                          marginTop: "20px"
                        }}
                        className="slider-get-started-btn lg-float-left"
                      >
                        {t("General.GetStarted")}
                      </Button>
                    }>

                    </Link>
                  </Anchor>
                </div>
              </Col>
              <Col md={{ span: 12, order: 2 }} lg={{ span: 12, order: 2 }} xs={{ span: 24, order: 1 }}>
                <div className="home-feature-img feature-img-body marketPlace-page-images-right">
                  <img
                    className="marketPlace-page-images"
                    src="https://upappfactory-static-images.s3.us-east-2.amazonaws.com/marketplace/automatedbusiness.svg"
                    alt="image with animation for upapp"
                  />
                </div>
              </Col>
            </Row>
          </Fragment>}
        </div>
      </section>



      {/* ==========================
           End Services Section
      ========================== */}



      <section
        className="UAF-container-fluid marketplace-page-space-adjust3"
        style={{
          margin: "50px auto 50px"
        }}
      >
        <div
          style={{
            maxWidth: "1200px",
            margin: "0 auto",
            textAlign: "center",
            padding: "25px"
          }}
        >
          {/* <p
          className="marketplace-page-text-adjust1"
            style={{
              fontSize: "45px",
              marginTop: "0px",
              marginBottom: "80px",
              lineHeight: "1"
            }}
          >
            Multi-Vertical Marketplaces
            <br />
          we have build

          </p> */}
          {direction === DIRECTIONS.RTL && <Fragment>
            <Row type="flex" justify="center" align="top" gutter={30} style={{ alignItems: 'center' }}>
              <Col md={{ span: 12, order: 2 }} lg={{ span: 12, order: 2 }} xs={{ span: 24, order: 2 }}>
                <div
                  className="tagline-heading tagline-body sub-points lg-rtl"
                  style={{ padding: "auto 15px" }}
                >
                  <Title level={1} className="texty-demo lg-rtl" >
                    {t("MarketPlacePage.layer8.title")}
                    <br />
                  </Title>
                  <p
                    className="text-demo-sub-text lg-rtl lg-float-right"
                    style={{
                      fontSize: "18px",
                      lineHeight: "20px",
                      fontWeight: 400,
                      maxWidth: "100%"
                    }}
                  >{t("MarketPlacePage.layer8.desc")}</p>

                  <Anchor>
                    <Link href="/contactUs#GetInTouchForm" title={
                      <Button
                        // type="dashed"
                        size={"large"}
                        style={{
                          background: "#4FBFDD",
                          color: "#fff",
                          marginTop: "0px"
                        }}
                        className="slider-get-started-btn lg-float-right"
                      >
                        {t("General.GetStarted")}
                      </Button>
                    }>

                    </Link>
                  </Anchor>
                </div>
              </Col>
              <Col md={{ span: 12, order: 1 }} lg={{ span: 12, order: 1 }} xs={{ span: 24, order: 1 }}>
                <div className="home-feature-img feature-img-body marketPlace-page-images-right">
                  <img
                    className="marketPlace-page-images"
                    src="https://upappfactory-static-images.s3.us-east-2.amazonaws.com/marketplace/interface.svg"
                    alt="Deliver food faster"
                  />
                </div>
              </Col>
            </Row>
          </Fragment>}
          {direction === DIRECTIONS.LTR && <Fragment>
            <Row type="flex" justify="center" align="top" gutter={30} style={{ alignItems: 'center' }}>
              <Col md={{ span: 12, order: 2 }} lg={{ span: 12, order: 2 }} xs={{ span: 24, order: 2 }}>
                <div
                  className="tagline-heading tagline-body sub-points lg-rtl"
                  style={{ padding: "auto 15px" }}
                >
                  <Title level={1} className="texty-demo lg-rtl" >
                    {t("MarketPlacePage.layer8.title")}
                    <br />
                  </Title>
                  <p
                    className="text-demo-sub-text lg-rtl lg-float-right"
                    style={{
                      fontSize: "18px",
                      lineHeight: "20px",
                      fontWeight: 400,
                      maxWidth: "100%"
                    }}
                  >{t("MarketPlacePage.layer8.desc")}</p>

                  <Anchor>
                    <Link href="/contactUs#GetInTouchForm" title={
                      <Button
                        // type="dashed"
                        size={"large"}
                        style={{
                          background: "#4FBFDD",
                          color: "#fff",
                          marginTop: "0px"
                        }}
                        className="slider-get-started-btn lg-float-right"
                      >
                        {t("General.GetStarted")}
                      </Button>
                    }>

                    </Link>
                  </Anchor>
                </div>
              </Col>
              <Col md={{ span: 12, order: 1 }} lg={{ span: 12, order: 1 }} xs={{ span: 24, order: 1 }}>
                <div className="home-feature-img feature-img-body marketPlace-page-images-left">
                  <img
                    className="marketPlace-page-images"
                    src="https://upappfactory-static-images.s3.us-east-2.amazonaws.com/marketplace/interface.svg"
                    alt="Deliver food faster"
                  />
                </div>
              </Col>
            </Row>
          </Fragment>}
        </div>
      </section>

      <section
        className="UAF-container-fluid marketplace-page-space-adjust3"
        style={{
          margin: "50px auto 50px"
        }}
      >
        <div
          style={{
            maxWidth: "1200px",
            margin: "0 auto",
            textAlign: "center",
            padding: "25px"
          }}
        >
          {direction === DIRECTIONS.RTL && <Fragment>
            <Row type="flex" justify="center" align="top" style={{ alignItems: 'center' }}>
              <Col md={{ span: 12, order: 1 }} lg={{ span: 12, order: 1 }} xs={{ span: 24, order: 2 }}>
                <div
                  className="tagline-heading tagline-body sub-points"
                  style={{ padding: "auto 15px" }}
                >
                  <Title level={1} className="texty-demo rtl" >
                    {t("MarketPlacePage.layer9.title")}

                  </Title>
                  <p
                    className="text-demo-sub-text rtl"
                    style={{
                      fontSize: "18px",
                      lineHeight: "20px",
                      fontWeight: 400,
                      maxWidth: "100%"
                    }}
                  >
                    {t("MarketPlacePage.layer9.desc")}
                  </p>

                  <Anchor>
                    <Link href="/contactUs#GetInTouchForm" title={
                      <Button
                        // type="dashed"
                        size={"large"}
                        style={{
                          background: "#4FBFDD",
                          color: "#fff",
                          marginTop: "20px"
                        }}
                        className="slider-get-started-btn lg-float-right"
                      >
                        {t("General.GetStarted")}
                      </Button>
                    }>

                    </Link>
                  </Anchor>
                </div>
              </Col>
              <Col md={{ span: 12, order: 2 }} lg={{ span: 12, order: 2 }} xs={{ span: 24, order: 1 }}>
                <div className="home-feature-img feature-img-body marketPlace-page-images-left">
                  <img
                    className="marketPlace-page-images"
                    src="https://upappfactory-static-images.s3.us-east-2.amazonaws.com/marketplace/multiplePayment.svg"
                    alt="image with animation for upapp"
                  />
                </div>
              </Col>
            </Row>
          </Fragment>}
          {direction === DIRECTIONS.LTR && <Fragment>
            <Row type="flex" justify="center" align="top" style={{ alignItems: 'center' }}>
              <Col md={{ span: 12, order: 1 }} lg={{ span: 12, order: 1 }} xs={{ span: 24, order: 2 }}>
                <div
                  className="tagline-heading tagline-body sub-points"
                  style={{ padding: "auto 15px" }}
                >
                  <Title level={1} className="texty-demo lg-ltr" >
                    {t("MarketPlacePage.layer9.title")}

                  </Title>
                  <p
                    className="text-demo-sub-text lg-ltr"
                    style={{
                      fontSize: "18px",
                      lineHeight: "20px",
                      fontWeight: 400,
                      maxWidth: "100%"
                    }}
                  >
                    {t("MarketPlacePage.layer9.desc")}
                  </p>

                  <Anchor>
                    <Link href="/contactUs#GetInTouchForm" title={
                      <Button
                        // type="dashed"
                        size={"large"}
                        style={{
                          background: "#4FBFDD",
                          color: "#fff",
                          marginTop: "20px"
                        }}
                        className="slider-get-started-btn lg-float-left"
                      >
                        {t("General.GetStarted")}
                      </Button>
                    }>

                    </Link>
                  </Anchor>
                </div>
              </Col>
              <Col md={{ span: 12, order: 2 }} lg={{ span: 12, order: 2 }} xs={{ span: 24, order: 1 }}>
                <div className="home-feature-img feature-img-body marketPlace-page-images-right">
                  <img
                    className="marketPlace-page-images"
                    src="https://upappfactory-static-images.s3.us-east-2.amazonaws.com/marketplace/multiplePayment.svg"
                    alt="image with animation for upapp"
                  />
                </div>
              </Col>
            </Row>
          </Fragment>}
        </div>
      </section>

      <section
        className="UAF-container-fluid marketplace-page-space-adjust3"
        style={{
          margin: "50px auto 50px"
        }}
      >
        <div
          style={{
            maxWidth: "1200px",
            margin: "0 auto",
            textAlign: "center",
            padding: "25px"
          }}
        >
          {direction === DIRECTIONS.LTR && <Fragment>
            <Row type="flex" justify="center" align="top" gutter={30} style={{ alignItems: 'center' }}>
              <Col md={{ span: 12, order: 2 }} lg={{ span: 12, order: 2 }} xs={{ span: 24, order: 2 }}>
                <div
                  className="tagline-heading tagline-body sub-points lg-rtl"
                  style={{ padding: "auto 15px" }}
                >
                  <Title level={1} className="texty-demo lg-rtl" >
                    {t("MarketPlacePage.layer10.title")}

                    <br />
                  </Title>
                  <p
                    className="text-demo-sub-text lg-rtl lg-float-right"
                    style={{
                      fontSize: "18px",
                      lineHeight: "20px",
                      fontWeight: 400,
                      maxWidth: "100%"
                    }}
                  >
                    {t("MarketPlacePage.layer10.desc")}

                  </p>

                  <Anchor>
                    <Link href="/contactUs#GetInTouchForm" title={
                      <Button
                        // type="dashed"
                        size={"large"}
                        style={{
                          background: "#4FBFDD",
                          color: "#fff",
                          marginTop: "0px"
                        }}
                        className="slider-get-started-btn lg-float-right"
                      >
                        {t("General.GetStarted")}
                      </Button>
                    }>

                    </Link>
                  </Anchor>
                </div>
              </Col>
              <Col md={{ span: 12, order: 1 }} lg={{ span: 12, order: 1 }} xs={{ span: 24, order: 1 }}>
                <div className="home-feature-img feature-img-body marketPlace-page-images-left">
                  <img
                    className="marketPlace-page-images"
                    src="https://upappfactory-static-images.s3.us-east-2.amazonaws.com/marketplace/notification.svg"
                    alt="Deliver food faster"
                  />
                </div>
              </Col>
            </Row>
          </Fragment>}
          {direction === DIRECTIONS.RTL && <Fragment>
            <Row type="flex" justify="center" align="top" gutter={30} style={{ alignItems: 'center' }}>
              <Col md={{ span: 12, order: 2 }} lg={{ span: 12, order: 2 }} xs={{ span: 24, order: 2 }}>
                <div
                  className="tagline-heading tagline-body sub-points lg-rtl"
                  style={{ padding: "auto 15px" }}
                >
                  <Title level={1} className="texty-demo lg-rtl" >
                    {t("MarketPlacePage.layer10.title")}

                    <br />
                  </Title>
                  <p
                    className="text-demo-sub-text lg-rtl lg-float-right"
                    style={{
                      fontSize: "18px",
                      lineHeight: "20px",
                      fontWeight: 400,
                      maxWidth: "100%"
                    }}
                  >
                    {t("MarketPlacePage.layer10.desc")}

                  </p>

                  <Anchor>
                    <Link href="/contactUs#GetInTouchForm" title={
                      <Button
                        // type="dashed"
                        size={"large"}
                        style={{
                          background: "#4FBFDD",
                          color: "#fff",
                          marginTop: "0px"
                        }}
                        className="slider-get-started-btn lg-float-right"
                      >
                        {t("General.GetStarted")}
                      </Button>
                    }>

                    </Link>
                  </Anchor>
                </div>
              </Col>
              <Col md={{ span: 12, order: 1 }} lg={{ span: 12, order: 1 }} xs={{ span: 24, order: 1 }}>
                <div className="home-feature-img feature-img-body marketPlace-page-images-right">
                  <img
                    className="marketPlace-page-images"
                    src="https://upappfactory-static-images.s3.us-east-2.amazonaws.com/marketplace/notification.svg"
                    alt="Deliver food faster"
                  />
                </div>
              </Col>
            </Row>
          </Fragment>}
        </div>
      </section>



      <section
        style={{
          margin: "80px auto 0px auto"
        }}
      >
        <div
          className="UAF-container-fluid"
          style={{
            maxWidth: "1200px",
            margin: "0 auto",
            textAlign: "center",
            padding: "25px"
          }}
        >
          <p style={{ fontSize: "45px", marginBottom: "10px" }}>
            {t('MarketPlacePage.build.multivendormarketBuild')}
          </p>
          {/* <p style={{ fontSize: "18px", marginBottom: "60px" }}>
            {t('Home.industry.androidIos')}
          </p> */}
          <br />
          <MultivendorMarketBuild />
        </div>
      </section>





      {/* <div className="UAF-container-fluid">
        <div className="single-app-heading-text">
          <br />
        How Your Website Will Look To Your Customers
      </div>
        <WebsiteDemo />
      </div> */}
      <div className="single-app-heading-text marketplace-page-text-adjust1">
        <br />
        {t("restaurentApp.yourCutomer")}
      </div>
      <SSsliderFacility screenshots={mstore_Screenshot} />
      <br />

      <section className="" style={{ marginTop: "80px", marginBottom: "80px" }}>
        <PackagePriceVerticleMarketPlace
          currency={currency}
          duration="Mo"
          PackType="Annually"
          MFPP={MFPP}
        />
      </section>
<ContactUsForm />
    </div>
  );
};

const HomeRFC = Form.create({ name: "Request_for_customize" })(
  RestaurentLandingPage
);

HomeRFC.propTypes = {
  loadApplication: PropTypes.func.isRequired,
  requestForCustomisedApp: PropTypes.func.isRequired,
  Applications: PropTypes.array.isRequired,
  loading: PropTypes.bool,
  direction: withDirectionPropTypes.direction,
};

const mapStateToProps = state => ({
  Applications: state.application.Applications,
  loading: state.application.loading
});


export default connect(mapStateToProps, {
  loadApplication,
  requestForCustomisedApp,
  setAlert
})(withDirection(HomeRFC));
// export default connect(mapStateToProps, {})(HomeRFC);
