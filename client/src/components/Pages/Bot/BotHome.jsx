import React, { useState, useEffect, Fragment } from "react";
import { Typography, Row, Col, Icon, Button, Collapse, Anchor } from "antd";
import ScrollTop from "../../ScrollTop";
import { Helmet } from "react-helmet";
import { connect } from "react-redux";
import { useTranslation } from 'react-i18next';
import withDirection, { withDirectionPropTypes, DIRECTIONS } from 'react-with-direction';
import ContactUsForm from "../ContactUsForm";
import i18next from 'i18next';
// import css
import '../../../assets/css/UPappBot.css'
import '../../../assets/css/botPageHoneyCum.css'
import BotHomePricing from '../BotHomePricing'
const { Title, Text } = Typography;
const { Panel } = Collapse;
const { Link } = Anchor;
const UPappbot = ({ direction }) => {
    const { t } = useTranslation();
    return (
        <div style={{ marginTop: "80px" }}>
            <ScrollTop />
            <Helmet>
                <title>Build AI Chatbots for your Business with UPapp | Automate Customer Chats‎</title>
                <meta name="description" content="Automate your customer service with ChatBot and never miss a chance to sell or help your customers. Send updates, provide customer support and more." />
                <meta name="keywords" content="Chatbots Online, Chatbot for business, Chatbot for Website, AI Chatbot, AI Chatbot Builder, Chatbot in UAE, Chatbot in Oman" />
            </Helmet>
            {/* ============================================
                            Start Banner Section
                ============================================ */}
            {direction === DIRECTIONS.LTR &&
                <Fragment>
                    <section className="botHome_bannersection" >
                        <Row type="flex" className="UAF-container-fluid" >
                            <Col sm={{ span: 24, order: 2 }} md={{ span: 15, order: 1 }} lg={{ span: 14, order: 1 }} xs={{ span: 24, order: 2 }} >
                                <div className="uaf_bp_slider_content" >
                                    <h1 className="uaf_bp_slider_content_title">
                                        {t('bot.Intelligentconversationalexperience')}
                                    </h1>
                                    <p className="uaf_bp_slider_content_text">
                                        {t('bot.Indulgeyourusers')}
                                    </p>
                                    <div className="uaf_bp_slider_content_btn_outer">
                                        <Anchor>
                                            <Link href="/contactUs#GetInTouchForm" title={
                                                <Button className="uaf_bp_slider_content_btn">
                                                    {t('bot.getstarted')}
                                                </Button>
                                            } />
                                        </Anchor>
                                    </div>
                                </div>
                            </Col>
                        </Row>
                    </section>
                </Fragment>
            }
            {direction === DIRECTIONS.RTL &&
                <Fragment>
                    <section className="botHome_bannersection2" >
                        <Row type="flex" className="UAF-container-fluid" >
                            <Col sm={{ span: 24, order: 2 }} md={{ span: 15, order: 1 }} lg={{ span: 14, order: 1 }} xs={{ span: 24, order: 2 }} >
                                <div className="uaf_bp_slider_content lg-rtl" >
                                    <h1 className="uaf_bp_slider_content_title">
                                        {t('bot.Intelligentconversationalexperience')}
                                    </h1>
                                    <p className="uaf_bp_slider_content_text">
                                        {t('bot.Indulgeyourusers')}
                                    </p>
                                    <div className="uaf_bp_slider_content_btn_outer">
                                        <Anchor>
                                            <Link href="/contactUs#GetInTouchForm" title={
                                                <Button className="uaf_bp_slider_content_btn">
                                                    {t('bot.getstarted')}
                                                </Button>
                                            } />
                                        </Anchor>
                                    </div>
                                </div>
                            </Col>
                        </Row>
                    </section>
                </Fragment>
            }
            {/* ============================================
                            End Banner Section
                ============================================ */}

            {/* ============================================
                            Start Section1
                ============================================ */}
            {direction === DIRECTIONS.LTR &&
                <Fragment>
                    <section className="botHome_section1 botHome_gradBg">
                        <div className="UAF-container-fluid">
                            <Row type="flex" className="botHome_contentsection_row1">
                                <Col lg={{ span: 10, order: 1 }} md={{ span: 11, order: 1 }} xs={{ span: 24, order: 2 }} className="botHome_contentsection_contentCol1">
                                    <div className="botHome_contentsection_content lg-ltr">
                                        <h1 className="lg-ltr botHome_contentsection_titleleft">
                                            {t('bot.Transformtheway')}
                                        </h1>
                                        <p className="lg-ltr botHome_contentsection_textleft">
                                            {t('bot.UPappBotsareanewway')}
                                        </p>
                                        <Anchor>
                                            <Link href="/contactUs#GetInTouchForm" title={
                                                <Button size={"large"} className="lg-ltr botHome_contentsection_getitnowbtnleft">
                                                    {t('bot.letsChat')}
                                                </Button>
                                            } />
                                        </Anchor>
                                    </div>
                                </Col>
                                <Col lg={{ span: 14, order: 2 }} md={{ span: 13, order: 2 }} xs={{ span: 24, order: 1 }} className="botHome_contentsection_imgbody">
                                    <img src="https://upappfactory-static-images.s3.us-east-2.amazonaws.com/Products/bot/img1.svg" />
                                </Col>
                            </Row>
                        </div>
                    </section>
                </Fragment>
            }
            {direction === DIRECTIONS.RTL &&
                <Fragment>
                    <section className="botHome_section1 botHome_gradBg">
                        <div className="UAF-container-fluid">
                            <Row type="flex" className="botHome_contentsection_row1">
                                <Col lg={{ span: 10, order: 1 }} md={{ span: 11, order: 1 }} xs={{ span: 24, order: 2 }} className="botHome_contentsection_contentCol1">
                                    <div className="botHome_contentsection_content">
                                        <h1 className="lg-rtl botHome_contentsection_titleleft">
                                            {t('bot.Transformtheway')}
                                        </h1>
                                        <p className="lg-rtl botHome_contentsection_textleft">
                                            {t('bot.UPappBotsareanewway')}
                                        </p>
                                        <Anchor>
                                            <Link href="/contactUs#GetInTouchForm" title={
                                                <Button size={"large"} className="lg-rtl botHome_contentsection_getitnowbtnleft">
                                                    {t('bot.letsChat')}
                                                </Button>
                                            } />
                                        </Anchor>
                                    </div>
                                </Col>
                                <Col lg={{ span: 14, order: 2 }} md={{ span: 13, order: 2 }} xs={{ span: 24, order: 1 }} className="botHome_contentsection_imgbody">
                                    <img src="https://upappfactory-static-images.s3.us-east-2.amazonaws.com/Products/bot/img1.svg" />
                                </Col>
                            </Row>
                        </div>
                    </section>
                </Fragment>
            }
            {/* ============================================
                            End Section1
                ============================================ */}
            {/* ============================================
                            Start Section2
                ============================================ */}
            <section className="botHome_section2">
                <div className="UAF-container-fluid">
                    <Row type="flex" className="botHome_contentsection_row2">
                        <Col lg={{ span: 14, order: 1 }} md={{ span: 13, order: 1 }} xs={{ span: 24, order: 1 }}>
                            <div className="botHome_contentsection_imgbody">
                                <img src="https://upappfactory-static-images.s3.us-east-2.amazonaws.com/Products/bot/img2.svg" />
                            </div>
                        </Col>
                        <Col lg={{ span: 10, order: 2 }} md={{ span: 11, order: 2 }} xs={{ span: 24, order: 2 }} className="botHome_contentsection_contentCol2">
                            <div className="botHome_contentsection_content lg-rtl">
                                <h1 className="lg-rtl botHome_contentsection_titleright">
                                    {t('bot.Helpstoreducefixedcost')}
                                </h1>
                                <p className="lg-rtl botHome_contentsection_textright">
                                    {t('bot.UPappbotquicklyleverage')}
                                </p>
                                <Anchor>
                                    <Link href="/contactUs#GetInTouchForm" title={
                                        <Button size={"large"} className="lg-rtl botHome_contentsection_getitnowbtnleft">
                                            {t('bot.letsChat')}
                                        </Button>
                                    } />
                                </Anchor>
                            </div>
                        </Col>
                    </Row>
                </div>
            </section>
            {/* ============================================
                            End Section2
                ============================================ */}
            {/* ============================================
                            Start Section3
                ============================================ */}
            {direction === DIRECTIONS.LTR &&
                <Fragment>
                    <section className="botHome_section3 botHome_gradBg">
                        <div className="UAF-container-fluid">
                            <Row type="flex" className="botHome_contentsection_row3">
                                <Col lg={{ span: 10, order: 1 }} md={{ span: 11, order: 1 }} xs={{ span: 24, order: 2 }} className="botHome_contentsection_contentCol3">
                                    <div className="botHome_contentsection_content lg-ltr">
                                        <h1 className="lg-ltr botHome_contentsection_titleleft">
                                            {t('bot.Increasecustomerengagement')}
                                        </h1>
                                        <p className="lg-ltr botHome_contentsection_textleft">
                                            {t('bot.UPappbotstransformthebuyingprocess')}
                                        </p>
                                        <Anchor>
                                            <Link href="/contactUs#GetInTouchForm" title={
                                                <Button size={"large"} className="lg-ltr botHome_contentsection_getitnowbtnleft">
                                                    {t('bot.letsChat')}
                                                </Button>
                                            } />
                                        </Anchor>
                                    </div>
                                </Col>
                                <Col lg={{ span: 14, order: 2 }} md={{ span: 13, order: 2 }} xs={{ span: 24, order: 1 }} className="botHome_contentsection_imgbody">
                                    <img src="https://upappfactory-static-images.s3.us-east-2.amazonaws.com/Products/bot/img3.svg" />
                                </Col>
                            </Row>
                        </div>
                    </section>
                </Fragment>
            }
            {direction === DIRECTIONS.RTL &&
                <Fragment>
                    <section className="botHome_section3 botHome_gradBg">
                        <div className="UAF-container-fluid">
                            <Row type="flex" className="botHome_contentsection_row3">
                                <Col lg={{ span: 10, order: 1 }} md={{ span: 11, order: 1 }} xs={{ span: 24, order: 2 }} className="botHome_contentsection_contentCol3">
                                    <div className="botHome_contentsection_content">
                                        <h1 className="lg-rtl botHome_contentsection_titleleft">
                                            {t('bot.Increasecustomerengagement')}
                                        </h1>
                                        <p className="lg-rtl botHome_contentsection_textleft">
                                            {t('bot.UPappbotstransformthebuyingprocess')}
                                        </p>
                                        <Anchor>
                                            <Link href="/contactUs#GetInTouchForm" title={
                                                <Button size={"large"} className="lg-rtl botHome_contentsection_getitnowbtnleft">
                                                    {t('bot.letsChat')}
                                                </Button>
                                            } />
                                        </Anchor>
                                    </div>
                                </Col>
                                <Col lg={{ span: 14, order: 2 }} md={{ span: 13, order: 2 }} xs={{ span: 24, order: 1 }} className="botHome_contentsection_imgbody">
                                    <img src="https://upappfactory-static-images.s3.us-east-2.amazonaws.com/Products/bot/img3.svg" />
                                </Col>
                            </Row>
                        </div>
                    </section>
                </Fragment>
            }
            {/* ============================================
                            End Section3
                ============================================ */}
            {/* ============================================
                            Start Section4
                ============================================ */}
            <section className="botHome_section4">
                <div className="UAF-container-fluid">
                    <Row type="flex" className="botHome_contentsection_row4">
                        <Col lg={{ span: 14, order: 1 }} md={{ span: 13, order: 1 }} xs={{ span: 24, order: 1 }}>
                            <div className="botHome_contentsection_imgbody">
                                <img src="https://upappfactory-static-images.s3.us-east-2.amazonaws.com/Products/bot/img4.svg" />
                            </div>
                        </Col>
                        <Col lg={{ span: 10, order: 2 }} md={{ span: 11, order: 2 }} xs={{ span: 24, order: 2 }} className="botHome_contentsection_contentCol4">
                            <div className="botHome_contentsection_content lg-rtl">
                                <h1 className="lg-rtl botHome_contentsection_titleright">
                                    {t('bot.CreateadvancedCampaign')}
                                </h1>
                                <p className="lg-rtl botHome_contentsection_textright">
                                    {t('bot.UPappBotscaninteractwiththeprospect')}
                                </p>
                                <Anchor>
                                    <Link href="/contactUs#GetInTouchForm" title={
                                        <Button size={"large"} className="lg-rtl botHome_contentsection_getitnowbtnleft">
                                            {t('bot.letsChat')}
                                        </Button>
                                    } />
                                </Anchor>
                            </div>
                        </Col>
                    </Row>
                </div>
            </section>
            {/* ============================================
                            End Section4
                ============================================ */}
            {/* ============================================
                            Start Section5
                ============================================ */}
            <section className="botHome_section5 botHome_gradBg">
                <div className="UAF-container-fluid">
                    <Row type="flex" className="botHome_contentsection_row5">
                        <Col lg={{ span: 24, order: 1 }} md={{ span: 24, order: 1 }} xs={{ span: 24, order: 1 }}>
                            <h1 className="botHome_contentsection_title">
                                {t('bot.IntelligentandUniqueUPappBots')}
                            </h1>
                        </Col>
                        {direction === DIRECTIONS.LTR &&
                            <Fragment>
                                <Col lg={{ span: 22, order: 1 }} md={{ span: 22, order: 1 }} xs={{ span: 24, order: 1 }} className="bhome_s5_cols_outermain">
                                    <Row type="flex" className="bhome_s5_cols_outer">
                                        <Col lg={{ span: 12, order: 1 }} md={{ span: 20, order: 1 }} xs={{ span: 24, order: 1 }} className="bHome_s5_col">
                                            <div className="bHome_s5_img_outer">
                                                <img src="https://upappfactory-static-images.s3.us-east-2.amazonaws.com/Products/bot/nlp.png" />
                                            </div>
                                            <h1 className="bHome_s5_heading">{t('bot.NLPdriven')}</h1>
                                            <p className="bHome_s5_text">
                                                {t('bot.Botsconverseonbehalf')}
                                            </p>
                                        </Col>
                                        <Col lg={{ span: 12, order: 2 }} md={{ span: 20, order: 2 }} xs={{ span: 24, order: 2 }} className="bHome_s5_col">
                                            <div className="bHome_s5_img_outer">
                                                <img src="https://upappfactory-static-images.s3.us-east-2.amazonaws.com/Products/bot/crm.png" />
                                            </div>
                                            <h1 className="bHome_s5_heading">{t('bot.EasyIntegration')}<br />{t('bot.withCRM')}</h1>
                                            <p className="bHome_s5_text">
                                                {t('bot.UPappchatbotsareequipped')}
                                            </p>
                                        </Col>
                                        <Col lg={{ span: 12, order: 3 }} md={{ span: 20, order: 3 }} xs={{ span: 24, order: 3 }} className="bHome_s5_col">
                                            <div className="bHome_s5_img_outer">
                                                <img src="https://upappfactory-static-images.s3.us-east-2.amazonaws.com/Products/bot/opa.png" />
                                            </div>
                                            <h1 className="bHome_s5_heading">{t('bot.SimpleandInteractive')}</h1>
                                            <p className="bHome_s5_text">
                                                {t('bot.Collectautomatedpayments')}
                                            </p>
                                            <p className="bHome_s5_text">
                                                {t('bot.Integrateyourpayment')}
                                            </p>
                                        </Col>
                                        <Col lg={{ span: 12, order: 4 }} md={{ span: 20, order: 4 }} xs={{ span: 24, order: 4 }} className="bHome_s5_col">
                                            <div className="bHome_s5_img_outer2">
                                                <img src="https://upappfactory-static-images.s3.us-east-2.amazonaws.com/Products/bot/agr.png" className="bHome_s5_roboimg" />
                                            </div>
                                            <h1 className="bHome_s5_heading">{t('bot.OneBotaccount')}</h1>
                                            <p className="bHome_s5_text">{t('bot.Youcanassignmultiple')}</p>
                                        </Col>
                                    </Row>
                                </Col>
                            </Fragment>
                        }
                        {direction === DIRECTIONS.RTL &&
                            <Fragment>
                                <Col lg={{ span: 22, order: 1 }} md={{ span: 22, order: 1 }} xs={{ span: 24, order: 1 }} className="bhome_s5_cols_outermain">
                                    <Row type="flex" className="bhome_s5_cols_outer">
                                        <Col lg={{ span: 12, order: 1 }} md={{ span: 20, order: 1 }} xs={{ span: 24, order: 1 }} className="bHome_s5_col">
                                            <div className="bHome_s5_img_outer">
                                                <img src="https://upappfactory-static-images.s3.us-east-2.amazonaws.com/Products/bot/nlp.png" />
                                            </div>
                                            <h1 className="bHome_s5_heading">{t('bot.NLPdriven')}</h1>
                                            <p className="bHome_s5_text" style={{ padding: "0" }}>
                                                {t('bot.Botsconverseonbehalf')}
                                            </p>
                                        </Col>
                                        <Col lg={{ span: 12, order: 2 }} md={{ span: 20, order: 2 }} xs={{ span: 24, order: 2 }} className="bHome_s5_col">
                                            <div className="bHome_s5_img_outer">
                                                <img src="https://upappfactory-static-images.s3.us-east-2.amazonaws.com/Products/bot/crm.png" />
                                            </div>
                                            <h1 className="bHome_s5_heading">{t('bot.EasyIntegration')}</h1>
                                            <p className="bHome_s5_text" style={{ padding: "0" }}>
                                                {t('bot.UPappchatbotsareequipped')}
                                            </p>
                                        </Col>
                                        <Col lg={{ span: 12, order: 3 }} md={{ span: 20, order: 3 }} xs={{ span: 24, order: 3 }} className="bHome_s5_col">
                                            <div className="bHome_s5_img_outer">
                                                <img src="https://upappfactory-static-images.s3.us-east-2.amazonaws.com/Products/bot/opa.png" />
                                            </div>
                                            <h1 className="bHome_s5_heading">{t('bot.SimpleandInteractive')}</h1>
                                            <p className="bHome_s5_text" style={{ padding: "0" }}>
                                                {t('bot.Collectautomatedpayments')}
                                            </p>
                                            <p className="bHome_s5_text" style={{ padding: "0" }}>
                                                {t('bot.Integrateyourpayment')}
                                            </p>
                                        </Col>
                                        <Col lg={{ span: 12, order: 4 }} md={{ span: 20, order: 4 }} xs={{ span: 24, order: 4 }} className="bHome_s5_col">
                                            <div className="bHome_s5_img_outer2">
                                                <img src="https://upappfactory-static-images.s3.us-east-2.amazonaws.com/Products/bot/agr.png" className="bHome_s5_roboimg" />
                                            </div>
                                            <h1 className="bHome_s5_heading">{t('bot.OneBotaccount')}</h1>
                                            <p className="bHome_s5_text" style={{ padding: "0" }}>{t('bot.Youcanassignmultiple')}</p>
                                        </Col>
                                    </Row>
                                </Col>
                            </Fragment>
                        }

                    </Row>
                </div>
            </section>
            {/* ============================================
                            End Section5
                ============================================ */}
            {/* ============================================
                            Start Section6
                ============================================ */}
            <section className="botHome_section6">
                <div className="UAF-container-fluid">
                    {direction === DIRECTIONS.LTR &&
                        <Fragment>
                            <Row type="flex" className="botHome_contentsection_row6">
                                <Col lg={{ span: 24, order: 1 }} md={{ span: 24, order: 1 }} xs={{ span: 24, order: 1 }}>
                                    <h1 className="botHome_contentsection_title">{t('bot.ConversationalLanding')}</h1>
                                </Col>
                                <Col lg={{ span: 10, order: 1 }} md={{ span: 11, order: 1 }} xs={{ span: 24, order: 2 }} className="botHome_contentsection_contentCol6">
                                    <div className="botHome_contentsection_content lg-ltr">
                                        <h1 className="lg-ltr botHome_contentsection_titleleft">
                                            {t('bot.Increasetheconversionrate')}
                                        </h1>
                                        <p className="lg-ltr botHome_contentsection_textleft">
                                            {t('bot.Makingaconversationallanding')}
                                        </p>
                                        <Anchor>
                                            <Link href="/contactUs#GetInTouchForm" title={
                                                <Button size={"large"} className="lg-ltr botHome_contentsection_getitnowbtnleft">
                                                    {t('bot.letsChat')}
                                                </Button>
                                            } />
                                        </Anchor>
                                    </div>
                                </Col>
                                <Col lg={{ span: 14, order: 2 }} md={{ span: 13, order: 2 }} xs={{ span: 24, order: 1 }} className="botHome_contentsection_imgbody">
                                    <img src="https://upappfactory-static-images.s3.us-east-2.amazonaws.com/Products/bot/img5.svg" />
                                </Col>
                            </Row>
                            <Row type="flex" className="botHome_contentsection_row7">
                                <Col lg={{ span: 14, order: 1 }} md={{ span: 13, order: 1 }} xs={{ span: 24, order: 1 }}>
                                    <div className="botHome_contentsection_imgbody">
                                        <img src="https://upappfactory-static-images.s3.us-east-2.amazonaws.com/Products/bot/img6.svg" />
                                    </div>
                                </Col>
                                <Col lg={{ span: 10, order: 2 }} md={{ span: 11, order: 2 }} xs={{ span: 24, order: 2 }} className="botHome_contentsection_contentCol7">
                                    <div className="botHome_contentsection_content lg-rtl">
                                        <h1 className="lg-rtl botHome_contentsection_titleright">
                                            {t('bot.DeliversTransformation')}
                                        </h1>
                                        <p className="lg-rtl botHome_contentsection_textright">
                                            {t('bot.Conversationalweblandingpages')}
                                        </p>
                                        <Anchor>
                                            <Link href="/contactUs#GetInTouchForm" title={
                                                <Button size={"large"} className="lg-rtl botHome_contentsection_getitnowbtnleft">
                                                    {t('bot.letsChat')}
                                                </Button>
                                            } />
                                        </Anchor>
                                    </div>
                                </Col>
                            </Row>
                            <Row type="flex" className="botHome_contentsection_row8">
                                <Col lg={{ span: 10, order: 1 }} md={{ span: 11, order: 1 }} xs={{ span: 24, order: 2 }} className="botHome_contentsection_contentCol8">
                                    <div className="botHome_contentsection_content lg-ltr">
                                        <h1 className="lg-ltr botHome_contentsection_titleleft">
                                            {t('bot.Qualifygoodleads')}
                                        </h1>
                                        <p className="lg-ltr botHome_contentsection_textleft">
                                            {t('bot.UPappbotsaresmartenough')}
                                        </p>
                                        <Anchor>
                                            <Link href="/contactUs#GetInTouchForm" title={
                                                <Button size={"large"} className="lg-ltr botHome_contentsection_getitnowbtnleft">
                                                    {t('bot.letsChat')}
                                                </Button>
                                            } />
                                        </Anchor>
                                    </div>
                                </Col>
                                <Col lg={{ span: 14, order: 2 }} md={{ span: 13, order: 2 }} xs={{ span: 24, order: 1 }} className="botHome_contentsection_imgbody">
                                    <img src="https://upappfactory-static-images.s3.us-east-2.amazonaws.com/Products/bot/img7.svg" />
                                </Col>
                            </Row>
                        </Fragment>
                    }
                    {direction === DIRECTIONS.RTL &&
                        <Fragment>
                            <Row type="flex" className="botHome_contentsection_row6">
                                <Col lg={{ span: 24, order: 1 }} md={{ span: 24, order: 1 }} xs={{ span: 24, order: 1 }}>
                                    <h1 className="botHome_contentsection_title">{t('bot.ConversationalLanding')}</h1>
                                </Col>
                                <Col lg={{ span: 10, order: 1 }} md={{ span: 11, order: 1 }} xs={{ span: 24, order: 2 }} className="botHome_contentsection_contentCol6">
                                    <div className="botHome_contentsection_content lg-rtl">
                                        <h1 className="lg-rtl botHome_contentsection_titleleft">
                                            {t('bot.Increasetheconversionrate')}
                                        </h1>
                                        <p className="lg-rtl botHome_contentsection_textleft">
                                            {t('bot.Makingaconversationallanding')}
                                        </p>
                                        <Anchor>
                                            <Link href="/contactUs#GetInTouchForm" title={
                                                <Button size={"large"} className="lg-rtl botHome_contentsection_getitnowbtnleft">
                                                    {t('bot.letsChat')}
                                                </Button>
                                            } />
                                        </Anchor>
                                    </div>
                                </Col>
                                <Col lg={{ span: 14, order: 2 }} md={{ span: 13, order: 2 }} xs={{ span: 24, order: 1 }} className="botHome_contentsection_imgbody">
                                    <img src="https://upappfactory-static-images.s3.us-east-2.amazonaws.com/Products/bot/img5.svg" />
                                </Col>
                            </Row>
                            <Row type="flex" className="botHome_contentsection_row7">
                                <Col lg={{ span: 14, order: 1 }} md={{ span: 13, order: 1 }} xs={{ span: 24, order: 1 }}>
                                    <div className="botHome_contentsection_imgbody">
                                        <img src="https://upappfactory-static-images.s3.us-east-2.amazonaws.com/Products/bot/img6.svg" />
                                    </div>
                                </Col>
                                <Col lg={{ span: 10, order: 2 }} md={{ span: 11, order: 2 }} xs={{ span: 24, order: 2 }} className="botHome_contentsection_contentCol7">
                                    <div className="botHome_contentsection_content lg-rtl">
                                        <h1 className="lg-rtl botHome_contentsection_titleright">
                                            {t('bot.DeliversTransformation')}
                                        </h1>
                                        <p className="lg-rtl botHome_contentsection_textright">
                                            {t('bot.Conversationalweblandingpages')}
                                        </p>
                                        <Anchor>
                                            <Link href="/contactUs#GetInTouchForm" title={
                                                <Button size={"large"} className="lg-rtl botHome_contentsection_getitnowbtnleft">
                                                    {t('bot.letsChat')}
                                                </Button>
                                            } />
                                        </Anchor>
                                    </div>
                                </Col>
                            </Row>
                            <Row type="flex" className="botHome_contentsection_row8">
                                <Col lg={{ span: 10, order: 1 }} md={{ span: 11, order: 1 }} xs={{ span: 24, order: 2 }} className="botHome_contentsection_contentCol8">
                                    <div className="botHome_contentsection_content lg-rtl">
                                        <h1 className="lg-rtl botHome_contentsection_titleleft">
                                            {t('bot.Qualifygoodleads')}
                                        </h1>
                                        <p className="lg-rtl botHome_contentsection_textleft">
                                            {t('bot.UPappbotsaresmartenough')}
                                        </p>
                                        <Anchor>
                                            <Link href="/contactUs#GetInTouchForm" title={
                                                <Button size={"large"} className="lg-rtl botHome_contentsection_getitnowbtnleft">
                                                    {t('bot.letsChat')}
                                                </Button>
                                            } />
                                        </Anchor>
                                    </div>
                                </Col>
                                <Col lg={{ span: 14, order: 2 }} md={{ span: 13, order: 2 }} xs={{ span: 24, order: 1 }} className="botHome_contentsection_imgbody">
                                    <img src="https://upappfactory-static-images.s3.us-east-2.amazonaws.com/Products/bot/img7.svg" />
                                </Col>
                            </Row>
                        </Fragment>
                    }
                </div>
            </section>
            {/* ============================================
                            End Section6
                ============================================ */}
            {/* ============================================
                            Start Section7
                ============================================ */}
            <section className="botHome_section7 botHome_gradBg">
                <div className="UAF-container-fluid">
                    <Row type="flex" className="botHome_contentsection_row9">
                        {direction === DIRECTIONS.LTR &&
                            <Fragment>
                                <Col lg={{ span: 24, order: 1 }} md={{ span: 24, order: 1 }} xs={{ span: 24, order: 1 }}>
                                    <h1 className="botHome_contentsection_title">
                                        <span style={{ color: "#4FBFDD" }}>{t('bot.KEY')}</span> {t('bot.features')}
                                    </h1>
                                </Col>
                            </Fragment>}
                        {direction === DIRECTIONS.RTL &&
                            <Fragment>
                                <Col lg={{ span: 24, order: 1 }} md={{ span: 24, order: 1 }} xs={{ span: 24, order: 1 }}>
                                    <h1 className="botHome_contentsection_title">
                                        {t('bot.features')} <span style={{ color: "#4FBFDD" }}>{t('bot.KEY')}</span>
                                    </h1>
                                </Col>
                            </Fragment>}
                        <Col lg={{ span: 24, order: 2 }} md={{ span: 24, order: 2 }} xs={{ span: 24, order: 2 }}>
                            <Row type="flex" gutter={16} className="s7_tile_Row">
                                <Col lg={{ span: 4, order: 1 }} md={{ span: 8, order: 1 }} xs={{ span: 15, order: 1 }} className="s7_tile_outer">
                                    <div className="s7_tile_inner">
                                        <img src="https://upappfactory-static-images.s3.us-east-2.amazonaws.com/Products/bot/bot.svg" />
                                        <h3>{t('bot.IntelligentPersonalized')}</h3>
                                    </div>
                                </Col>
                                <Col lg={{ span: 4, order: 2 }} md={{ span: 8, order: 2 }} xs={{ span: 15, order: 2 }} className="s7_tile_outer">
                                    <div className="s7_tile_inner">
                                        <img src="https://upappfactory-static-images.s3.us-east-2.amazonaws.com/Products/bot/builder.svg" />
                                        <h3>{t('bot.DragDropBotBuilder')}</h3>
                                    </div>
                                </Col>
                                <Col lg={{ span: 4, order: 3 }} md={{ span: 8, order: 3 }} xs={{ span: 15, order: 3 }} className="s7_tile_outer">
                                    <div className="s7_tile_inner">
                                        <img src="https://upappfactory-static-images.s3.us-east-2.amazonaws.com/Products/bot/integrate.svg" />
                                        <h3>{t('bot.EasytoIntegratewith')}</h3>
                                    </div>
                                </Col>
                                <Col lg={{ span: 4, order: 4 }} md={{ span: 8, order: 4 }} xs={{ span: 15, order: 4 }} className="s7_tile_outer">
                                    <div className="s7_tile_inner">
                                        <img src="https://upappfactory-static-images.s3.us-east-2.amazonaws.com/Products/bot/media.svg" />
                                        <h3>{t('bot.SupportsRichMediaFiles')}</h3>
                                    </div>
                                </Col>
                                <Col lg={{ span: 4, order: 5 }} md={{ span: 8, order: 5 }} xs={{ span: 15, order: 5 }} className="s7_tile_outer">
                                    <div className="s7_tile_inner">
                                        <img src="https://upappfactory-static-images.s3.us-east-2.amazonaws.com/Products/bot/template.svg" />
                                        <h3>{t('bot.HighlyCustomizablewithReady')}</h3>
                                    </div>
                                </Col>
                                <Col lg={{ span: 4, order: 6 }} md={{ span: 8, order: 6 }} xs={{ span: 15, order: 6 }} className="s7_tile_outer">
                                    <div className="s7_tile_inner">
                                        <img src="https://upappfactory-static-images.s3.us-east-2.amazonaws.com/Products/bot/train.png" />
                                        <h3>{t('bot.Easytotrain')}</h3>
                                    </div>
                                </Col>
                            </Row>
                        </Col>
                    </Row>
                </div>
            </section>
            {/* ============================================
                            End Section7
                ============================================ */}
            {/* ============================================
                            Start Section8
                ============================================ */}
            <section className="botHome_section8">
                <div className="UAF-container-fluid">
                    <Row type="flex" className="botHome_contentsection_row10">
                        {direction === DIRECTIONS.LTR &&
                            <Fragment>
                                <Col lg={{ span: 24, order: 1 }} md={{ span: 24, order: 1 }} xs={{ span: 24, order: 1 }}>
                                    <h1 className="botHome_contentsection_title_full">
                                        <span style={{ color: "#4FBFDD" }}>{t('bot.AI')}</span> {t('bot.PoweredWhatsAppChatbots')}
                                    </h1>
                                </Col>
                            </Fragment>}
                        {direction === DIRECTIONS.RTL &&
                            <Fragment>
                                <Col lg={{ span: 24, order: 1 }} md={{ span: 24, order: 1 }} xs={{ span: 24, order: 1 }}>
                                    <h1 className="botHome_contentsection_title_full">
                                        {t('bot.PoweredWhatsAppChatbots')} <span style={{ color: "#4FBFDD" }}>{t('bot.AI')}</span>
                                    </h1>
                                </Col>
                            </Fragment>}

                        <Col lg={{ span: 24, order: 2 }} md={{ span: 24, order: 2 }} xs={{ span: 24, order: 2 }}>
                            {direction === DIRECTIONS.LTR &&
                                <Fragment>
                                    <Row type="flex" gutter={16} className="s8_tile_Row" justify="space-between">
                                        <Col lg={{ span: 9, order: 1 }} md={{ span: 9, order: 1 }} xs={{ span: 20, order: 1 }} className="s8_tile_outer">
                                            <div className="s8_tile_inner">
                                                <h3 className="bHome_s8_heading">{t('bot.DrivesalesfromWhatsApp')}</h3>
                                                <p className="bHome_s8_text">{t('bot.DesignintegratechatbotswithyourWhatsApp')}</p>
                                            </div>
                                        </Col>
                                        <Col lg={{ span: 9, order: 2 }} md={{ span: 9, order: 2 }} xs={{ span: 20, order: 2 }} className="s8_tile_outer">
                                            <div className="s8_tile_inner">
                                                <h3 className="bHome_s8_heading">{t('bot.DragandDropbuilder')}</h3>
                                                <p className="bHome_s8_text">
                                                    {t('bot.Providesaneasyuserinterface')}
                                                </p>
                                            </div>
                                        </Col>
                                        <Col lg={{ span: 9, order: 3 }} md={{ span: 9, order: 3 }} xs={{ span: 20, order: 3 }} className="s8_tile_outer">
                                            <div className="s8_tile_inner">
                                                <h3 className="bHome_s8_heading">{t('bot.RevolutionizeWhatsApp')}</h3>
                                                <p className="bHome_s8_text">{t('bot.Upappbotsareequippedwith')}</p>
                                            </div>
                                        </Col>
                                        <Col lg={{ span: 9, order: 4 }} md={{ span: 9, order: 4 }} xs={{ span: 20, order: 4 }} className="s8_tile_outer">
                                            <div className="s8_tile_inner">
                                                <h3 className="bHome_s8_heading">{t('bot.Integrationwithsoftware')}</h3>
                                                <p className="bHome_s8_text">{t('bot.Engagingtheuser')}</p>
                                            </div>
                                        </Col>
                                        <Col lg={{ span: 9, order: 5 }} md={{ span: 9, order: 5 }} xs={{ span: 20, order: 5 }} className="s8_tile_outer">
                                            <div className="s8_tile_inner">
                                                <h3 className="bHome_s8_heading">{t('bot.Experiencehumanlike')}</h3>
                                                <p className="bHome_s8_text">{t('bot.Emojisimagesvideos')}</p>
                                            </div>
                                        </Col>
                                        <Col lg={{ span: 9, order: 6 }} md={{ span: 9, order: 6 }} xs={{ span: 20, order: 6 }} className="s8_tile_outer">
                                            <div className="s8_tile_inner">
                                                <h3 className="bHome_s8_heading">{t('bot.FreeTemplatesforWhatsApp')}</h3>
                                                <p className="bHome_s8_text">{t('bot.Getaccesstoalibrary')}</p>
                                            </div>
                                        </Col>
                                    </Row>
                                </Fragment>
                            }
                            {direction === DIRECTIONS.RTL &&
                                <Fragment>
                                    <Row type="flex" gutter={16} className="s8_tile_Row" justify="space-between">
                                        <Col lg={{ span: 9, order: 1 }} md={{ span: 9, order: 1 }} xs={{ span: 20, order: 1 }} className="s8_tile_outer">
                                            <div className="s8_tile_inner">
                                                <h3 className="bHome_s8_heading">{t('bot.DrivesalesfromWhatsApp')}</h3>
                                                <p className="bHome_s8_text" style={{ padding: "0" }}>{t('bot.DesignintegratechatbotswithyourWhatsApp')}</p>
                                            </div>
                                        </Col>
                                        <Col lg={{ span: 9, order: 2 }} md={{ span: 9, order: 2 }} xs={{ span: 20, order: 2 }} className="s8_tile_outer">
                                            <div className="s8_tile_inner">
                                                <h3 className="bHome_s8_heading">{t('bot.DragandDropbuilder')}</h3>
                                                <p className="bHome_s8_text" style={{ padding: "0" }}>
                                                    {t('bot.Providesaneasyuserinterface')}
                                                </p>
                                            </div>
                                        </Col>
                                        <Col lg={{ span: 9, order: 3 }} md={{ span: 9, order: 3 }} xs={{ span: 20, order: 3 }} className="s8_tile_outer">
                                            <div className="s8_tile_inner">
                                                <h3 className="bHome_s8_heading">{t('bot.RevolutionizeWhatsApp')}</h3>
                                                <p className="bHome_s8_text" style={{ padding: "0" }}>{t('bot.Upappbotsareequippedwith')}</p>
                                            </div>
                                        </Col>
                                        <Col lg={{ span: 9, order: 4 }} md={{ span: 9, order: 4 }} xs={{ span: 20, order: 4 }} className="s8_tile_outer">
                                            <div className="s8_tile_inner">
                                                <h3 className="bHome_s8_heading">{t('bot.Integrationwithsoftware')}</h3>
                                                <p className="bHome_s8_text" style={{ padding: "0" }}>{t('bot.Engagingtheuser')}</p>
                                            </div>
                                        </Col>
                                        <Col lg={{ span: 9, order: 5 }} md={{ span: 9, order: 5 }} xs={{ span: 20, order: 5 }} className="s8_tile_outer">
                                            <div className="s8_tile_inner">
                                                <h3 className="bHome_s8_heading">{t('bot.Experiencehumanlike')}</h3>
                                                <p className="bHome_s8_text" style={{ padding: "0" }}>{t('bot.Emojisimagesvideos')}</p>
                                            </div>
                                        </Col>
                                        <Col lg={{ span: 9, order: 6 }} md={{ span: 9, order: 6 }} xs={{ span: 20, order: 6 }} className="s8_tile_outer">
                                            <div className="s8_tile_inner">
                                                <h3 className="bHome_s8_heading">{t('bot.FreeTemplatesforWhatsApp')}</h3>
                                                <p className="bHome_s8_text" style={{ padding: "0" }}>{t('bot.Getaccesstoalibrary')}</p>
                                            </div>
                                        </Col>
                                    </Row>
                                </Fragment>
                            }
                        </Col>
                    </Row>
                </div>
            </section>
            {/* ============================================
                            End Section8
                ============================================ */}
            {/* ============================================
                            Start Section9
                ============================================ */}
            <section className="botHome_section9">
                <div className="UAF-container-fluid">
                    <Row type="flex" className="botHome_contentsection_row11">
                        <Col lg={{ span: 24, order: 1 }} md={{ span: 24, order: 1 }} xs={{ span: 24, order: 1 }}>
                            <h1 className="botHome_contentsection_title_full">{t('bot.PopularUPappBotsTemplate')}</h1>
                        </Col>
                        <Col lg={{ span: 24, order: 2 }} md={{ span: 24, order: 2 }} xs={{ span: 24, order: 2 }} className="botHome_IframeOutermain">
                            {
                                direction === DIRECTIONS.LTR &&
                                <Fragment>
                                    <Row type="flex" className="botHome_IframeInnermainBody">
                                        <Col lg={{ span: 8, order: 1 }} md={{ span: 12, order: 1 }} xs={{ span: 24, order: 1 }} className="botHome_IframeColMain">
                                            <iframe src="https://event.hellotars.com/conv/V1CJBv/" className="botHome_iframe"></iframe>
                                            <p>{t('bot.Event')}<br />{t('bot.Chatbot')}</p>
                                        </Col>
                                        <Col lg={{ span: 8, order: 2 }} md={{ span: 12, order: 2 }} xs={{ span: 24, order: 2 }} className="botHome_IframeColMain">
                                            <iframe src="https://onboarding.hellotars.com/conv/VkatIr/" className="botHome_iframe"></iframe>
                                            <p>{t('bot.Onboarding')}<br />{t('bot.Chatbot')}</p>
                                        </Col>
                                        <Col lg={{ span: 8, order: 3 }} md={{ span: 12, order: 3 }} xs={{ span: 24, order: 3 }} className="botHome_IframeColMain">
                                            <iframe src="https://cravitycakes.hellotars.com/conv/VkQZXB/" className="botHome_iframe"></iframe>
                                            <p>{t('bot.Cake')}<br />{t('bot.Chatbot')}</p>
                                        </Col>
                                        <Col lg={{ span: 8, order: 4 }} md={{ span: 12, order: 4 }} xs={{ span: 24, order: 4 }} className="botHome_IframeColMain">
                                            <iframe src="https://review.hellotars.com/conv/4kP4gD/" className="botHome_iframe"></iframe>
                                            <p>{t('bot.Travel')}<br />{t('bot.Chatbot')}</p>
                                        </Col>
                                        <Col lg={{ span: 8, order: 5 }} md={{ span: 12, order: 5 }} xs={{ span: 24, order: 5 }} className="botHome_IframeColMain">
                                            <iframe src="https://covid.hellotars.com/conv/4kVXmD/" className="botHome_iframe"></iframe>
                                            <p>{t('bot.COVIDAssessment')}<br />{t('bot.Chatbot')}</p>
                                        </Col>
                                    </Row>
                                </Fragment>
                            }
                            {
                                direction === DIRECTIONS.RTL &&
                                <Fragment>
                                    <Row type="flex" className="botHome_IframeInnermainBody">
                                        <Col lg={{ span: 8, order: 1 }} md={{ span: 12, order: 1 }} xs={{ span: 24, order: 1 }} className="botHome_IframeColMain">
                                            <iframe src="https://event.hellotars.com/conv/V1CJBv/" className="botHome_iframe"></iframe>
                                            <p>{t('bot.Chatbot')}<br />{t('bot.Event')}</p>
                                        </Col>
                                        <Col lg={{ span: 8, order: 2 }} md={{ span: 12, order: 2 }} xs={{ span: 24, order: 2 }} className="botHome_IframeColMain">
                                            <iframe src="https://onboarding.hellotars.com/conv/VkatIr/" className="botHome_iframe"></iframe>
                                            <p>{t('bot.Chatbot')}<br />{t('bot.Onboarding')}</p>
                                        </Col>
                                        <Col lg={{ span: 8, order: 3 }} md={{ span: 12, order: 3 }} xs={{ span: 24, order: 3 }} className="botHome_IframeColMain">
                                            <iframe src="https://cravitycakes.hellotars.com/conv/VkQZXB/" className="botHome_iframe"></iframe>
                                            <p>{t('bot.Chatbot')}<br />{t('bot.Cake')}</p>
                                        </Col>
                                        <Col lg={{ span: 8, order: 4 }} md={{ span: 12, order: 4 }} xs={{ span: 24, order: 4 }} className="botHome_IframeColMain">
                                            <iframe src="https://review.hellotars.com/conv/4kP4gD/" className="botHome_iframe"></iframe>
                                            <p>{t('bot.Chatbot')}<br />{t('bot.Travel')}</p>
                                        </Col>
                                        <Col lg={{ span: 8, order: 5 }} md={{ span: 12, order: 5 }} xs={{ span: 24, order: 5 }} className="botHome_IframeColMain">
                                            <iframe src="https://covid.hellotars.com/conv/4kVXmD/" className="botHome_iframe"></iframe>
                                            <p>{t('bot.Chatbot')}<br />{t('bot.COVIDAssessment')}</p>
                                        </Col>
                                    </Row>
                                </Fragment>
                            }
                        </Col>
                    </Row>
                </div>
            </section>
            {/* ============================================
                            End Section9
                ============================================ */}
            {/* ============================================
                            Start Section10
                ============================================ */}
            <section className="botHome_section10">
                <BotHomePricing />
            </section>
            {/* ============================================
                            End Section10
                ============================================ */}

            {/* ============================================
                            Start Section11
                ============================================ */}
            <section id="form_section">
                <ContactUsForm />
            </section>
            {/* ============================================
                            End Section11
                ============================================ */}

        </div >
    );
};

UPappbot.propTypes = { direction: withDirectionPropTypes.direction };
const mapStateToProps = state => ({
    Applications: state.application.Applications,
    recentBlog: state.BlogManagement.recentBlog,
    loading: state.application.loading
});
export default connect(mapStateToProps)(withDirection(UPappbot));