import React, { Fragment, useEffect, useState } from "react";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import { Link } from "react-router-dom";
import { Helmet } from "react-helmet";
import { Steps, Button, message } from "antd";

const { Step } = Steps;

const stepStyle = {
  marginBottom: 60,
  boxShadow: "0px -1px 0 0 #e8e8e8 inset"
};

const CheckoutJourney = () => {
  const [state, setState] = useState({
    current: 0
  });

  const { current } = state;

  const next = () => {
    // const current = state.current + 1;
    setState({ current: current + 1 });
  };

  const prev = () => {
    // const current = state.current - 1;
    setState({ current: current - 1 });
  };

  return (
    <div style={{ marginTop: "100px" }}>
      <Steps
        type="navigation"
        current={current}
        // onChange={this.onChange}
        style={stepStyle}
      >
        <Step status="finish" title="Step 1" />
        <Step status="finish" title="Step 2" />
        <Step status="finish" title="Step 3" />
        <Step status="finish" title="Step 4" />
      </Steps>

      <div className="steps-content">
        {current === 0 && <div>Step 1</div>}

        {current === 1 && <div>Step 2</div>}
        {current === 2 && <div>Step 3</div>}
        {current === 3 && <div>Step 4</div>}
      </div>

      <div className="steps-action">
        {current < 3 && (
          <Button type="primary" onClick={() => next()}>
            Next
          </Button>
        )}
        {current === 3 && (
          <Button
            type="primary"
            onClick={() => message.success("Processing complete!")}
          >
            Done
          </Button>
        )}
        {current > 0 && (
          <Button style={{ marginLeft: 8 }} onClick={() => prev()}>
            Previous
          </Button>
        )}
      </div>
    </div>
  );
};

CheckoutJourney.propTypes = {};

const mapStateToProps = state => ({});

export default connect(mapStateToProps, null)(CheckoutJourney);
