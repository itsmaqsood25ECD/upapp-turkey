import React, { Fragment, useState } from "react";
import { connect } from "react-redux";
import PhoneInput from "react-phone-number-input";
import "react-phone-number-input/style.css";
import SmartInput from "react-phone-number-input/smart-input";
import { CountryDropdown, RegionDropdown, CountryRegionData } from 'react-country-region-selector';
import { register } from "../../../../actions/auth.action";
import { setAlert } from "../../../../actions/alert.action.jsx"
import { Link, Redirect } from "react-router-dom";
import { useTranslation } from 'react-i18next';
import withDirection, { withDirectionPropTypes, DIRECTIONS } from 'react-with-direction';
import i18next from 'i18next';
import ReCAPTCHA from "react-google-recaptcha";
import { Form, Icon, Input, Button, Typography, Tooltip, Row, Col, Select, Spin, message } from "antd";
import PropTypes from "prop-types";
import ScrollTop from "../../../ScrollTop";
import "antd/dist/antd.css";
import "./register.style.css";
import PasswordStrengthMeter from "../../../PasswordIndicator/PasswordIndicator";


const recaptchaRef = React.createRef();
const { Option } = Select;
const RegisterForm = ({
	register,
	isAuthenticated,
	loading,
	direction,
	setAlert,
	form: {
		getFieldDecorator,
		validateFields,
		setFieldsValue,
		validateFieldsAndScroll
	}
}) => {
	const { Title } = Typography;
	const [formData, setFormData] = useState({
		disableSignup: false,
		confirmDirty: false,
		password: "",
		HumanVarification: false,
	});
	const { disableSignup, confirmDirty, password, HumanVarification } = formData;
	const onChange = e => {
		setFieldsValue({ [e.target.name]: e.target.value });
		if (e.target.name === "password") {
			setFormData({
				...formData,
				password: e.target.value
			});
		}
	};

	const { t } = useTranslation();
	const onCapchaChange = (e) => {
		console.log(e);
		setFormData({
			...formData,
			HumanVarification: true
		})

	}

	const onExpired = () => {
		console.log('expired');
		setFormData({
			...formData,
			HumanVarification: false
		})
	}
	const selectCountry = (val) => {
		setFieldsValue({ country: val });
		console.log(val);
		console.log(formData);
	}

	const onSelect = e => {
		setFieldsValue({ gender: e });
	};

	const validateToNextPassword = (rule, value, callback) => {
		if (value && confirmDirty) {
			validateFields(["confirm"], { force: true });
		}
		callback();
	};

	const onSubmit = e => {
		e.preventDefault();

		validateFieldsAndScroll((err, values) => {
			if (!err) {
				if (
					!/^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*])(?=.{7,})/.test(
						values.password
					)
				) {
					message.error(
						"Password must contain atleast 1 Digit, 1 Lowercase, 1 Uppercase and 1 Special Character with no spaces and greater than 7 characters"
					);
				} else if (values.password !== values.confirmPassword) {
					setAlert("passwords do not match", "danger")
					message.error("passwords do not match");
				} else if (!HumanVarification && !err) {
					console.log("You are BOT")
					setAlert("Please Verify reCAPTCHA", "danger")

				} else {
					setFormData({ ...formData, disableSignup: loading });
					message.loading("Signup in progress..", 3.5);
					const {
						firstName,
						lastName,
						email,
						phone,
						password,
						age,
						gender,
						country, region

					} = values;
					register({
						firstName,
						lastName,
						email,
						phone,
						password,
						age,
						gender,
						country, region
					});
				}
			}
		});
	};

	if (isAuthenticated) {
		message.success("Signup success", 3);
		return <Redirect to="/verifyaccount" />;
	}

	return loading ? (
		<Fragment>
			<div style={{ height: "100Vh", width: "100Vh" }}>
				<Spin
					size="large"
					style={{
						width: "150px",
						height: "150px",
						position: "absolute",
						top: "45Vh",
						left: "45Vw"
					}}
				/>
			</div>
		</Fragment>
	) : (
			<Fragment>
				<ScrollTop />
				<div className="white-bg">
					<div className="my-signup-form">
						<Row>
							<Col lg={24} xs={24}>
								<div
									style={{ textAlign: "center", margin: "10px auto 50px auto" }}
								>
									<Title>
										<Icon type="user-add" /> {t('auth.signUp')}
									</Title>
								</div>
							</Col>
							<Col lg={24} xs={24}>
								{direction === DIRECTIONS.LTR &&
									<Fragment>
										<Form onSubmit={e => onSubmit(e)} className="signup-form">
											<Row
												type="flex"
												justify="space-around"
												align="middle"
												gutter={16}
											>
												<Col lg={12} md={12} xs={24}>
													<Form.Item>
														{getFieldDecorator("firstName", {
															rules: [
																{
																	required: true,
																	message: "Please type your first name"
																}
															]
														})(
															<Input
																placeholder={t('auth.firstName')}
																name="firstName"
																onChange={e => onChange(e)}
																prefix={
																	<Icon
																		type="user"
																		style={{ color: "rgba(0,0,0,.25)" }}
																	/>
																}
																suffix={
																	<Tooltip title="First Name">
																		<Icon
																			type="info-circle"
																			style={{ color: "rgba(0,0,0,.45)" }}
																		/>
																	</Tooltip>
																}
															/>
														)}
													</Form.Item>
												</Col>
												<Col lg={12} md={12} xs={24}>
													<Form.Item>
														{getFieldDecorator("lastName", {
															rules: [
																{
																	required: true,
																	message: "Please type your last name"
																}
															]
														})(
															<Input
																placeholder={t('auth.lastName')}
																name="lastName"
																onChange={e => onChange(e)}
																prefix={
																	<Icon
																		type="user"
																		style={{ color: "rgba(0,0,0,.25)" }}
																	/>
																}
																suffix={
																	<Tooltip title="Last Name">
																		<Icon
																			type="info-circle"
																			style={{ color: "rgba(0,0,0,.45)" }}
																		/>
																	</Tooltip>
																}
															/>
														)}
													</Form.Item>
												</Col>
												<Col lg={12} md={12} xs={24}>
													<Form.Item>
														{getFieldDecorator("age", {})(
															<Input
																placeholder={t('auth.age')}
																name="age"
																type="number"
																onChange={e => onChange(e)}
																prefix={
																	<Icon
																		type="number"
																		style={{ color: "rgba(0,0,0,.25)" }}
																	/>
																}
																suffix={
																	<Tooltip title="age">
																		<Icon
																			type="info-circle"
																			style={{ color: "rgba(0,0,0,.45)" }}
																		/>
																	</Tooltip>
																}
															/>
														)}
													</Form.Item>
												</Col>
												<Col lg={12} md={12} xs={24}>
													<Form.Item>
														{getFieldDecorator("gender", {
															rules: [
																{
																	required: true,
																	message: t('auth.pleaseselectgender')
																}
															]
														})(
															<Select
																name="gender"
																placeholder={t('auth.pleaseselectgender')}
																onChange={e => onSelect(e)}
															>
																<Option value="gender">{t('auth.selectgender')}</Option>
																<Option value="male">{t('auth.male')}</Option>
																<Option value="female">{t('auth.female')}</Option>
															</Select>
														)}
													</Form.Item>
												</Col>
												<Col lg={12} md={12} xs={24}>
													<Form.Item>
														{getFieldDecorator("country", {
															rules: [
																{
																	required: true,
																	message: "Please type your country"
																}
															]
														})(
															// <Input
															// 	placeholder="Country"
															// 	name="country"
															// 	onChange={e => onChange(e)}
															// 	prefix={
															// 		<Icon
															// 			type="flag"
															// 			style={{ color: "rgba(0,0,0,.25)" }}
															// 		/>
															// 	}
															// 	suffix={
															// 		<Tooltip title="country">
															// 			<Icon
															// 				type="info-circle"
															// 				style={{ color: "rgba(0,0,0,.45)" }}
															// 			/>
															// 		</Tooltip>
															// 	}
															// />

															<CountryDropdown
																blacklist={['AF', 'AO', 'DJ', 'GQ', 'ER', 'GA', 'IR', 'KG', 'LY', 'MD', 'NP', 'NG', 'ST', 'SL', 'SD', 'SY', 'SR', 'TM', 'VE', 'ZW', 'IL']}
																name="country"
																valueType="short"
																onChange={(val) => selectCountry(val)} />
														)}
													</Form.Item>
												</Col>
												<Col lg={12} md={12} xs={24}>
													<Form.Item>
														{getFieldDecorator("phone", {
															rules: [
																{
																	required: true,
																	message: "Please input your mobile number!"
																}
															]
														})(
															<PhoneInput
																name="phone"
																inputComponent={SmartInput}
																placeholder={t('auth.enterPhone')}
															/>
														)}
													</Form.Item>
												</Col>
												<Col lg={24} md={24} xs={24}>
													<Form.Item>
														{getFieldDecorator("email", {
															rules: [
																{
																	type: "email",
																	message: "The input is not valid E-mail!"
																},
																{
																	required: true,
																	message: "Please input your E-mail!"
																}
															]
														})(
															<Input
																placeholder={t('auth.email')}
																name="email"
																onChange={e => onChange(e)}
																prefix={
																	<Icon
																		type="mail"
																		style={{ color: "rgba(0,0,0,.25)" }}
																	/>
																}
																suffix={
																	<Tooltip title="email">
																		<Icon
																			type="info-circle"
																			style={{ color: "rgba(0,0,0,.45)" }}
																		/>
																	</Tooltip>
																}
															/>
														)}
													</Form.Item>
												</Col>
												<Col lg={24} md={24} xs={24}>
													<Form.Item>
														{getFieldDecorator("password", {
															rules: [
																{
																	required: true,
																	message: "Please input your password!"
																},
																{
																	validator: validateToNextPassword
																}
															]
														})(
															<Input.Password
																onChange={e => onChange(e)}
																name="password"
																placeholder={t('auth.password')}
																prefix={
																	<Icon
																		type="lock"
																		style={{ color: "rgba(0,0,0,.25)" }}
																	/>
																}
															/>
														)}
														<PasswordStrengthMeter password={password} />
													</Form.Item>
													<div style={{ lineHeight: "1.5", textAlign: "right" }}>
														{/* <div>Password must contain atleast 1 Digit</div>
													<div>Password must contain 1 Lowercase, 1 Uppercase</div>
													<div>Password must contain Special Character</div>
													<div>Password length must be greater than 7 characters</div> */}
														<div>{t('auth.passwordtext.text1')}</div>
														<div>{t('auth.passwordtext.text2')}</div>
														<div>{t('auth.passwordtext.text3')}</div>
														<div>{t('auth.passwordtext.text4')}</div>
													</div>
												</Col>
												<Col lg={24} md={24} xs={24}>
													<Form.Item>
														{getFieldDecorator("confirmPassword", {
															rules: [
																{
																	required: true,
																	message: "Please input your password!"
																},
																{
																	validator: validateToNextPassword
																}
															]
														})(
															<Input.Password
																onChange={e => onChange(e)}
																placeholder={t('auth.cnfrmpassword')}
																name="confirmPassword"
																prefix={
																	<Icon
																		type="lock"
																		style={{ color: "rgba(0,0,0,.25)" }}
																	/>
																}
															/>
														)}
													</Form.Item>
												</Col>
												<Col xs={24} style={{ marginBottom: "15px" }}>
													<ReCAPTCHA
														ref={recaptchaRef}
														onExpired={onExpired}
														sitekey="6LcG18EUAAAAAGweYgM2TTt89iBNGtyYlsP_rfKd"
														onChange={(e) => onCapchaChange(e)}
													/>
												</Col>
												<Col lg={24} md={24} xs={24}>
													<Form.Item>
														<Button
															type="primary"
															htmlType="submit"
															className="login-form-button"
															disabled={disableSignup}
														>
															{t('auth.signUp')}
														</Button>
													</Form.Item>
												</Col>
												<Col xs={24}>
													<p>
														{t('auth.alreadyHaveAccount')} <Link to="/Login">{t('auth.logIn')}</Link>
													</p>
												</Col>

											</Row>
										</Form>
									</Fragment>
								}
								{direction === DIRECTIONS.RTL &&
									<Fragment>
										<Form onSubmit={e => onSubmit(e)} className="signup-form lg-rtl">
											<Row
												type="flex"
												justify="space-around"
												align="middle"
												gutter={16}
											>
												<Col lg={12} md={12} xs={24}>
													<Form.Item>
														{getFieldDecorator("firstName", {
															rules: [
																{
																	required: true,
																	message: "Please type your first name"
																}
															]
														})(
															<Input
																placeholder={t('auth.firstName')}
																name="firstName"
																onChange={e => onChange(e)}
																prefix={
																	<Icon
																		type="user"
																		style={{ color: "rgba(0,0,0,.25)" }}
																	/>
																}
																suffix={
																	<Tooltip title="First Name">
																		<Icon
																			type="info-circle"
																			style={{ color: "rgba(0,0,0,.45)" }}
																		/>
																	</Tooltip>
																}
															/>
														)}
													</Form.Item>
												</Col>
												<Col lg={12} md={12} xs={24}>
													<Form.Item>
														{getFieldDecorator("lastName", {
															rules: [
																{
																	required: true,
																	message: "Please type your last name"
																}
															]
														})(
															<Input
																placeholder={t('auth.lastName')}
																name="lastName"
																onChange={e => onChange(e)}
																prefix={
																	<Icon
																		type="user"
																		style={{ color: "rgba(0,0,0,.25)" }}
																	/>
																}
																suffix={
																	<Tooltip title="Last Name">
																		<Icon
																			type="info-circle"
																			style={{ color: "rgba(0,0,0,.45)" }}
																		/>
																	</Tooltip>
																}
															/>
														)}
													</Form.Item>
												</Col>
												<Col lg={12} md={12} xs={24}>
													<Form.Item>
														{getFieldDecorator("age", {})(
															<Input
																placeholder={t('auth.age')}
																name="age"
																type="number"
																onChange={e => onChange(e)}
																prefix={
																	<Icon
																		type="number"
																		style={{ color: "rgba(0,0,0,.25)" }}
																	/>
																}
																suffix={
																	<Tooltip title="age">
																		<Icon
																			type="info-circle"
																			style={{ color: "rgba(0,0,0,.45)" }}
																		/>
																	</Tooltip>
																}
															/>
														)}
													</Form.Item>
												</Col>
												<Col lg={12} md={12} xs={24}>
													<Form.Item className="lg-rtl" style={{ textAlign: "right!important" }}>
														{getFieldDecorator("gender", {
															rules: [
																{
																	required: true,
																	message: t('auth.pleaseselectgender')
																}
															]
														})(
															<Select

																className="lg-rtl"
																name="gender"
																placeholder={t('auth.pleaseselectgender')}
																onChange={e => onSelect(e)}
															>
																<Option value="gender" className="lg-rtl">{t('auth.selectgender')}</Option>
																<Option value="male" className="lg-rtl">{t('auth.male')}</Option>
																<Option value="female" className="lg-rtl">{t('auth.female')}</Option>
															</Select>
														)}
													</Form.Item>
												</Col>
												<Col lg={12} md={12} xs={24}>
													<Form.Item>
														{getFieldDecorator("country", {
															rules: [
																{
																	required: true,
																	message: "Please type your country"
																}
															]
														})(
															// <Input
															// 	placeholder="Country"
															// 	name="country"
															// 	onChange={e => onChange(e)}
															// 	prefix={
															// 		<Icon
															// 			type="flag"
															// 			style={{ color: "rgba(0,0,0,.25)" }}
															// 		/>
															// 	}
															// 	suffix={
															// 		<Tooltip title="country">
															// 			<Icon
															// 				type="info-circle"
															// 				style={{ color: "rgba(0,0,0,.45)" }}
															// 			/>
															// 		</Tooltip>
															// 	}
															// />

															<CountryDropdown
																blacklist={['AF', 'AO', 'DJ', 'GQ', 'ER', 'GA', 'IR', 'KG', 'LY', 'MD', 'NP', 'NG', 'ST', 'SL', 'SD', 'SY', 'SR', 'TM', 'VE', 'ZW', 'IL']}
																name="country"
																valueType="short"
																onChange={(val) => selectCountry(val)} />
														)}
													</Form.Item>
												</Col>
												<Col lg={12} md={12} xs={24}>
													<Form.Item>
														{getFieldDecorator("phone", {
															rules: [
																{
																	required: true,
																	message: "Please input your mobile number!"
																}
															]
														})(
															<PhoneInput
																name="phone"
																inputComponent={SmartInput}
																placeholder={t('auth.enterPhone')}
															/>
														)}
													</Form.Item>
												</Col>
												<Col lg={24} md={24} xs={24}>
													<Form.Item>
														{getFieldDecorator("email", {
															rules: [
																{
																	type: "email",
																	message: "The input is not valid E-mail!"
																},
																{
																	required: true,
																	message: t('auth.pleaseInputemail')
																}
															]
														})(
															<Input
																placeholder={t('auth.email')}
																name="email"
																onChange={e => onChange(e)}
																prefix={
																	<Icon
																		type="mail"
																		style={{ color: "rgba(0,0,0,.25)" }}
																	/>
																}
																suffix={
																	<Tooltip title="email">
																		<Icon
																			type="info-circle"
																			style={{ color: "rgba(0,0,0,.45)" }}
																		/>
																	</Tooltip>
																}
															/>
														)}
													</Form.Item>
												</Col>
												<Col lg={24} md={24} xs={24}>
													<Form.Item>
														{getFieldDecorator("password", {
															rules: [
																{
																	required: true,
																	message: t('auth.pleaseInputpassword')
																},
																{
																	validator: validateToNextPassword
																}
															]
														})(
															<Input.Password
																onChange={e => onChange(e)}
																name="password"
																placeholder={t('auth.password')}
																prefix={
																	<Icon
																		type="lock"
																		style={{ color: "rgba(0,0,0,.25)" }}
																	/>
																}
															/>
														)}
														<PasswordStrengthMeter password={password} />
													</Form.Item>
													<div style={{ lineHeight: "1.5", textAlign: "left" }}>
														{/* <div>Password must contain atleast 1 Digit</div>
													<div>Password must contain 1 Lowercase, 1 Uppercase</div>
													<div>Password must contain Special Character</div>
													<div>Password length must be greater than 7 characters</div> */}
														<div>{t('auth.passwordtext.text1')}</div>
														<div>{t('auth.passwordtext.text2')}</div>
														<div>{t('auth.passwordtext.text3')}</div>
														<div>{t('auth.passwordtext.text4')}</div>
													</div>
												</Col>
												<Col lg={24} md={24} xs={24}>
													<Form.Item className="lg-rtl">
														{getFieldDecorator("confirmPassword", {
															rules: [
																{
																	required: true,
																	message: t('auth.pleaseInputpassword')
																},
																{
																	validator: validateToNextPassword
																}
															]
														})(
															<Input.Password
																onChange={e => onChange(e)}
																placeholder={t('auth.cnfrmpassword')}
																name="confirmPassword"
																prefix={
																	<Icon
																		type="lock"
																		style={{ color: "rgba(0,0,0,.25)" }}
																	/>
																}
															/>
														)}
													</Form.Item>
												</Col>
												<Col xs={24} style={{ marginBottom: "15px" }}>
													<ReCAPTCHA
														ref={recaptchaRef}
														onExpired={onExpired}
														sitekey="6LcG18EUAAAAAGweYgM2TTt89iBNGtyYlsP_rfKd"
														onChange={(e) => onCapchaChange(e)}
													/>
												</Col>
												<Col lg={24} md={24} xs={24}>
													<Form.Item>
														<Button
															type="primary"
															htmlType="submit"
															className="login-form-button"
															disabled={disableSignup}
														>
															{t('auth.signUp')}
														</Button>
													</Form.Item>
												</Col>
												<Col xs={24}>
													<p>
														{t('auth.alreadyHaveAccount')} <Link to="/Login">{t('auth.logIn')}</Link>
													</p>
												</Col>

											</Row>
										</Form>
									</Fragment>
								}
							</Col>
						</Row>
					</div>
				</div>
			</Fragment>
		);
};

const Register = Form.create({ name: "register_form" })(RegisterForm);
Register.prototypes = {
	register: PropTypes.func.isRequired,
	isAuthenticated: PropTypes.bool,
	loading: PropTypes.bool,
	setAlert: PropTypes.func.isRequired,
	direction: withDirectionPropTypes.direction
};

const mapStateToProps = state => ({
	isAuthenticated: state.auth.isAuthenticated,
	isVerified: state.auth.isVerified,
	loading: state.auth.loading
});
export default connect(
	mapStateToProps,
	{ register, setAlert }
)(withDirection(Register));
