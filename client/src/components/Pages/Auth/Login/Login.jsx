import React, { Fragment } from "react";
import { connect } from "react-redux";
import { Link, Redirect } from "react-router-dom";
import ScrollTop from "../../../ScrollTop";
import { Form, Icon, Input, Button, Typography, Row, Col, message } from "antd";
import PropTypes from "prop-types";
import { login } from "../../../../actions/auth.action";
import { setAlert } from "../../../../actions/alert.action.jsx"
import setAuthToken from '../../../../utils/setAuthToken'
import { useTranslation } from 'react-i18next';
import withDirection, { withDirectionPropTypes, DIRECTIONS } from 'react-with-direction';
import i18next from 'i18next';
import "antd/dist/antd.css";
import "./login.style.css";

const { Title } = Typography;
const NormalLoginForm = ({
	login,
	isAuthenticated,
	setAlert,
	user,
	direction,
	form: { getFieldDecorator, validateFields, setFieldsValue }
}) => {
	const onChange = e => {
		setFieldsValue({
			[e.target.name]: e.target.value
		});
	};

	const onSubmit = e => {
		e.preventDefault();
		validateFields((err, values) => {
			if (!err) {

				login(values.email, values.password);
				setAuthToken()
				if (user && !user.isVerified)
					message.error("Please Verify your account");
			}
		});
	};

	const { t } = useTranslation();
	// Redirect If Logged in
	if (isAuthenticated && user && user.role === "admin") {
		return <Redirect to="/cms/admin/dashboard" />;
	} else if (isAuthenticated && user && user.role === "agency") {
		return <Redirect to='/cms/admin/blog-list' />;
	} else if (isAuthenticated) {

		return <Redirect to="/" />;
	}
	// if (isAuthenticated && user && user.role === "admin") {
	// 	return <Redirect to="/cms/admin/dashboard" />;
	// } else if (isAuthenticated) {
	// 	return <Redirect to="/" />;
	// }

	return (
		<Fragment>
			<ScrollTop />
			<div className="login-white-bg">
				<div
					className="my-login-form"

				>
					<Row>
						<Col xs={24}>
							<div style={{ textAlign: "center", margin: "10px auto 50px auto" }}>
								<Title>
									<Icon type="user" /> {t('auth.signIn')}
								</Title>
							</div>
						</Col>
						<Col xs={24}>
							{direction === DIRECTIONS.LTR &&
								<Fragment>
									<Form onSubmit={e => onSubmit(e)} className="login-form">
										<Form.Item>
											{getFieldDecorator("email", {
												rules: [
													{ required: true, message: t('auth.pleaseInputemail') }
												]
											})(
												<Input
													onChange={e => onChange(e)}
													name="email"
													prefix={
														<Icon type="mail" style={{ color: "rgba(0,0,0,.25)" }} />
													}
													placeholder={t('auth.email')}
												/>
											)}
										</Form.Item>
										<Form.Item>
											{getFieldDecorator("password", {
												rules: [
													{ required: true, message: t('auth.pleaseInputpassword') }
												]
											})(
												<Input
													onChange={e => onChange(e)}
													type="password"
													name="password"
													prefix={
														<Icon type="lock" style={{ color: "rgba(0,0,0,.25)" }} />
													}
													placeholder={t('auth.password')}
												/>
											)}
										</Form.Item>
										<Form.Item>
											<Link to="/forgot-password" className="login-form-forgot">
												{t('auth.forgotPassword')}
											</Link>
											<Button
												type="primary"
												htmlType="submit"
												className="login-form-button"
											>
												{t('auth.logIn')}
											</Button>
											{t('auth.donthaveaccount')} <Link to="/SignUp">{t('auth.signUp')}!</Link>
										</Form.Item>
									</Form>
								</Fragment>
							}
							{direction === DIRECTIONS.RTL &&
								<Fragment>
									<Form onSubmit={e => onSubmit(e)} className="login-form">
										<Form.Item className="lg-rtl" style={{textAlign:"right"}}>
											{getFieldDecorator("email", {
												rules: [
													{ required: true, message: t('auth.pleaseInputemail') }
												]
											})(
												<Input
													onChange={e => onChange(e)}
													name="email"
													prefix={
														<Icon type="mail" style={{ color: "rgba(0,0,0,.25)" }} />
													}
													placeholder={t('auth.email')}
												/>
											)}
										</Form.Item>
										<Form.Item className="lg-rtl" style={{textAlign:"right"}}>
											{getFieldDecorator("password", {
												rules: [
													{ required: true, message: t('auth.pleaseInputpassword') }
												]
											})(
												<Input
													onChange={e => onChange(e)}
													type="password"
													name="password"
													prefix={
														<Icon type="lock" style={{ color: "rgba(0,0,0,.25)" }} />
													}
													placeholder={t('auth.password')}
												/>
											)}
										</Form.Item>
										<Form.Item className="lg-rtl" style={{textAlign:"right"}}>
											<Link to="/forgot-password" className="login-form-forgot">
												{t('auth.forgotPassword')}
											</Link>
											<Button
												type="primary"
												htmlType="submit"
												className="login-form-button"
											>
												{t('auth.logIn')}
											</Button>
											{t('auth.donthaveaccount')} <Link to="/SignUp">{t('auth.signUp')}!</Link>
										</Form.Item>
									</Form>
								</Fragment>
							}
						</Col>
					</Row>
				</div>
			</div>
		</Fragment>
	);
};

const Login = Form.create({ name: "normal_login" })(NormalLoginForm);

Login.propTypes = {
	login: PropTypes.func.isRequired,
	isAuthenticated: PropTypes.bool,
	isVerified: PropTypes.bool,
	user: PropTypes.object.isRequired,
	setAlert: PropTypes.func.isRequired,
	direction: withDirectionPropTypes.direction
};

const mapStateToProps = state => ({
	user: state.auth.user,
	isAuthenticated: state.auth.isAuthenticated,
	isVerified: state.auth.isVerified
});

export default connect(
	mapStateToProps,
	{
		login, setAlert
	}
)(withDirection(Login));
