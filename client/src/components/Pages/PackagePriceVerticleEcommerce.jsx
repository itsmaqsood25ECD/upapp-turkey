/* eslint react/prop-types: 0 */
import React, { Component } from "react";
import { connect } from "react-redux";
import { Row, Col, Typography, Radio, Button, Icon, Tabs, Anchor } from "antd";
import Walkthrugh from "./Walkthrugh";
import { Link, NavLink } from 'react-router-dom'
import CurrencyFormat from "react-currency-format";
import base64 from "base-64";
import MobileApp from "../../assets/img/PackIcon/mobileApp.svg";
import WebApp from "../../assets/img/PackIcon/WebApp.svg";
import Website from "../../assets/img/PackIcon/Website.svg";
import "../../assets/css/packagePrice.css";
import withDirection, { withDirectionPropTypes, DIRECTIONS } from 'react-with-direction';
import { withTranslation } from 'react-i18next';
import i18next from 'i18next';


const { TabPane } = Tabs;
const { Title } = Typography;

class PackagePriceVerticleEcommerce extends Component {
  constructor(props) {
    super(props);
    this.state = {

      // Starting from Price For Basic and  Only

      Basic_Starting_price : this.props.BSP,
      Premium_Starting_price :  this.props.PSP,


      //plan B
      WFPP: this.props.WFPP,
      WAFPP: this.props.WAFPP,
      MFPP: this.props.MFPP,

      // Plan A
      MBDSPM: this.props.MBDSPM,
      MADSPM: this.props.MADSPM,
      MBDSPY: this.props.MBDSPY,
      MADSPY: this.props.MADSPY,
      WBDSPM: this.props.WBDSPM,
      WADSPM: this.props.WADSPM,
      WBDSPY: this.props.WBDSPY,
      WADSPY: this.props.WADSPY,
      WABDSPM: this.props.WABDSPM,
      WAADSPM: this.props.WAADSPM,
      WABDSPY: this.props.WABDSPY,
      WAADSPY: this.props.WAADSPY,
      Duration: this.props.duration,
      currency: this.props.currency,
      MBDSP: this.props.MBDSPY,
      MADSP: this.props.MADSPY,
      WBDSP: this.props.WBDSPY,
      WADSP: this.props.WADSPY,
      WABDSP: this.props.WABDSPY,
      WAADSP: this.props.WAADSPY,
      PackType: this.props.PackType,
      SLUG: this.props.SLUG,
      ID: this.props.ID,

      // Setup Charge
      WSC: this.props.WSC,
      MSC: this.props.MSC,
      WMSC: this.props.WMSC,

      // User 
      USER: this.props.USER


    };

    console.log("New Price Starting >> ", this.state.Premium_Starting_price)
  }

  

  componentWillReceiveProps(nextApp) {
    this.setState({
       // Starting from Price For Basic and  Only

       Basic_Starting_price : nextApp.BSP,
       Premium_Starting_price :  nextApp.PSP,

       
      // Plan B
      WFPP: nextApp.WFPP,
      WAFPP: nextApp.WAFPP,
      MFPP: nextApp.MFPP,
      // Plan A
      MBDSPM: nextApp.MBDSPM,
      MADSPM: nextApp.MADSPM,
      MBDSPY: nextApp.MBDSPY,
      MADSPY: nextApp.MADSPY,
      WBDSPM: nextApp.WBDSPM,
      WADSPM: nextApp.WADSPM,
      WBDSPY: nextApp.WBDSPY,
      WADSPY: nextApp.WADSPY,
      WABDSPM: nextApp.WABDSPM,
      WAADSPM: nextApp.WAADSPM,
      WABDSPY: nextApp.WABDSPY,
      WAADSPY: nextApp.WAADSPY,
      Duration: nextApp.duration,
      currency: nextApp.currency,
      MBDSP: nextApp.MBDSPY,
      MADSP: nextApp.MADSPY,
      WBDSP: nextApp.WBDSPY,
      WADSP: nextApp.WADSPY,
      WABDSP: nextApp.WABDSPY,
      WAADSP: nextApp.WAADSPY,
      PackType: nextApp.PackType,
      SLUG: nextApp.SLUG,
      ID: nextApp.ID,
      // Setup Charge
      WSC: nextApp.WSC,
      MSC: nextApp.MSC,
      WMSC: nextApp.WMSC,
      USER: nextApp.USER,
    });
  }

  handleChange = e => {
    if (e.target.value === "Year") {
      this.setState({
        MBDSP: this.state.MBDSPY,
        MADSP: this.state.MADSPY,
        WBDSP: this.state.WBDSPY,
        WADSP: this.state.WADSPY,
        WABDSP: this.state.WABDSPY,
        WAADSP: this.state.WAADSPY,
        PackType: "Annually"
      });
    } else if (e.target.value === "Month") {
      this.setState({
        MBDSP: this.state.MBDSPM,
        MADSP: this.state.MADSPM,
        WBDSP: this.state.WBDSPM,
        WADSP: this.state.WADSPM,
        WABDSP: this.state.WABDSPM,
        WAADSP: this.state.WAADSPM,
        PackType: "Monthly"
      });
    }
  };

  render() {
    const currency = this.props.currency;
    const WSC = this.props.WSC;
    const WMSC = this.props.WMSC;
    const MSC = this.props.MSC;
    // const USER = this.props.USER;
    const { t } = this.props;

    return (

      <div className="package-bg" dir="ltr">
        {/* <RequestForDemo /> */}
{   console.log("New Price Starting >> ", this.state.Premium_Starting_price)
 }
        <div style={{ textAlign: "center" }}>
          <Title level="3">
            {t('pricing.Boost')}
          </Title>

          <div className="price-verticle-tagline">
            {t('pricing.noCommitment')}
            <br />
            {t('pricing.changeCancel')}
          </div>
        </div>
        <Row
          gutter={16}
          type="flex"
          justify="center"
          align="middle"
          style={{ margin: "0px 0px", border: "0px solid #eee" }}
        >
        <Col xs={24}>

            <Row gutter={16}>
              <div style={{ margin: "100px auto", maxWidth: "720px" }}>
                {/* Website Package */}
                <Col xs={24} md={12} lg={12}>
                  <div className="pack-card Basic">
                    <div className="Pack-identifier Basic">
                      <p>
                        {/* <span>Only Website </span> */}
                        <span>{t("pricing.onlyweb")}</span>
                        {/* <br />{" "} */}
                      </p>
                    </div>
                    <div className="package-img-cont">
                      <img
                        className="package-img"
                        src={Website}
                        alt=""
                        style={{ width: "150px" }}
                      />
                    </div>
                    <div style={{ textAlign: "center" }}>
                      <Title className="package-title" level={3}>
                        {t("pricing.basic")}
                      </Title>
                    </div>
                    <div
                      style={{
                        textAlign: "center",
                        margin: "10px auto",
                        fontSize: "14px"
                      }}
                    >
                      <div
                        style={{
                          maxWidth: "230px",
                          borderBottom: "0.5px solid #ddd",
                          margin: "0px auto",
                          lineHeight: "2"
                        }}
                      >
                        {t("pricing.ecommWebsite")}
                      </div>
                      <div
                        style={{
                          maxWidth: "150px",
                          margin: "0px auto",
                          lineHeight: "2"
                        }}
                      >
                        {t("pricing.allFeatures")}
                      </div>

                      <div></div>
                    </div>
                    <div>
                          <Title
                            level={2}
                            class="light-text"
                            style={{
                              marginTop: "10px",
                              marginBottom: "30px",
                              textAlign: "center"
                            }}
                          >
                            
                &nbsp;
                <span className="PriceNum">
                              <span className="currency">{currency}</span>
                              <CurrencyFormat
                                value={Number(this.state.Basic_Starting_price)}
                                displayType={"text"}
                                thousandSeparator={true}
                              />
                              <sup className="sup-star">*</sup>
                              <span></span>
                              <span className="per-month-tag"></span>
                            </span>
                          </Title>
                          <div style={{ textAlign: "center" }}>
                          <Link to="/make-inquiry" >
                            <Button className="UAFprimaryButton1" type="primary">{t('General.GetStarted')}</Button>
                          </Link>
                           
                          </div>
                        </div>
                      

                   
                  </div>
                </Col>
                {/* End Website Package */}
               
               {/* Web App Package */}
               <Col xs={24} md={12} lg={12}>
                  <div
                    className="pack-card ultra"
                    style={{ marginTop: "-10px" }}
                  >
                    <div className="Pack-identifier ultra">
                      <p>
                        {" "}
                        <span>{t("pricing.mobileweb")}</span>
                        <br />
                      </p>
                    </div>
                    <div className="package-img-cont">
                      <img className="package-img" src={WebApp} alt="" />
                    </div>

                    <div style={{ textAlign: "center" }}>
                      <Title className="package-title" level={3}>
                        {" "}
                        {t("pricing.ULTRA")}
                      </Title>
                    </div>
                    <div
                      style={{
                        textAlign: "center",
                        margin: "10px auto",
                        fontSize: "14px"
                      }}
                    >
                      <div
                        style={{
                          maxWidth: "230px",
                          borderBottom: "0.5px solid #ddd",
                          margin: "0px auto",
                          lineHeight: "2"
                        }}
                      >
                        {t("pricing.androidIos")}
                      </div>
                      <div
                        style={{
                          maxWidth: "230px",
                          borderBottom: "0.5px solid #ddd",
                          margin: "0px auto",
                          lineHeight: "2"
                        }}
                      >
                        {t("pricing.ecommWebsite")}
                      </div>
                      <div
                        style={{
                          maxWidth: "150px",
                          margin: "0px auto",
                          lineHeight: "2"
                        }}
                      >
                        {t("pricing.allFeatures")}
                      </div>

                      <div></div>
                    </div>

                    {/* New Tags */}

                    <div>
                          <Title level={2} class="light-text" style={{ marginTop: "10px", marginBottom: "30px", textAlign: "center" }} >
                            &nbsp;
                            <span className="PriceNum">
                              <span className="currency">{currency}</span>
                              <CurrencyFormat value={Number(this.state.Premium_Starting_price)} displayType={"text"} thousandSeparator={true} />
                              <sup className="sup-star">*</sup>
                              <span></span>
                              <span className="per-month-tag"></span>
                            </span>
                          </Title>
                          <div style={{ textAlign: "center" }}>
                          <Link to="/make-inquiry" >
                                <Button className="UAFprimaryButton1" type="primary">{t('General.GetStarted')}</Button>
                              </Link>
                            
                          </div>
                        </div>
                      

                    
                  </div>
                </Col>
                {/* End Web App Package */}
                
             </div>
            </Row>

          </Col>
        </Row>
      </div>
    );
  }
}
PackagePriceVerticleEcommerce.propTypes = {
  ...withDirectionPropTypes,
};

export default withTranslation()(withDirection(PackagePriceVerticleEcommerce));
