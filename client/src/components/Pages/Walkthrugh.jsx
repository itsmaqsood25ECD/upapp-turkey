import React, { Component } from "react";
import { Steps, Button, message, Row, Col, Modal, Anchor } from "antd";
import { NavLink } from "react-router-dom";
import withDirection, { withDirectionPropTypes, DIRECTIONS } from 'react-with-direction';
import { withTranslation } from 'react-i18next';
import i18next from 'i18next';

const { Link } = Anchor;
const { Step } = Steps;

class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      current: 0,
      PLink: "",
      PlinkID: "",
      ApplicationName: "",
      getStarted: ""
    };
  }
  state = { visible: false };

  componentWillReceiveProps(nextApp) {
    this.setState({
      PLink: nextApp.PLink,
      PlinkID: nextApp.PlinkID,
      getStarted: nextApp.getStarted,
      ApplicationName: nextApp.ApplicationName
    });
  }

  componentWillMount() {
    this.setState({
      PLink: this.props.PLink,
      PlinkID: this.props.PlinkID,
      getStarted: this.props.getStarted,
      ApplicationName: this.props.ApplicationName
    });
  }

  showModal = () => {
    this.setState({
      visible: true
    });
  };

  handleOk = e => {
    this.setState({
      visible: false
    });
  };

  handleCancel = e => {
    this.setState({
      visible: false
    });
  };

  next() {
    const current = this.state.current + 1;
    this.setState({ current });
  }

  prev() {
    const current = this.state.current - 1;
    this.setState({ current });
  }

  render() {
    const { t } = this.props;
    const { current } = this.state;
    return (
      <div>
        <Anchor>
          <Link href="/contactUs#GetInTouchForm" title={<Button className="UAFprimaryButton1" type="primary">{t('General.GetStarted')}</Button>}></Link>
        </Anchor>
      </div>
    );
  }
}

export default withTranslation()(App);