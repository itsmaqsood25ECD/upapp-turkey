/* eslint react/prop-types: 0 */
import React, { Component } from "react";
import { connect } from "react-redux";
import { Row, Col, Typography, Radio, Button, Icon, Tabs, Anchor, List, Divider, } from "antd";
import Walkthrugh from "./Walkthrugh";
import CurrencyFormat from "react-currency-format";
import base64 from "base-64";
import "../../assets/css/packagePrice.css";
import withDirection, { withDirectionPropTypes, DIRECTIONS } from 'react-with-direction';
import { withTranslation } from 'react-i18next';
import i18next from 'i18next';

const { Title } = Typography;
const { TabPane } = Tabs;
const { Link } = Anchor;


export class PackagePriceVerticleFacility extends Component {
  constructor(props) {
    super(props);
    this.state = {
      // Plan A
      MBDSPM: this.props.MBDSPM,
      MADSPM: this.props.MADSPM,
      MBDSPY: this.props.MBDSPY,
      MADSPY: this.props.MADSPY,
      Duration: this.props.duration,
      currency: this.props.currency,
      MBDSP: this.props.MBDSPY,
      MADSP: this.props.MADSPY,
      PackType: this.props.PackType,
      SLUG: this.props.SLUG,
      ID: this.props.ID,

      //plan B
      MFPP: this.props.MFPP,

      // Setup Charge
      MSC: this.props.MSC
    };
  }



  componentWillReceiveProps(nextApp) {
    this.setState({
      // Plan B
      MFPP: nextApp.MFPP,
      // Plan A
      MBDSPM: nextApp.MBDSPM,
      MADSPM: nextApp.MADSPM,
      MBDSPY: nextApp.MBDSPY,
      MADSPY: nextApp.MADSPY,
      Duration: nextApp.duration,
      currency: nextApp.currency,
      PackType: nextApp.PackType,
      MBDSP: nextApp.MBDSPY,
      MADSP: nextApp.MADSPY,
      // Setup Charge
      MSC: nextApp.MSC
    });
  }

  handleChange = e => {
    if (e.target.value === "Year") {
      this.setState({
        MBDSP: this.state.MBDSPY,
        MADSP: this.state.MADSPY,

        PackType: "Annually"
      });
    } else if (e.target.value === "Month") {
      this.setState({
        MBDSP: this.state.MBDSPM,
        MADSP: this.state.MADSPM,

        PackType: "Monthly"
      });
    }
  };

  render() {
    const { t } = this.props;
    const currency = this.props.currency;
    const MSC = this.props.MSC;

    const featuresList = [
      {
        title: `${t("restaurentApp.UnlimitedOrders")}`,
      },
      {
        title: `${t("restaurentApp.UnlimitedBranches")}`
      },
      {
        title: `${t("restaurentApp.UnlimitedMenu")}`
      },
      {
        title: `${t("restaurentApp.iosAndroid")}`
      },
      {
        title: `${t("restaurentApp.PaymentGatewaty")}`
      },
      {
        title: `${t("restaurentApp.LoyalityProgram")}`
      },
      {
        title: `${t("restaurentApp.TableReservation")}`
      },
      {
        title: `${t("restaurentApp.CouponsPromotions")}`
      },
      {
        title: `${t("restaurentApp.DeliveryOptions")}`
      },
      {
        title: `${t("restaurentApp.ModesofPaymentOptions")}`
      }
    ]

    return (
      <div className="package-bg">
        {/* <RequestForDemo /> */}
        <div style={{ textAlign: "center" }}>
          <Title level={3}>{t('pricing.ourPricing')}</Title>

          <div className="price-verticle-tagline">
            {t('pricing.noCommitment')}
            <br />
            {t('pricing.changeCancel')}
          </div>
        </div>
        <Row
          gutter={16}
          type="flex"
          justify="center"
          align="middle"
          style={{ margin: "0px 0px", border: "0px solid #eee" }}
        >
          <Col lg={24} xs={24} className="">
            <div
              className="V-Price-Switcher"
              style={{
                textAlign: "center",
                marginTop: "15px",
                marginBottom: "15px"
              }}
            >
              {/* <Radio.Group
                defaultValue="Year"
                buttonStyle="solid"
                onChange={this.handleChange}
                defaultValue="Year"
                size="large"
                className="Pack-chooser-container"
                shape="circle"
              >
                <Radio.Button className="year" value="Year">
                  Yearly
                </Radio.Button>
                <Radio.Button className="month" value="Month">
                  Monthly
                </Radio.Button>
              </Radio.Group> */}
            </div>
          </Col>
          <Col xs={24}>
            <Row gutter={16}>
              <div className="uaf_pricingVertical_outer">
                <div className="priceing-card-horizontal">
                  <Row type="flex" className="">
                    <Col xs={{ span: 24, order: 1 }} lg={{ span: 8, order: 1 }}>
                      <div style={{ textAlign: "center" }}>
                        <img
                          src="https://upappfactory-static-images.s3.us-east-2.amazonaws.com/icons/androidiOS.svg"
                          alt=""
                        />

                        <div
                          style={{
                            textAlign: "center",
                            margin: "10px auto",
                            fontSize: "14px"
                          }}
                        >
                          <div
                            style={{
                              maxWidth: "230px",
                              borderBottom: "0.5px solid #ddd",
                              margin: "0px auto",
                              lineHeight: "2",
                              color: "#333"
                            }}
                          >
                            {t('pricing.androidIos')}
                          </div>
                          <div
                            style={{
                              maxWidth: "150px",
                              borderBottom: "0.5px solid #ddd",
                              margin: "0px auto",
                              lineHeight: "2",
                              color: "#333"
                            }}
                          >
                            {t('pricing.allFeatures')}
                          </div>

                          <div></div>
                        </div>
                      </div>
                    </Col>

                    <Col xs={{ span: 24, order: 3 }} lg={{ span: 8, order: 2 }}>

                      <div>
                        {/* <Title
                              level={2}
                              class=""
                              style={{
                                marginTop: "20px",
                                marginBottom: "10px",
                                textAlign: "center",
                                color: "#333"
                              }}
                            >
                              &nbsp;
                              <span
                                className="PriceNum"
                                style={{ color: "#333" }}
                              >
                                <span
                                  className="currency"
                                  style={{ color: "#333" }}
                                >
                                  {currency}
                                </span>
                                <CurrencyFormat
                                  value={Number(this.state.MFPP)}
                                  displayType={"text"}
                                  thousandSeparator={true}
                                  style={{ color: "#333" }}
                                />
                                
                                <sup
                                  className="sup-star"
                                  style={{ color: "#333" }}
                                >
                                  *
                                </sup>
                                <span></span>
                                <span
                                  className="per-month-tag"
                                  style={{ color: "#333" }}
                                >
                                  /Mo
                                </span>
                              </span>
                              <br/>
                              <span style={{fontSize:"30px"}}>
                              {t('pricing.NosetupCost')}
                              </span>
                            </Title>
                            <div
                            style={{
                              maxWidth: "230px",
                           
                              margin: "30px auto",
                              lineHeight: "2",
                              color: "#333",
                              fontSize:"14px",
                              textAlign:"center"
                            }}
                          >
                          
                          </div> */}
                        <div style={{ textAlign: "center", marginTop: "80px" }}>
                          {/* <Walkthrugh
                                PLink={`/readymade-apps/${this.state.SLUG}/${
                                  this.state.ID
                                }/checkout/${base64.encode(
                                  Number(this.state.MADSP)
                                )}`}
                              ></Walkthrugh> */}
                          <div>
                            <Anchor>
                              <Link
                                href="/contactUs#GetInTouchForm"
                                title={
                                  <Button className="UAFprimaryButton1" type="primary">{t('General.ContactSales')}</Button>
                                }
                              ></Link>
                            </Anchor>
                          </div>
                        </div>
                      </div>
                    </Col>

                    <Col xs={{ span: 24, order: 2 }} lg={{ span: 8, order: 3 }}>
                      <div style={{ textAlign: "center" }}>
                        <img
                          src="https://upappfactory-static-images.s3.us-east-2.amazonaws.com/icons/30Daysmore.svg"
                          alt=""
                        />
                        <div
                          style={{
                            textAlign: "center",
                            margin: "10px auto",
                            fontSize: "14px"
                          }}
                        >
                          <div
                            style={{
                              maxWidth: "230px",
                              borderBottom: "0.5px solid #ddd",
                              margin: "0px auto",
                              lineHeight: "2",
                              color: "#333"
                            }}
                          >
                            {t('pricing.deliver30Days')}
                          </div>
                          <div
                            style={{
                              maxWidth: "150px",
                              borderBottom: "0.5px solid #ddd",
                              margin: "0px auto",
                              lineHeight: "2",
                              color: "#333"
                            }}
                          >
                            {t('pricing.allFeatures')}
                          </div>

                          <div></div>
                        </div>
                      </div>
                    </Col>

                  </Row>
                  <Divider style={{ fontSize: "25px", marginTop: "80px", marginBottom: "40px" }} >{t("restaurentApp.FeaturesIncluded")}</Divider>

                  <List
                    grid={{
                      gutter: 16,
                      xs: 1,
                      sm: 2,
                      md: 4,
                      lg: 4,
                      xl: 4,
                      xxl: 3,
                    }}
                    dataSource={featuresList}
                    renderItem={item => (
                      <List.Item style={{ fontSize: "14px" }}>
                        <Icon style={{ color: "green", fontSize: "20px" }} type="check" />  {item.title}
                      </List.Item>
                    )}
                  />


                  {/* <Row type="flex" justify="center"> 
                  <Col xs={{ span: 24, order: 4 }} lg={{ span: 8, order: 1 }}>
                      <div style={{ textAlign: "center" }}>
                        <img
                          src="https://upappfactory-static-images.s3.us-east-2.amazonaws.com/RestaurentApp/Deliveryboy.svg"
                          alt=""
                          style={{width:"100%",maxWidth:"200px",Height:"200px"}}
                        />

                        <div
                          style={{
                            textAlign: "center",
                            margin: "10px auto",
                            fontSize: "14px"
                          }}
                        >
                          <div
                            style={{
                              maxWidth: "230px",
                              borderBottom: "0.5px solid #ddd",
                              margin: "0px auto",
                              lineHeight: "2",
                              color: "#333"
                            }}
                          >
                         Cash On Delivery Payment Option
                          </div>
                          <Title
                            level={2}
                            
                            style={{
                              marginTop: "30px",
                              marginBottom: "38px",
                              textAlign: "center",
                              color:"#333"
                            }}
                          >
                            &nbsp;
                            <span className="PriceNum" style={{color:"#333"}}>
                              <span className="currency" style={{color:"#333"}}>{currency}</span>
                              <CurrencyFormat
                                value={Number(this.state.MADSPM)}
                                displayType={"text"}
                                style={{color:"#333"}}
                                thousandSeparator={true}
                              />
                              <sup className="sup-star" style={{color:"#333"}}>*</sup>
                              <span></span>
                              <span className="per-month-tag" style={{color:"#333"}}>/Mo</span>
                            </span>
                          </Title><div></div>
                        </div>
                      </div>
                    </Col>
                  <Col xs={{ span: 24, order: 4 }} lg={{ span: 8, order: 2 }}>
                      <div style={{ textAlign: "center" }}>
                        <img
                          src="https://upappfactory-static-images.s3.us-east-2.amazonaws.com/RestaurentApp/Mangnet.svg"
                          alt=""
                          style={{width:"100%",maxWidth:"200px",maxHeight:"200px"}}
                        />

                        <div
                          style={{
                            textAlign: "center",
                            margin: "10px auto",
                            fontSize: "14px"
                          }}
                        >
                          <div
                            style={{
                              maxWidth: "230px",
                              borderBottom: "0.5px solid #ddd",
                              margin: "0px auto",
                              lineHeight: "2",
                              color: "#333"
                            }}
                          >
                            Loyality Feature
                          </div>
                          <div>
                          <Title
                            level={2}
                            
                            style={{
                              marginTop: "30px",
                              marginBottom: "38px",
                              textAlign: "center",
                              color:"#333"
                            }}
                          >
                            &nbsp;
                            <span className="PriceNum" style={{color:"#333"}}>
                              <span className="currency" style={{color:"#333"}}>{currency}</span>
                              <CurrencyFormat
                                value={Number(this.state.MADSPM)}
                                displayType={"text"}
                                style={{color:"#333"}}
                                thousandSeparator={true}
                              />
                              <sup className="sup-star" style={{color:"#333"}}>*</sup>
                              <span></span>
                              <span className="per-month-tag" style={{color:"#333"}}>/Mo</span>
                            </span>
                          </Title><div></div>
                        
                          </div>
                        </div>
                      </div>
                    </Col>
                  </Row> */}
                </div>
              </div>
            </Row>
          </Col>
        </Row>
      </div>
    );
  }
}

export default withTranslation()(PackagePriceVerticleFacility);
