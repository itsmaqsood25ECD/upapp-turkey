/* eslint react/prop-types: 0 */
import React, { Fragment, useState, useEffect } from "react";
import {
  Button,
  Typography,
  Row,
  Col,
  Input,
  Form,
  Icon,
  message,
  Upload
} from "antd";
import "rc-banner-anim/assets/index.css";
import ScrollTop from "../../ScrollTop";
import { Redirect } from "react-router-dom";
import {
  getJob,
  PostResume
} from "../../../actions/admin/career.management.action";
import PropTypes from "prop-types";
import { connect } from "react-redux";

const { Dragger } = Upload;
// const { Title, Text } = Typography;

const ApplyJob = ({
  getJob,
  Job,
  PostResume,
  loading,
  match,
  form: { getFieldDecorator, setFieldsValue, validateFieldsAndScroll }
}) => {
  const [formData, setFormData] = useState({
    disableSignup: false,
    url: [],
    job_title: "",
    job_location: "",
    job_vacancy: "",
    job_exp: "",
    isSubmitted: false
  });

  const {
    job_title,
    job_location,
    // job_vacancy,
    // job_exp,
    isSubmitted
  } = formData;

  useEffect(() => {
    getJob(match.params.id);
    console.log("Jobs >>>", Job);
    setFormData({
      ...formData,
      job_title: Job.en.job_title,
      job_location: Job.en.job_location,
      job_vacancy: Job.en.job_vacancy,
      job_exp: Job.en.job_exp
    });
  }, []);

  const onChange = e => {
    setFieldsValue({ [e.target.name]: e.target.value });
  };

  const onSubmit = e => {
    e.preventDefault();

    validateFieldsAndScroll((err, values) => {
      if (!err) {
        setFormData({ ...formData, disableSignup: loading });
        // message.loading("Submitting Resume..", 3.5);
        const {
          name,
          email,
          resume: { url }
        } = values;
        PostResume({
          name,
          email,
          resume: {
            url: formData.url
          },
          job_title: job_title,
          job_location: job_location
        });
        setFormData({ ...formData, isSubmitted: true });
      } else {
        message.loading("Please fill required Fields..", 3.5);
      }
    });
  };

  if (isSubmitted) {
    return <Redirect to="/career/apply/resume/sumbitted"></Redirect>;
  }

  const props = {
    name: "resume",
    multiple: false,
    action: "http://localhost:5000/upload/reciept",
    onChange(info) {
      const { status } = info.file;
      if (status !== "uploading") {
        console.log(info.file, info.fileList[0].response.locationArray[0]);
      }
      if (status === "done") {
        setFormData({
          ...formData,
          url: info.fileList[0].response.locationArray[0]
        });

        message.success(`${info.file.name} file uploaded successfully.`);
      } else if (status === "error") {
        message.error(`${info.file.name} file upload failed.`);
      }
    }
  };

  return (
    <Fragment>
      <ScrollTop />
      <Row
        type="flex"
        justify="space-around"
        align="middle"
        style={{
          textAlign: "center",
          marginBottom: "150px"
        }}
      ></Row>
      <Row>
        <Col xs={24} lg={14}>
          <Row
            type="flex"
            justify="space-around"
            align="middle"
            className="tagline-apply-Job-section-1"
          ></Row>
        </Col>
        <Col xs={24} lg={8}>
          <Form className="apply-for-job-form">
            <Row gutter={16}>
              <Col xs={24} lg={24}>
                <Form.Item>
                  {getFieldDecorator("name", {
                    rules: [
                      {
                        required: true,
                        message: "Please type your name"
                      }
                    ]
                  })(
                    <Input
                      placeholder="Name"
                      name="name"
                      onChange={e => onChange(e)}
                      prefix={
                        <Icon
                          type="user"
                          style={{ color: "rgba(0,0,0,.25)" }}
                        />
                      }
                    />
                  )}
                </Form.Item>
              </Col>
              <Col xs={24} lg={24}>
                <Form.Item>
                  {getFieldDecorator("email", {
                    rules: [
                      {
                        required: true,
                        message: "Please type your email"
                      }
                    ]
                  })(
                    <Input
                      placeholder="Email"
                      name="email"
                      onChange={e => onChange(e)}
                      prefix={
                        <Icon
                          type="mail"
                          style={{ color: "rgba(0,0,0,.25)" }}
                        />
                      }
                    />
                  )}
                </Form.Item>
              </Col>
              <Col xs={24} lg={24}>
                <Form.Item>
                  {getFieldDecorator("resume", {
                    rules: [
                      {
                        required: true,
                        message: "Please drop your resume"
                      }
                    ]
                  })(
                    <Dragger {...props}>
                      <p className="ant-upload-drag-icon">
                        <Icon type="inbox" />
                      </p>
                      <p className="ant-upload-text">
                        Click or drag file to this area to upload
                      </p>
                      <p className="ant-upload-hint">
                        Support for a single or bulk upload. Strictly prohibit
                        from uploading company data or other band files
                      </p>
                    </Dragger>
                  )}
                </Form.Item>
              </Col>
            </Row>

            <div style={{ textAlign: "center" }}>
              <Button
                style={{
                  width: "100%",
                  height: "40px",
                  fontSize: "18px",
                  marginTop: "15px"
                }}
                type="primary"
                onClick={onSubmit}
              >
                Submit
              </Button>
            </div>
          </Form>
        </Col>
      </Row>
    </Fragment>
  );
};

const AFJ = Form.create({ name: "Apply_for_job" })(ApplyJob);

AFJ.propTypes = {
  Job: PropTypes.object.isRequired
};

const mapStateToProps = state => ({
  // user: state.auth.user,
  Job: state.careersManagement.Job
});
export default connect(mapStateToProps, {
  getJob,
  PostResume
})(AFJ);
