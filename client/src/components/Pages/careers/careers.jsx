/* eslint react/prop-types: 0 */
import React, { Fragment, useState, useEffect } from "react";
import { Button, Typography, Row, Col, Icon, Carousel, Card } from "antd";
import { Element } from "rc-banner-anim";
import "rc-banner-anim/assets/index.css";
import ScrollTop from "../../ScrollTop";
import { Link } from "react-router-dom";
import "../../../assets/css/home.css";
import { getAllJobs } from "../../../actions/admin/career.management.action";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import Slider from "react-slick";
import "slick-carousel/slick/slick.css";

const BgElement = Element.BgElement;
const { Title, Text } = Typography;

const Career = ({ getAllJobs, Jobs, loading }) => {
  useEffect(() => {
    getAllJobs();
    console.log("jobs >>>>", Jobs);
    // console.log()
  }, []);

  function NextArrow(props) {
    const { onClick } = props;
    return (
      <Button
        type="primary"
        onClick={onClick}
        className="ss-next-btn"
        shape="circle"
      >
        <Icon type="arrow-right" />
      </Button>
    );
  }

  function PrevArrow(props) {
    const { onClick } = props;
    return (
      <Button
        type="primary"
        onClick={onClick}
        className="ss-prev-btn"
        shape="circle"
      >
        <Icon type="arrow-left" />
      </Button>
    );
  }

  var settings = {
    prevArrow: <PrevArrow />,
    nextArrow: <NextArrow />,
    dots: false,
    infinite: true,
    centerMode: true,
    speed: 200,
    swipeToSlide: true,
    slidesToShow: 3,
    slidesToScroll: 1,
    autoplay: false,
    autoplaySpeed: 2000,
    lazyLoad: true,
    initialSlide: 0,

    responsive: [
      {
        breakpoint: 1024,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 1,
          infinite: true,
          dots: false
        }
      },
      {
        breakpoint: 600,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 1,
          initialSlide: 2
        }
      },
      {
        breakpoint: 480,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
          initialSlide: 1
        }
      }
    ]
  };

  return (
    <Fragment>
      <ScrollTop />
      <Row
        className="bg section-carrer"
        type="flex"
        justify="space-around"
        align="middle"
      >
        <Col xs={24} md={24} lg={10}>
          <div className="tagline-career" style={{ padding: "auto 15px" }}>
            <Title level={1} className="career-title" style={{ color: "#fff" }}>
              GREAT WORKPLACE ASK FOR NOTHING IN RETURN
            </Title>
            <Title
              level={4}
              className="text-demo-sub-text"
              style={{ color: "#ffffff", fontWeight: "400" }}
            >
              We challenge ourselves to do better every day, every moment, At
              UPapp factory, we enrich a workplace where each member grows and
              succeed. Our working culture encourages creativity, hunder for
              constant advancement of new skill sets, along with healthy dose of
              fun & creativity. Are you ready to lead and influance change ?
              explore what advantage you can have from us.
            </Title>
          </div>
        </Col>
        <Col xs={24} md={10} lg={14}></Col>
      </Row>
      <Row type="flex" justify="space-around" align="middle">
        <Col xs={24} style={{ textAlign: "center", marginTop: "80px" }}>
          <Title level={1} className="career-title">
            GREATE CULTURE + PERKS
          </Title>
        </Col>
      </Row>
      <Row
        type="flex"
        justify="space-around"
        align="middle"
        className="tagline-career-section-1"
      ></Row>
      <Row
        type="flex"
        justify="space-around"
        align="middle"
        style={{ background: "#f2f2f2" }}
      >
        <Col xs={24} style={{ textAlign: "center", marginTop: "80px" }}>
          <Title level={1} className="career-title">
            UNZIP OUR REQUIREMENT PROCESS
          </Title>
        </Col>
      </Row>
      <Row
        type="flex"
        justify="space-around"
        align="middle"
        className="tagline-career-section-2"
      ></Row>
      <Row
        style={{ marginTop: "80px" }}
        type="flex"
        justify="space-around"
        align="middle"
        className="tagline-career-section-3"
      ></Row>
      <Row type="flex" justify="space-around" align="middle" style={{}}>
        <Col
          xs={24}
          style={{
            textAlign: "center",
            marginTop: "80px"
          }}
        >
          <Title level={1} className="career-title">
            EXPLORE JOB OPPORTUNITIES
          </Title>
        </Col>
        <Col xs={24} style={{ marginTop: "50px", marginBottom: "100px" }}>
          <div className="job-slider">
            <Slider {...settings}>
              {Jobs &&
                Jobs.map((data, i) => {
                  return (
                    <div key={i}>
                      <Card className="job-card">
                        <p className="job-title">{data.en.job_title}</p>
                        <div className="job-description">
                          <p className="job-location">
                            Location: {data.en.job_location}
                          </p>
                          <p className="job-exp">
                            Experience : {data.en.job_exp}
                          </p>
                          <p className="job-vacancy">
                            Vacancy : {data.en.job_vacancy}
                          </p>
                        </div>
                        <div className="job-apply-btn-container">
                          <Link to={`/career/apply/${data._id}`}>
                            <Button
                              className="apply-now-btn"
                              type="primary"
                              size="large"
                            >
                              Apply Now
                            </Button>
                          </Link>
                        </div>
                      </Card>
                    </div>
                  );
                })}
            </Slider>
          </div>
        </Col>
      </Row>
    </Fragment>
  );
};

Career.propTypes = {
  getAllJobs: PropTypes.func.isRequired,
  Jobs: PropTypes.object.isRequired,
  loading: PropTypes.bool
};

const mapStateToProps = state => ({
  // user: state.auth.user,
  Jobs: state.careersManagement.Jobs,
  loading: state.careersManagement.loading
});
export default connect(mapStateToProps, {
  getAllJobs
  // getLocation
})(Career);
