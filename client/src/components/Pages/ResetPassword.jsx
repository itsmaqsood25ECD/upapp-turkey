/* eslint react/prop-types: 0 */
import React, { Fragment, useState, useEffect } from "react";
import { connect } from "react-redux";
import { Redirect } from "react-router-dom";
import ScrollTop from "../ScrollTop";
import { Form, Icon, Input, Button, Typography, Row, Col, message } from "antd";
import { resetPassword } from "../../actions/auth.action";

import PropTypes from "prop-types";

const { Title } = Typography;

const ResetPasswordForm = ({
  resetPassword,
  isAuthenticated,
  match,
  form: { getFieldDecorator, validateFields, validateFieldsAndScroll }
}) => {
  const [formData, setFormData] = useState({
    token: "",
    password: "",
    confirmPassword: "",
    disableSubmit: false,
    confirmDirty: false
  });
  const {
    password,
    confirmPassword,
    disableSubmit,
    loading,
    confirmDirty
  } = formData;

  useEffect(() => {
    setFormData({
      token: loading || !match.params.token ? "" : match.params.token
    });
  }, []);

  const onChange = e =>
    setFormData({
      ...formData,
      [e.target.name]: e.target.value
    });

  const validateToNextPassword = (rule, value, callback) => {
    if (value && confirmDirty) {
      validateFields(["confirm"], { force: true });
    }
    callback();
  };

  const onSubmit = e => {
    e.preventDefault();
    validateFieldsAndScroll((err, values) => {
      if (!err) {
        if (
          !/^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*])(?=.{7,})/.test(
            password
          )
        ) {
          message.error(
            "Password must contain atleast 1 Digit, 1 Lowercase, 1 Uppercase and 1 Special Character with no spaces and greater than 7 characters"
          );
        } else if (password !== confirmPassword) {
          message.error("passwords do not match");
        } else {
          setFormData({ ...formData, disableSignup: loading });
          message.loading("Please wait", 3.5);
          resetPassword(formData);
        }
      }
    });
  };

  // Redirect to msg page
  if (isAuthenticated) {
    return <Redirect to="/reset-password/success" />;
  }
  return (
    <Fragment>
      <ScrollTop />
      <div
        className="my-login-form"
        style={{
          background: "#FAFFFF",
          marginTop: "100px",
          marginBottom: "100px"
        }}
      >
        <Row>
          <Col xs={24}>
            <div style={{ textAlign: "center", margin: "10px auto 50px auto" }}>
              <Title>
                <Icon type="lock" />
                Reset Password
              </Title>
            </div>
          </Col>
          <Col xs={24}>
            <Form onSubmit={e => onSubmit(e)} className="login-form">
              <Form.Item>
                {getFieldDecorator("password", {
                  rules: [
                    {
                      required: true,
                      message: "Please input your password!"
                    },
                    {
                      validator: validateToNextPassword
                    }
                  ]
                })(
                  <Input.Password
                    onChange={e => onChange(e)}
                    name="password"
                    prefix={
                      <Icon type="lock" style={{ color: "rgba(0,0,0,.25)" }} />
                    }
                  />
                )}
                {/* <PasswordStrengthMeter password={password} /> */}
              </Form.Item>
              <Form.Item>
                {getFieldDecorator("confirmPassword", {
                  rules: [
                    {
                      required: true,
                      message: "Please confirm your password!"
                    },
                    {
                      validator: validateToNextPassword
                    }
                  ]
                })(
                  <Input.Password
                    onChange={e => onChange(e)}
                    name="confirmPassword"
                    prefix={
                      <Icon type="lock" style={{ color: "rgba(0,0,0,.25)" }} />
                    }
                  />
                )}
              </Form.Item>
              <Form.Item>
                <Button
                  type="primary"
                  htmlType="submit"
                  className="login-form-button"
                  disabled={disableSubmit}
                >
                  Submit
                </Button>
              </Form.Item>
            </Form>
          </Col>
        </Row>
      </div>
    </Fragment>
  );
};

const ResetPassword = Form.create({ name: "register_password_form" })(
  ResetPasswordForm
);

ResetPassword.propTypes = {
  resetPassword: PropTypes.func.isRequired,
  isAuthenticated: PropTypes.bool,
  loading: PropTypes.bool
};
const mapStateToProps = state => ({
  isAuthenticated: state.auth.isAuthenticated,
  loading: state.auth.loading
});
export default connect(mapStateToProps, {
  resetPassword
})(ResetPassword);
