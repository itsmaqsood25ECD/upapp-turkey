/* eslint react/prop-types: 0 */
import React, { useEffect, useState } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";

import { getPayemntTokenDetails } from "../../actions/payment.action";
import { Form } from "antd";

const PaymentForm = ({
  form: { getFieldDecorator, setFieldsValue },
  getPayemntTokenDetails
}) => {
  const [appData, setAppData] = useState({
    enc_request: "",
    access_code: ""
  });

  const { enc_request, access_code } = appData;
  useEffect(() => {
    setTimeout(async function() {
      const res = await getPayemntTokenDetails();
      setAppData({
        ...appData,
        enc_request: res && res.enc_request,
        access_code: res && res.acess_code
      });
    }, 5000);
  }, []);

  // window.onload = function () {
  //   document.forms['customerData'].submit()
  // }
  // document.addEventListener('DOMContentLoaded', function (event) {
  //   console.log('event listner')
  // })
  if (enc_request && access_code) {
    setTimeout(() => {
      document.forms["customerData"].submit();
    }, 3000);
  }

  return (
    // <div>
    <>
      <div style={{ paddingTop: "200px" }}>
        "Please Do Not Refresh While we are redirecting you to the payment page"
      </div>
      <form
        method="POST"
        style={{ paddingTop: "200px" }}
        name="customerData"
        id="frm1"
        action="https://secure.ccavenue.ae/transaction/transaction.do?command=initiateTransaction"
      >
        <input
          type="hidden"
          id="encRequest"
          name="encRequest"
          value={enc_request}
        />
        <input
          type="hidden"
          name="access_code"
          id="access_code"
          value={access_code}
        />
        <script language="javascript">document.redirect.submit();</script>
        {/* <input type="submit" /> */}
      </form>
    </>
  );
};

const CheckoutPaymentForm = Form.create({ name: "Payment_Form" })(PaymentForm);

CheckoutPaymentForm.propTypes = {
  getPayemntTokenDetails: PropTypes.func.isRequired
};

export default connect(null, { getPayemntTokenDetails })(CheckoutPaymentForm);
