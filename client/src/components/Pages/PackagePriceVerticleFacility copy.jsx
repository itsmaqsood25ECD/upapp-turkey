/* eslint react/prop-types: 0 */
import React, { Component } from "react";
import { connect } from "react-redux";
import { Row, Col, Typography, Radio, Button, Icon, Tabs, Anchor } from "antd";
import Walkthrugh from "./Walkthrugh";
import CurrencyFormat from "react-currency-format";
import base64 from "base-64";
import "../../assets/css/packagePrice.css";


import withDirection, { withDirectionPropTypes, DIRECTIONS } from 'react-with-direction';
import { withTranslation } from 'react-i18next';
import i18next from 'i18next';


const { Title } = Typography;

export class PackagePriceVerticleFacility extends Component {
  constructor(props) {
    super(props);
    this.state = {
      // Plan A
      MBDSPM: this.props.MBDSPM,
      MADSPM: this.props.MADSPM,
      MBDSPY: this.props.MBDSPY,
      MADSPY: this.props.MADSPY,
      Duration: this.props.duration,
      currency: this.props.currency,
      MBDSP: this.props.MBDSPY,
      MADSP: this.props.MADSPY,
      PackType: this.props.PackType,
      SLUG: this.props.SLUG,
      ID: this.props.ID,

      // Setup Charge
      MSC: this.props.MSC
    };
  }

  componentWillReceiveProps(nextApp) {
    this.setState({
      // Plan A
      MBDSPM: nextApp.MBDSPM,
      MADSPM: nextApp.MADSPM,
      MBDSPY: nextApp.MBDSPY,
      MADSPY: nextApp.MADSPY,
      Duration: nextApp.duration,
      currency: nextApp.currency,
      PackType: nextApp.PackType,
      MBDSP: nextApp.MBDSPY,
      MADSP: nextApp.MADSPY,
      // Setup Charge
      MSC: nextApp.MSC
    });
  }

  handleChange = e => {
    if (e.target.value === "Year") {
      this.setState({
        MBDSP: this.state.MBDSPY,
        MADSP: this.state.MADSPY,

        PackType: "Annually"
      });
    } else if (e.target.value === "Month") {
      this.setState({
        MBDSP: this.state.MBDSPM,
        MADSP: this.state.MADSPM,

        PackType: "Monthly"
      });
    }
  };

  render() {
    const { t } = this.props;
    const currency = this.props.currency;
    const MSC = this.props.MSC;

    return (
      <div className="package-bg">
        {/* <RequestForDemo /> */}

        <div style={{ textAlign: "center" }}>
          <Title level={3}>{t('pricing.ourPricing')}</Title>

          <div className="price-verticle-tagline">
          {t('pricing.noCommitment')}
            <br />
            {t('pricing.changeCancel')}
          </div>
        </div>
        <Row
          gutter={16}
          type="flex"
          justify="center"
          align="middle"
          style={{ margin: "0px 0px", border: "0px solid #eee" }}
        >
          <Col lg={24} xs={24} className="">
            <div
              className="V-Price-Switcher"
              style={{
                textAlign: "center",
                marginTop: "15px",
                marginBottom: "15px"
              }}
            >
              <Radio.Group
                defaultValue="Year"
                buttonStyle="solid"
                onChange={this.handleChange}
                defaultValue="Year"
                size="large"
                className="Pack-chooser-container"
                shape="circle"
              >
                <Radio.Button className="year" value="Year">
                {t('pricing.yearly')}
                </Radio.Button>
                <Radio.Button className="month" value="Month">
                {t('pricing.Monthly')}
                </Radio.Button>
              </Radio.Group>
            </div>
          </Col>
          <Col xs={24}>
            <Row gutter={16}>
              <div style={{ margin: "100px auto", maxWidth: "1080px" }}>
                <div className="priceing-card-horizontal">
                  <Row type="flex" className="">
                    <Col xs={{ span: 24, order: 1 }} lg={{ span: 8, order: 1 }}>
                      <div style={{ textAlign: "center" }}>
                        <img
                          src="https://upappfactory-static-images.s3.us-east-2.amazonaws.com/icons/androidiOS.svg"
                          alt=""
                        />

                        <div
                          style={{
                            textAlign: "center",
                            margin: "10px auto",
                            fontSize: "14px"
                          }}
                        >
                          <div
                            style={{
                              maxWidth: "230px",
                              borderBottom: "0.5px solid #ddd",
                              margin: "0px auto",
                              lineHeight: "2",
                              color: "#333"
                            }}
                          >
                            {t('pricing.androidIos')}
                          </div>
                          <div
                            style={{
                              maxWidth: "150px",
                              borderBottom: "0.5px solid #ddd",
                              margin: "0px auto",
                              lineHeight: "2",
                              color: "#333"
                            }}
                          >
                           {t('pricing.allFeatures')}
                          </div>

                          <div></div>
                        </div>
                      </div>
                    </Col>
                    <Col xs={{ span: 24, order: 3 }} lg={{ span: 8, order: 2 }}>
                      {/* New Tags */}
                      <div>
                        <Title
                          level={2}
                          class=""
                          style={{
                            marginTop: "10px",
                            marginBottom: "30px",
                            textAlign: "center",
                            color: "#333"
                          }}
                        >
                          <div
                            className="before-discount-price"
                            style={{ color: "#333" }}
                          >
                            {currency}&nbsp;
                            <CurrencyFormat
                              value={Number(this.state.MBDSP)}
                              displayType={"text"}
                              thousandSeparator={true}
                            />
                          </div>
                          &nbsp;
                          <span className="PriceNum" style={{ color: "#333" }}>
                            <span
                              className="currency"
                              style={{ color: "#333" }}
                            >
                              {currency}
                            </span>
                            <CurrencyFormat
                              value={Number(this.state.MADSP)}
                              displayType={"text"}
                              thousandSeparator={true}
                              style={{ color: "#333" }}
                            />
                            <sup className="sup-star" style={{ color: "#333" }}>
                              *
                            </sup>
                            <span></span>
                            <span
                              className="per-month-tag"
                              style={{ color: "#333" }}
                            >
                              /Mo
                            </span>
                          </span>
                          <div
                            className="one-time-setup-tag"
                            style={{ color: "#333" }}
                          >
                            *{t('pricing.oneTimeSetup')} {currency} {MSC} {t('pricing.willApply')}
                          </div>
                        </Title>
                        <div style={{ textAlign: "center" }}>
                          <Walkthrugh
                            PLink={`/readymade-apps/${this.state.SLUG}/${
                              this.state.ID
                            }/checkout/${base64.encode(
                              Number(this.state.MADSP)
                            )}`}
                          ></Walkthrugh>
                        </div>
                      </div>

                      {/* New Tags end */}
                    </Col>
                    <Col xs={{ span: 24, order: 2 }} lg={{ span: 8, order: 3 }}>
                      <div style={{ textAlign: "center" }}>
                        <img
                          src="https://upappfactory-static-images.s3.us-east-2.amazonaws.com/icons/30Daysmore.svg"
                          alt=""
                        />
                        <div
                          style={{
                            textAlign: "center",
                            margin: "10px auto",
                            fontSize: "14px"
                          }}
                        >
                          <div
                            style={{
                              maxWidth: "230px",
                              borderBottom: "0.5px solid #ddd",
                              margin: "0px auto",
                              lineHeight: "2",
                              color: "#333"
                            }}
                          >
                            {t('pricing.deliver30Days')}
                          </div>
                          <div
                            style={{
                              maxWidth: "150px",
                              borderBottom: "0.5px solid #ddd",
                              margin: "0px auto",
                              lineHeight: "2",
                              color: "#333"
                            }}
                          >
                           {t('pricing.allFeatures')}
                          </div>

                          <div></div>
                        </div>
                      </div>
                    </Col>
                  </Row>
                </div>
              </div>
            </Row>
          </Col>
        </Row>
      </div>
    );
  }
}


export default withTranslation()(PackagePriceVerticleFacility)

