/* eslint react/prop-types: 0 */
import React, { useEffect, useState, Fragment } from "react";
import PropTypes from "prop-types";
import { CountryDropdown } from "react-country-region-selector";
import PhoneInput from "react-phone-number-input";
import "react-phone-number-input/style.css";
import SmartInput from "react-phone-number-input/smart-input";
import { connect } from "react-redux";
import moment from "moment";
import ReactGA from "react-ga";
import SrollTop from "../ScrollTop";
import { Icon, Button, Typography, Modal, Input, Row, Col, Form } from "antd";
import { requestDemo } from "../../actions/contact.action";
import { loadApplication } from "../../actions/application.action";

import appRequestBG from "../../assets/img/AppRequest.svg";

const ApplicationRequest = ({
  requestDemo,
  form: { getFieldDecorator, setFieldsValue, validateFieldsAndScroll },
  Applications
}) => {
  const [appDetails, setAppData] = useState({
    apps: [],
    selectApplication: ""
  });

  useEffect(() => {
    if (Applications) {
      setAppData({
        ...appDetails,
        apps:
          Applications &&
          Applications.map(arg => {
            return arg;
          })
      });
    }
  }, [Applications]);

  //  Request For Demo

  const selectCountry = val => {
    setFormData({
      ...formData,
      country: val
    });
  };

  const [modal, setModal] = useState({
    visible: false
  });

  const [formData, setFormData] = useState({
    type: "demo"
  });
  const demoRequest = () => {
    showModal();
  };

  const showModal = () => {
    setModal({
      visible: true
    });
  };

  const handleOk = e => {
    validateFieldsAndScroll((err, values) => {
      if (!err) {
        setModal({
          visible: false
        });
        console.log(formData);
        ReactGA.event({
          category: "Contact",
          label: "Request_Demo",
          action: "Request Demo Submit",
          value: 10
        });
        requestDemo(formData);
        Modal.success({
          title: "Demo Request Successful",
          content:
            "Your request to have a demo with UP app factory’s demo team is now received and our pre-sales consultant will be in touch with you soon."
        });
      }
    });
  };

  const handleCancel = e => {
    setModal({
      visible: false
    });
  };

  const onPhoneChange = e => {
    setFormData({
      ...formData,
      mobile: e
    });
  };
  const onChange = e =>
    setFormData({ ...formData, [e.target.name]: e.target.value });

  const onDateChange = e => {
    setFormData({ ...formData, prefered_date: e._d });
  };

  const disabledDate = current => {
    // Can not select days before today and today
    return current && current < moment().endOf("day");
  };

  // Request for Demo

  return (
    <Fragment>
      <SrollTop />

      {/* <Button
				type="primary"
				className="walk-through-step-btn"
				size="large"
				onClick={demoRequest}
				style={{marginTop:"250px"}}

			>
				Application Request
			</Button>
			<Modal
				centered={true}
				title="Application Request"
				visible={modal.visible}
				onOk={handleOk}
				onCancel={handleCancel}
			>
			</Modal> */}

      <div
        style={{
          marginTop: "80px",
          minHeight: "650px",
          height: "100%",
          background:
            "linear-gradient(to right, rgb(33, 147, 176), rgb(109, 213, 237))"
        }}
      >
        <Row>
          <Col lg={16} xs={24} style={{ marginTop: "30px" }}>
            <img className="appRequestBG" src={appRequestBG} alt="" />
          </Col>
          <Col lg={8} xs={24}>
            <Form
              style={{
                marginTop: "100px",
                boxShadow: "0px 0px 10px #ccc",
                padding: "40px 20px",
                marginLeft: "30px",
                marginRight: "30px",
                background: "#fff"
              }}
            >
              <Row gutter={16}>
                <Col xs={24} lg={24}>
                  <Form.Item>
                    {getFieldDecorator("name", {
                      rules: [
                        {
                          required: true,
                          message: "Please type your name"
                        }
                      ]
                    })(
                      <Input
                        placeholder="Name"
                        name="name"
                        onChange={e => onChange(e)}
                        prefix={
                          <Icon
                            type="user"
                            style={{ color: "rgba(0,0,0,.25)" }}
                          />
                        }
                      />
                    )}
                  </Form.Item>
                </Col>
                <Col xs={24} lg={24}>
                  <Form.Item>
                    {getFieldDecorator("email", {
                      rules: [
                        {
                          required: true,
                          message: "Please type your email"
                        }
                      ]
                    })(
                      <Input
                        placeholder="Email"
                        name="email"
                        onChange={e => onChange(e)}
                        prefix={
                          <Icon
                            type="mail"
                            style={{ color: "rgba(0,0,0,.25)" }}
                          />
                        }
                      />
                    )}
                  </Form.Item>
                </Col>
                <Col xs={24} lg={24}>
                  <Form.Item>
                    {getFieldDecorator("country", {
                      rules: [
                        {
                          required: true,
                          message: "Please type your country"
                        }
                      ]
                    })(
                      <CountryDropdown
                        blacklist={[
                          "AF",
                          "AO",
                          "DJ",
                          "GQ",
                          "ER",
                          "GA",
                          "IR",
                          "KG",
                          "LY",
                          "MD",
                          "NP",
                          "NG",
                          "ST",
                          "SL",
                          "SD",
                          "SY",
                          "SR",
                          "TM",
                          "VE",
                          "ZW",
                          "IL"
                        ]}
                        name="country"
                        valueType="short"
                        style={{ marginTop: "12px" }}
                        onChange={val => selectCountry(val)}
                      />
                    )}
                  </Form.Item>
                </Col>
                <Col xs={24} lg={24}>
                  <Form.Item>
                    {getFieldDecorator("mobile", {
                      rules: [
                        {
                          required: true,
                          message: "Please input your mobile number!"
                        }
                      ]
                    })(
                      <PhoneInput
                        style={{ marginTop: "10px" }}
                        name="mobile"
                        inputComponent={SmartInput}
                        placeholder="Enter phone number"
                        onChange={e => onPhoneChange(e)}
                      />
                    )}
                  </Form.Item>
                </Col>
              </Row>
              <div style={{ textAlign: "center" }}>
                <Button className="UAFprimaryButton1" type="primary" onClick={handleOk} >Submit</Button>
              </div>
            </Form>
          </Col>
        </Row>
      </div>
    </Fragment>
  );
};

const RFA = Form.create({ name: "Request_App" })(ApplicationRequest);
RFA.propTypes = {
  requestDemo: PropTypes.func.isRequired,
  // loadApplication: PropTypes.func.isRequired,
  // Applications: PropTypes.array.isRequired,
  loading: PropTypes.bool
};

const mapStateToProps = state => ({
  // Applications: state.application.Applications,
  loading: state.application.loading
});

const mapDispatchToProps = {};

export default connect(mapStateToProps, { requestDemo, loadApplication })(RFA);
