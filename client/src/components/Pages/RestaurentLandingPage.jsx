/* eslint react/prop-types: 0 */
import React, { useState, useEffect, Fragment } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { Button, Typography, Row, Col, Spin, Form } from "antd";
import "rc-banner-anim/assets/index.css";
import ContactUsForm from "./ContactUsForm";
import ScrollTop from "../ScrollTop";
import { Link } from "react-router-dom";
import "antd/dist/antd.css";
import "../../assets/css/custom.css";
import "../../assets/css/home.css";
import "../../assets/css/homeNew.css";
import "react-animated-slider/build/horizontal.css";
import SSsliderFacility from "./single page/SSsliderFacility";
import WebsiteDemo from "./RestaurantAppPages/websitedemo"

import withDirection, { withDirectionPropTypes, DIRECTIONS } from 'react-with-direction';
import { useTranslation } from 'react-i18next';
import i18next from 'i18next'

// images
import AndroidIosAppIncluded from "../../assets/img/eCommerce Page/Android&IosAppIncluded.svg";
import LoyalityImg from "../../assets/img/restaurant/LoyalityProgram.png"

import PackagePriceVerticleRestaurant from "./PackagePriceVerticleRestaurant";
import { Helmet } from "react-helmet";

const { Title } = Typography;

const RestaurentLandingPage = ({ loading, direction }) => {

  const { t } = useTranslation();

  const [appDetails, setAppData] = useState({
    apps: [],
    selectApplication: "",
    currency: "",
    // EcommcercenewBDSPM: '',
    newADSPM: "",
    newBDSPY: "",
    newADSPY: "",
    newMobileBDSPM: "",
    newMobileADSPM: "",
    newMobileBDSPY: "",
    newMobileADSPY: "",
    country: "",
    MSC: "",
    // Plan B
    MFPP: ""
  });

  const {
    country,
    currency,
    newMobileBDSPM,
    newMobileADSPM,
    newMobileBDSPY,
    newMobileADSPY,
    MSC,
    MFPP
  } = appDetails;



  const Restaurant_Screenshot = [
    {
      img: "https://upappfactory-static-images.s3.us-east-2.amazonaws.com/RestaurentApp/Mobile+Demo+Screens/iPhone+XS+%E2%80%93+1%402x.png",
      name: "Home Page"
    },
    {
      img: "https://upappfactory-static-images.s3.us-east-2.amazonaws.com/RestaurentApp/Mobile+Demo+Screens/iPhone+XS+1%402x.png",
      name: "Navigation Menu"
    },
    {
      img: "https://upappfactory-static-images.s3.us-east-2.amazonaws.com/RestaurentApp/Mobile+Demo+Screens/iPhone+XS+2%402x.png",
      name: "Menu Page"
    },
    {
      img: "https://upappfactory-static-images.s3.us-east-2.amazonaws.com/RestaurentApp/Mobile+Demo+Screens/iPhone+XS+14%402x.png",
      name: "Detailed Menu Page"
    },
    {
      img: "https://upappfactory-static-images.s3.us-east-2.amazonaws.com/RestaurentApp/Mobile+Demo+Screens/iPhone+XS+15%402x.png",
      name: "My Cart Page"
    },
    {
      img: "https://upappfactory-static-images.s3.us-east-2.amazonaws.com/RestaurentApp/Mobile+Demo+Screens/iPhone+XS+4.png",
      name: "Registration Page"
    },
    {
      img: "https://upappfactory-static-images.s3.us-east-2.amazonaws.com/RestaurentApp/Mobile+Demo+Screens/iPhone+XS+5.png",
      name: "Verification Page"
    },
    {
      img: "https://upappfactory-static-images.s3.us-east-2.amazonaws.com/RestaurentApp/Mobile+Demo+Screens/iPhone+XS+6%402x.png",
      name: "Feedback Page"
    },
    {
      img: "https://upappfactory-static-images.s3.us-east-2.amazonaws.com/RestaurentApp/Mobile+Demo+Screens/iPhone+XS+7%402x.png",
      name: "Feedback Submit Page"
    },
    {
      img: "https://upappfactory-static-images.s3.us-east-2.amazonaws.com/RestaurentApp/Mobile+Demo+Screens/iPhone+XS+8%402x.png",
      name: "Loyalty Page"
    },
    {
      img: "https://upappfactory-static-images.s3.us-east-2.amazonaws.com/RestaurentApp/Mobile+Demo+Screens/iPhone+XS+9%402x.png",
      name: "Promotion Page"
    },
    {
      img: "https://upappfactory-static-images.s3.us-east-2.amazonaws.com/RestaurentApp/Mobile+Demo+Screens/iPhone+XS+10.png",
      name: "Near Me Page"
    },
    {
      img: "https://upappfactory-static-images.s3.us-east-2.amazonaws.com/RestaurentApp/Mobile+Demo+Screens/iPhone+XS+12%402x.png",
      name: "Reservation Page"
    },
    {
      img: "https://upappfactory-static-images.s3.us-east-2.amazonaws.com/RestaurentApp/Mobile+Demo+Screens/iPhone+XS+13%402x.png",
      name: "Reservation Confirmed Page"
    },

    {
      img: "https://upappfactory-static-images.s3.us-east-2.amazonaws.com/RestaurentApp/Mobile+Demo+Screens/iPhone+XS+18%402x.png",
      name: "Notification Page"
    },
    {
      img: "https://upappfactory-static-images.s3.us-east-2.amazonaws.com/RestaurentApp/Mobile+Demo+Screens/iPhone+XS+16%402x.png",
      name: "My Order Page"
    },
    {
      img: "https://upappfactory-static-images.s3.us-east-2.amazonaws.com/RestaurentApp/Mobile+Demo+Screens/iPhone+XS+17%402x.png",
      name: "Order Confirmed Page"
    },
  ];

  const keyFeatures = [
    {
      icon:
        "https://upappfactory-static-images.s3.us-east-2.amazonaws.com/RestaurentApp/Icon/OrderManagement.svg",
      title: `${t("restaurentApp.OrderManagement")}`
    },
    {
      icon:
        "https://upappfactory-static-images.s3.us-east-2.amazonaws.com/RestaurentApp/Icon/TableReservation.svg",
      title: `${t("restaurentApp.OnlineTableReservation")}`
    },
    {
      icon:
        "https://upappfactory-static-images.s3.us-east-2.amazonaws.com/RestaurentApp/Icon/OnlineReview.svg",
      title: `${t("restaurentApp.OnlineReview")}`
    },
    {
      icon:
        "https://upappfactory-static-images.s3.us-east-2.amazonaws.com/RestaurentApp/Icon/DigitalMenu.svg",
      title: `${t("restaurentApp.OnlineDigitalMenu")}`
    },
    {
      icon:
        "https://upappfactory-static-images.s3.us-east-2.amazonaws.com/RestaurentApp/Icon/coupons.svg",
      title: `${t("restaurentApp.Coupon&Promotional")}`
    },
    {
      icon:
        "https://upappfactory-static-images.s3.us-east-2.amazonaws.com/RestaurentApp/Icon/OnlinePayments.svg",
      title: `${t("restaurentApp.OnlinePayments")}`
    },
    {
      icon: "https://upappfactory-static-images.s3.us-east-2.amazonaws.com/RestaurentApp/Icon/LoyaltyManagement.svg",
      title: `${t("restaurentApp.LoyaltyPoints")}`
    }
  ];

  useEffect(() => {
    if (sessionStorage.getItem("currency") === "KWD") {
      setAppData({
        ...appDetails,
        country: sessionStorage.getItem("country"),
        currency: "KWD",
        // Mobile
        newMobileBDSPM: Math.round(3.08),
        newMobileADSPM: Math.round(3.08),
        newMobileBDSPY: Math.round(3.08),
        newMobileADSPY: Math.round(3.08),
        MSC: Math.round(15.69),
        // Plan B
        MFPP: Math.round(15.69)
      });
    } else if (sessionStorage.getItem("currency") === "AED") {
      setAppData({
        ...appDetails,
        country: sessionStorage.getItem("country"),
        currency: "AED",
        // Mobile
        newMobileBDSPM: Math.round(36.36),
        newMobileADSPM: Math.round(36.36),
        newMobileBDSPY: Math.round(36.36),
        newMobileADSPY: Math.round(36.36),
        // setup charge
        MSC: Math.round(183.29),
        // Plan B
        MFPP: Math.round(183.29)
      });
    } else if (sessionStorage.getItem("currency") === "SGD") {
      setAppData({
        ...appDetails,
        country: sessionStorage.getItem("country"),
        currency: "SGD",
        // Mobile
        newMobileBDSPM: Math.round(14.32),
        newMobileADSPM: Math.round(14.32),
        newMobileBDSPY: Math.round(14.32),
        newMobileADSPY: Math.round(14.32),
        MSC: Math.round(71.22),
        // Plan B
        MFPP: Math.round(71.22)
      });
    } else if (sessionStorage.getItem("currency") === "OMR") {
      setAppData({
        ...appDetails,
        country: sessionStorage.getItem("country"),
        currency: "OMR",
        // Mobile
        newMobileBDSPM: Math.round(3.81),
        newMobileADSPM: Math.round(3.81),
        newMobileBDSPY: Math.round(3.81),
        newMobileADSPY: Math.round(3.81),
        MSC: Math.round(19.22),
        //  Plan B
        MFPP: Math.round(19.22)
      });
    } else if (sessionStorage.getItem("currency") === "QAR") {
      setAppData({
        ...appDetails,
        country: sessionStorage.getItem("country"),
        currency: "QAR",
        // Mobile
        newMobileBDSPM: Math.round(36.05),
        newMobileADSPM: Math.round(36.05),
        newMobileBDSPY: Math.round(36.05),
        newMobileADSPY: Math.round(36.05),
        MSC: Math.round(181.66),
        //  Plan B
        MFPP: Math.round(181.66)
      });
    } else if (sessionStorage.getItem("currency") === "BHD") {
      setAppData({
        ...appDetails,
        country: sessionStorage.getItem("country"),
        currency: "BD",

        // Mobile
        newMobileBDSPM: Math.round(3.74),
        newMobileADSPM: Math.round(3.74),
        newMobileBDSPY: Math.round(3.74),
        newMobileADSPY: Math.round(3.74),
        MSC: Math.round(18.80),
        //  Plan B
        MFPP: Math.round(18.80)
      });
    } else if (sessionStorage.getItem("currency") === "MYR") {
      setAppData({
        ...appDetails,
        country: sessionStorage.getItem("country"),
        currency: "MYR",
        // Mobile
        newMobileBDSPM: Math.round(43.86),
        newMobileADSPM: Math.round(43.86),
        newMobileBDSPY: Math.round(43.86),
        newMobileADSPY: Math.round(43.86),
        MSC: Math.round(216.71),
        //  Plan B
        MFPP: Math.round(216.71)
      });
    } else if (sessionStorage.getItem("currency") === "LKR") {
      setAppData({
        ...appDetails,
        country: sessionStorage.getItem("country"),
        currency: "LKR",

        // Mobile
        newMobileBDSPM: Math.round(1857.55),
        newMobileADSPM: Math.round(1857.55),
        newMobileBDSPY: Math.round(1857.55),
        newMobileADSPY: Math.round(1857.55),
        MSC: Math.round(9310.65),
        //  Plan B
        MFPP: Math.round(9310.65)
      });
    } else if (sessionStorage.getItem("currency") === "EGP") {
      setAppData({
        ...appDetails,
        country: sessionStorage.getItem("country"),
        currency: "EGP",

        // Mobile
        newMobileBDSPM: Math.round(155.93),
        newMobileADSPM: Math.round(155.93),
        newMobileBDSPY: Math.round(155.93),
        newMobileADSPY: Math.round(155.93),
        MSC: Math.round(787.31),
        //  Plan B
        MFPP: Math.round(787.31)
      });
    } else if (sessionStorage.getItem("currency") === "SAR") {
      setAppData({
        ...appDetails,
        country: sessionStorage.getItem("country"),
        currency: "SAR",
        // Mobile
        newMobileBDSPM: Math.round(37.19),
        newMobileADSPM: Math.round(37.19),
        newMobileBDSPY: Math.round(37.19),
        newMobileADSPY: Math.round(37.19),
        MSC: Math.round(187.71),
        //  Plan B
        MFPP: Math.round(74.76)
      });
    } else if (sessionStorage.getItem("currency") === "JOD") {
      setAppData({
        ...appDetails,
        country: sessionStorage.getItem("country"),
        currency: "JOD",

        // Mobile
        newMobileBDSPM: Math.round(7.02),
        newMobileADSPM: Math.round(7.02),
        newMobileBDSPY: Math.round(7.02),
        newMobileADSPY: Math.round(7.02),
        MSC: Math.round(35.38),
        //  Plan B
        MFPP: Math.round(35.38)
      });
    } else if (sessionStorage.getItem("currency") === "KYD") {
      setAppData({
        ...appDetails,
        country: sessionStorage.getItem("country"),
        currency: "KYD",
        // Mobile
        newMobileBDSPM: Math.round(9.16),
        newMobileADSPM: Math.round(9.16),
        newMobileBDSPY: Math.round(9.16),
        newMobileADSPY: Math.round(9.16),
        MSC: Math.round(45.04),
        //  Plan B
        MFPP: Math.round(45.04)
      });
    } else if (sessionStorage.getItem("currency") === "ZAR") {
      setAppData({
        ...appDetails,
        country: sessionStorage.getItem("country"),
        currency: "ZAR",
        // Mobile
        newMobileBDSPM: Math.round(174.04),
        newMobileADSPM: Math.round(174.04),
        newMobileBDSPY: Math.round(174.04),
        newMobileADSPY: Math.round(174.04),
        MSC: Math.round(889.77),
        //  Plan B
        MFPP: Math.round(889.77)
      });
    } else if (sessionStorage.getItem("currency") === "USD") {
      setAppData({
        ...appDetails,
        country: sessionStorage.getItem("country"),
        currency: "USD",
        // Mobile
        newMobileBDSPM: 9.9,
        newMobileADSPM: 9.9,
        newMobileBDSPY: 9.9,
        newMobileADSPY: 9.9,

        MSC: Math.round(49.9),
        //  Plan B
        MFPP: 49.9
      });
    } else {
      setAppData({
        ...appDetails,
        country: sessionStorage.getItem("country"),
        currency: sessionStorage.getItem("currency_sign"),

        // Mobile
        newMobileBDSPM: Math.round(
          9.9 * sessionStorage.getItem("currency_rate")
        ),
        newMobileADSPM: Math.round(
          9.9 * sessionStorage.getItem("currency_rate")
        ),
        newMobileBDSPY: Math.round(
          9.9 * sessionStorage.getItem("currency_rate")
        ),
        newMobileADSPY: Math.round(
          9.9 * sessionStorage.getItem("currency_rate")
        ),
        MSC: Math.round(49.9 * sessionStorage.getItem("currency_rate")),
        //  Plan B
        MFPP: Math.round(49.9 * sessionStorage.getItem("currency_rate"))
      });
    }
  }, [country, currency]);

  return (
    <div>
      <ScrollTop />
      <Helmet>
        {/* <title>UPapp factory - Restaurant Application</title> */}
        <title>Restaurant Application for your business | Android & IOS | UPapp factory</title>
        <meta name="description" content="Create your own Restaurant Application in Android and iOS for your Business. Deliver a Delicious Experience With Your Own Restaurant App." />
        <meta name="keywords" content="Restaurant Application Development, Food Delivery System, Online Food Ordering platform" />
      </Helmet>
      <section
        className="UAF-container-fluid"
        style={{
          background: "#fff",
          padding: 0,
          maxWidth: "1200px",
          margin: "100px auto 180px auto"
        }}
      >
        <Row type="flex" gutter={50}>
          <Col md={{ span: 12, order: 1 }} lg={{ span: 12, order: 1 }} xs={{ span: 24, order: 2 }}>
            {direction === DIRECTIONS.LTR && <Fragment>
              <div className="tagline-heading" style={{ padding: "auto 15px" }}>
                <Title level={1} className="texty-demo" style={{}}>
                  {direction === DIRECTIONS.LTR && <Fragment>
                    {t("restaurentApp.tagline1")} <br /> {t("restaurentApp.tagline2")} <br />{" "}
                    {t("restaurentApp.tagline3")}
                  </Fragment>
                  }
                  {direction === DIRECTIONS.RTL && <Fragment>
                    {t("restaurentApp.tagline1")}
                  </Fragment>
                  }
                </Title>
                {/* <Link to="/factory">
                  <Button size={"large"} className="slider-get-started-btn uaf_allPageBtn_bg_Color">
                    {t("General.GetStarted")}
                  </Button>
                </Link> */}
                <a href="#form_section">
                  <Button size={"large"} className="slider-get-started-btn uaf_allPageBtn_bg_Color">
                    {t("General.GetStarted")}
                  </Button>
                </a>
              </div>
            </Fragment>
            }
            {direction === DIRECTIONS.RTL && <Fragment>
              <div className="tagline-heading lg-rtl" style={{ padding: "auto 15px" }}>
                <Title level={1} className="texty-demo" style={{}}>
                  {direction === DIRECTIONS.LTR && <Fragment>
                    {t("restaurentApp.tagline1")} <br /> {t("restaurentApp.tagline2")} <br />{" "}
                    {t("restaurentApp.tagline3")}
                  </Fragment>
                  }
                  {direction === DIRECTIONS.RTL && <Fragment>
                    {t("restaurentApp.tagline1")}
                  </Fragment>
                  }
                </Title>
                {/* <Link to="/factory">
                  <Button size={"large"} className="slider-get-started-btn main lg-float-right uaf_allPageBtn_bg_Color">
                    {t("General.GetStarted")}
                  </Button>
                </Link> */}
                <a href="#form_section">
                  <Button size={"large"} className="slider-get-started-btn main lg-float-right uaf_allPageBtn_bg_Color">
                    {t("General.GetStarted")}
                  </Button>
                </a>
              </div>
            </Fragment>
            }

          </Col>
          <Col md={{ span: 12, order: 2 }} lg={{ span: 12, order: 2 }} xs={{ span: 24, order: 1 }}>
            <div className="home-feature-img main">
              <img
                style={{ marginTop: "-50px" }}
                src="https://upappfactory-static-images.s3.us-east-2.amazonaws.com/RestaurentApp/FeaturedIMG.png"
                alt="Restaurant Application"
              />
            </div>
          </Col>
        </Row>
      </section>
      <section
        className="UAF-container-fluid"
        style={{
          background: "#fff",
          padding: 0,
          maxWidth: "1200px",
          margin: "0 auto 80px auto",
          textAlign: "center"
        }}
      >
        <p style={{ fontSize: "45px", marginBottom: "30px" }}> {t("restaurentApp.KeyFeatures")} </p>
        <Row type="flex" justify="space-around" align="middle">
          {keyFeatures.map(index => {
            return (
              <Col xs={12} sm={6} lg={3}>
                <div
                  style={{
                    width: "100%",
                    maxWidth: "180px",
                    height: "130px",
                    margin: "30px auto"
                  }}
                >
                  <img
                    style={{ width: "100px", height: "100px" }}
                    src={index.icon}
                    alt=""
                  />
                  <p>{index.title}</p>
                </div>
              </Col>
            );
          })}
        </Row>
      </section>

      <section
        className="UAF-container-fluid"
        style={{
          margin: "80px auto 0px auto"
        }}
      >
        <div
          style={{
            maxWidth: "1200px",
            margin: "0 auto",
            textAlign: "center",
            padding: "25px"
          }}
        >
          <Row type="flex" gutter={30}>
            <Col md={{ span: 12, order: 2 }} lg={{ span: 12, order: 2 }} xs={{ span: 24, order: 2 }}>
              {direction === DIRECTIONS.LTR && <Fragment>
                <div
                  className="tagline-heading tagline-body sub-points lg-rtl"
                  style={{ padding: "auto 15px" }}
                >
                  <Title level={1} className="texty-demo lg-rtl">
                    {t("ecommercePage.layer4.title1")}
                  </Title>
                  <Title
                    level={1}
                    className="text-demo-sub-text lg-rtl lg-float-right"
                    style={{
                      fontSize: "18px",
                      lineHeight: "20px",
                      fontWeight: 400,
                      maxWidth: "100%"
                    }}
                  >
                    {t("ecommercePage.layer4.desc")}
                  </Title>

                  {/* <Link to="/factory">
                    <Button size={"large"} className="slider-get-started-btn lg-float-right uaf_allPageBtn_bg_Color">
                      {t("General.GetStarted")}
                    </Button>
                  </Link> */}
                  <a href="#form_section">
                    <Button size={"large"} className="slider-get-started-btn lg-float-right uaf_allPageBtn_bg_Color">
                      {t("General.GetStarted")}
                    </Button>
                  </a>
                </div>
              </Fragment>
              }
              {direction === DIRECTIONS.RTL && <Fragment>
                <div
                  className="tagline-heading tagline-body sub-points lg-rtl lg-p-rtl-15"
                  style={{ padding: "auto 15px" }}
                >
                  <Title level={1} className="texty-demo lg-rtl">
                    {t("ecommercePage.layer4.title1")}
                  </Title>
                  <Title
                    level={1}
                    className="text-demo-sub-text lg-rtl lg-float-right"
                    style={{
                      fontSize: "18px",
                      lineHeight: "20px",
                      fontWeight: 400,
                      maxWidth: "100%"
                    }}
                  >
                    {t("ecommercePage.layer4.desc")}
                  </Title>

                  {/* <Link to="/factory">
                    <Button size={"large"} className="slider-get-started-btn lg-float-right uaf_allPageBtn_bg_Color">
                      {t("General.GetStarted")}
                    </Button>
                  </Link> */}
                  <a href="#form_section">
                    <Button size={"large"} className="slider-get-started-btn lg-float-right uaf_allPageBtn_bg_Color">
                      {t("General.GetStarted")}
                    </Button>
                  </a>
                </div>
              </Fragment>
              }
            </Col>
            <Col md={{ span: 12, order: 1 }} lg={{ span: 12, order: 1 }} xs={{ span: 24, order: 1 }}>
              <div className="home-feature-img feature-img-body">
                <img
                  style={{ maxWidth: "200px" }}
                  src={AndroidIosAppIncluded}
                  alt="image with animation for upapp"
                />
              </div>
            </Col>
          </Row>
        </div>
      </section>

      <section
        className="UAF-container-fluid"
        style={{
          margin: "0px auto 60px auto"
        }}
      >
        <div
          style={{
            maxWidth: "1200px",
            margin: "0 auto",
            textAlign: "center",
            padding: "25px"
          }}
        >
          {direction === DIRECTIONS.LTR && <Fragment>
            <p
              style={{
                fontSize: "45px",
                marginTop: "100px",
                marginBottom: "80px",
                lineHeight: "1"
              }}
            >
              {t("restaurentApp.restaurantNeeds1")}
              <br />
              {t("restaurentApp.restaurantNeeds2")}
            </p>
          </Fragment>
          }
          {direction === DIRECTIONS.RTL && <Fragment>
            <p
              style={{
                fontSize: "45px",
                marginTop: "100px",
                marginBottom: "80px",
                lineHeight: "1"
              }}
            >
              {t("restaurentApp.restaurantNeeds1")}
              <br />
              {t("restaurentApp.restaurantNeeds2")}
            </p>
          </Fragment>
          }

          <Row type="flex" justify="center" align="top">
            <Col md={{ span: 12, order: 1 }} lg={{ span: 12, order: 1 }} xs={{ span: 24, order: 2 }}>
              {direction === DIRECTIONS.LTR && <Fragment>
                <div
                  className="tagline-heading tagline-body sub-points"
                  style={{ padding: "auto 15px" }}
                >
                  <Title level={1} className="texty-demo lg-ltr" style={{}}>
                    {t("restaurentApp.multipleMode")}
                    <br />
                  </Title>
                  <p
                    className="text-demo-sub-text lg-ltr"
                    style={{
                      fontSize: "18px",
                      // lineHeight: "20px",
                      fontWeight: 400,
                      maxWidth: "100%"
                    }}
                  >
                    <ul className="m-dir-ltr m-align-left facility-lists">
                      <li>
                        <p>
                          {" "}
                        Order Online - Order Online & Get Delivered your order
                        on Time
                      </p>
                      </li>
                      <li>
                        <p>
                          Take Away - Order Online & Collect your Order from
                          restaurant
                      </p>
                      </li>
                      <li>
                        <p>
                          {" "}
                        Serve at Outlet -Visit Restaurant & Get Served The
                        Delicious Food
                      </p>
                      </li>
                    </ul>
                  </p>

                  {/* <Link to="/factory/booking">
                    <Button
                      size={"large"}
                      style={{
                        background: "#4FBFDD",
                        color: "#fff",
                        marginTop: "20px"
                      }}
                      className="slider-get-started-btn lg-float-left"
                    >
                      {t("General.GetStarted")}
                    </Button>
                  </Link> */}
                  <a href="#form_section">
                    <Button size={"large"} className="slider-get-started-btn lg-float-left uaf_allPageBtn_bg_Color">
                      {t("General.GetStarted")}
                    </Button>
                  </a>
                </div>
              </Fragment>
              }
              {direction === DIRECTIONS.RTL && <Fragment>
                <div
                  className="tagline-heading tagline-body sub-points"
                  style={{ padding: "auto 15px" }}
                >
                  <Title level={1} className="texty-demo lg-rtl" style={{}}>
                    {t("restaurentApp.multipleMode")}
                    <br />
                  </Title>
                  <p
                    className="text-demo-sub-text lg-rtl"
                    style={{
                      fontSize: "18px",
                      // lineHeight: "20px",
                      fontWeight: 400,
                      maxWidth: "100%"
                    }}
                  >
                    <ul className="m-dir-rtl m-align-right facility-lists">
                      <li>
                        <p>
                          {" "}
                          {t("restaurentApp.Layer1.1")}
                        </p>
                      </li>
                      <li>
                        <p>
                          {t("restaurentApp.Layer1.2")}
                        </p>
                      </li>
                      <li>
                        <p>
                          {" "}
                          {t("restaurentApp.Layer1.3")}
                        </p>
                      </li>
                    </ul>
                  </p>

                  {/* <Link to="/factory/booking">
                    <Button
                      size={"large"}
                      style={{
                        background: "#4FBFDD",
                        color: "#fff",
                        marginTop: "20px"
                      }}
                      className="slider-get-started-btn lg-float-right"
                    >
                      {t("General.GetStarted")}
                    </Button>
                  </Link> */}
                  <a href="#form_section">
                    <Button size={"large"} className="slider-get-started-btn lg-float-right uaf_allPageBtn_bg_Color">
                      {t("General.GetStarted")}
                    </Button>
                  </a>
                </div>
              </Fragment>
              }
            </Col>
            <Col md={{ span: 12, order: 2 }} lg={{ span: 12, order: 2 }} xs={{ span: 24, order: 1 }}>
              <div className="home-feature-img feature-img-body">
                <img
                  style={{ maxWidth: "200px" }}
                  src="https://upappfactory-static-images.s3.us-east-2.amazonaws.com/RestaurentApp/multipleMode+OfService.svg"
                  alt="image with animation for upapp"
                />
              </div>
            </Col>
          </Row>
        </div>
      </section>

      <section
        className="UAF-container-fluid"
        style={{
          margin: "0px auto 60px auto"
        }}
      >
        <div
          style={{
            maxWidth: "1200px",
            margin: "0 auto",
            textAlign: "center",
            padding: "25px"
          }}
        >
          <Row type="flex" justify="center" align="top" gutter={30}>
            <Col md={{ span: 12, order: 2 }} lg={{ span: 12, order: 2 }} xs={{ span: 24, order: 2 }}>
              {direction === DIRECTIONS.LTR && <Fragment>
                <div
                  className="tagline-heading tagline-body sub-points lg-rtl"
                  style={{ padding: "auto 15px" }}
                >
                  <Title level={1} className="texty-demo lg-rtl" style={{}}>
                    {t("restaurentApp.deliverFastOrder")}
                    <br />
                  </Title>
                  <p
                    className="text-demo-sub-text lg-rtl lg-float-right"
                    style={{
                      fontSize: "18px",
                      lineHeight: "20px",
                      fontWeight: 400,
                      maxWidth: "100%"
                    }}
                  >
                    <div
                      dir="rtl"
                      className="m-dir-ltr m-align-left facility-lists facility-lists-right"
                    >
                      <p>
                        {t("restaurentApp.Layer2.subtitle")}
                      </p>
                    </div>
                  </p>

                  {/* <Link to="/factory/booking">
                    <Button
                      size={"large"}
                      style={{
                        background: "#4FBFDD",
                        color: "#fff",
                        marginTop: "0px"
                      }}
                      className="slider-get-started-btn lg-float-right"
                    >
                      {t("General.GetStarted")}
                    </Button>
                  </Link> */}
                  <a href="#form_section">
                    <Button size={"large"} className="slider-get-started-btn lg-float-right uaf_allPageBtn_bg_Color">
                      {t("General.GetStarted")}
                    </Button>
                  </a>
                </div>
              </Fragment>
              }
              {direction === DIRECTIONS.RTL && <Fragment>
                <div
                  className="tagline-heading tagline-body sub-points lg-rtl"
                  style={{ padding: "auto 15px" }}
                >
                  <Title level={1} className="texty-demo lg-rtl" style={{}}>
                    {t("restaurentApp.deliverFastOrder")}
                    <br />
                  </Title>
                  <p
                    className="text-demo-sub-text lg-rtl lg-float-right"
                    style={{
                      fontSize: "18px",
                      lineHeight: "20px",
                      fontWeight: 400,
                      maxWidth: "100%"
                    }}
                  >
                    <div
                      dir="ltr"
                      className="m-dir-ltr m-align-left facility-lists facility-lists-right"
                    >
                      <p>
                        {t("restaurentApp.Layer2.subtitle")}
                      </p>
                    </div>
                  </p>

                  {/* <Link to="/factory/booking">
                    <Button
                      // type="dashed"
                      size={"large"}
                      style={{
                        background: "#4FBFDD",
                        color: "#fff",
                        marginTop: "0px"
                      }}
                      className="slider-get-started-btn lg-float-right"
                    >
                      {t("General.GetStarted")}
                    </Button>
                  </Link> */}
                  <a href="#form_section">
                    <Button size={"large"} className="slider-get-started-btn lg-float-right uaf_allPageBtn_bg_Color">
                      {t("General.GetStarted")}
                    </Button>
                  </a>
                </div>
              </Fragment>
              }
            </Col>
            <Col md={{ span: 12, order: 1 }} lg={{ span: 12, order: 1 }} xs={{ span: 24, order: 1 }}>
              <div className="home-feature-img feature-img-body">
                <img
                  style={{ maxWidth: "200px" }}
                  src="https://upappfactory-static-images.s3.us-east-2.amazonaws.com/RestaurentApp/DeliverOrders+FasterFlip.svg"
                  alt="Deliver food faster"
                />
              </div>
            </Col>
          </Row>
        </div>
      </section>

      <section
        className="UAF-container-fluid"
        style={{
          margin: "0px auto 60px auto"
        }}
      >
        <div
          style={{
            maxWidth: "1200px",
            margin: "0 auto",
            textAlign: "center",
            padding: "25px"
          }}
        >
          <Row type="flex" justify="center" align="top">
            <Col md={{ span: 12, order: 1 }} lg={{ span: 12, order: 1 }} xs={{ span: 24, order: 2 }}>
              {direction === DIRECTIONS.LTR && <Fragment>
                <div
                  className="tagline-heading tagline-body sub-points"
                  style={{ padding: "auto 15px" }}
                >
                  <Title level={1} className="texty-demo lg-ltr" style={{}}>
                    {t("restaurentApp.reservationProcess1")}
                    <br />
                    {t("restaurentApp.reservationProcess2")}
                  </Title>
                  <p
                    className="text-demo-sub-text lg-ltr"
                    style={{
                      fontSize: "18px",
                      lineHeight: "20px",
                      fontWeight: 400,
                      maxWidth: "100%"
                    }}
                  >
                    {t("restaurentApp.Layer3.subtitle")}
                  </p>

                  {/* <Link to="/factory/booking">
                    <Button
                      // type="dashed"
                      size={"large"}
                      style={{
                        background: "#4FBFDD",
                        color: "#fff",
                        marginTop: "20px"
                      }}
                      className="slider-get-started-btn lg-float-left"
                    >
                      {t("General.GetStarted")}
                    </Button>
                  </Link> */}
                  <a href="#form_section">
                    <Button size={"large"} className="slider-get-started-btn lg-float-left uaf_allPageBtn_bg_Color">
                      {t("General.GetStarted")}
                    </Button>
                  </a>
                </div>

              </Fragment>
              }
              {direction === DIRECTIONS.RTL && <Fragment>
                <div
                  className="tagline-heading tagline-body sub-points"
                  style={{ padding: "auto 15px" }}
                >
                  <Title level={1} className="texty-demo lg-rtl" style={{}}>
                    {t("restaurentApp.reservationProcess1")}
                    <br />
                    {t("restaurentApp.reservationProcess2")}
                  </Title>
                  <p
                    className="text-demo-sub-text lg-rtl"
                    style={{
                      fontSize: "18px",
                      lineHeight: "20px",
                      fontWeight: 400,
                      maxWidth: "100%"
                    }}
                  >
                    {t("restaurentApp.Layer3.subtitle")}
                  </p>

                  {/* <Link to="/factory/booking">
                    <Button
                      // type="dashed"
                      size={"large"}
                      style={{
                        background: "#4FBFDD",
                        color: "#fff",
                        marginTop: "20px"
                      }}
                      className="slider-get-started-btn lg-float-right"
                    >
                      {t("General.GetStarted")}
                    </Button>
                  </Link> */}
                  <a href="#form_section">
                    <Button size={"large"} className="slider-get-started-btn lg-float-right uaf_allPageBtn_bg_Color">
                      {t("General.GetStarted")}
                    </Button>
                  </a>
                </div>

              </Fragment>
              }
            </Col>
            <Col md={{ span: 12, order: 2 }} lg={{ span: 12, order: 2 }} xs={{ span: 24, order: 1 }}>
              <div className="home-feature-img feature-img-body">
                <img
                  style={{ maxWidth: "200px" }}
                  src="https://upappfactory-static-images.s3.us-east-2.amazonaws.com/RestaurentApp/TableReservation.svg"
                  alt="image with animation for upapp"
                />
              </div>
            </Col>
          </Row>
        </div>
      </section>
      <section
        className="UAF-container-fluid"
        style={{
          margin: "0px auto 60px auto"
        }}
      >
        <div
          style={{
            maxWidth: "1200px",
            margin: "0 auto",
            textAlign: "center",
            padding: "25px"
          }}
        >
          <Row type="flex" justify="center" align="middle" gutter={30}>
            <Col md={{ span: 12, order: 2 }} lg={{ span: 12, order: 2 }} xs={{ span: 24, order: 2 }}>
              {direction === DIRECTIONS.LTR && <Fragment>
                <div
                  className="tagline-heading tagline-body sub-points lg-rtl"
                  style={{ padding: "auto 15px" }}
                >
                  <Title level={1} className="texty-demo lg-rtl" style={{}}>
                    {t("restaurentApp.GoodByePaperless1")}
                    <br />
                    {t("restaurentApp.GoodByePaperless2")}
                    <br />
                  </Title>
                  <p
                    className="text-demo-sub-text lg-rtl lg-float-right"
                    style={{
                      fontSize: "18px",
                      lineHeight: "20px",
                      fontWeight: 400,
                      maxWidth: "100%"
                    }}
                  >
                    {t("restaurentApp.Layer4.subtitle")}
                  </p>

                  {/* <Link to="/factory/booking">
                    <Button
                      // type="dashed"
                      size={"large"}
                      style={{
                        background: "#4FBFDD",
                        color: "#fff",
                        marginTop: "0px"
                      }}
                      className="slider-get-started-btn lg-float-right"
                    >
                      {t("General.GetStarted")}
                    </Button>
                  </Link> */}
                  <a href="#form_section">
                    <Button size={"large"} className="slider-get-started-btn lg-float-right uaf_allPageBtn_bg_Color">
                      {t("General.GetStarted")}
                    </Button>
                  </a>
                </div>
              </Fragment>
              }
              {direction === DIRECTIONS.RTL && <Fragment>
                <div
                  className="tagline-heading tagline-body sub-points lg-rtl"
                  style={{ padding: "auto 15px" }}
                >
                  <Title level={1} className="texty-demo lg-rtl" style={{}}>
                    {t("restaurentApp.GoodByePaperless1")}

                  </Title>
                  <p
                    className="text-demo-sub-text lg-rtl lg-float-right"
                    style={{
                      fontSize: "18px",
                      lineHeight: "20px",
                      fontWeight: 400,
                      maxWidth: "100%"
                    }}
                  >
                    {t("restaurentApp.Layer4.subtitle")}
                  </p>

                  {/* <Link to="/factory/booking">
                    <Button
                      // type="dashed"
                      size={"large"}
                      style={{
                        background: "#4FBFDD",
                        color: "#fff",
                        marginTop: "0px"
                      }}
                      className="slider-get-started-btn lg-float-right"
                    >
                      {t("General.GetStarted")}
                    </Button>
                  </Link> */}
                  <a href="#form_section">
                    <Button size={"large"} className="slider-get-started-btn lg-float-right uaf_allPageBtn_bg_Color">
                      {t("General.GetStarted")}
                    </Button>
                  </a>
                </div>
              </Fragment>
              }
            </Col>
            <Col md={{ span: 12, order: 1 }} lg={{ span: 12, order: 1 }} xs={{ span: 24, order: 1 }}>
              <div className="home-feature-img feature-img-body">
                <img
                  style={{ maxWidth: "200px" }}
                  src="https://upappfactory-static-images.s3.us-east-2.amazonaws.com/RestaurentApp/RatingReviewSm.svg"
                  alt="Rating Reviews"
                />
              </div>
            </Col>
          </Row>
        </div>
      </section>
      <section
        className="UAF-container-fluid"
        style={{
          margin: "0px auto 60px auto"
        }}
      >
        <div
          style={{
            maxWidth: "1200px",
            margin: "0 auto",
            textAlign: "center",
            padding: "25px"
          }}
        >
          <Row type="flex" justify="center" align="top">
            <Col md={{ span: 12, order: 1 }} lg={{ span: 12, order: 1 }} xs={{ span: 24, order: 2 }}>
              {direction === DIRECTIONS.LTR && <Fragment>
                <div
                  className="tagline-heading tagline-body sub-points"
                  style={{ padding: "auto 15px" }}
                >
                  <Title level={1} className="texty-demo lg-ltr" style={{}}>
                    {t("restaurentApp.Layer5.title")}

                  </Title>
                  <p
                    className="text-demo-sub-text lg-ltr"
                    style={{
                      fontSize: "18px",
                      lineHeight: "20px",
                      fontWeight: 400,
                      maxWidth: "100%"
                    }}
                  >
                    {t("restaurentApp.Layer5.subtitle")}
                  </p>

                  {/* <Link to="/factory/booking">
                    <Button
                      // type="dashed"
                      size={"large"}
                      style={{
                        background: "#4FBFDD",
                        color: "#fff",
                        marginTop: "20px"
                      }}
                      className="slider-get-started-btn lg-float-left"
                    >
                      {t("General.GetStarted")}
                    </Button>
                  </Link> */}
                  <a href="#form_section">
                    <Button size={"large"} className="slider-get-started-btn lg-float-left uaf_allPageBtn_bg_Color">
                      {t("General.GetStarted")}
                    </Button>
                  </a>
                </div>

              </Fragment>
              }
              {direction === DIRECTIONS.RTL && <Fragment>
                <div
                  className="tagline-heading tagline-body sub-points"
                  style={{ padding: "auto 15px", marginTop: "50px" }}
                >
                  <Title level={1} className="texty-demo lg-rtl" style={{}}>
                    {t("restaurentApp.Layer5.title")}
                  </Title>
                  <p
                    className="text-demo-sub-text lg-rtl"
                    style={{
                      fontSize: "18px",
                      lineHeight: "20px",
                      fontWeight: 400,
                      maxWidth: "100%"
                    }}
                  >
                    {t("restaurentApp.Layer5.subtitle")}
                  </p>

                  {/* <Link to="/factory/booking">
                    <Button
                      // type="dashed"
                      size={"large"}
                      style={{
                        background: "#4FBFDD",
                        color: "#fff",
                        marginTop: "20px"
                      }}
                      className="slider-get-started-btn lg-float-right"
                    >
                      {t("General.GetStarted")}
                    </Button>
                  </Link> */}
                  <a href="#form_section">
                    <Button size={"large"} className="slider-get-started-btn lg-float-right uaf_allPageBtn_bg_Color">
                      {t("General.GetStarted")}
                    </Button>
                  </a>
                </div>

              </Fragment>
              }
            </Col>
            <Col md={{ span: 12, order: 2 }} lg={{ span: 12, order: 2 }} xs={{ span: 24, order: 1 }}>
              <div className="home-feature-img feature-img-body">
                <img
                  style={{ maxWidth: "200px" }}
                  src={LoyalityImg}
                  alt="image with animation for upapp"
                />
              </div>
            </Col>
          </Row>
        </div>
      </section>

      {/* <div className="UAF-container-fluid">
        <div className="single-app-heading-text">
          <br />
        How Your Website Will Look To Your Customers
      </div>
        <WebsiteDemo />
      </div> */}
      <div className="single-app-heading-text">
        <br />
        {t("restaurentApp.yourCutomer")}

      </div>
      <SSsliderFacility screenshots={Restaurant_Screenshot} />
      <br />
      <section className="" style={{ marginTop: "80px", marginBottom: "80px" }} id="plan_section_restaurant">
        <PackagePriceVerticleRestaurant
          currency={currency}
          MBDSPM={newMobileBDSPM}
          MADSPM={newMobileADSPM}
          MBDSPY={newMobileBDSPY}
          MADSPY={newMobileADSPY}
          duration="Mo"
          PackType="Annually"
          MSC={MSC}
          MFPP={MFPP}
        />
      </section>
      <section id="form_section">
        <ContactUsForm />
      </section>
    </div>
  );
};

const HomeRFC = Form.create({ name: "Request_for_customize" })(
  RestaurentLandingPage
);

HomeRFC.propTypes = {
  loading: PropTypes.bool
};

const mapStateToProps = state => ({
  loading: state.application.loading
});

export default connect(mapStateToProps, {})(withDirection(HomeRFC));
