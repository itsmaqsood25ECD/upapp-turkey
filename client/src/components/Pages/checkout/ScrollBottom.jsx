import React, { Component } from 'react'
// import 'react-toggle/style.css'
import BottomScrollListener from 'react-bottom-scroll-listener'

class ScrollBottom extends Component {
  handleOnDocumentBottom = () => {
    console.log('I am at bottom! ' + Math.round(performance.now()))

    if (this.props.alertOnBottom) {
      alert('Bottom hit! Too slow? Reduce "debounce" value in props')
    }
  }

  handleContainerOnBottom = () => {
    console.log('I am at bottom in optional container! ' + Math.round(performance.now()))

    if (this.props.alertOnBottom) {
      alert('Bottom of this container hit! Too slow? Reduce "debounce" value in props')
    }
  }

  render() {
    return (
        <BottomScrollListener onBottom={this.handleContainerOnBottom}>
          {scrollRef => (
            <div ref={scrollRef} className="inner-scroll-example">
              <h4>Callback when this container hits bottom</h4>
              <div>Scroll down! ▼▼▼</div>
              <div>A bit more... ▼▼</div>
              <div>Almost there... ▼</div>
              <div>You've reached the bottom!</div>
            </div>
          )}
        </BottomScrollListener>
    )
  }
}

export default ScrollBottom
