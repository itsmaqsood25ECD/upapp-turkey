import React, { useState, useEffect, Fragment } from "react";
import {
  Typography, Layout, Input, Row, Col, Form, Spin, Radio, Card,
  Checkbox, message, Breadcrumb, Tabs,
  Icon, Button, Steps, Tooltip, Select, Upload, Divider, Modal
} from "antd";
import StripeCheckout from "react-stripe-checkout";
import ScrollTop from "../../ScrollTop";
import { connect } from "react-redux";
import { Redirect } from "react-router-dom";
import "antd/dist/antd.css";
import "../../../assets/css/checkoutResponsive.css";
import "../../../assets/css/custom.css";
import { Helmet } from "react-helmet";
import CurrencyFormat from "react-currency-format";
import withDirection, { withDirectionPropTypes, DIRECTIONS } from 'react-with-direction';
import { useTranslation } from 'react-i18next';
import axios from 'axios'
import MobileAppView from "../../MobileAppView/MobileAppPage";
import data from '../../../data';
import Commoncurrency from '../../../common-currency';
import CountryList from '../../../country-list';
import { proceedToPay } from '../../../actions/checkout.action'
import PropTypes from 'prop-types'
import SingleVendorImg from '../../../assets/img/UP-app-single Vendor.webp'

const { Title, Text } = Typography;
const { TabPane } = Tabs;
const InputGroup = Input.Group;
const { Step } = Steps;
const { Option } = Select;
const { Dragger } = Upload;
const { TextArea } = Input;


const Store = ({ match, proceedToPay, success, order, user, direction }) => {

  const { t } = useTranslation();

  const [state, setState] = useState({
    current: 0,
    fileList: [],
    uploading: false,
    ownDomain: 'Yes',
    MobVisible: false,
    delProvider: 'Not Needed Now',
    PaymentGatewayProvider: 'gateway-paypal',
    trainingHour: 2,
    trainingPrice: 0,
    ProductListRate: '',
    PackageChoosed: match.params.Package,
    packages: '',
    plan: data && data[0].packages.filter(pkg => {
      return pkg.type.includes(match.params.Package)
    }),
    PlanCost: [{
      after_discount_price: '',
      before_discount_price: '',
      type: "",
      value: ""
    }],
    plan_name: '',
    plan_price: '',
    plan_type: '',
    BillingAddressSame: 'Yes',
    languages: [],
    productListCount: 100,
    productListCountPrice: 50,
    paymetGatewayCharge: 0,
    shippingGatewatCharge: 0,
    business_category: "",
    business_email: "",
    business_description: "",
    domain_provider: "",
    domain_username: "",
    domain_password: "",
    Primary_color: " ",
    Secondary_color: " ",
    default_currency: "USD",
    defaultPlanTabSelected: match.params.Plan,
    business_logo: "",
    pg_country: "",
    loading: false,
    one_time_setup_charge: "",
    disablePayment: true,
    selfDomainAdd: false,
    ProductDetailsValidated: false,
    payment_gateway_provider: "",
    payment_gateway_username: "",
    payment_gateway_password: "",
    payment_other_details: "",
    shipping_provider: "",
    shipping_other_details: "",
    sp_country: "SG",
    business_country: "",
    SelfProductUpload: false,
    SGDPrice: '',
    StripeAvaialble: true,
    AboutBusinessWordCount: 250,
    AboutPaymentOtherWordCount: 250,
    AboutShippingOtherWordCount: 250,
    AboutLimWords: 250,
    needABWords: 250
  });

  const {
    packages,
    PackageChoosed,
    productListCount,
    productListCountPrice,
    current,
    ProductListRate,
    delProvider,
    trainingHour,
    trainingPrice,
    PaymentGatewayProvider,
    uploading,
    fileList,
    ownDomain,
    MobVisible,
    PlanChoosed,
    plan,
    languages,
    paymetGatewayCharge,
    BillingAddressSame,
    shippingGatewatCharge,
    PlanCost,
    business_category,
    business_email,
    business_description,
    Primary_color,
    Secondary_color,
    domain_password,
    domain_provider,
    domain_username,
    default_currency,
    plan_name,
    plan_price,
    plan_type,
    defaultPlanTabSelected,
    business_logo,
    payment_gateway_username,
    payment_gateway_password,
    pg_country,
    payment_gateway_provider,
    shipping_provider,
    payment_other_details,
    shipping_other_details,
    one_time_setup_charge,
    sp_country,
    loading,
    disablePayment,
    selfDomainAdd,
    ProductDetailsValidated,
    SelfProductUpload,
    business_country,
    SGDPrice,
    AboutBusinessWordCount,
    AboutPaymentOtherWordCount,
    AboutShippingOtherWordCount,
    AboutLimWords,
    needABWords,
    StripeAvaialble
  } = state

  const StripeCountry = [
    {
      "code": "AT",
      "unicode": "U+1F1E6 U+1F1F9",
      "name": "Austria",
      "emoji": "🇦🇹"
    },
    {
      "code": "AU",
      "unicode": "U+1F1E6 U+1F1FA",
      "name": "Australia",
      "emoji": "🇦🇺"
    },
    {
      "code": "BE",
      "unicode": "U+1F1E7 U+1F1EA",
      "name": "Belgium",
      "emoji": "🇧🇪"
    },
    {
      "code": "BR",
      "unicode": "U+1F1E7 U+1F1F7",
      "name": "Brazil",
      "emoji": "🇧🇷"
    },
    {
      "code": "BG",
      "unicode": "U+1F1E7 U+1F1EC",
      "name": "Bulgaria",
      "emoji": "🇧🇬"
    },
    {
      "code": "CA",
      "unicode": "U+1F1E8 U+1F1E6",
      "name": "Canada",
      "emoji": "🇨🇦"
    },
    {
      "code": "CY",
      "unicode": "U+1F1E8 U+1F1FE",
      "name": "Cyprus",
      "emoji": "🇨🇾"
    },
    {
      "code": "CZ",
      "unicode": "U+1F1E8 U+1F1FF",
      "name": "Czechia",
      "emoji": "🇨🇿"
    },
    {
      "code": "DK",
      "unicode": "U+1F1E9 U+1F1F0",
      "name": "Denmark",
      "emoji": "🇩🇰"
    },
    {
      "code": "EE",
      "unicode": "U+1F1EA U+1F1EA",
      "name": "Estonia",
      "emoji": "🇪🇪"
    },
    {
      "code": "FI",
      "unicode": "U+1F1EB U+1F1EE",
      "name": "Finland",
      "emoji": "🇫🇮"
    },
    {
      "code": "FR",
      "unicode": "U+1F1EB U+1F1F7",
      "name": "France",
      "emoji": "🇫🇷"
    },
    {
      "code": "DE",
      "unicode": "U+1F1E9 U+1F1EA",
      "name": "Germany",
      "emoji": "🇩🇪"
    },
    {
      "code": "GR",
      "unicode": "U+1F1EC U+1F1F7",
      "name": "Greece",
      "emoji": "🇬🇷"
    },
    {
      "code": "HK",
      "unicode": "U+1F1ED U+1F1F0",
      "name": "Hong Kong SAR China",
      "emoji": "🇭🇰"
    },
    {
      "code": "IN",
      "unicode": "U+1F1EE U+1F1F3",
      "name": "India",
      "emoji": "🇮🇳"
    },
    {
      "code": "IE",
      "unicode": "U+1F1EE U+1F1EA",
      "name": "Ireland",
      "emoji": "🇮🇪"
    },
    {
      "code": "IT",
      "unicode": "U+1F1EE U+1F1F9",
      "name": "Italy",
      "emoji": "🇮🇹"
    },
    {
      "code": "JP",
      "unicode": "U+1F1EF U+1F1F5",
      "name": "Japan",
      "emoji": "🇯🇵"
    },
    {
      "code": "LT",
      "unicode": "U+1F1F1 U+1F1F9",
      "name": "Lithuania",
      "emoji": "🇱🇹"
    },
    {
      "code": "LU",
      "unicode": "U+1F1F1 U+1F1FA",
      "name": "Luxembourg",
      "emoji": "🇱🇺"
    },
    {
      "code": "MY",
      "unicode": "U+1F1F2 U+1F1FE",
      "name": "Malaysia",
      "emoji": "🇲🇾"
    },
    {
      "code": "MT",
      "unicode": "U+1F1F2 U+1F1F9",
      "name": "Malta",
      "emoji": "🇲🇹"
    },
    {
      "code": "MX",
      "unicode": "U+1F1F2 U+1F1FD",
      "name": "Mexico",
      "emoji": "🇲🇽"
    },
    {
      "code": "NL",
      "unicode": "U+1F1F3 U+1F1F1",
      "name": "Netherlands",
      "emoji": "🇳🇱"
    },
    {
      "code": "NZ",
      "unicode": "U+1F1F3 U+1F1FF",
      "name": "New Zealand",
      "emoji": "🇳🇿"
    },
    {
      "code": "NO",
      "unicode": "U+1F1F3 U+1F1F4",
      "name": "Norway",
      "emoji": "🇳🇴"
    },
    {
      "code": "PL",
      "unicode": "U+1F1F5 U+1F1F1",
      "name": "Poland",
      "emoji": "🇵🇱"
    },
    {
      "code": "PT",
      "unicode": "U+1F1F5 U+1F1F9",
      "name": "Portugal",
      "emoji": "🇵🇹"
    },
    {
      "code": "RO",
      "unicode": "U+1F1F7 U+1F1F4",
      "name": "Romania",
      "emoji": "🇷🇴"
    },
    {
      "code": "SG",
      "unicode": "U+1F1F8 U+1F1EC",
      "name": "Singapore",
      "emoji": "🇸🇬"
    },
    {
      "code": "SK",
      "unicode": "U+1F1F8 U+1F1F0",
      "name": "Slovakia",
      "emoji": "🇸🇰"
    },
    {
      "code": "SI",
      "unicode": "U+1F1F8 U+1F1EE",
      "name": "Slovenia",
      "emoji": "🇸🇮"
    },

    {
      "code": "ES",
      "unicode": "U+1F1EA U+1F1F8",
      "name": "Spain",
      "emoji": "🇪🇸"
    },
    {
      "code": "SE",
      "unicode": "U+1F1F8 U+1F1EA",
      "name": "Sweden",
      "emoji": "🇸🇪"
    },
    {
      "code": "CH",
      "unicode": "U+1F1E8 U+1F1ED",
      "name": "Switzerland",
      "emoji": "🇨🇭"
    },
    {
      "code": "GB",
      "unicode": "U+1F1EC U+1F1E7",
      "name": "United Kingdom",
      "emoji": "🇬🇧"
    },
    {
      "code": "US",
      "unicode": "U+1F1FA U+1F1F8",
      "name": "United States",
      "emoji": "🇺🇸"
    },
  ]

  useEffect(() => {
    const USDtoSGD = JSON.parse(sessionStorage.getItem("exchange_rate_response"));
    console.log("SGDPrice",)

    console.log("Currency >>", sessionStorage.getItem("currency"))

    console.log("common currency >>", Commoncurrency)

    const pl = data && data[0].packages.filter(pkg => {
      return pkg.type.includes(match.params.Package)
    })

    const plansArray = plan[0].plan.map(p => {
      return p.planCosts.filter(plnCost => {
        return plnCost.value.includes(`${(match.params.Plan).toLowerCase()}-monthly`)
      })
    })
    const p = plansArray.filter(p => {
      return p.length > 0
    })


    setState({
      ...state,
      disabled: false,
      disabled: false,
      plan: pl,
      SGDPrice: USDtoSGD && USDtoSGD.data.quotes.USDSGD,
      plan_name: match.params.Plan,
      plan_price: p && p[0][0].after_discount_price,
      plan_type: p && p[0][0].type,
      one_time_setup_charge: p && p[0][0].OneTimeSetupCost,
      defaultPlanTabSelected: match.params.Plan,
      PlanChoosed: `${(match.params.Plan).toLowerCase()}-monthly`,
      PlanCost: p[0],
      Primary_color: match.params.Package != "Basic" ? '' : "HEX CODE",
      Secondary_color: match.params.Package != "Basic" ? '' : "HEX CODE",
    })
    console.log("match", match)
    // console.log(data);


  }, []);

  if (success) {
    console.log(order)
    return <Redirect to={`/eCommerce-Single-Vendor-Platform/${match.params.Package}/${match.params.Plan}/${match.params.themeID}/checkout/success/${order && order.UID}`}></Redirect>;
  }

  const businessCat = [
    'Art & Design',
    'Auto & Vehicles',
    'Beauty',
    'Books & Reference',
    'Business',
    'Comics',
    'Communication',
    'Dating',
    'Education',
    'Entertainment',
    'Events',
    'Finance',
    'Food & Drink',
    'Health & Fitness',
    'House & Home',
    'Libraries & Demo',
    'LifeStyle',
    'Maps & Navigation',
    'Medical',
    'Music & Audio',
    'News & Magazines',
    'Parenting',
    'Personalization',
    'Photography',
    'Productivity',
    'Shopping',
    'Social',
    'Sports',
    'Tools',
    'Travel & Local',
    'Video Players & Editors',
    'Weather'
  ]

  const handleUpload = async () => {

    setState({
      ...state,
      uploading: true,
    });

    try {
      const data = new FormData();
      fileList.forEach(file => {
        data.append('business_logo', file);
      });
      const response = await axios.post(`https://upappfactory.com/upload/businses_logo`, data)

      console.log("response >>", response);

      setState({
        ...state,
        business_logo: response.data.location,
        uploading: false,
        uploadDisable: true,
      })
      message.success('Featured Image Added Successfully.');

    } catch (error) {
      console.log(error, "error")
      message.error('Featured Image upload Failed.');
      setState({
        ...state,
        uploading: false,
      })
    }
  };

  const props = {
    onRemove: file => {
      setState(state => {
        const index = state.fileList.indexOf(file);
        const newFileList = state.fileList.slice();
        newFileList.splice(index, 1);
        return {
          ...state,
          fileList: newFileList,
          uploadDisable: false,
        };
      });
    },
    beforeUpload: file => {
      const isLt2M = file.size / 1024 / 1024 < 5;
      if (!isLt2M) {
        message.error('Image must smaller than 300KB!');
      } else {
        setState(state => ({
          ...state,
          fileList: [...state.fileList, file],
        }));
        return false;
      }

    },
    fileList,
  };

  const showModal = () => {
    setState({
      ...state,
      MobVisible: true,
    });
  };

  const handleOk = e => {
    console.log(e);
    setState({
      ...state,
      MobVisible: false,
    });
  };

  const handleCancel = e => {
    console.log(e);
    setState({
      ...state,
      MobVisible: false,
    });
  };

  const handleChange = e => {
    setState({
      ...state,
      languages: e
    })
    console.log(e)
  }

  const onSearch = val => {
    console.log('search:', val);
  }

  const handleDefaultCurrencyChange = e => {
    setState({
      ...state,
      default_currency: e
    })
    console.log(e)
  }
  const handlePaymentGatewayCountry = e => {
    setState({
      ...state,
      pg_country: e
    })
    console.log(e)
  }
  const handleBusinessCountry = e => {
    const available = StripeCountry.filter(con => con.code === e)

    setState({
      ...state,
      business_country: e,
      StripeAvaialble: available.length > 0 ? true : false
    })
    console.log(e)
  }

  const handleShippingProviderCountry = e => {
    setState({
      ...state,
      sp_country: e
    })
    console.log(e)
  }

  const handlePackageChange = e => {

    console.log(e.target.value, "plan")
    const pc = data && data[0].packages.filter(pkg => {
      return pkg.type.includes(e.target.value)
    })

    console.log("pc >>", pc);
    if (e.target.value != "Basic") {
      console.log(e.target.value, "true")
      setState({
        ...state,
        PackageChoosed: e.target.value,
        plan: pc,
        plan_name: pc && pc[0].plan[0].Name,
        plan_price: pc && pc[0].plan[0].planCosts[0].after_discount_price,
        plan_type: pc && pc[0].plan[0].planCosts[0].type,
        one_time_setup_charge: pc && pc[0].plan[0].planCosts[0].OneTimeSetupCost,
        defaultPlanTabSelected: "Standard",
        PlanChoosed: "standard-monthly",
        primary_color: " ",
        secondary_color: " "
      });
    } else {
      console.log(e.target.value, "false")
      setState({
        ...state,
        PackageChoosed: e.target.value,
        plan: pc,
        plan_name: pc && pc[0].plan[0].Name,
        plan_price: pc && pc[0].plan[0].planCosts[0].after_discount_price,
        plan_type: pc && pc[0].plan[0].planCosts[0].type,
        one_time_setup_charge: pc && pc[0].plan[0].planCosts[0].OneTimeSetupCost,
        defaultPlanTabSelected: "Standard",
        PlanChoosed: "standard-monthly",
        primary_color: "NA",
        secondary_color: "NA"
      });
    }


    console.log(plan_name, plan_price, plan_type)
  }

  const handlePlanChange = e => {

    const plansArray = plan[0].plan.map(p => {
      return p.planCosts.filter(plnCost => {
        return plnCost.value.includes(e.target.value)
      })
    })
    const p = plansArray.filter(p => {
      return p.length > 0
    })
    console.log("plansArray >", p[0][0])
    setState({
      ...state,
      PlanChoosed: e.target.value,
      PlanCost: p[0],
      plan_name: p[0][0].value,
      plan_type: p[0][0].type,
      plan_price: p[0][0].after_discount_price,
      one_time_setup_charge: p[0][0].OneTimeSetupCost
    });
  }

  const PlanTabsChanged = e => {
    console.log("tab changes", e)
    setState({
      ...state,
      defaultPlanTabSelected: e,
    })
  }

  const onTrainingHourChange = e => {
    setState({
      ...state,
      trainingHour: e.target.value,
      trainingPrice: e.target.value <= 2 ? 0 : (e.target.value * 15 - 30)
    });
  }

  const onProductListingChange = e => {
    if (e.target.value > 2) {
      setState({
        ...state,
        productListCount: e.target.value,
        productListCountPrice: e.target.value <= 2 ? 0 : (e.target.value * 0.5)
      });
    } else {
      setState({
        ...state,
        productListCount: e.target.value,
        productListCountPrice: e.target.value <= 2 ? 0 : (e.target.value * 100 - 200)
      });
    }

  }

  const handleProviderChange = e => {
    console.log(e)
    if (e.target.value == 'deliveryProvider-others')
      setState({ ...state, shippingGatewatCharge: 500, delProvider: e.target.value, })
    else
      setState({
        ...state,
        shippingGatewatCharge: 0,
        delProvider: e.target.value,
      });
  }
  const handlePaymentGatewayChange = e => {
    console.log(e)
    if (e.target.value == 'payment-gateway-other')
      setState({
        ...state,
        paymetGatewayCharge: 500,
        PaymentGatewayProvider: e.target.value
      })
    else if (e.target.value === "Not Required") {
      setState({
        ...state,
        paymetGatewayCharge: 0,
        payment_gateway_username: "NA",
        payment_gateway_password: "NA",
        PaymentGatewayProvider: e.target.value,
      });

    } else {
      setState({
        ...state,
        paymetGatewayCharge: 0,
        PaymentGatewayProvider: e.target.value,
        payment_gateway_username: "",
        payment_gateway_password: "",
      });
    }
  }

  const handleProductListChange = e => {
    console.log(e)
    let productListCountPrice, productListCount
    switch (e.target.value) {
      case 'product-list-1': {
        productListCount = 100
        productListCountPrice = 100
      }
        break;
      case 'product-list-2': {
        productListCount = 200
        productListCountPrice = 200
      }
        break;
      case 'product-list-3': {
        productListCount = 500
        productListCountPrice = 500
      }
        break;
      default:
        break;
    }
    setState({
      ...state,
      ProductListRate: e.target.value,
      productListCountPrice: 0,
      productListCount: 0
    });

  }




  const next = () => {
    console.log(current, "current")
    let nextStep = false, status
    switch (current) {
      case 0:
        status = validatePlanDetails()
        nextStep = status
        break;
      case 1:
        status = validateProductDetails()
        console.log(status, "status>>>>>>>>")
        nextStep = status
        break;
      case 2:
        status = validateAddOnDetails()
        nextStep = status
        break;
      case 3:
        // status = validateProductDetails()
        nextStep = status
        break;
      default:
        break;
    }
    console.log(nextStep)
    if (nextStep) {
      const currents = current + 1;
      setState({ ...state, current: currents });
      nextStep = false
    }
  }

  const handleToken = async (token, addresses) => {
    try {
      const data = {
        pack_type: PackageChoosed,
        plan_name: plan_name,
        plan_type: plan_type,
        product_name: "Ecommerce Single Vendor App",
        theme_id: match.params.themeID,
        plan_price: plan_price,
        business_category: business_category,
        business_email: business_email,
        business_logo: business_logo,
        domain_provider: domain_provider,
        domain_username: domain_username,
        domain_password: domain_password,
        default_currency: default_currency,
        primary_color: Primary_color,
        secondary_color: Secondary_color,
        languages: languages,
        own_domain: ownDomain,
        self_domain_add: selfDomainAdd,
        payment_gateway_Option_Selected: PaymentGatewayProvider,
        shipping_gateway_provider_Selected: delProvider,
        payment_gateway_provider: PaymentGatewayProvider,
        payment_gateway_country: pg_country,
        payment_gateway_details: payment_other_details,
        shipping_gateway_provider: delProvider,
        shipping_gateway_country: sp_country,
        shipping_gateway_details: shipping_other_details,
        training_hours: trainingHour,
        product_list_count: productListCount,

        additional_costs: {
          shipping_cost: (shippingGatewatCharge * SGDPrice).toFixed(1),
          training_cost: (trainingPrice * SGDPrice).toFixed(1),
          product_cost: (productListCountPrice * SGDPrice).toFixed(1),
          payment_gateway_cost: (paymetGatewayCharge * SGDPrice).toFixed(1),
          language_cost: (languages.length * 100 * SGDPrice).toFixed(1),
          one_time_setup_charge: (one_time_setup_charge * SGDPrice).toFixed(1)
        },
        token,
        total_price: ((+(PlanCost[0].type === "Yearly" ? (+plan_price * +12).toFixed(1) : plan_price) + +(languages && languages.length * 100) + +(trainingPrice & trainingPrice) + +(paymetGatewayCharge && paymetGatewayCharge) + +(shippingGatewatCharge && shippingGatewatCharge) + +(productListCountPrice && productListCountPrice) + one_time_setup_charge) * SGDPrice).toFixed(1),

      }
      // console.log("Data", data)
      setState({
        ...state,
        loading: true
      })
      message.loading(`${t('CheckoutFlow.PaymentInProgress')}`, 30);
      proceedToPay(data)


    } catch (error) {
      console.log(error)
      setState({
        ...state,
        loading: false
      })
      if (error) {
        return <Redirect to={`/eCommerce-Single-Vendor-Platform/${match.params.Package}/${match.params.Plan}/${match.params.themeID}/checkout/failed`}></Redirect>;
      }

    }
  }

  // if (success)
  // Redirect to thank you or success page maqsood

  const validateProductDetails = () => {
    console.log(!delProvider, "provider")
    if (!business_category || !business_email || !business_description || !domain_provider || !domain_username || !domain_password || !Primary_color || !Secondary_color || !default_currency || !languages || !PaymentGatewayProvider || !pg_country || !delProvider || !selfDomainAdd) {
      console.log("if")
      if (!business_category && !business_email && !business_description && !domain_provider && !domain_username && !domain_password && !Primary_color && !Secondary_color && !default_currency && !languages && !PaymentGatewayProvider && !pg_country && !delProvider && !selfDomainAdd) {
        message.error("Please complete the form")
        return false
      }
      if (!business_category) {
        message.error(`${t('CheckoutFlow.errorMsg.business_category')}`)
        return false
      } else if (!business_email) {
        message.error(`${t('CheckoutFlow.errorMsg.business_email')}`)
        return false
      } else if (!business_description) {
        message.error(`${t('CheckoutFlow.errorMsg.business_description')}`)
        return false
      }
      else if (!business_logo) {
        message.error(`${t('CheckoutFlow.errorMsg.business_logo')}`)
        return false
      }
      else if (ownDomain === 'Yes' && !selfDomainAdd) {
        console.log("if1")
        if (domain_provider == "") {
          message.error(`${t('CheckoutFlow.errorMsg.domain_provider')}`)
          return false
        } else if (domain_username == "") {
          message.error(`${t('CheckoutFlow.errorMsg.domain_username')}`)
          return false
        } else if (domain_password == "") {
          message.error(`${t('CheckoutFlow.errorMsg.domain_password')}`)
          return false
        }
      } else if (!default_currency) {
        console.log("if2")
        message.error(`${t('CheckoutFlow.errorMsg.default_currency')}`)
        return false
      } else if (!business_country) {
        console.log("if4")
        message.error(`${t('CheckoutFlow.errorMsg.Business_country')}`)
        return false
      } else if (!Primary_color) {
        console.log("if3")
        message.error(`${t('CheckoutFlow.errorMsg.Primary_color')}`)
        return false
      } else if (!Secondary_color) {
        console.log("if4")
        message.error(`${t('CheckoutFlow.errorMsg.Secondary_color')}`)
        return false
      } else if ((PaymentGatewayProvider != "payment-gateway-other") && (payment_gateway_username == "" || payment_gateway_password == "")) {
        console.log("if5")
        if (payment_gateway_username == "") {
          message.error(`${t('CheckoutFlow.errorMsg.payment_gateway_username')}`)
          return false
        } else if (payment_gateway_password == "") {
          message.error(`${t('CheckoutFlow.errorMsg.payment_gateway_password')}`)
          return false
        }
      } else if ((PaymentGatewayProvider == 'payment-gateway-other') && (payment_gateway_provider == "" || pg_country == "" || payment_other_details == "")) {
        console.log("if6")
        if (payment_gateway_provider == "") {
          message.error(`${t('CheckoutFlow.errorMsg.payment_gateway_provider')}`)
          return false
        } else if (pg_country == "") {
          message.error(`${t('CheckoutFlow.errorMsg.pg_country')}`)
          return false
        } else if (payment_other_details == "") {
          message.error(`${t('CheckoutFlow.errorMsg.payment_other_details')}`)
          return false
        }
      } else if ((delProvider == "deliveryProvider-others") && (shipping_provider == "" || sp_country == "" || shipping_other_details == "")) {
        console.log("if7")
        if (shipping_provider == "") {
          message.error(`${t('CheckoutFlow.errorMsg.shipping_provider')}`)
          return false
        } else if (sp_country == "") {
          message.error(`${t('CheckoutFlow.errorMsg.sp_country')}`)
          return false
        } else if (shipping_other_details == "") {
          message.error(`${t('CheckoutFlow.errorMsg.shipping_other_details')}`)
          return false
        }
      }
    }
    return true
  }

  const validateAddOnDetails = e => {
    if (SelfProductUpload) {
      return true
    } else if (productListCount < 100) {
      message.error(`${t('CheckoutFlow.errorMsg.productListCount')}`)
      return false
    } return true
  }


  const validatePlanDetails = async () => {
    console.log('current')
    try {
      if (!PackageChoosed || !PlanChoosed) {
        if (!PackageChoosed && !PlanChoosed) {
          message.error('Please choose a package and plan')
          return false
        }
        else if (!PackageChoosed) {
          message.error('Please choose a package')
          return false
        }
        else if (!PlanChoosed) {
          message.error('Please choose a plan')
          return false
        }
        else return true
      }
    } catch (error) {
      console.log(error)
    }
  }
  const prev = () => {
    const currents = current - 1;
    setState({ ...state, current: currents });
  }



  const checkboxHandle = e => {
    if (e.target.checked) {
      setState({
        ...state,
        disablePayment: false
      });
    } else {
      setState({
        ...state,
        disablePayment: true
      });
    }
  };



  const onProductListAddOnChange = e => {
    if (e.target.checked) {
      setState({
        ...state,
        SelfProductUpload: true,
        productListCount: 0,
        productListCountPrice: 0
      });
    } else {
      setState({
        ...state,
        SelfProductUpload: false
      });
    }
  };

  const OnDomainSelfChange = e => {
    if (e.target.checked) {
      setState({
        ...state,
        selfDomainAdd: true
      });
    } else {
      setState({
        ...state,
        selfDomainAdd: false
      });
    }

  };

  const onDomainChange = e => {
    console.log('radio checked', e.target.value);
    setState({
      ...state,
      ownDomain: e.target.value,
    });

  }

  const onBillingAddressChange = e => {
    console.log('radio checked', e.target.value);
    setState({
      ...state,
      BillingAddressSame: e.target.value,
    });

  }

  const onChange = e => {
    setState({
      ...state,
      [e.target.name]: e.target.value
    })
    console.log(e.target.name, e.target.value)
  }

  const onAboutDescChange = e => {

    const charCount = e.target.value.length;
    const maxChar = AboutLimWords;
    const charLength = maxChar - charCount;

    setState({
      ...state,
      business_description: e.target.value,
      AboutBusinessWordCount: charLength
    });

    console.log(e.target.name, e.target.value)
  }

  const onPaymentAboutDescChange = e => {

    const charCount = e.target.value.length;
    const maxChar = AboutLimWords;
    const charLength = maxChar - charCount;

    setState({
      ...state,
      payment_other_details: e.target.value,
      AboutPaymentOtherWordCount: charLength
    });

    // console.log(e.target.name, e.target.value)
  }

  const onShippingAboutDescChange = e => {

    const charCount = e.target.value.length;
    const maxChar = AboutLimWords;
    const charLength = maxChar - charCount;

    setState({
      ...state,
      shipping_other_details: e.target.value,
      AboutShippingOtherWordCount: charLength
    });

    // console.log(e.target.name, e.target.value)
  }



  const PlanDetails = () => {
    return (
      <Card
        className="Select_plan"
        // extra={<p />}
        style={{}}
      >
        <div style={{ textAlign: "left", padding: "0px 15px" }}>
    <Title level={4}>{t('CheckoutFlow.Plan.Package')}</Title>
        </div>
        <Radio.Group
          onChange={handlePackageChange}
          defaultValue={match.params.Package}
          value={PackageChoosed}
          // buttonStyle="solid"
          className="plan-container uaf_chkout_package_outer"
        >
          {data && data[0].packages.map(pack => (
            <Radio.Button value={pack.type} className="uaf_chkout_package_Inner">
              <Row
                style={{ textAlign: "center", marginTop: "10px" }}
                type="flex"
                justify="space-around"
                align="middle"
              >
                <Col span={24}>
                  <Text
                    style={{
                      fontWeight: "600",
                      fontSize: "16px",
                      color: "rgb(60, 60, 60)"
                    }}
                  >
                    {direction === DIRECTIONS.LTR && <Fragment>
                      {pack.type}
                    </Fragment>}
                    {direction === DIRECTIONS.RTL && <Fragment>
                      {pack.typeAR}
                    </Fragment>}
                    
                  </Text>
                </Col>
                <div style={{ height: "90px" }} className="ant-row-flex ant-row-flex-space-around ant-row-flex-middle">
                  <div>
                  {direction === DIRECTIONS.LTR && <Fragment>

                    {pack.includes.map(inc => (
                      <div
                        style={{
                          maxWidth: "230px",
                          borderBottom: "0.5px solid #ddd",
                          margin: "0px auto",
                          lineHeight: "2",
                          fontSize: "12px",
                          color: "#000"
                        }}
                      >
                        {t(inc)}
                        {/* {t("pricing.ecommWebsite")} */}
                      </div>

                    ))}
                    </Fragment>}
                    {direction === DIRECTIONS.RTL && <Fragment>

{pack.includesAR.map(inc => (
  <div
    style={{
      maxWidth: "230px",
      borderBottom: "0.5px solid #ddd",
      margin: "0px auto",
      lineHeight: "2",
      fontSize: "12px",
      color: "#000"
    }}
  >
    {t(inc)}
    {/* {t("pricing.ecommWebsite")} */}
  </div>

))}
</Fragment>}
                    <div
                      style={{
                        maxWidth: "150px",
                        margin: "0px auto",
                        lineHeight: "2",
                        fontSize: "12px",
                        color: "#000"
                      }}
                    >
                      {t("pricing.allFeatures")}
                    </div>

                  </div>
                </div>
                <Col span={24} className="radio-checkout-Price">
                  <Text
                    style={{
                      fontSize: "25px",
                      fontWeight: "600",
                      color: "#00BCD4"
                    }}
                  >
                    <span className="Check-curr">{sessionStorage.getItem("currency_sign")}</span>{" "}
                    <CurrencyFormat
                      value={((pack.plan[0].planCosts[0].after_discount_price) * sessionStorage.getItem("currency_rate")).toFixed(1)}
                      displayType={"text"}
                      thousandSeparator={true}
                    />
                  </Text>
                  <span style={{ color: "rgb(60, 60, 60)", fontSize: "12px" }}>/Mo</span>
                </Col>

              </Row>
            </Radio.Button>
          ))}
        </Radio.Group>

        {/* Plan  */}

        <div style={{ textAlign: "left", padding: "20px 15px" }}>
                  <Title level={4}>{t('CheckoutFlow.Plan.Plan')}</Title>
        </div>
        <div dir="ltr" style={{ textAlign: "left", }}>
          {console.log(plan[0] && plan[0].plan)}


          <Tabs className="plan-tab" onChange={PlanTabsChanged}
            activeKey={defaultPlanTabSelected}
            defaultActiveKey={defaultPlanTabSelected}
          >
            {plan[0] && plan[0].plan.map(pln => (
              <TabPane  tab={direction === 'rtl' ? pln.NameAR : pln.Name} key={pln.Name}>

                <Radio.Group
                  onChange={handlePlanChange}
                  defaultValue="standard-yearly"
                  value={PlanChoosed}
                  className="plan-container plan-btn chkout_page_planCol"
                >
                  {pln.planCosts.map(pln => {
                    return <Radio.Button value={pln.value} className="chkout_page_planCol_btns">
                      <Row style={{ textAlign: "center", marginTop: "10px", height: "85px" }} type="flex" justify="space-around" align="middle">
                        <Col span={12}>
                          <Text style={{ fontWeight: "600", fontSize: "25px", color: "rgb(60, 60, 60)" }}>
                            {direction === 'ltr' ? pln.type : pln.typeAR}
                          </Text>
                        </Col>
                        <Col span={12} className="radio-checkout-Price plan-price">
                          <Text style={{ fontSize: "25px", fontWeight: "600", color: "#00BCD4" }}>
                            <span className="Check-curr">{sessionStorage.getItem("currency_sign")} </span>{" "}
                            <CurrencyFormat
                              value={(+pln.after_discount_price * sessionStorage.getItem("currency_rate")).toFixed(1)}
                              displayType={"text"}
                              thousandSeparator={true}
                            />
                          </Text>
                          <span style={{ color: "rgb(60, 60, 60)", fontSize: "12px" }}>/Mo</span>
                        </Col>
                      </Row>
                    </Radio.Button>
                  })}

                </Radio.Group>

              </TabPane>

            ))}
          </Tabs>

        </div>
      </Card>
    )

  }

  const ProductDetails = () => {

    return (
      <Card
        className="Select_plan"
        // extra={<p />}
        style={{}}
      >

        <Row type="flex" gutter={16}>
          <Col xs={24} lg={12} md={12} className="Pricing_page2_col_business_category">
            <div className="Pricing_page2_business_category" style={direction === 'ltr' ? {textAlign:"left"} : {textAlign:"right"}}>
              <Title level={4}>{t('CheckoutFlow.Product.BusinessCategory')} <Tooltip title={`${t('CheckoutFlow.Product.BusinessCategoryInfo')}`}>
                <Icon
                  style={{
                    fontSize: '12px',
                    color: '#607D8B',
                    cursor: 'pointer'
                  }}
                  type="info-circle"
                />
              </Tooltip></Title>
            </div>
            <Select
              showSearch
              style={{ width: "100%" }}
              value={business_category}
              defaultValue=""
              onChange={(value) => setState({
                ...state,
                business_category: value
              })}
              placeholder={`${t('CheckoutFlow.Product.BusinessCategory')}`}
              optionFilterProp="children"
              filterOption={(input, option) =>
                option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
              }
            >
              {businessCat.map(b => {
                return <Option value={b}>{b}</Option>
              })}
            </Select>
            {/* <Select
              onChange={(value) => setState({
                ...state,
                business_category: value
              })}
              value={business_category}
              style={{ width: "100%" }} >
              {businessCat.map(b => {
                return <Option value={b}>{b}</Option>
              })}

            </Select> */}
          </Col>
          <Col xs={24} lg={12} md={12} className="Pricing_page2_col_business_email">
            <div className="Pricing_page2_business_email" style={direction === 'ltr' ? {textAlign:"left"} : {textAlign:"right"}}>
              <Title level={4}>{t('CheckoutFlow.Product.BusinessEmail')}  <Tooltip title={`${t('CheckoutFlow.Product.BusinessEmailInfo')}`}>
                <Icon style={{ fontSize: '12px', color: '#607D8B', cursor: 'pointer' }} type="info-circle" />
              </Tooltip></Title>
            </div>
            <Input type="email" value={business_email} placeholder={`${t('CheckoutFlow.Product.BusinessEmailPlaceholder')}`} name="business_email" onChange={onChange} />
          </Col>
        </Row>
        <br />
        <Row type="flex" style={direction === 'ltr' ? {textAlign:"left"} : {textAlign:"right"}}>
          <Col xs={24} lg={24} md={24} className="Pricing_page2_col_business_about">
            <div className="Pricing_page2_business_about" style={direction === 'ltr' ? {textAlign:"left"} : {textAlign:"right"}}>
              <Title level={4}>{t('CheckoutFlow.Product.AboutBusiness')} <Tooltip title={`${t('CheckoutFlow.Product.AboutBusinessInfo')}`}>
                <Icon style={{ fontSize: '12px', color: '#607D8B', cursor: 'pointer' }} type="info-circle" />
              </Tooltip></Title>
            </div>
            <TextArea maxlength="250" value={business_description} style={{ width: "100%" }} rows={4} name="business_description" onChange={onAboutDescChange} />
            <div style={{ textAlign: "right" }}>
              <Text>{AboutBusinessWordCount}</Text>
            </div>
          </Col>
        </Row>

        <div className="Pricing_page2_col_business_logo" style={direction === 'ltr' ? {textAlign:"left"} : {textAlign:"right"}}>
          <Title level={4}>{t('CheckoutFlow.Product.Logo')} <Tooltip title={`${t('CheckoutFlow.Product.LogoInfo')}`}>
            <Icon style={{ fontSize: '12px', color: '#607D8B', cursor: 'pointer' }} type="info-circle" />
          </Tooltip> <span className="upload_your_file">( {t('CheckoutFlow.Product.uploadOpenFile')} )</span>
          </Title>
        </div>
        <div style={direction === 'ltr' ? {textAlign:"left"} : {textAlign:"right"}}>
          {/* <Upload {...props}>
        <Button>
          <Icon type="upload" /> Select File
      </Button>
      </Upload> */}
          <Dragger {...props}>
            <p className="ant-upload-drag-icon">
              <Icon type="inbox" />
            </p>
            <p className="ant-upload-text">{t('CheckoutFlow.Product.LogoDropText')}</p>
          </Dragger>
          <Button
            type="primary"
            onClick={handleUpload}
            // disabled={uploadDisable}
            loading={uploading}
            style={{ marginTop: 16 }}
          >
            {uploading ? `${t('CheckoutFlow.Product.Uploading')}` : `${t('CheckoutFlow.Product.StartUploading')}`}
          </Button>
        </div>
        {/*  Domain */}
        <div  style={direction === 'ltr' ? {textAlign:"left", margin: "20px 0px"} : {textAlign:"right", margin: "20px 0px"}}>
          <Card size="small" title={`${t('CheckoutFlow.Product.OwnDomain')}`} extra={
            <Radio.Group onChange={onDomainChange} value={ownDomain} >
              <Radio value="Yes">{t('CheckoutFlow.Product.Yes')}</Radio>
              <Radio value="No">{t('CheckoutFlow.Product.No')}</Radio>
            </Radio.Group>
          } style={{ width: "100%" }}>
            {ownDomain === 'Yes' ? <Row type="flex" gutter={16}>
              <Col xs={24} lg={24} md={24}>
                <div style={{ textAlign: "left" }}>
                </div>
                <Input disabled={selfDomainAdd} value={domain_provider} placeholder={`${t('CheckoutFlow.Product.DomainProvider')}`} name="domain_provider" onChange={onChange} />
              </Col>
              <Col xs={24} lg={12} md={12}>
                <div style={{ textAlign: "left", marginTop: "15px" }}>
                </div>
                <Input disabled={selfDomainAdd} value={domain_username} placeholder={`${t('CheckoutFlow.Product.Username')}`} name="domain_username" onChange={onChange} />
              </Col>
              <Col xs={24} lg={12} md={12}>
                <div style={{ textAlign: "left", marginTop: "15px" }}>
                </div>
                <Input disabled={selfDomainAdd} value={domain_password} placeholder={`${t('CheckoutFlow.Product.Password')}`} name="domain_password" onChange={onChange} />
              </Col>
              <Col xs={24} style={direction === "ltr" ? {textAlign:"left"} : {textAlign:"right"}}>
                <br></br>
                {console.log("selfDomainAdd", selfDomainAdd)}
                <Checkbox checked={selfDomainAdd} onChange={OnDomainSelfChange}
                // disabled={disable}
                >
                  {t('CheckoutFlow.Product.DNSMySelfSetting')}
            </Checkbox>
              </Col>
            </Row> : null
            }
          </Card>
        </div>
        {/* Currency */}
        <div className="Pricing_page2_main_outer">

          <Row type="flex" gutter={16}>

            <Col xs={24} lg={12} className="Pricing_page2_col_business_currency" style={direction === 'ltr' ? {textAlign:"left"} : {textAlign:"right"}}>
              <Title level={4}>{t('CheckoutFlow.Product.Currency')} <Tooltip title={`${t('CheckoutFlow.Product.CurrencyInfo')}`}>
                <Icon
                  style={{
                    fontSize: '12px',
                    color: '#607D8B',
                    cursor: 'pointer'
                  }}
                  type="info-circle"
                />
              </Tooltip> <span className="Set_Your_Default_Currency">( {t('CheckoutFlow.Product.SetYourDefaultCurrency')} )</span>
              </Title>
              <Select
                showSearch
                value={default_currency}
                onChange={handleDefaultCurrencyChange}
                optionFilterProp="children"
                filterOption={(input, option) =>
                  option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
                }
                defaultValue="INR"
                style={{ width: "100%" }} >
                {Commoncurrency && Commoncurrency.map(curr => {
                  return <Option value={curr.code}>{curr.code}</Option>
                })}
              </Select>
            </Col>
            <Col xs={24} lg={12} style={direction === 'ltr' ? {textAlign:"left"} : {textAlign:"right"}}>
              <Title level={4}>{t('CheckoutFlow.Product.Country')} <Tooltip title={`${t('CheckoutFlow.Product.CountryInfo')}`}>
                <Icon
                  style={{
                    fontSize: '12px',
                    color: '#607D8B',
                    cursor: 'pointer'
                  }}
                  type="info-circle"
                />
              </Tooltip> <span style={{ opacity: 0.4, fontSize: "14px" }}></span>
              </Title>
              <Select
                showSearch
                style={{ width: "100%" }}
                value={business_country} defaultValue="" onChange={handleBusinessCountry}
                placeholder="Select a Country"
                optionFilterProp="children"
                filterOption={(input, option) =>
                  option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
                }
              >
                {CountryList.map((country, i) => {
                  return <Option Key={i} value={country.code}>{country.name}</Option>
                })}
              </Select>
            </Col>
          </Row>
        </div>
        {/* Colour */}
        {PackageChoosed != "Basic" ? (
          <Fragment>
            <div className="Pricing_page2_main_outer" style={direction === 'ltr' ? {textAlign:"left"} : {textAlign:"right"}}>
              <Title level={4}>{t('CheckoutFlow.Product.Colour')} <Tooltip title={`${t('CheckoutFlow.Product.ColourInfo')}`}>
                <Icon style={{ fontSize: '12px', color: '#607D8B', cursor: 'pointer' }} type="info-circle" />
              </Tooltip>
              </Title>
              <Row type="flex" gutter={16}>

                <Col xs={24} lg={12} md={12} style={direction === 'ltr' ? {textAlign:"left"} : {textAlign:"right"}}>
                  <div style={{marginTop: "15px" }}></div>
                  <Text>{t('CheckoutFlow.Product.PrimaryColorHEX')}</Text>
                  {console.log("Primary_color ", Primary_color)}
                  <Input value={Primary_color} maxlength="7" style={{ textTransform: "uppercase" }} name="Primary_color" onChange={onChange} placeholder={`${t('CheckoutFlow.Product.PrimaryColorHEX')}`} />
                </Col>
                <Col xs={24} lg={12} md={12} style={direction === 'ltr' ? {textAlign:"left"} : {textAlign:"right"}}>
                  <div style={{marginTop: "15px" }}>
                  </div>
                  <Text>{t('CheckoutFlow.Product.SecondaryColourHEX')}</Text>
                  <Input value={Secondary_color} maxlength="7" style={{ textTransform: "uppercase" }} name="Secondary_color" onChange={onChange} placeholder={`${t('CheckoutFlow.Product.SecondaryColourHEX')}`} />
                </Col>
                <Divider >{t('CheckoutFlow.Product.Or')}</Divider>
                <Col xs={24} lg={24} md={24}>
                  <div onClick={showModal} className="checkout-mobile-app-appearance">
                    <p style={{ paddingTop: "18px" }}><Icon type="highlight" theme="twoTone" /> {t('CheckoutFlow.Product.youAppearance')}</p>
                  </div>
                  <Modal width="70%" title={`${t('CheckoutFlow.Product.youAppearance')}`} visible={MobVisible} footer={false} onOk={handleOk} onCancel={handleCancel} >
                    <div style={{ transform: "scale(0.7)" }}>
                      <MobileAppView />
                    </div>
                  </Modal>
                </Col>
              </Row>
            </div>
          </Fragment>
        ) : null}
        {/* Language */}
        <div className="Pricing_page2_main_outer" style={direction === 'ltr' ? {textAlign:"left"} : {textAlign:"right"}}>
          <Title level={4}>{t('CheckoutFlow.Product.AdditionalLanguage')} <Tooltip title={`${t('CheckoutFlow.Product.AdditionalLanguageInfo')}`}>
            <Icon
              style={{
                fontSize: '12px',
                color: '#607D8B',
                cursor: 'pointer'
              }}
              type="info-circle"
            />
          </Tooltip>
          </Title>
          <Row type="flex" gutter={16}>
            <Col xs={24} lg={12}>
              <Select mode="multiple" style={{ width: '100%' }} placeholder={`${t('CheckoutFlow.Product.AdditionalLanguage')}`} defaultValue={[]} value={languages} onChange={handleChange} optionLabelProp="label" >
                <Option value="Arabic" label="Arabic">Arabic</Option>
              </Select>
            </Col>
          </Row>
          <p style={{ color: "rgb(0, 188, 212)", marginTop: "5px" }}>{sessionStorage.getItem("currency_sign")} <CurrencyFormat
            value={(+100 * sessionStorage.getItem("currency_rate")).toFixed(1)}
            displayType={"text"}
            thousandSeparator={true}
          /> {t('CheckoutFlow.Product.AdditionalLanguageText')}</p>
        </div>
        <br />
        {/* Payment Gateway */}
        <div className="Pricing_page2_main_outer" style={direction === 'ltr' ? {textAlign:"left"} : {textAlign:"right"}}>
          <Title level={4}>{t('CheckoutFlow.Product.PaymentGateway')} <Tooltip title={`${t('CheckoutFlow.Product.PaymentGatewayInfo')}`}>
            <Icon style={{ fontSize: '12px', color: '#607D8B', cursor: 'pointer', }} type="info-circle" />
          </Tooltip> <span style={{ opacity: 0.4, fontSize: "14px" }}></span>
          </Title>
          <br />
          <br />
          <Radio.Group onChange={handlePaymentGatewayChange} className="plan-container plan-btn planContainer_main" value={PaymentGatewayProvider}>
            {StripeAvaialble === true ? <Radio.Button className="chkout_paymentGateway_option" value="gateway-stripe">
              <div className="paymet-gateway-logo-container">
                <div className="paymet-gateway-tag" >{t('CheckoutFlow.Product.FREE')}</div>
                <img className="paymet-gateway-logo" src="https://upappfactory-static-images.s3.us-east-2.amazonaws.com/payment+gateway/stripe.png"></img>
              </div>
            </Radio.Button> : null}

            <Radio.Button className="chkout_paymentGateway_option" value="gateway-paypal">
              <div className="paymet-gateway-logo-container">
                <div className="paymet-gateway-tag" >{t('CheckoutFlow.Product.FREE')}</div>
                <img className="paymet-gateway-logo" src="https://upappfactory-static-images.s3.us-east-2.amazonaws.com/payment+gateway/paypal.png"></img>
              </div>
            </Radio.Button>
            <Radio.Button className="chkout_paymentGateway_option" value="payment-gateway-other">
              <div className="paymet-gateway-logo-container">
                <div className="paymet-gateway-tag" >{sessionStorage.getItem("currency_sign")}
                  <CurrencyFormat value={(+500 * sessionStorage.getItem("currency_rate")).toFixed(1)} displayType={"text"} thousandSeparator={true} />
                </div>
                <div style={{ fontSize: '18px', color: "#000", marginTop: "20px" }}>
                {t('CheckoutFlow.Product.Gateway')} <sup style={{ fontSize: "12px" }}>{t('CheckoutFlow.Product.Other')}</sup>
                  <p style={{ fontSize: "10px" }}>{t('CheckoutFlow.Product.AdditionalCostWillApply')}</p>
                </div>
              </div>
            </Radio.Button>
            <Radio.Button className="chkout_paymentGateway_option" value="Not Required">
              <div className="paymet-gateway-logo-container">
            <p style={{ fontSize: "16px", marginTop: "20px", color: "#333" }}>{t('CheckoutFlow.Product.DoNotWantNow')}</p>
              </div>
            </Radio.Button>
          </Radio.Group>

          {PaymentGatewayProvider === "payment-gateway-other" ? (<div style={{ margin: "15px 10px" }}>
            <Row type="flex" gutter={16}>
              <Col xs={24} lg={12}>
                <Input value={payment_gateway_provider} placeholder={`${t('CheckoutFlow.Product.PaymentGatewayProvider')}`} name="payment_gateway_provider" onChange={onChange} />
              </Col>
              <Col xs={24} lg={12}>
                <Select
                  showSearch
                  value={pg_country} defaultValue="SG" onChange={handlePaymentGatewayCountry} style={{ width: "100%" }}
                  placeholder="Select a Country"
                  optionFilterProp="children"
                  filterOption={(input, option) =>
                    option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
                  }
                >
                  {CountryList.map((country, i) => {
                    return <Option Key={i} value={country.code}>{country.name}</Option>
                  })}
                </Select>
              </Col>
              <Col xs={24} lg={24}>
                <TextArea maxlength="250" placeholder={`${t('CheckoutFlow.Product.OtherDetails')}`} style={{ marginTop: "15px" }} rows={3} name="payment_other_details" onChange={onPaymentAboutDescChange} />
                <div style={{ textAlign: "right" }}>
                  <Text>{AboutPaymentOtherWordCount}</Text>
                </div>
              </Col>
            </Row>
          </div>) : null}
          {PaymentGatewayProvider != "payment-gateway-other" ? PaymentGatewayProvider != "Not Required" ? (<div style={{ margin: "15px 10px" }}>
            <Row type="flex" gutter={16}>
              <Col xs={24} lg={12} className="payment_gateway_username">
                <Input value={payment_gateway_username} placeholder={`${t('CheckoutFlow.Product.PaymentGatewayUsername')}`} name="payment_gateway_username" onChange={onChange} />
              </Col>
              <Col xs={24} lg={12}>
                <Input value={payment_gateway_password} placeholder={`${t('CheckoutFlow.Product.PaymentGatewayPassword')}`} name="payment_gateway_password" onChange={onChange} />
              </Col>

            </Row>
            
          </div>) : null : null}

        </div>
        <br />
        {/* Shipping Provider */}
        <div className="Pricing_page2_main_outer" style={direction === 'ltr' ? {textAlign:"left"} : {textAlign:"right"}}>
          <Title level={4}>{t('CheckoutFlow.Product.ShippingProvider')} <Tooltip title={`${t('CheckoutFlow.Product.ShippingProviderInfo')}`}>
            <Icon style={{ fontSize: '12px', color: '#607D8B', cursor: 'pointer', }} type="info-circle" />
          </Tooltip>
          </Title>
          <br />
          <br />

          <Radio.Group
            onChange={handleProviderChange}
            // defaultValue={}
            // buttonStyle="solid"
            value={delProvider}
            className="plan-container plan-btn plan_container_shippingProvider"
          >

            <Radio.Button value="deliveryProvider-others">
              <div className="paymet-gateway-logo-container">
                <div className="paymet-gateway-tag" >
                  {sessionStorage.getItem("currency_sign")} <CurrencyFormat
                    value={(+500 * sessionStorage.getItem("currency_rate")).toFixed(1)}
                    displayType={"text"}
                    thousandSeparator={true}
                  />
                </div>
                <div style={{ fontSize: '25px', color: "#000", marginTop: "20px" }}>
                  {t('CheckoutFlow.Product.ShippingProvider')} <sup style={{ fontSize: "14px" }}>{t('CheckoutFlow.Product.Other')}</sup>
                  <p style={{ fontSize: "13px" }}>{t('CheckoutFlow.Product.AdditionalCostWillApply')}</p>
                </div>
              </div>
            </Radio.Button>
            <Radio.Button value="Not Needed Now">
              <div className="paymet-gateway-logo-container">
                <div style={{ fontSize: '25px', color: "#000", marginTop: "35px" }}>
                {t('CheckoutFlow.Product.DoNotWantNow')}
                </div>
              </div>
            </Radio.Button>
          </Radio.Group>

          {delProvider === "deliveryProvider-others" ? (<div style={{ margin: "15px 10px" }}>
            <Row type="flex" gutter={16}>
              <Col xs={24} lg={12}>
                <Input value={shipping_provider} placeholder={`${t('CheckoutFlow.Product.ShippingProvider')}`} name="shipping_provider" onChange={onChange} />
              </Col>
              <Col xs={24} lg={12}>

                <Select
                  showSearch
                  style={{ width: "100%" }}
                  onChange={handleShippingProviderCountry}
                  defaultValue="SG"
                  style={{ width: "100%" }}
                  placeholder="Select a Country"
                  optionFilterProp="children"
                  filterOption={(input, option) =>
                    option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
                  }
                >
                  {CountryList.map(country => {
                    return <Option value={country.code}>{country.name}</Option>
                  })}
                </Select>
              </Col>
              <Col xs={24} lg={24}>
                <TextArea value={shipping_other_details} placeholder={`${t('CheckoutFlow.Product.OtherDetails')}`} style={{ marginTop: "15px" }} rows={3} name="shipping_other_details" onChange={onShippingAboutDescChange} />
                <div style={{ textAlign: "right" }}><Text>
                  {AboutShippingOtherWordCount}
                </Text></div>
              </Col>
            </Row>
          </div>) : null}

        </div>

      </Card>
    )

  }

  const AddOns = () => {
    return (
      <Card
        className="Select_plan"
        // extra={<p />}
        style={{}}
      >
        <div style={direction === 'ltr' ? {textAlign:"left",padding: "0px 15px" } : {textAlign:"right",padding: "0px 15px" }}>
          <Title level={4}>{t('CheckoutFlow.AddOne.Training')} <Tooltip title={`${t('CheckoutFlow.AddOne.TrainingInfoTrainingInfo')}`}>
            <Icon
              style={{
                fontSize: '12px',
                color: '#607D8B',
                cursor: 'pointer',

              }}
              type="info-circle"
            />
          </Tooltip> <span className="first_hours_train_free">( {t('CheckoutFlow.AddOne.TrainingText')} )</span> </Title>
        </div>

        <div style={direction === 'ltr' ? {textAlign:"left"} : {textAlign:"right"}}>
          <Card className="Select_plan" style={{}}>
            <Row gutter={16}  dir="ltr">
              <Col xs={24} lg={12} className="on_training_hour_change">
                <Input type="number" addonAfter={`${t('CheckoutFlow.AddOne.Hours')}`} placeholder={`${t('CheckoutFlow.AddOne.Hours')}`} allowClear
                  onChange={onTrainingHourChange}
                  value={trainingHour}
                  min="1"
                />
              </Col>
              <Col xs={24} lg={12} className="on_training_hour_change">
                <Input type="number" addonBefore={sessionStorage.getItem("currency_sign")} placeholder="Price" allowClear
                  value={(trainingPrice * sessionStorage.getItem("currency_rate")).toFixed(1)}
                />
              </Col>
            </Row>
          </Card>
        </div>
        <br />

        <div style={direction === 'ltr' ? {textAlign:"left",padding: "0px 15px" } : {textAlign:"right",padding: "0px 15px" }} >
          <Title level={4}>{t('CheckoutFlow.AddOne.ProductListing')} <Tooltip title={`${t('CheckoutFlow.AddOne.ProductListingInfo')}`}>
            <Icon style={{ fontSize: '12px', color: '#607D8B', cursor: 'pointer', }} type="info-circle" />
          </Tooltip> </Title>
        </div>
        <Card className="Select_plan" style={{}}>
          <Row gutter={16} dir="ltr">
            <Col xs={24} lg={12} className="self_product_upload on_training_hour_change">
              <Input disabled={SelfProductUpload} type="number" addonAfter={`${t('CheckoutFlow.AddOne.Products')}`} placeholder={`${t('CheckoutFlow.AddOne.Products')}`} allowClear onChange={onProductListingChange} value={productListCount} min="100" />
            </Col>
            <Col xs={24} lg={12} className="on_training_hour_change">
              <Input disabled={SelfProductUpload} type="number" addonBefore={sessionStorage.getItem("currency_sign")} placeholder="Price" allowClear
                value={(productListCountPrice * sessionStorage.getItem("currency_rate")).toFixed(1)}
              // onChange={onChange}
              />
            </Col>
          </Row>
          <br />
          <div style={direction === 'ltr' ? {textAlign:"left" } : {textAlign:"right"}}>
            <Checkbox checked={SelfProductUpload} onChange={onProductListAddOnChange}>{t('CheckoutFlow.AddOne.ProductUploadBySelf')}</Checkbox>
          </div>
        </Card>
      </Card>
    )

  }

  const Summary = () => {
    return (
      <Fragment >
        <Card className="Select_plan" title="Plan Details"
          extra={<Button onClick={() => setState({ ...state, current: 0 })} type="primary" icon="edit" />}>
          <Row gutter={16} type="flex" style={direction === 'ltr' ? {textAlign:"left",padding: "15px" } : {textAlign:"right",padding: "15px" }} >
            <Col xs={24} lg={6}>
              <p>{t('CheckoutFlow.Plan.Package')}</p>
            </Col>
            <Col xs={24} lg={18}>
              <p>{PackageChoosed == "Basic" ? (<Text>{t('CheckoutFlow.WebsiteIncluded')}</Text>) : (PackageChoosed === "Ultra" ? (<Text>{t('CheckoutFlow.Website')} + {t('CheckoutFlow.MobileAppIncluded')}</Text>) : (PackageChoosed === "Premium" ? (<Text>{t('CheckoutFlow.MobileAppIncluded')}</Text>) : null))}</p>
            </Col>
            <Col xs={24} lg={6}>
              <p>{t('CheckoutFlow.Plan.Plan')}</p>
            </Col>
            <Col xs={24} lg={12}>
              <p> {plan_name === "standard-yearly" ? `${t('CheckoutFlow.Plan.Standard')}-${t('CheckoutFlow.Plan.Yearly')}` : plan_name === "standard-monthly" ? `${t('CheckoutFlow.Plan.Standard')}-${t('CheckoutFlow.Plan.Monthly')}` : plan_name === "rental-monthly" ? `${t('CheckoutFlow.Plan.Rental')} - ${t('CheckoutFlow.Plan.Monthly')}` :  `${t('CheckoutFlow.Plan.Standard')}-${t('CheckoutFlow.Plan.Monthly')}`}</p>
            </Col>
            <Col xs={24} lg={6}>
              {sessionStorage.getItem("currency_sign")} {PlanCost[0].type == "Yearly" ? ((+PlanCost[0].after_discount_price * +12) * sessionStorage.getItem("currency_rate")).toFixed(1) : (PlanCost[0].after_discount_price * sessionStorage.getItem("currency_rate")).toFixed(1)}/{PlanCost[0].type == "Monthly" ? 'Mo' : 'Yr'}
            </Col>
          </Row>
        </Card>
        <br />
        <Card
          className="Select_plan"
          title="Product Details"
          extra={
            <Button onClick={() => setState({
              ...state,
              current: 1
            })} type="primary" icon="edit" />
          }

          style={{}}
        >

          <div>
            <Row gutter={16} type="flex" style={direction === 'ltr' ? {textAlign:"left",padding: "15px" } : {textAlign:"right",padding: "15px" }} >
              <Col xs={24} lg={6}>
                <p>{t('CheckoutFlow.Product.BusinessCategory')}</p>
              </Col>
              <Col xs={24} lg={18}>
                <p>{state.business_category && state.business_category}</p>
              </Col>
              <Col xs={24} lg={6}>
                <p>{t('CheckoutFlow.Product.BusinessEmail')}</p>
              </Col>
              <Col xs={24} lg={18}>
                <p>{state.business_email && state.business_email}</p>
              </Col>
              <Col xs={24} lg={6}>
                <p>{t('CheckoutFlow.Product.AboutBusiness')}</p>
              </Col>
              <Col xs={24} lg={18}>
                <p>{business_description && business_description}</p>
              </Col>
              <Col xs={24} lg={6}>
                <p>{t('CheckoutFlow.Product.Logo')}</p>
              </Col>
              <Col xs={24} lg={18}>
                <p>{business_logo}</p>
              </Col>
              <Col xs={24} lg={6}>
                <p>{t('CheckoutFlow.Product.Domain')}</p>
              </Col>
              <Col xs={24} lg={9}>
                <p>{domain_provider}</p>
              </Col>
              <Col xs={24} lg={9}>
                {/* <p>Country</p> */}
              </Col>
              <Col xs={24} lg={6}>
              {t('CheckoutFlow.Product.DomainDetails')}
              </Col>
              <Col xs={24} lg={9}>
                <p>{state.domain_username}</p>
              </Col>
              <Col xs={24} lg={9}>
                <p>{state.domain_password}</p>
              </Col>
              <Col xs={24} lg={6}>
                <p>{t('CheckoutFlow.Product.Currency')}</p>
              </Col>
              <Col xs={24} lg={18}>
                <p>{state.default_currency && state.default_currency}</p>
              </Col>
              <Col xs={24} lg={6}>
                <p>{t('CheckoutFlow.Product.AdditionalLanguage')}</p>
              </Col>
              <Col xs={24} lg={18}>
                {languages.map(l => (<p>{l},</p>))}
              </Col>
              <Col xs={24} lg={6}>
                <p>{t('CheckoutFlow.Product.Colour')}</p>
              </Col>
              <Col xs={24} lg={9}>
                <p>{t('CheckoutFlow.Product.PrimaryColorHEX')}</p>
                <div style={{ display: "flex" }}>
                  <div style={{ background: Primary_color, height: "20px", width: "20px", marginRight: "10px" }}></div>
                  <p>{Primary_color}</p>
                </div>
              </Col>
              <Col xs={24} lg={9}>
                <p>{t('CheckoutFlow.Product.SecondaryColourHEX')}</p>
                <div style={{ display: "flex" }}>
                  <div style={{ background: Secondary_color, height: "20px", width: "20px", marginRight: "10px" }}></div>
                  <p>{Secondary_color}</p>
                </div>
              </Col>
              <Col xs={24} lg={6}>
                <p>{t('CheckoutFlow.Product.PaymentGateway')}</p>
              </Col>
              <Col xs={24} lg={9}>
                <p>{state.payment_gateway_provider}</p>
                <div>
                  <p>{state.payment_country && state.payment_country}</p>
                </div>
              </Col>
              <Col xs={24} lg={9}>
                <p>{sessionStorage.getItem("currency_sign")} {" "}<CurrencyFormat
                  value={(((paymetGatewayCharge) * sessionStorage.getItem("currency_rate")).toFixed(1))}
                  displayType={"text"}
                  thousandSeparator={true}
                /> </p>
                <div>
                  <p>{state.payment_other_details}</p>
                </div>
              </Col>
              <Col xs={24} lg={6}>
                <p>{t('CheckoutFlow.Product.ShippingProvider')}</p>
              </Col>
              <Col xs={24} lg={9}>

                <p>{state.shipping_provider}</p>
                <div>
                  <p>{state.shipping_country && state.shipping_country}</p>
                </div>
              </Col>
              <Col xs={24} lg={9}>
                <p>{sessionStorage.getItem("currency_sign")} {" "} <CurrencyFormat
                  value={(((shippingGatewatCharge) * sessionStorage.getItem("currency_rate")).toFixed(1))}
                  displayType={"text"}
                  thousandSeparator={true}
                /></p>
                <div>
                  <p>{state.shipping_other_details}</p>
                </div>
              </Col>
            </Row>
          </div>


        </Card>
        <br />
        <Card
          className="Select_plan"
          title="Add Ons"
          extra={
            <Button onClick={() => setState({
              ...state,
              current: 2
            })} type="primary" icon="edit" />
          }

          style={{}}
        >
          <Row gutter={16} type="flex"  style={direction === 'ltr' ? {textAlign:"left",padding: "15px" } : {textAlign:"right",padding: "15px" }} >
            <Col xs={24} lg={6}>
              <p>{t('CheckoutFlow.AddOne.Training')}</p>
            </Col>
            <Col xs={24} lg={12}>
              <p>{trainingHour} {t('CheckoutFlow.AddOne.Hours')}</p>
            </Col>
            <Col xs={24} lg={6}>
              {trainingHour > 2 &&
                <div>
                  {sessionStorage.getItem("currency_sign")} {(trainingPrice * sessionStorage.getItem("currency_rate")).toFixed(1)}
                </div>
              }
            </Col>
            <Col xs={24} lg={6}>
              <p>{t('CheckoutFlow.AddOne.ProductListing')}</p>
            </Col>
            <Col xs={24} lg={12}>
              <p>{productListCount}</p>
            </Col>
            <Col xs={24} lg={6}>
              {sessionStorage.getItem("currency_sign")} {(productListCountPrice * sessionStorage.getItem("currency_rate")).toFixed(1)}
            </Col>
          </Row>


        </Card>
      </Fragment>
    )

  }



  const steps = [
    {
      title: `${t('CheckoutFlow.PlanDetails')}`,
      content: PlanDetails(),
    },
    {
      title: `${t('CheckoutFlow.ProductDetails')}`,
      content: ProductDetails()

    },
    {
      title: `${t('CheckoutFlow.Add-ons')}`,
      content: AddOns()
      //  AddOns()

    },
    {
      title: `${t('CheckoutFlow.Summary')}`,
      content: Summary()
      // Summary()
    }
  ];

  // if (user === null) {
  //   return <Redirect to="/login"></Redirect>;
  // }else 
  if (loading) {
    return (
      <Fragment>
        <ScrollTop></ScrollTop>
        <Spin></Spin>
      </Fragment>
    )
  } else return (
    <Fragment>
      <ScrollTop></ScrollTop>
      <Helmet>
        <title>Checkout</title>
        <meta
          name="Subscribe your ready made apps for your business. Start your eCommerce business in a minute. Just choose the product which suits best to your work and you are all done."
          content="Buy Ready Made eCommerce App Business Application Work Purchase"
        />
      </Helmet>
      <ScrollTop />
      {console.log("plan cost >> ", PlanCost)}
      <section className="UAF-container-fluid" style={{ marginTop: "100px", marginBottom: "80px" }}>
        <Title level={2}>{t('CheckoutFlow.Checkout')}</Title>
        {direction === DIRECTIONS.LTR && <Fragment>
        <Steps current={current} style={{ marginBottom: "15px" }} size="small">
          {steps.map(item => (
            <Step key={item.title} title={item.title} />
          ))}
        </Steps>
          </Fragment> }

          {direction === DIRECTIONS.RTL && <Fragment>
        <Steps current={current} style={{ marginBottom: "15px" }} size="small">
          {steps.map(item => (
            <Step key={item.title} className="checkout-title-rtl" title={item.title} />
          ))}
        </Steps>
          </Fragment> }

        <Row>

          <Col xs={24} lg={16} style={{ padding: "10px 15px" }}>
            {steps[current].content}
            {current > 0 && (
               
              <Button
                type="primary"
                style={{ width: "100%", height: "35px", marginTop: "10px" }} onClick={() => prev()}>
                  {direction === DIRECTIONS.LTR && <Fragment>
                    <Icon type="left" /> {t('CheckoutFlow.Back')} {steps[current - 1].title}
                    </Fragment>}
                    {direction === DIRECTIONS.RTL && <Fragment>
                     {t('CheckoutFlow.Back')} {steps[current - 1].title} <Icon type="left" />
                    </Fragment>}
              </Button>
   
            )}
            {current < steps.length - 1 && (
              <Button

                style={{ width: "100%", height: "35px", marginTop: "10px" }}
                type="primary" onClick={() => next()}>
                     {direction === DIRECTIONS.LTR && <Fragment>
                      {t('CheckoutFlow.Continue')} {steps[current + 1].title} <Icon type="right" />
                     </Fragment>}
                     {direction === DIRECTIONS.RTL && <Fragment>
                      <Icon type="right" /> {t('CheckoutFlow.Continue')} {steps[current + 1].title}
                     </Fragment>}
              </Button>
            )}
          </Col>

          <Col xs={24} lg={8} style={{ padding: "10px 15px" }}>
            {/* Cart Summery */}
            {direction === DIRECTIONS.LTR && <Fragment>
            <Card
              className="slim-card"

              style={{ width: "100%" }}
            >
              <div>
                <Row type="flex">
                  <Col xs={8} sm={3} lg={7}>
                    <div style={{ width: "80px", height: "80px", borderRadius: "15px", objectFit: "cover" }}>
                      <img style={{ width: "100%", height: "100%", borderRedius: "5px", objectFit: "cover" }} src={SingleVendorImg}></img>
                    </div>
                  </Col>
                  <Col xs={16} sm={20} lg={17} className="chkout_Prive_right_Col_head">
                    <Text className="chkoutPage_fontsize" style={{ fontWeight: "600" }}>
                    {t('CheckoutFlow.SingleVendorEcommerceApp')}
                    </Text>
                    <br />
                    <br />
                    <Text className="chkoutPage_fontsize" style={{ fontWeight: "400", marginTop: "10px" }}>
                    {t('CheckoutFlow.ThemeID')}
                    </Text>
                    <br />
                    <Text className="chkoutPage_fontsize" style={{ fontWeight: "600" }}>
                      {`UAF-TH-${match.params.themeID}`}
                    </Text>

                  </Col>
                </Row>
              </div>
              <hr style={{ opacity: "0.3" }} />
              <Row type="flex" justify="space-around" align="middle">
                <Col xs={16} lg={10} className="left-text ">
                  <Text>{t('CheckoutFlow.PackageType')}</Text>
                </Col>
                <Col xs={8} lg={14} className="right-text">
                  <Title
                    style={{
                      fontSize: "16px",
                      color: "rgb(0, 188, 212)",
                      fontWeight: "600"
                    }}
                  >
                    <p style={{ textTransform: "capitalize", marginBottom: "0px" }}>{PackageChoosed}</p>
                    {/* <p style={{ textTransform: "capitalize" }}>{PlanCost[0].value}</p> */}
                  </Title>
                </Col>
                <Col xs={16} lg={10} className="left-text ">
                  <Text>{t('CheckoutFlow.PlanType')}</Text>
                </Col>
                <Col xs={8} lg={14} className="right-text">
                  <Title
                    style={{
                      fontSize: "16px",
                      color: "rgb(0, 188, 212)",
                      fontWeight: "600"
                    }}
                  >
                    <p style={{ textTransform: "capitalize", marginBottom: "0px" }}>
                      {/* {PlanCost[0].value} */}
                      {plan_name}
                    </p>
                  </Title>
                </Col>
              </Row>
              {PlanCost[0].after_discount_price == '' ? null : <Row type="flex" justify="space-around" align="middle">
                <Col xs={16} lg={10} className="left-text sm-text-center package_addition_price_left">
                  {PackageChoosed == "Basic" ? (<Text>{t('CheckoutFlow.WebsiteIncluded')}</Text>) : (PackageChoosed === "Ultra" ? (<Text>{t('CheckoutFlow.Website')} + {t('CheckoutFlow.MobileAppIncluded')}</Text>) : (PackageChoosed === "Premium" ? (<Text>{t('CheckoutFlow.MobileAppIncluded')}</Text>) : null))}
                </Col>
                <Col xs={8} lg={14} className="right-text sm-text-center package_addition_price_right">
                  <Title

                    style={{
                      fontSize: "16px",
                      color: "rgb(0, 188, 212)",
                      fontWeight: "600"
                    }}
                  >
                    <span>{sessionStorage.getItem("currency_sign")}</span>{" "}
                    <CurrencyFormat
                      value={plan_type === "Yearly" ? ((+(plan_price * sessionStorage.getItem("currency_rate")) * +12).toFixed(1)) : (((plan_price) * sessionStorage.getItem("currency_rate")).toFixed(1))}
                      displayType={"text"}
                      thousandSeparator={true}
                    />

                  </Title>
                  <Text>
                    {plan_type}
                    {/* {PlanCost[0].type} */}
                  </Text>
                </Col>
              </Row>
              }

              {PlanCost[0].type == "Monthly" && <div>
                <hr style={{ opacity: "0.3", height: "1px" }} />
                <Row type="flex" justify="space-around" align="middle">
                  <Col xs={16} lg={14} className="left-text sm-text-center package_addition_price_left">
                    <Text style={{ fontSize: "14px" }}>{t('CheckoutFlow.Recurringmonthlycharges')}</Text>
                    <br />
                    <Text>{t('CheckoutFlow.Startingnextmonth')}</Text>
                  </Col>
                  <Col xs={8} lg={10} className="right-text sm-text-center package_addition_price_right" >
                    <Title level={4} style={{ fontSize: "16px", color: "rgb(0, 188, 212)", fontWeight: "600" }}>
                      <span>{sessionStorage.getItem("currency_sign")} </span>{" "}

                      {/* <CurrencyFormat
                        value={PlanCost[0].type == "Yearly" ? (+PlanCost[0].after_discount_price * 12).toFixed(1) : PlanCost[0].after_discount_price}
                        displayType={"text"}
                        thousandSeparator={true}
                      /> */}

                      <CurrencyFormat
                        value={plan_type === "Yearly" ? ((+(plan_price * sessionStorage.getItem("currency_rate")) * +12).toFixed(1)) : (((plan_price) * sessionStorage.getItem("currency_rate")).toFixed(1))}
                        displayType={"text"}
                        thousandSeparator={true}
                      />

                    </Title>
                    <Text>
                      {plan_type}
                      {/* {PlanCost[0].type} */}
                    </Text>
                  </Col>
                </Row>
              </div>}
              <hr style={{ opacity: "0.3" }} />
              <Row type="flex" justify="space-around" align="middle">
                <Col xs={15} lg={10} className="left-text package_addition_price_left">
                  <Text>{t('CheckoutFlow.OneTimeSetupCost')}</Text>
                </Col>
                <Col xs={9} lg={14} className="right-text package_addition_price_right">
                  <Title
                    style={{
                      fontSize: "16px",
                      color: "rgb(0, 188, 212)",
                      fontWeight: "600"
                    }}
                  >
                    <p style={{ textTransform: "capitalize", marginBottom: "0px" }}>
                      {/* {PlanCost[0].value} */}
                      {sessionStorage.getItem("currency_sign")} {" "}

                      <CurrencyFormat
                        value={(one_time_setup_charge * sessionStorage.getItem("currency_rate")).toFixed(1)}
                        displayType={"text"}
                        thousandSeparator={true}
                      />
                    </p>
                  </Title>
                </Col>
              </Row>

              {languages && languages.length > 0 && <div>
                <hr style={{ opacity: "0.3", height: "1px" }} />
                <Row type="flex" justify="space-around" align="middle">
                  <Col xs={14} lg={14} className="left-text sm-text-center package_addition_price_left" >
                    <Text style={{ fontSize: "14px" }}>{t('CheckoutFlow.Additionallanguagecharges')}</Text>
                  </Col>
                  <Col xs={10} lg={10} className="right-text sm-text-center package_addition_price_right" >
                    <Title level={4} style={{ fontSize: "16px", color: "rgb(0, 188, 212)", fontWeight: "600" }}>
                      <span>{sessionStorage.getItem("currency_sign")} </span>{" "}

                      <CurrencyFormat
                        value={((languages.length * 100) * sessionStorage.getItem("currency_rate")).toFixed(1)}
                        displayType={"text"}
                        thousandSeparator={true}
                      />

                    </Title>
                  </Col>
                </Row>
              </div>}
              {PaymentGatewayProvider && PaymentGatewayProvider === "payment-gateway-other" && <div>
                <hr style={{ opacity: "0.3", height: "1px" }} />
                <Row type="flex" justify="space-around" align="middle">
                  <Col xs={16} lg={14} className="left-text sm-text-center" >
                    <Text style={{ fontSize: "14px" }}>{t('CheckoutFlow.Custompaymentgatewaycharges')}</Text>
                  </Col>
                  <Col xs={8} lg={10} className="right-text sm-text-center" >
                    <Title
                      level={4}
                      style={{
                        fontSize: "16px",
                        color: "rgb(0, 188, 212)",
                        fontWeight: "600"
                      }}
                    >
                      <span>{sessionStorage.getItem("currency_sign")} </span>{" "}

                      <CurrencyFormat
                        value={(paymetGatewayCharge * sessionStorage.getItem("currency_rate")).toFixed(1)}
                        displayType={"text"}
                        thousandSeparator={true}
                      />

                    </Title>
                  </Col>
                </Row>
              </div>}
              {delProvider && delProvider === 'deliveryProvider-others' && <div>
                <hr style={{ opacity: "0.3", height: "1px" }} />
                <Row type="flex" justify="space-around" align="middle">
                  <Col
                    xs={24}
                    lg={14}
                    className="left-text sm-text-center"
                  >
                    <Text style={{ fontSize: "14px" }}>{t('CheckoutFlow.Customshippinggatewaycharges')}</Text>
                  </Col>
                  <Col
                    xs={24}
                    lg={10}
                    className="right-text sm-text-center"
                  >
                    <Title
                      level={4}
                      style={{
                        fontSize: "16px",
                        color: "rgb(0, 188, 212)",
                        fontWeight: "600"
                      }}
                    >
                      <span>{sessionStorage.getItem("currency_sign")} </span>{" "}

                      <CurrencyFormat
                        value={(shippingGatewatCharge * sessionStorage.getItem("currency_rate")).toFixed(1)}
                        displayType={"text"}
                        thousandSeparator={true}
                      />

                    </Title>
                  </Col>
                </Row>
              </div>}
              {trainingPrice && trainingPrice > 0 ? <div>
                <hr style={{ opacity: "0.3", height: "1px" }} />
                <Row type="flex" justify="space-around" align="middle">
                  <Col
                    xs={24}
                    lg={14}
                    className="left-text sm-text-center"
                  >
                    <Text style={{ fontSize: "14px" }}>{t('CheckoutFlow.Trainingcharge')}</Text>
                  </Col>
                  <Col
                    xs={24}
                    lg={10}
                    className="right-text sm-text-center"
                  >
                    <Title
                      level={4}
                      style={{
                        fontSize: "16px",
                        color: "rgb(0, 188, 212)",
                        fontWeight: "600"
                      }}
                    >
                      <span>{sessionStorage.getItem("currency_sign")} </span>{" "}

                      <CurrencyFormat
                        value={(trainingPrice * sessionStorage.getItem("currency_rate")).toFixed(1)}
                        displayType={"text"}
                        thousandSeparator={true}
                      />

                    </Title>
                  </Col>
                </Row>
              </div> : null}

              {productListCount && productListCount > 0 ? <div>
                <hr style={{ opacity: "0.3", height: "1px" }} />
                <Row type="flex" justify="space-around" align="middle">
                  <Col xs={16} lg={14} className="left-text sm-text-center package_addition_price_left" >
                    <Text style={{ fontSize: "14px" }}>{t('CheckoutFlow.Productlistingcharges')}</Text>
                  </Col>
                  <Col xs={8} lg={10} className="right-text sm-text-center package_addition_price_right" >
                    <Title level={4} style={{ fontSize: "16px", color: "rgb(0, 188, 212)", fontWeight: "600" }} >
                      <span>{sessionStorage.getItem("currency_sign")} </span>{" "}

                      <CurrencyFormat
                        value={(productListCountPrice * sessionStorage.getItem("currency_rate")).toFixed(1)}
                        displayType={"text"}
                        thousandSeparator={true}
                      />

                    </Title>
                  </Col>
                </Row>
              </div> : null}

              {/* Coupon */}

              {/* <div>

                <hr style={{ opacity: "0.3", height: "1px" }} />
                <InputGroup compact style={{ margin: "30px auto" }}>
                  <Input
                    // disabled={disablecouponfield}
                    name="coupon_code"
                    placeholder="Coupon/Partner Code"
                    // value={coupon_code}
                    // onChange={e => onChange(e)}
                    className="coupon-code-input-box"
                  />
                  <Button
                    type="primary"
                    // disabled={disablecouponfield}
                    // onClick={applyCouponCode}
                    className="coupon-code-apply-btn"
                  >
                    Apply Coupon
            </Button>
                </InputGroup>
                <Card className="slim-card" style={{ width: "100%" }}>
         

            <Button
              // onClick={enableCouponFields}
              className="cancelCoupon"
              size="small"
              shape="circle"
              icon="close"
            />
          </Card>
              </div>
               */}
              {/* Coupon End */}
              <hr style={{ opacity: "0.3", height: "1px" }} />
              <Row type="flex" justify="space-around" align="middle">
                <Col xs={8} lg={14} className="left-text sm-text-center package_addition_price_left" >
                  <Text style={{ fontSize: "18px" }}>{t('CheckoutFlow.Total')}</Text>
                </Col>
                <Col xs={16} lg={10} className="right-text sm-text-center package_addition_price_right" >
                  <Title className="chkout_price_totalCost" level={4}>
                    <span>{sessionStorage.getItem("currency_sign")} </span>{" "}
                    {console.log("One time Setup Cost >>", one_time_setup_charge)}
                    <CurrencyFormat
                      value={((+(PlanCost[0].type === "Yearly" ? (+plan_price * +12).toFixed(1) : plan_price) + +(languages && languages.length * 100) + +(trainingPrice & trainingPrice) + +(paymetGatewayCharge && paymetGatewayCharge) + +(shippingGatewatCharge && shippingGatewatCharge) + +(productListCountPrice && productListCountPrice) + one_time_setup_charge) * sessionStorage.getItem("currency_rate")).toFixed(1)}
                      displayType={"text"} thousandSeparator={true} />
                  </Title>
                </Col>

                <Col xs={24}>
                  <p style={{ marginBottom: "0px" }}>( {t('CheckoutFlow.AmountToBePaid')} <span style={{ color: "rgb(0, 188, 212)", fontWeight: "600" }}>SGD{" "}
                    <CurrencyFormat
                      value={((+(PlanCost[0].type === "Yearly" ? (+plan_price * +12).toFixed(1) : plan_price) + +(languages && languages.length * 100) + +(trainingPrice & trainingPrice) + +(paymetGatewayCharge && paymetGatewayCharge) + +(shippingGatewatCharge && shippingGatewatCharge) + +(productListCountPrice && productListCountPrice) + one_time_setup_charge) * SGDPrice).toFixed(1)}
                      displayType={"text"}
                      thousandSeparator={true}
                    /></span> )</p>
                </Col>
              </Row>


            </Card>
            </Fragment>}
            
            {/* RTL */}
            
            {direction === DIRECTIONS.RTL && <Fragment>
            <Card
              className="slim-card"

              style={{ width: "100%" }}
            >
              <div>
                <Row type="flex">
                  <Col xs={8} sm={3} lg={7}>
                    <div style={{ width: "80px", height: "80px", borderRadius: "15px", objectFit: "cover" }}>
                      <img style={{ width: "100%", height: "100%", borderRedius: "5px", objectFit: "cover" }} src="https://file-uploads-application-management.s3.us-east-2.amazonaws.com/sports%20shop-1572965291565-1592462198060.jpg"></img>
                    </div>
                  </Col>
                  <Col xs={16} sm={20} lg={17} className="chkout_Prive_right_Col_head">
                    <Text className="chkoutPage_fontsize" style={{ fontWeight: "600" }}>
                    {t('CheckoutFlow.SingleVendorEcommerceApp')}
                    </Text>
                    <br />
                    <br />
                    <Text className="chkoutPage_fontsize" style={{ fontWeight: "400", marginTop: "10px" }}>
                    {t('CheckoutFlow.ThemeID')}
                    </Text>
                    <br />
                    <Text className="chkoutPage_fontsize" style={{ fontWeight: "600" }}>
                      {`UAF-TH-${match.params.themeID}`}
                    </Text>

                  </Col>
                </Row>
              </div>
              <hr style={{ opacity: "0.3" }} />
              <Row type="flex" justify="space-around" align="middle">
                <Col xs={16} lg={10} className="right-text">
                  <Text>{t('CheckoutFlow.PackageType')}</Text>
                </Col>
                <Col xs={8} lg={14} className="left-text">
                  <Title
                    style={{
                      fontSize: "16px",
                      color: "rgb(0, 188, 212)",
                      fontWeight: "600"
                    }}
                  >
                    <p style={{ textTransform: "capitalize", marginBottom: "0px" }}>
                      {PackageChoosed === "Basic" ? `${t('CheckoutFlow.Plan.Basic')}` : PackageChoosed === "Ultra" ? `${t('CheckoutFlow.Plan.Ultra')}` : PackageChoosed === "Premium" ? `${t('CheckoutFlow.Plan.Premium')}` : null }
                      </p>

                  </Title>
                </Col>
                <Col xs={16} lg={10} className="right-text ">
                  <Text>{t('CheckoutFlow.PlanType')}</Text>
                </Col>
                <Col xs={8} lg={14} className="left-text">
                  <Title
                    style={{
                      fontSize: "16px",
                      color: "rgb(0, 188, 212)",
                      fontWeight: "600"
                    }}
                  >
                    <p style={{ textTransform: "capitalize", marginBottom: "0px" }}>
                      {/* {PlanCost[0].value} */}

                      {plan_name === "standard-yearly" ? `${t('CheckoutFlow.Plan.Standard')}-${t('CheckoutFlow.Plan.Yearly')}` : plan_name === "standard-monthly" ? `${t('CheckoutFlow.Plan.Standard')}-${t('CheckoutFlow.Plan.Monthly')}` : plan_name === "rental-monthly" ? `${t('CheckoutFlow.Plan.Rental')} - ${t('CheckoutFlow.Plan.Monthly')}` :  `${t('CheckoutFlow.Plan.Standard')}-${t('CheckoutFlow.Plan.Monthly')}`}
                
                      {console.log("plan name",plan_name)}
                    </p>
                  </Title>
                </Col>
              </Row>
              {PlanCost[0].after_discount_price == '' ? null : <Row type="flex" justify="space-around" align="middle">
                <Col xs={16} lg={10} className="right-text sm-text-center package_addition_price_left">
                  {PackageChoosed == "Basic" ? (<Text>{t('CheckoutFlow.WebsiteIncluded')}</Text>) : (PackageChoosed === "Ultra" ? (<Text>{t('CheckoutFlow.Website')} + {t('CheckoutFlow.MobileAppIncluded')}</Text>) : (PackageChoosed === "Premium" ? (<Text>{t('CheckoutFlow.MobileAppIncluded')}</Text>) : null))}
                </Col>
                <Col xs={8} lg={14} className="left-text sm-text-center package_addition_price_right">
                  <Title

                    style={{
                      fontSize: "16px",
                      color: "rgb(0, 188, 212)",
                      fontWeight: "600"
                    }}
                  >
                    <span>{sessionStorage.getItem("currency_sign")}</span>{" "}
                    <CurrencyFormat
                      value={plan_type === "Yearly" ? ((+(plan_price * sessionStorage.getItem("currency_rate")) * +12).toFixed(1)) : (((plan_price) * sessionStorage.getItem("currency_rate")).toFixed(1))}
                      displayType={"text"}
                      thousandSeparator={true}
                    />

                  </Title>
                  <Text>
                    {plan_type === 'Monthly' ? `${t('CheckoutFlow.Plan.Monthly')}` : `${t('CheckoutFlow.Plan.Yearly')}` }
                    {/* {PlanCost[0].type} */}
                  </Text>
                </Col>
              </Row>
              }

              {PlanCost[0].type == "Monthly" && <div>
                <hr style={{ opacity: "0.3", height: "1px" }} />
                <Row type="flex" justify="space-around" align="middle">
                  <Col xs={16} lg={14} className="right-text sm-text-center package_addition_price_left">
                    <Text style={{ fontSize: "14px" }}>{t('CheckoutFlow.Recurringmonthlycharges')}</Text>
                    <br />
                    <Text>{t('CheckoutFlow.Startingnextmonth')}</Text>
                  </Col>
                  <Col xs={8} lg={10} className="left-text sm-text-center package_addition_price_right" >
                    <Title level={4} style={{ fontSize: "16px", color: "rgb(0, 188, 212)", fontWeight: "600" }}>
                      <span>{sessionStorage.getItem("currency_sign")} </span>{" "}
                      <CurrencyFormat
                        value={plan_type === "Yearly" ? ((+(plan_price * sessionStorage.getItem("currency_rate")) * +12).toFixed(1)) : (((plan_price) * sessionStorage.getItem("currency_rate")).toFixed(1))}
                        displayType={"text"}
                        thousandSeparator={true}
                      />

                    </Title>
                    <Text>
                    {t('CheckoutFlow.Plan.Monthly')}
                      {/* {PlanCost[0].type} */}
                    </Text>
                  </Col>
                </Row>
              </div>}
              <hr style={{ opacity: "0.3" }} />
              <Row type="flex" justify="space-around" align="middle">
                <Col xs={15} lg={10} className="right-text package_addition_price_left">
                  <Text>{t('CheckoutFlow.OneTimeSetupCost')}</Text>
                </Col>
                <Col xs={9} lg={14} className="left-text package_addition_price_right">
                  <Title
                    style={{
                      fontSize: "16px",
                      color: "rgb(0, 188, 212)",
                      fontWeight: "600"
                    }}
                  >
                    <p style={{ textTransform: "capitalize", marginBottom: "0px" }}>
                      {/* {PlanCost[0].value} */}
                      {sessionStorage.getItem("currency_sign")} {" "}

                      <CurrencyFormat
                        value={(one_time_setup_charge * sessionStorage.getItem("currency_rate")).toFixed(1)}
                        displayType={"text"}
                        thousandSeparator={true}
                      />
                    </p>
                  </Title>
                </Col>
              </Row>

              {languages && languages.length > 0 && <div>
                <hr style={{ opacity: "0.3", height: "1px" }} />
                <Row type="flex" justify="space-around" align="middle">
                  <Col xs={14} lg={14} className="right-text sm-text-center package_addition_price_left" >
                    <Text style={{ fontSize: "14px" }}>{t('CheckoutFlow.Additionallanguagecharges')}</Text>
                  </Col>
                  <Col xs={10} lg={10} className="left-text sm-text-center package_addition_price_right" >
                    <Title level={4} style={{ fontSize: "16px", color: "rgb(0, 188, 212)", fontWeight: "600" }}>
                      <span>{sessionStorage.getItem("currency_sign")} </span>{" "}

                      <CurrencyFormat
                        value={((languages.length * 100) * sessionStorage.getItem("currency_rate")).toFixed(1)}
                        displayType={"text"}
                        thousandSeparator={true}
                      />

                    </Title>
                  </Col>
                </Row>
              </div>}
              {PaymentGatewayProvider && PaymentGatewayProvider === "payment-gateway-other" && <div>
                <hr style={{ opacity: "0.3", height: "1px" }} />
                <Row type="flex" justify="space-around" align="middle">
                  <Col xs={16} lg={14} className="right-text sm-text-center" >
                    <Text style={{ fontSize: "14px" }}>{t('CheckoutFlow.Custompaymentgatewaycharges')}</Text>
                  </Col>
                  <Col xs={8} lg={10} className="left-text sm-text-center" >
                    <Title
                      level={4}
                      style={{
                        fontSize: "16px",
                        color: "rgb(0, 188, 212)",
                        fontWeight: "600"
                      }}
                    >
                      <span>{sessionStorage.getItem("currency_sign")} </span>{" "}

                      <CurrencyFormat
                        value={(paymetGatewayCharge * sessionStorage.getItem("currency_rate")).toFixed(1)}
                        displayType={"text"}
                        thousandSeparator={true}
                      />

                    </Title>
                  </Col>
                </Row>
              </div>}
              {delProvider && delProvider === 'deliveryProvider-others' && <div>
                <hr style={{ opacity: "0.3", height: "1px" }} />
                <Row type="flex" justify="space-around" align="middle">
                  <Col
                    xs={24}
                    lg={14}
                    className="right-text sm-text-center"
                  >
                    <Text style={{ fontSize: "14px" }}>{t('CheckoutFlow.Customshippinggatewaycharges')}</Text>
                  </Col>
                  <Col
                    xs={24}
                    lg={10}
                    className="left-text sm-text-center"
                  >
                    <Title
                      level={4}
                      style={{
                        fontSize: "16px",
                        color: "rgb(0, 188, 212)",
                        fontWeight: "600"
                      }}
                    >
                      <span>{sessionStorage.getItem("currency_sign")} </span>{" "}

                      <CurrencyFormat
                        value={(shippingGatewatCharge * sessionStorage.getItem("currency_rate")).toFixed(1)}
                        displayType={"text"}
                        thousandSeparator={true}
                      />

                    </Title>
                  </Col>
                </Row>
              </div>}
              {trainingPrice && trainingPrice > 0 ? <div>
                <hr style={{ opacity: "0.3", height: "1px" }} />
                <Row type="flex" justify="space-around" align="middle">
                  <Col
                    xs={24}
                    lg={14}
                    className="right-text sm-text-center"
                  >
                    <Text style={{ fontSize: "14px" }}>{t('CheckoutFlow.Trainingcharge')}</Text>
                  </Col>
                  <Col
                    xs={24}
                    lg={10}
                    className="left-text sm-text-center"
                  >
                    <Title
                      level={4}
                      style={{
                        fontSize: "16px",
                        color: "rgb(0, 188, 212)",
                        fontWeight: "600"
                      }}
                    >
                      <span>{sessionStorage.getItem("currency_sign")} </span>{" "}

                      <CurrencyFormat
                        value={(trainingPrice * sessionStorage.getItem("currency_rate")).toFixed(1)}
                        displayType={"text"}
                        thousandSeparator={true}
                      />

                    </Title>
                  </Col>
                </Row>
              </div> : null}

              {productListCount && productListCount > 0 ? <div>
                <hr style={{ opacity: "0.3", height: "1px" }} />
                <Row type="flex" justify="space-around" align="middle">
                  <Col xs={16} lg={14} className="right-text sm-text-center package_addition_price_left" >
                    <Text style={{ fontSize: "14px" }}>{t('CheckoutFlow.Productlistingcharges')}</Text>
                  </Col>
                  <Col xs={8} lg={10} className="left-text sm-text-center package_addition_price_right" >
                    <Title level={4} style={{ fontSize: "16px", color: "rgb(0, 188, 212)", fontWeight: "600" }} >
                      <span>{sessionStorage.getItem("currency_sign")} </span>{" "}

                      <CurrencyFormat
                        value={(productListCountPrice * sessionStorage.getItem("currency_rate")).toFixed(1)}
                        displayType={"text"}
                        thousandSeparator={true}
                      />

                    </Title>
                  </Col>
                </Row>
              </div> : null}

              {/* Coupon */}

              {/* <div>

                <hr style={{ opacity: "0.3", height: "1px" }} />
                <InputGroup compact style={{ margin: "30px auto" }}>
                  <Input
                    // disabled={disablecouponfield}
                    name="coupon_code"
                    placeholder="Coupon/Partner Code"
                    // value={coupon_code}
                    // onChange={e => onChange(e)}
                    className="coupon-code-input-box"
                  />
                  <Button
                    type="primary"
                    // disabled={disablecouponfield}
                    // onClick={applyCouponCode}
                    className="coupon-code-apply-btn"
                  >
                    Apply Coupon
            </Button>
                </InputGroup>
                <Card className="slim-card" style={{ width: "100%" }}>
         

            <Button
              // onClick={enableCouponFields}
              className="cancelCoupon"
              size="small"
              shape="circle"
              icon="close"
            />
          </Card>
              </div>
               */}
              {/* Coupon End */}
              <hr style={{ opacity: "0.3", height: "1px" }} />
              <Row type="flex" justify="space-around" align="middle">
                <Col xs={8} lg={14} className="right-text sm-text-center package_addition_price_left" >
                  <Text style={{ fontSize: "18px" }}>{t('CheckoutFlow.Total')}</Text>
                </Col>
                <Col xs={16} lg={10} className="left-text sm-text-center package_addition_price_right" >
                  <Title className="chkout_price_totalCost" level={4}>
                    <span>{sessionStorage.getItem("currency_sign")} </span>{" "}
                    {console.log("One time Setup Cost >>", one_time_setup_charge)}
                    <CurrencyFormat
                      value={((+(PlanCost[0].type === "Yearly" ? (+plan_price * +12).toFixed(1) : plan_price) + +(languages && languages.length * 100) + +(trainingPrice & trainingPrice) + +(paymetGatewayCharge && paymetGatewayCharge) + +(shippingGatewatCharge && shippingGatewatCharge) + +(productListCountPrice && productListCountPrice) + one_time_setup_charge) * sessionStorage.getItem("currency_rate")).toFixed(1)}
                      displayType={"text"} thousandSeparator={true} />
                  </Title>
                </Col>

                <Col xs={24}>
                  <p style={{ marginBottom: "0px" }}>( {t('CheckoutFlow.AmountToBePaid')} <span style={{ color: "rgb(0, 188, 212)", fontWeight: "600" }}>SGD{" "}
                    <CurrencyFormat
                      value={((+(PlanCost[0].type === "Yearly" ? (+plan_price * +12).toFixed(1) : plan_price) + +(languages && languages.length * 100) + +(trainingPrice & trainingPrice) + +(paymetGatewayCharge && paymetGatewayCharge) + +(shippingGatewatCharge && shippingGatewatCharge) + +(productListCountPrice && productListCountPrice) + one_time_setup_charge) * SGDPrice).toFixed(1)}
                      displayType={"text"}
                      thousandSeparator={true}
                    /></span> )</p>
                </Col>
              </Row>


            </Card>
            </Fragment>}
           
           {/* Cart Summery */}
            {/* Terms & Condition */}
            <br />
            {/* Cart Summery */}
            {/* Terms & Condition */}
            <Checkbox onChange={checkboxHandle}
                // disabled={disable}
                >
              {t('CheckoutFlow.Iaccept')} <a target="_blank" href="https://www.upappfactory.com/terms-service">{t('CheckoutFlow.terms&condition')}</a>
            </Checkbox>
            {/* <Card className="slim-card" title="Terms & Condition" extra={<Title level={4} />} style={{ width: "100%" }} >
              <div>
                <div className="inner-scroll-example scroll_terms_contdition">
                  <h4 style={{ fontSize: "14px", fontWeight: "700" }}>
                    I agree that I have read all Terms & Conditions, Terms
                    of User & Privacy Policy on UP app factory website
                    <a href="https://www.upappfactory.com/terms-service" target="_blank" >(www.upappfactory.com)</a>. I also agree the following,
                  </h4>
                  <ol>
                    <li>
                      The delivery period committed to me by UPapp factory
                      of 7-12 business days will include the working days of
                      the country where I am based in and I have signed up
                      from. The business days commitment from UPapp factory
                      starts on the day I have supplied all the materials
                      required to complete my apps and admin panel (system).
                  <br />
                    </li>
                    <li>
                      The time frame does not include the time it would take
                      for the apps to be approved by app stores (Google Play
                      & iTunes) and the time frame committed to me by UPapp
                      factory will only include my apps being ready to go
                      live in the stores.
                  <br />
                    </li>
                    <li>
                      The plan that I am committed to along with the amount
                      being deducted from my card would start from now or
                      else I make the bank transfer to UPapp factory’s
                      corporate account.
                  <br />
                    </li>
                    <li>
                      The time frame does not include the time it would take
                      for the content to be live as it depends on me how
                      soon I provide the content to UPapp factory team to
                      ensure the content is live on my app.
                </li>
                    <li>
                      I agree to have my training session within the
                      committed time frame of 7-12 working days.
                  <br />
                    </li>
                    <li>
                      The time frame does not include the time it would take
                      for the payment gateway to be live as it depends on me
                      confirming the documents to the payment gateway
                      company recommended by UP app factory.
                  <br />
                    </li>
                  </ol>
                </div>
               
              </div>
            </Card> */}
            <br />

            {/* <Button
          type="primary"
          // onClick={bankTransfer}
          // disabled={disablePayment}
          style={{ width: "100%", height: "35px", marginTop: "10px" }}
        >
          Continue to Product Details
        </Button> */}
             {current > 0 && (
               
               <Button
                 type="primary"
                 style={{ width: "100%", height: "35px", marginTop: "10px" }} onClick={() => prev()}>
                   {direction === DIRECTIONS.LTR && <Fragment>
                     <Icon type="left" /> {t('CheckoutFlow.Back')} {steps[current - 1].title}
                     </Fragment>}
                     {direction === DIRECTIONS.RTL && <Fragment>
                      {t('CheckoutFlow.Back')} {steps[current - 1].title} <Icon type="left" />
                     </Fragment>}
               </Button>
    
             )}
            {/* Add  the total price and product name */}
            {current == 3 &&
              <div className="StripeCheckout-btn">
                <StripeCheckout
                  disabled={disablePayment}
                  currency="SGD"
                  // stripeKey="pk_live_A2VJciGTTF8M71aVAmNU1D1Q00llCnNX4Q"
                  stripeKey="pk_test_NBeYb9RNkdsAOz1dryn3OfQD001eWF4Fm5"
                  token={handleToken}
                  amount={((((+(PlanCost[0].type === "Yearly" ? (+plan_price * +12).toFixed(1) : plan_price) + +(languages && languages.length * 100) + +(trainingPrice & trainingPrice) + +(paymetGatewayCharge && paymetGatewayCharge) + +(shippingGatewatCharge && shippingGatewatCharge) + +(productListCountPrice && productListCountPrice) + one_time_setup_charge) * SGDPrice) * 100).toFixed(1))}
                  name={`Single Vendor Ecommerce App`}
                  billingAddress
                  shippingAddress
                >
                </StripeCheckout>
              </div>
            }

{current < steps.length - 1 && (
              <Button

                style={{ width: "100%", height: "35px", marginTop: "10px" }}
                type="primary" onClick={() => next()}>
                     {direction === DIRECTIONS.LTR && <Fragment>
                      {t('CheckoutFlow.Continue')} {steps[current + 1].title} <Icon type="right" />
                     </Fragment>}
                     {direction === DIRECTIONS.RTL && <Fragment>
                      <Icon type="right" /> {t('CheckoutFlow.Continue')} {steps[current + 1].title}
                     </Fragment>}
              </Button>
            )}
          </Col>
        </Row>
      </section>

    </Fragment>
  );
};

const RFC = Form.create({ name: "Request_for_customize" })(Store);

RFC.propTypes = {
  proceedToPay: PropTypes.func.isRequired,
  success: PropTypes.bool,
  order: PropTypes.object,
  user: PropTypes.object.isRequired,
};
const mapStateToProps = state => ({
  direction: withDirectionPropTypes.direction,
  success: state.checkout.success,
  order: state.checkout.order,
  user: state.auth.user
});
export default connect(mapStateToProps, {
  proceedToPay
})(withDirection(RFC));
