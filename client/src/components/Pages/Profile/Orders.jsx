import React, { Fragment, useEffect, useState } from 'react';
import { CSVLink } from 'react-csv';
import moment from 'moment';
import { Link, Redirect } from 'react-router-dom';
import ScrollTop from '../../ScrollTop';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import {Row, Col, Modal, Tabs, Spin, Collapse, Icon, Card, Result, Statistic} from 'antd';
import {
	getUserOrders,
} from '../../../actions/order.action';
import Hero from '../../../components/hero';

const { TabPane } = Tabs;
const { confirm } = Modal;
const { Panel } = Collapse;

const Orders = ({
	orders,
	loading,
	user,
	getUserOrders,
}) => {

	const [state, setState] = useState({
		SGDcurrex:''
	})

const {SGDcurrex} = state;

	useEffect(() => {
		getUserOrders();
		setState({
			SGDcurrex: JSON.parse(sessionStorage.getItem("sgd_exchange_rate_response"))
		})
	}, []);

	console.log(orders)
	console.log("SGDcurrex",SGDcurrex)


	return user === null ? (
		<Fragment>
		<Redirect to="/login" />
	  </Fragment>
	) : (
			<Fragment>
				<ScrollTop></ScrollTop>
				<Hero Title="My Orders" />
				{orders && orders.length === 0 ? (
<div style={{marginTop:"100px",marginBottom:"100px"}}>
<Result
    title="No Order Placed Yet"
    // extra={
    //   <Button type="primary" key="console">
        
    //   </Button>
    // }
  />
</div>
       ): (
					<div style={{maxWidth:"1260px",margin:"0 auto"}}>
				 <Collapse defaultActiveKey={['1']} style={{marginTop:"80px",marginBottom:"100px"}}>
					{orders && orders.map(order => 
					<Panel header={order.UID} key="1" style={{background:"#fff"}} >
							<Row gutter={16} type="flex" style={{marginTop:"20px",marginBottom:"20px"}}>
						<Col xs={24} md={12} lg={8}>
							<Card className="Order-cards"
							 title={`${order.product_name}`}
							 bordered={false}
							 extra={<div><Icon type="check-circle" theme="twoTone" twoToneColor="#52c41a" /> Active</div>}
							 style={{ width: "100%",
							 maxWidth:"450px",
							 boxShadow:"0px 0px 10px #cccccc61" }}
							>
							<Row className="" style={{borderBottom:"1px solid rgb(204 204 204 / 50%)",marginBottom:"10px",paddingBottom:"10px"}}>
							<Col xs={24} lg={8} style={{fontWeight:"600",marginBottom:"5px"}}>Order ID : </Col>
							<Col xs={24} lg={16} style={{marginBottom:"5px",textAlign:"right", fontSize:"16px"}}>{order.UID}</Col>
							</Row>
							<Row className="" style={{borderBottom:"1px solid rgb(204 204 204 / 50%)",marginBottom:"10px",paddingBottom:"10px"}}>
								<Col xs={24} lg={8} style={{fontWeight:"600",marginBottom:"5px"}}>Package Type : </Col>
								<Col xs={24} lg={16} style={{marginBottom:"5px",textAlign:"right", fontSize:"16px"}}>{order.pack_type}</Col>
							</Row>
							<Row className="" style={{borderBottom:"1px solid rgb(204 204 204 / 50%)",marginBottom:"10px",paddingBottom:"10px"}}>
								<Col xs={24} lg={8} style={{fontWeight:"600",marginBottom:"5px"}}>Plan Name : </Col>
								<Col xs={24} lg={16} style={{marginBottom:"5px",textAlign:"right", fontSize:"16px"
						}}>{order.plan_name === "rental-monthly" ? "Rental" : "Standard"  }</Col>
							</Row>
							<Row className="" style={{borderBottom:"1px solid rgb(204 204 204 / 50%)",marginBottom:"10px",paddingBottom:"10px"}}>
								<Col xs={24} lg={8} style={{fontWeight:"600",marginBottom:"5px"}}>Plan Type : </Col>
								<Col xs={24} lg={16} style={{marginBottom:"5px",textAlign:"right", fontSize:"16px"
						}}>{order.plan_type}</Col>
							</Row>
							{orders.theme_id === "APP-01" ? null : <Row className="" style={{borderBottom:"1px solid rgb(204 204 204 / 50%)",marginBottom:"10px",paddingBottom:"10px"}}>
								<Col xs={24} lg={8} style={{fontWeight:"600",marginBottom:"5px"}}>Theme ID : </Col>
								<Col xs={24} lg={16} style={{marginBottom:"5px",textAlign:"right", fontSize:"16px"}}>{order.theme_id}</Col>
							</Row>}
							
							
							

							{/* <Statistic
            title="Active"
            value={11.28}
            precision={2}
            valueStyle={{ color: '#3f8600' }}
            prefix={<Icon type="arrow-up" />}
            suffix="%"
          /> */}
						
							</Card>
						</Col>
						<Col xs={24} md={12} lg={16}>
							<Card className="Order-cards"
							 title={`Other Details`}
							 bordered={false}
							//  extra={<div><Icon type="check-circle" theme="twoTone" twoToneColor="#52c41a" /> Active</div>}
							 style={{ width: "100%",
							//  maxWidth:"450px",
							 boxShadow:"0px 0px 10px #cccccc61" }}
							>
							<Row gutter={16}>
							<Col xs={24} lg={12} >
							<Row className="" style={{borderBottom:"1px solid rgb(204 204 204 / 50%)",marginBottom:"10px",paddingBottom:"10px"}}>
							<Col xs={24} lg={10} style={{fontWeight:"600",marginBottom:"5px"}}>Business Category : </Col>
							<Col xs={24} lg={14} style={{marginBottom:"5px",textAlign:"right", fontSize:"16px"}}>{order.business_category}</Col>
							</Row>
							<Row className="" style={{borderBottom:"1px solid rgb(204 204 204 / 50%)",marginBottom:"10px",paddingBottom:"10px"}}>
								<Col xs={24} lg={10} style={{fontWeight:"600",marginBottom:"5px"}}>Shipping Gateway : </Col>
								<Col xs={24} lg={14} style={{marginBottom:"5px",textAlign:"right", fontSize:"16px"}}>{order.shipping_gateway_provider === "Not Needed Now" ? "Not Needed Now" : order.shipping_gateway_provider}</Col>
							</Row>
							<Row className="" style={{borderBottom:"1px solid rgb(204 204 204 / 50%)",marginBottom:"10px",paddingBottom:"10px"}}>
								<Col xs={24} lg={8} style={{fontWeight:"600",marginBottom:"5px"}}>Business Email : </Col>
								<Col xs={24} lg={16} style={{marginBottom:"5px",textAlign:"right", fontSize:"16px"
						}}>{order.business_email }</Col>
							</Row>
							<Row className="" style={{borderBottom:"1px solid rgb(204 204 204 / 50%)",marginBottom:"10px",paddingBottom:"10px"}}>
								<Col xs={24} lg={8} style={{fontWeight:"600",marginBottom:"5px"}}>Business Logo : </Col>
								<Col xs={24} lg={16} style={{marginBottom:"5px",textAlign:"right", fontSize:"16px"
						}}> <a href={order.business_logo} target="_blank">Link</a> </Col>
							</Row>
							<Row className="" style={{borderBottom:"1px solid rgb(204 204 204 / 50%)",marginBottom:"10px",paddingBottom:"10px"}}>
								<Col xs={24} lg={8} style={{fontWeight:"600",marginBottom:"5px"}}>Own Domain : </Col>
								<Col xs={24} lg={16} style={{marginBottom:"5px",textAlign:"right", fontSize:"16px"
						}}>{order.own_domain}</Col>
							</Row>
							</Col>
							<Col xs={24} lg={12} style={{borderLeft:"1px solid #ddd"}} >
							<Row className="" style={{borderBottom:"1px solid rgb(204 204 204 / 50%)",marginBottom:"10px",paddingBottom:"10px"}}>
							<Col xs={24} lg={10} style={{fontWeight:"600",marginBottom:"5px"}}>Additional Languages : </Col>
							<Col xs={24} lg={14} style={{marginBottom:"5px",textAlign:"right", fontSize:"16px"}}>{order.languages.length === 0 ? "Not Added" : order.languages.map(lg => lg ,)}</Col>
							</Row>
							<Row className="" style={{borderBottom:"1px solid rgb(204 204 204 / 50%)",marginBottom:"10px",paddingBottom:"10px"}}>
								<Col xs={24} lg={10} style={{fontWeight:"600",marginBottom:"5px"}}>Default Currency : </Col>
								<Col xs={24} lg={14} style={{marginBottom:"5px",textAlign:"right", fontSize:"16px"}}>{order.default_currency}</Col>
							</Row>
							<Row className="" style={{borderBottom:"1px solid rgb(204 204 204 / 50%)",marginBottom:"10px",paddingBottom:"10px"}}>
								<Col xs={24} lg={8} style={{fontWeight:"600",marginBottom:"5px"}}>Payment Gateway : </Col>
								<Col xs={24} lg={16} style={{marginBottom:"5px",textAlign:"right", fontSize:"16px"
						}}>{order.payment_gateway_provider === "payment-gateway-other" ? "Other Gateway" :  order.payment_gateway_provider === "gateway-stripe" ? "Stripe" : "Paypal"}</Col>
							</Row>
							<Row className="" style={{borderBottom:"1px solid rgb(204 204 204 / 50%)",marginBottom:"10px",paddingBottom:"10px"}}>
								<Col xs={24} lg={8} style={{fontWeight:"600",marginBottom:"5px"}}>Tranning Hours: </Col>
								<Col xs={24} lg={16} style={{marginBottom:"5px",textAlign:"right", fontSize:"16px"
						}}> {order.additional_costs.training_cost === 0 ? "2 hours" : ((order.additional_costs.training_cost/15) + 2).toFixed(0)} </Col>
							</Row>
{order.primary_color != "HEX CODE" ? (order.primary_color != " " ? (<Row className="" style={{borderBottom:"1px solid rgb(204 204 204 / 50%)",marginBottom:"10px",paddingBottom:"10px"}}>
								<Col xs={24} lg={8} style={{fontWeight:"600",marginBottom:"5px"}}>Primary Color: </Col>
								<Col xs={24} lg={16} style={{marginBottom:"5px",textAlign:"right", fontSize:"16px"
						}}> {order.primary_color} </Col></Row>) : null) : null  }
						{order.secondary_color != "HEX CODE" ? (order.secondary_color != " " ? (<Row className="" style={{borderBottom:"1px solid rgb(204 204 204 / 50%)",marginBottom:"10px",paddingBottom:"10px"}}>
								<Col xs={24} lg={8} style={{fontWeight:"600",marginBottom:"5px"}}>Secondary Color: </Col>
								<Col xs={24} lg={16} style={{marginBottom:"5px",textAlign:"right", fontSize:"16px"
						}}> {order.secondary_color} </Col></Row>) : null) : null  }
							

							
							</Col>
							<Col xs={24} lg={24}>
								<br/>
							<hr/>
							<Row className="" style={{borderBottom:"1px solid rgb(204 204 204 / 50%)",marginBottom:"10px",paddingBottom:"10px"}}>
							<Col xs={24} lg={10} style={{fontWeight:"600",marginBottom:"5px"}}>Additional Lenguage Cost : </Col>
							<Col xs={24} lg={14} style={{marginBottom:"5px",textAlign:"right", fontSize:"16px"}}>
							<Statistic
							value={(order.additional_costs.language_cost * sessionStorage.getItem("sgd_currency_rate")).toFixed(1)}
							precision={2}
							valueStyle={{ fontSize:"16px" }}
							prefix={sessionStorage.getItem("currency_sign")}
							/>
							</Col>
							</Row>
							<Row className="" style={{borderBottom:"1px solid rgb(204 204 204 / 50%)",marginBottom:"10px",paddingBottom:"10px"}}>
							<Col xs={24} lg={10} style={{fontWeight:"600",marginBottom:"5px"}}>Payment Gateway Cost : </Col>
							<Col xs={24} lg={14} style={{marginBottom:"5px",textAlign:"right", fontSize:"16px"}}>
							<Statistic
							value={(order.additional_costs.payment_gateway_cost * sessionStorage.getItem("sgd_currency_rate")).toFixed(1)}
							precision={2}
							valueStyle={{ fontSize:"16px" }}
							prefix={sessionStorage.getItem("currency_sign")}
							/>
							</Col>
							</Row>
							<Row className="" style={{borderBottom:"1px solid rgb(204 204 204 / 50%)",marginBottom:"10px",paddingBottom:"10px"}}>
							<Col xs={24} lg={10} style={{fontWeight:"600",marginBottom:"5px"}}>Shipping Gateway Cost : </Col>
							<Col xs={24} lg={14} style={{marginBottom:"5px",textAlign:"right", fontSize:"16px"}}>
							<Statistic
							value={(order.additional_costs.shipping_cost * sessionStorage.getItem("sgd_currency_rate")).toFixed(1)}
							precision={2}
							valueStyle={{ fontSize:"16px" }}
							prefix={sessionStorage.getItem("currency_sign")}
							/>
							</Col>
							</Row>
							<Row className="" style={{borderBottom:"1px solid rgb(204 204 204 / 50%)",marginBottom:"10px",paddingBottom:"10px"}}>
							<Col xs={24} lg={10} style={{fontWeight:"600",marginBottom:"5px"}}>Tranning Cost : </Col>
							<Col xs={24} lg={14} style={{marginBottom:"5px",textAlign:"right", fontSize:"16px"}}>
							<Statistic
							value={(order.additional_costs.training_cost * sessionStorage.getItem("sgd_currency_rate")).toFixed(1)}
							precision={2}
							valueStyle={{ fontSize:"16px" }}
							prefix={sessionStorage.getItem("currency_sign")}
							/>
							</Col>
							</Row>
							<Row className="" style={{borderBottom:"1px solid rgb(204 204 204 / 50%)",marginBottom:"10px",paddingBottom:"10px"}}>
							<Col xs={24} lg={10} style={{fontWeight:"600",marginBottom:"5px"}}>Plan Cost : </Col>
							<Col xs={24} lg={14} style={{marginBottom:"5px",textAlign:"right", fontSize:"16px"}}>
							<Statistic
							value={(order.plan_price * sessionStorage.getItem("sgd_currency_rate")).toFixed(1)}
							precision={2}
							valueStyle={{ fontSize:"16px" }}
							prefix={sessionStorage.getItem("currency_sign")}
							/>
							</Col>
							</Row>
							<Row className="" style={{borderBottom:"1px solid rgb(204 204 204 / 50%)",marginBottom:"10px",paddingBottom:"10px"}}>
							<Col xs={24} lg={10} style={{fontWeight:"600",marginBottom:"5px",fontSize:"20px",color:"#000"}}>Total Cost : </Col>
							<Col xs={24} lg={14} style={{marginBottom:"5px",textAlign:"right", fontSize:"16px"}}>
							<Statistic
							value={(order.total_price * sessionStorage.getItem("sgd_currency_rate")).toFixed(1)}
							precision={2}
							valueStyle={{ color: '#3f8600' }}
							prefix={sessionStorage.getItem("currency_sign")}
							/>
		  </Col>
							</Row>
							</Col>
							</Row>	

						
						
							</Card>
						</Col>
						</Row>
					</Panel>
					)}
				</Collapse>
				
					</div>
				)}
			
			</Fragment>
		);
};

Orders.propTypes = {
	getUserOrders: PropTypes.func.isRequired,
	orders: PropTypes.array.isRequired,
	loading: PropTypes.bool,
	user: PropTypes.object.isRequired,
};

const mapStateToProps = state => ({
	orders: state.orders.orders,
	loading: state.orders.loading,
	user: state.auth.user
});

export default connect(
	mapStateToProps,
	{ getUserOrders, }
)(Orders);
