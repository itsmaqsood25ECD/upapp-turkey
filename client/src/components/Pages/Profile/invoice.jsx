import React, { Fragment, useEffect, useRef } from "react";
import { connect } from "react-redux";
import moment from "moment";
import PropTypes from "prop-types";
import { Button } from "antd";
import { getOrderDetails } from "../../../actions/order.action";
import "./invoice.css";
import Printinv from "./printInv";
import ReactToPrint from "react-to-print";

const Invoice = ({ match, user, order, loading, getOrderDetails }) => {
	useEffect(() => {
		getOrderDetails(match.params.invoice);
	}, [loading]);
	const componentRef = useRef();

	return loading && order === null ? (
		<Fragment>Spinner</Fragment>
	) : (
			<Fragment>
				<Printinv
					Fname={user && user.firstName}
					Lname={user && user.lastName}
					applicationName={order && order.appName}
					date={order && moment(order.start_date).format("MMMM Do YYYY")}
					id={order && order.UID}
					type={order && order.packType}
					coupon={order && order.coupon_code}
					totalPrice={Number(order && order.total_price)}
					discount={Number(order && order.discount_price)}
					ref={componentRef}
				/>

				<ReactToPrint
					trigger={() => (
						<div style={{ textAlign: "center", margin: "0px auto 20px auto" }}>
							<Button size="large" type="primary">
								Print this out!
						</Button>
						</div>
					)}
					content={() => componentRef.current}
				/>
			</Fragment>
		);
};

Invoice.propTypes = {
	user: PropTypes.object.isRequired,
	getOrderDetails: PropTypes.func.isRequired,
	order: PropTypes.object.isRequired,
	loading: PropTypes.bool
};

const mapStateToProps = state => ({
	user: state.auth.user,
	order: state.orders.order,
	loading: state.orders.loading
});

export default connect(
	mapStateToProps,
	{ getOrderDetails }
)(Invoice);
