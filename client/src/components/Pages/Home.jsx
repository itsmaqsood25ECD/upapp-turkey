import React, { Component } from "react";
import { Button, Typography, Row, Col, Icon, Carousel } from "antd";
import BannerAnim from "rc-banner-anim";
import { Element } from "rc-banner-anim";
import TweenOne from "rc-tween-one";
import "rc-banner-anim/assets/index.css";
import ScrollTop from "../ScrollTop";
import { Link } from "react-router-dom";
import "antd/dist/antd.css";
import "../../assets/css/custom.css";
import "../../assets/css/home.css";
import appSelection from "../../assets/img/homeSteps/appSelection.png";
import paymentSection from "../../assets/img/homeSteps/paymentSection.png";
import supportSection from "../../assets/img/homeSteps/supportSection.png";
import TrainingTeam from "../../assets/img/homeSteps/TrainingTeam.png";
import yourAppReady from "../../assets/img/homeSteps/yourAppReady.png";
import FTlogo from "../../assets/img/brand/Financial_Times_Logo.svg";
import "react-animated-slider/build/horizontal.css";
import { loadUser } from "../../actions/auth.action";
import { getLocation } from "../../actions/userlocation.action";
import { store } from "../../store/store";
import { loadApplication } from "../../actions/application.action";
import { Helmet } from "react-helmet";
const BgElement = Element.BgElement;
const { Title, Text } = Typography;
class Home extends Component {
  componentWillMount() {
    store.dispatch(loadUser());
    store.dispatch(loadApplication());
    store.dispatch(getLocation());
  }
  render() {
    return (
      <div>
        <ScrollTop />
        <Helmet>
          <title>UPapp factory - Buy Ready Made Apps</title>
          <meta
            name="Subscribe your ready made apps for your business. Start your eCommerce business in a minute. Just choose the product which suits best to your work and you are all done."
            content="Buy Ready Made eCommerce App Business Application Work Purchase"
          />
        </Helmet>
        <div style={{ background: "#fff", padding: 0 }}>
          {/*  Banner Animation Starts */}
          <BannerAnim
            prefixCls="home-slider"
            ease="easeOutExpo"
            type="acrossOverlay"
          >
            <Element
              prefixCls="banner-user-elem"
              key="0"
              className="home-slider"
            >
              <BgElement key="bg1" className="bg section-1" />
              <Row type="flex" justify="space-around" align="middle">
                <Col xs={24} md={24} lg={10}>
                  <div
                    className="tagline-heading pl-100"
                    style={{ padding: "auto 15px" }}
                  >
                    <TweenOne animation={{ y: 30, opacity: 0, type: "from" }}>
                      <Title
                        level={1}
                        className="texty-demo"
                        style={{ color: "#ffffff" }}
                      >
                        Get a Mobile App <br /> For Your Business
                      </Title>
                      <Title
                        level={1}
                        className="text-demo-sub-text"
                        style={{
                          fontSize: "18px",
                          lineHeight: "20px",
                          color: "#fff"
                        }}
                      >
                        Grow Your Business with us with only a Few Clicks!
                      </Title>
                    </TweenOne>
                    <TweenOne
                      animation={{
                        y: 30,
                        opacity: 0,
                        type: "from",
                        delay: 200
                      }}
                    >
                      <Link to="/factory">
                        <Button
                          // type="dashed"
                          size={"large"}
                          className="slider-get-started-btn"
                        >
                          Get Started
                        </Button>
                      </Link>
                    </TweenOne>
                  </div>
                </Col>
                <Col xs={24} md={10} lg={14}>
                  <TweenOne animation={{ x: 100, opacity: 0, type: "from" }}>
                    {/* <div className="home-feature-img">
											<img
												style={{ maxWidth: "200px" }}
												src={BookshopScreen}
												alt="image with animation for upapp"
											/>
										</div> */}
                  </TweenOne>
                </Col>
              </Row>
            </Element>
            <Element prefixCls="banner-user-elem" key="1">
              <BgElement key="bg2" className="section-2 bg" />
              <Row type="flex" justify="space-around" align="middle">
                <Col xs={24} md={24} lg={10}>
                  <div
                    className="tagline-heading pl-100"
                    style={{ padding: "auto 15px" }}
                  >
                    <TweenOne animation={{ y: 30, opacity: 0, type: "from" }}>
                      <Title
                        level={1}
                        className="texty-demo"
                        style={{ color: "#ffffff" }}
                      >
                        Start your eCommerce <br /> store in 7 Days
                      </Title>
                    </TweenOne>

                    <TweenOne
                      animation={{
                        y: 30,
                        opacity: 0,
                        delay: 200,
                        type: "from"
                      }}
                    >
                      <Link to="/factory/ecommerce">
                        <Button
                          // type="dashed"
                          size={"large"}
                          className="slider-get-started-btn"
                        >
                          Talk to Us!
                        </Button>
                      </Link>
                    </TweenOne>
                  </div>
                </Col>
                <Col xs={24} md={10} lg={14}>
                  <TweenOne
                    animation={{ x: 100, opacity: 0, type: "from", delay: 100 }}
                  >
                    {/* <div className="home-feature-img">
											<img
												style={{ maxWidth: "200px" }}
												src={fashionAppScreen}
												alt="image with animation for upapp"
											/>
										</div> */}
                  </TweenOne>
                </Col>
              </Row>
            </Element>
            <Element prefixCls="banner-user-elem" key="2">
              <BgElement key="bg3" className="section-3 bg" />
              <Row type="flex" justify="space-around" align="middle">
                <Col xs={24} md={24} lg={10}>
                  <div
                    className="tagline-heading pl-100"
                    style={{ padding: "auto 15px" }}
                  >
                    <TweenOne animation={{ y: 30, opacity: 0, type: "from" }}>
                      <Title
                        level={1}
                        className="texty-demo"
                        style={{ color: "#ffffff" }}
                      >
                        Sell Everything You Want!
                        <br />
                      </Title>
                      <Title
                        level={1}
                        className="text-demo-sub-text"
                        style={{
                          fontSize: "18px",
                          lineHeight: "20px",
                          color: "#fff"
                        }}
                      >
                        See Our Best Rates & Hottest Plans.
                      </Title>
                    </TweenOne>
                    <TweenOne
                      animation={{
                        y: 30,
                        opacity: 0,
                        delay: 200,
                        type: "from"
                      }}
                    >
                      <Link to="/factory/ecommerce">
                        <Button
                          // type="dashed"
                          size={"large"}
                          className="slider-get-started-btn"
                        >
                          Try Now!
                        </Button>
                      </Link>
                    </TweenOne>
                  </div>
                </Col>
                <Col xs={24} md={10} lg={14}>
                  <TweenOne
                    animation={{ x: 100, opacity: 0, type: "from", delay: 100 }}
                  >
                    {/* <div className="home-feature-img">
											<img
												style={{ maxWidth: "200px" }}
												src={menSpaSalon}
												alt="image with animation for upapp"
											/>
										</div> */}
                  </TweenOne>
                </Col>
              </Row>
            </Element>
          </BannerAnim>

          <Row>
            <Col xs={24}>
              <div style={{}}>
                <Title level={1} className="Jumbo-text">
                  Build Your Unique Online Presence
                </Title>
                <div className="jumbo-btn" style={{ marginBottom: "100px" }}>
                  <Link to="/factory">
                    <Button type="primary" size="large">
                      Get Started
                    </Button>
                  </Link>
                </div>
              </div>
            </Col>
          </Row>
          <Row
            type="flex"
            justify="space-around"
            align="middle"
            style={{ paddingTop: "50px", paddingBottom: "50px" }}
          >
            <Col lg={{ span: 12, order: 2 }} xs={{ span: 24, order: 1 }}>
              <img className="Steps-img" src={appSelection} />
            </Col>
            <Col
              lg={{ span: 12, order: 1 }}
              xs={{ span: 24, order: 2 }}
              style={{ textAlign: "center" }}
            >
              <Title level={1} className="Steps-Title">
                App selection —
              </Title>
              <Text className="Step-desc">
                select the app that suits your business
              </Text>
            </Col>
          </Row>
          <Row
            type="flex"
            justify="space-around"
            align="middle"
            style={{ paddingTop: "50px", paddingBottom: "50px" }}
          >
            <Col
              lg={{ span: 12, order: 2 }}
              xs={{ span: 24, order: 2 }}
              style={{ textAlign: "center" }}
            >
              <Title level={1} className="Steps-Title">
                Payment —
              </Title>
              <Text className="Step-desc">
                Subscribe for the selected app & complete your purchase
              </Text>
            </Col>
            <Col lg={{ span: 12, order: 1 }} xs={{ span: 24, order: 1 }}>
              <img className="Steps-img" src={paymentSection} />
            </Col>
          </Row>
          <Row
            type="flex"
            justify="space-around"
            align="middle"
            style={{ paddingTop: "50px", paddingBottom: "50px" }}
          >
            <Col lg={{ span: 12, order: 2 }} xs={{ span: 24, order: 1 }}>
              <img className="Steps-img" src={yourAppReady} />
            </Col>
            <Col
              lg={{ span: 12, order: 1 }}
              xs={{ span: 24, order: 2 }}
              style={{ textAlign: "center" }}
            >
              <Title level={1} className="Steps-Title">
                Your App Is Ready —
              </Title>
              <Text className="Step-desc">
                Our representative will be in touch with you
              </Text>
            </Col>
          </Row>
          <Row
            type="flex"
            justify="space-around"
            align="middle"
            style={{ paddingTop: "50px", paddingBottom: "50px" }}
          >
            <Col
              lg={{ span: 12, order: 2 }}
              xs={{ span: 24, order: 2 }}
              style={{ textAlign: "center" }}
            >
              <Title level={1} className="Steps-Title">
                Training —
              </Title>
              <Text className="Step-desc">
                Our team are available to provide system & app training for you
                and your team
              </Text>
            </Col>
            <Col lg={{ span: 12, order: 1 }} xs={{ span: 24, order: 1 }}>
              <img className="Steps-img" src={TrainingTeam} />
            </Col>
          </Row>
          <Row
            type="flex"
            justify="space-around"
            align="middle"
            style={{ paddingTop: "50px", paddingBottom: "50px" }}
          >
            <Col lg={{ span: 12, order: 2 }} xs={{ span: 24, order: 1 }}>
              <img className="Steps-img" src={supportSection} />
            </Col>
            <Col
              lg={{ span: 12, order: 1 }}
              xs={{ span: 24, order: 2 }}
              style={{ textAlign: "center" }}
            >
              <Title level={1} className="Steps-Title">
                Support for life —
              </Title>
              <Text className="Step-desc">
                We Are Here To Support You All The Way
              </Text>
            </Col>
          </Row>
          <Carousel
            className="d-bottom-carousel"
            arrows="true"
            autoplay
            speed="10"
            effect="fade"
            dots="false"
            easing="easeInOutQuart"
          >
            <div className="home-section-5-slider-1">
              <Row
                gutter={16}
                style={{ margin: "80px 0px" }}
                type="flex"
                justify="space-around"
                align="middle"
              >
                <Col
                  md={12}
                  lg={12}
                  xs={24}
                  className="global-ecommerce-container"
                >
                  <div
                    className="pl-40"
                    style={{ paddingRight: "40px", textAlign: "left" }}
                  >
                    <Title
                      level={1}
                      className="heading-45"
                      style={{ paddingBottom: "20px", fontWeight: "400" }}
                    >
                      50% of retailers list a mobile shopping app as a top
                      priority
                      <br></br>
                      <a
                        target="_blank"
                        href="https://www.retaildive.com/news/50-of-retailers-list-a-mobile-shopping-app-as-a-top-priority/547774/"
                      >
                        <img
                          className="src-logo-tag retail-dive-logo"
                          src="https://d12v9rtnomnebu.cloudfront.net/logo/publications/retail_white.svg"
                        />
                      </a>
                    </Title>
                  </div>
                </Col>
                <Col md={12} lg={12} xs={24} style={{ padding: 0 }}>
                  <div className="section-img" />
                </Col>
              </Row>
            </div>
            <div className="home-section-5-slider-1">
              <Row
                gutter={16}
                style={{ margin: "80px 0px" }}
                type="flex"
                justify="space-around"
                align="middle"
              >
                <Col
                  md={12}
                  lg={12}
                  xs={24}
                  className="global-ecommerce-container"
                >
                  <div
                    className="pl-40"
                    style={{ paddingRight: "40px", textAlign: "left" }}
                  >
                    <Title
                      level={1}
                      className="heading-45"
                      style={{ paddingBottom: "20px", fontWeight: "400" }}
                    >
                      Over Half of Consumers Use Mobile Apps When Shopping
                      In-store
                      <br />
                      <a
                        target="_blank"
                        style={{ width: "80px" }}
                        href="https://www.prnewswire.com/news-releases/data-over-half-of-consumers-use-mobile-apps-when-shopping-in-store-300773532.html"
                      >
                        <img
                          className="src-logo-tag cision-logo"
                          src="https://www.prnewswire.com/content/dam/prnewswire/homepage/prn_cision_logo_desktop.png"
                        />
                      </a>
                    </Title>
                  </div>
                </Col>
                <Col md={12} lg={12} xs={24} style={{ padding: 0 }}>
                  <div className="section-img1" />
                </Col>
              </Row>
            </div>

            <div className="home-section-5-slider-1">
              <Row
                gutter={16}
                style={{ margin: "80px 0px" }}
                type="flex"
                justify="space-around"
                align="middle"
              >
                <Col
                  md={12}
                  lg={12}
                  xs={24}
                  className="global-ecommerce-container"
                >
                  <div
                    className="pl-40"
                    style={{ paddingRight: "40px", textAlign: "left" }}
                  >
                    <Title
                      level={1}
                      className="heading-45"
                      style={{ paddingBottom: "20px", fontWeight: "400" }}
                    >
                      Spending on smartphones overtakes shopping centers
                      <br />
                      <a
                        target="_blank"
                        style={{ width: "80px" }}
                        href="https://www.ft.com/content/76095276-1995-11e9-b93e-f4351a53f1c3"
                      >
                        <img
                          src={FTlogo}
                          style={{ width: "150px" }}
                          className="src-logo-tag"
                        />
                      </a>
                    </Title>
                  </div>
                </Col>
                <Col md={12} lg={12} xs={24} style={{ padding: 0 }}>
                  <div className="section-img3 people-using-ecom" />
                </Col>
              </Row>
            </div>
          </Carousel>

          <Carousel
            className="m-bottom-carousel"
            arrows="true"
            autoplay
            speed="10"
            effect="fade"
            dots="false"
            easing="easeInOutQuart"
          >
            <div className="m-home-section-5-slider-1">
              <Row
                gutter={16}
                style={{ margin: "80px 0px" }}
                type="flex"
                justify="space-around"
                align="middle"
              >
                <Col xs={24} className="global-ecommerce-container">
                  <div>
                    <Title level={1} className="heading-25">
                      50% of retailers list a mobile shopping app as a top
                      priority
                      <br></br>
                      <a
                        target="_blank"
                        href="https://www.retaildive.com/news/50-of-retailers-list-a-mobile-shopping-app-as-a-top-priority/547774/"
                      >
                        <img
                          className="src-logo-tag retail-dive-logo"
                          src="https://d12v9rtnomnebu.cloudfront.net/logo/publications/retail_white.svg"
                        />
                      </a>
                    </Title>
                  </div>
                </Col>
              </Row>
            </div>
            <div className="m-home-section-5-slider-2">
              <Row
                gutter={16}
                style={{ margin: "80px 0px" }}
                type="flex"
                justify="space-around"
                align="middle"
              >
                <Col xs={24} className="global-ecommerce-container">
                  <div
                  // className="pl-40"
                  // style={{ paddingRight: "40px", textAlign: "left" }}
                  >
                    <Title
                      level={1}
                      className="heading-25"
                      style={{ textAlign: "center" }}
                    >
                      Surveyed over 1,000 consumers of all ages and found the
                      top reasons they use mobile apps when shopping
                      <br />
                      <a
                        target="_blank"
                        style={{ width: "80px" }}
                        href="https://www.prnewswire.com/news-releases/data-over-half-of-consumers-use-mobile-apps-when-shopping-in-store-300773532.html"
                      >
                        <img
                          className="src-logo-tag cision-logo"
                          src="https://www.prnewswire.com/content/dam/prnewswire/homepage/prn_cision_logo_desktop.png"
                        />
                      </a>
                    </Title>
                  </div>
                </Col>
              </Row>
            </div>
            <div className="m-home-section-5-slider-3">
              <Row
                gutter={16}
                style={{ margin: "80px 0px" }}
                type="flex"
                justify="space-around"
                align="middle"
              >
                <Col xs={24} className="global-ecommerce-container">
                  <div>
                    <Title
                      level={1}
                      className="heading-25"
                      style={{ textAlign: "center" }}
                    >
                      Spending on smartphones overtakes shopping centers
                      <br />
                      <a
                        target="_blank"
                        style={{ width: "150px" }}
                        href="ft.com/content/76095276-1995-11e9-b93e-f4351a53f1c3"
                      >
                        <img
                          className="src-logo-tag ft-logo"
                          src="https://www.ft.com/__origami/service/image/v2/images/raw/ftlogo:brand-ft-masthead?source=o-header&tint=%2333302E,%2333302E&format=svg"
                        />
                      </a>
                    </Title>
                  </div>
                </Col>
              </Row>
            </div>
          </Carousel>
          <div style={{ textAlign: "center", margin: "0px auto 50px auto" }}>
            <Link to="/factory">
              <Button
                type="primary"
                size={"large"}
                style={{
                  minWidth: "200px",
                  marginTop: "40px",
                  fontSize: "18px",
                  border: "none"
                }}
              >
                Get Started
              </Button>
            </Link>
          </div>
        </div>
      </div>
    );
  }
}

export default Home;
