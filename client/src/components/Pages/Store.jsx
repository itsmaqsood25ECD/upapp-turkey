import React, { useState, useEffect, Fragment } from "react";
import {
  List,
  Typography,
  Card,
  Layout,
  Input,
  Row,
  Col,
  Menu,
  Form,
  Spin,
  Drawer,
  Button,
  Modal
} from "antd";
import ScrollTop from "../ScrollTop";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import { Link, NavLink } from "react-router-dom";
import { CountryDropdown } from "react-country-region-selector";
import PhoneInput from "react-phone-number-input";
import SmartInput from "react-phone-number-input/smart-input";
import "antd/dist/antd.css";
import "../../assets/css/custom.css";
import { loadApplication } from "../../actions/application.action";
import { getLocation } from "../../actions/userlocation.action";
import { GetCategory } from "../../actions/admin/category.management.action";
import { requestForCustomisedApp } from "../../actions/contact.action";
import ReCAPTCHA from "react-google-recaptcha";
import { setAlert } from "../../actions/alert.action.jsx";
import QueueAnim from "rc-queue-anim";
import { Helmet } from "react-helmet";
import ReactGA from "react-ga";
import FilteredAppsRoute from "./single page/filteredApps";
import withDirection, { withDirectionPropTypes } from 'react-with-direction';
import { useTranslation} from 'react-i18next';
import DirectionProvider, { DIRECTIONS } from 'react-with-direction/dist/DirectionProvider';
import i18next from 'i18next';

const { Content } = Layout;
const { Search, TextArea } = Input;
const { Title } = Typography;
const recaptchaRef = React.createRef();

let filteredApps;
const Store = ({
  match,
  GetCategory,
  Category,
  loadApplication,
  Applications,
  loading,
  setAlert,
  userlocation,
  customApp,
  requestForCustomisedApp,
  form: { getFieldDecorator, validateFields, setFieldsValue },
  direction
}) => {
  const { t } = useTranslation();

  const [formData, setFormData] = useState({});

  const [applicationData, setApplicationData] = useState({
    apps: [],
    searchApplication: "",
    selectedKeys: "",
    visible: false
  });

  const [state, setState] = useState({
    visible: false,
    modalVisible: false,
    userlocation: "",
    HumanVarification: false
  });

  const { HumanVarification } = state;

  const showDrawer = () => {
    setState({
      visible: true
    });
  };

  const onClose = () => {
    setState({
      visible: false
    });
  };

  if (Applications === null) {
    // console.log(Applications, loading);
    loadApplication();
  }
  useEffect(() => {
    // console.log("store>>>>", match);
    if (Applications) {
      GetCategory();
      // console.log("if", Category);
      // console.log(Applications, loading);
      setApplicationData({
        ...applicationData,
        apps:
          Applications &&
          Applications.map(arg => {
            return arg;
          })
      });
    } else {
      // console.log("else", Applications, loading);
      loadApplication();
    }
  }, [match && match.params, Applications]);

  // Menu Component
  // console.log(Category);
  const CAT = Category && Category;

  const INDUSTRIES =
    CAT &&
    CAT.filter(cat => {
      return cat.type === "Industry";
    });
  const CATEGORY =
    CAT &&
    CAT.filter(cat => {
      return cat.type === "Category";
    });

  const TYPE =
    CAT &&
    CAT.filter(cat => {
      return cat.type === "Type";
    });

  // console.log(INDUSTRIES);
  // console.log(CATEGORY);
  // console.log(TYPE);
  // Menu Component End

  const { searchApplication, apps } = applicationData;

  const handleInput = e => {
    setApplicationData({
      ...applicationData,
      searchApplication: e.target.value
    });
  };

  const handleSelect = Keys => {
    // console.log(TYPE);
    const typeArray =
      TYPE &&
      TYPE.map(arg => {
        return arg.name;
      });
    const industryArray =
      INDUSTRIES &&
      INDUSTRIES.map(arg => {
        return arg.name;
      });
    const categoryArray =
      CATEGORY &&
      CATEGORY.map(arg => {
        return arg.name;
      });
    if (typeArray.includes(Keys.item.props.children)) {
      setApplicationData({
        ...applicationData,
        searchApplication: Keys.item.props.children
      });
    } else if (industryArray.includes(Keys.item.props.children)) {
      setApplicationData({
        ...applicationData,
        searchApplication: Keys.item.props.children
      });
    } else if (categoryArray.includes(Keys.item.props.children)) {
      setApplicationData({
        ...applicationData,
        searchApplication: Keys.item.props.children
      });
    } else {
      setApplicationData({ ...applicationData, searchApplication: "" });
    }
  };

  if (searchApplication === "") {
    filteredApps = apps && apps.filter(myapp => {
      return (myapp.fields &&
        myapp.fields.type
          .toLowerCase()
          .includes('ecommerce'))
    })
  } else {
    filteredApps =
      apps &&
      apps.filter(myapp => {
        return (
          (myapp.fields &&
            myapp.fields.name
              .toLowerCase()
              .includes(
                searchApplication && searchApplication.toLocaleLowerCase()
              )) ||
          (myapp.fields &&
            myapp.fields.type
              .toLowerCase()
              .includes(
                searchApplication && searchApplication.toLocaleLowerCase()
              )) ||
          (myapp.fields &&
            myapp.fields.industry
              .toLowerCase()
              .includes(
                searchApplication && searchApplication.toLocaleLowerCase()
              )) ||
          (myapp.fields &&
            myapp.fields.category
              .toLowerCase()
              .includes(
                searchApplication && searchApplication.toLocaleLowerCase()
              ))
        );
      });
  }

  return Applications === null || loading ? (
    <Fragment>
      <Spin></Spin>
    </Fragment>
  ) : (
    <Fragment>
      <Helmet>
        <title>The Factory - Readymade Apps For Business</title>
        <meta
          name="Subscribe your ready made apps for your business. Start your eCommerce business in a minute. Just choose the product which suits best to your work and you are all done."
          content="Buy Ready Made eCommerce App Business Application Work Purchase"
        />
      </Helmet>
      <ScrollTop />
      <QueueAnim delay={300} type="left" key="a" className="demo-content">
        <Content style={{ margin: "24px 0px 0", overflow: "initial" }}>
          <div className="hero-bg animated fadeIn fast">
            <Title
              className="fontSize-25"
              style={{
                color: "#474848",
                fontWeight: "300",
                marginBottom: "0px"
              }}
            ></Title>
            <div className="app-search-bar-container">
              <Search
                className="Appsearch"
                placeholder={`${t('factory.searchapp')}`}
                // onSearch={Searchvalue => console.log(Searchvalue)}
                onChange={handleInput}
              />
            </div>
          </div>
          <Button
            className="m-category-menu"
            type="primary"
            onClick={showDrawer}
          >
            {/* Select Category */}
           {t('factory.SelectCategory')}
          </Button>
          <Row
            gutter={16}
            type="flex"
            style={{
              marginBottom: "30px",
              marginLeft: "0px",
              marginRight: "0px"
            }}
          >
            <Col
              xs={24}
              sm={24}
              lg={5}
              className="lg-storesidebar animated fadeIn faster"
            >
              <Drawer
                title={`${t('factory.sidebarMenu.SelectCategory')}`}
                placement="bottom"
                className="m-category-drawer"
                closable={false}
                onClose={onClose}
                visible={state.visible}
              >
                <Menu
                  defaultSelectedKeys={["ecommerce"]}
                  mode="inline"
                  // onSelect={selectedKeys => console.log(selectedKeys)}
                  onClick={handleSelect}
                >
                  {" "}
                  {/* <Menu.Item key="All">
                    <NavLink to={`/factory/all`}>
                    {t('factory.sidebarMenu.all')}
                      </NavLink>
                  </Menu.Item> */}
                  <Menu.ItemGroup key="TYPE" title="APP TYPE">
                    {/* {TYPE &&
                      TYPE.map((data, i) => {
                        return (
                          <Menu.Item key={data._id}>
                            <NavLink to={`/factory/${data.name}`}>
                              UPapp {data.name}
                            </NavLink>
                          </Menu.Item>
                        );
                      })} */}
                    <Menu.Item key="ecommerce">
                      <NavLink to="/factory/ecommerce">
                      {t('header.ecommerce')}
                      </NavLink>
                    </Menu.Item>
                    <Menu.Item key="restaurant">
                      <NavLink to="/restaurant-application">
                      {t('header.Restaurant')}
                      </NavLink>
                    </Menu.Item>
                  </Menu.ItemGroup>
                  <Menu.ItemGroup key="industries" title="INDUSTRIES">
                    {INDUSTRIES &&
                      INDUSTRIES.map((data, i) => {
                        return (
                          <Menu.Item key={data._id}>
                            <NavLink to={`/factory/${data.name}`}>
                            {direction === DIRECTIONS.RTL && <Fragment>
                              {data.nameAR}
                              </Fragment>}
                              {direction === DIRECTIONS.LTR && <Fragment>
                              {data.name}
                              </Fragment>}
                            </NavLink>
                          </Menu.Item>
                        );
                      })}
                  </Menu.ItemGroup>
                  {/* 
								<Menu.ItemGroup key="Category" title="CATEGORIES">
									{CATEGORY && CATEGORY.map((data, i) => {
										return (

											<Menu.Item key={data._id}>{data.name}</Menu.Item>

										);
									})}
								</Menu.ItemGroup>
							 */}
                </Menu>
              </Drawer>
              <Menu
                defaultSelectedKeys={["ecommerce"]}
                mode="inline"
                // onSelect={selectedKeys => console.log(selectedKeys)}
                onClick={handleSelect}
                style={{ position: "sticky", top: "100px" }}
              >
                {" "}
                {/* <Menu.Item className="store-menu-sidebar" key="All">
                  <NavLink to={`/factory/all`}>
                    {t('factory.sidebarMenu.all')}
                    </NavLink>
                </Menu.Item> */}
                <Menu.ItemGroup key="TYPE" title={`${t('factory.sidebarMenu.apptype')}`}>
                  {/* {TYPE &&
                    TYPE.map((data, i) => {
                      return (
                        <Menu.Item
                          className="store-menu-sidebar"
                          key={data._id}
                        >
                          <NavLink to={`/factory/${data.name.toLowerCase()}`}>
                            UPapp {data.name}
                          </NavLink>
                        </Menu.Item>
                      );
                    })} */}
                    <Menu.Item key="ecommerce">
                      <NavLink to="/factory/ecommerce">
                      {t('header.ecommerce')}
                      </NavLink>
                    </Menu.Item>
                  {/* <Menu.Item
                    className="store-menu-sidebar"
                    key="facility-management"
                  >
                    <NavLink to="/facility-management-application">
                    {t('header.Facility')}
                    </NavLink>
                  </Menu.Item> */}
                  <Menu.Item key="restaurantlg" className="store-menu-sidebar">
                    <NavLink to="/restaurant-application">
                    {t('header.Restaurant')}
                    </NavLink>
                  </Menu.Item>
                </Menu.ItemGroup>
                <Menu.ItemGroup key="industries" title={`${t('factory.sidebarMenu.industries')}`}>
                  {INDUSTRIES &&
                    INDUSTRIES.map((data, i) => {
                      return (
                        <Menu.Item
                          className="store-menu-sidebar"
                          key={data._id}
                        >
                          <NavLink to={`/factory/${data.name.toLowerCase()}`}>
                          {direction === DIRECTIONS.RTL && <Fragment>
                              {data.nameAR}
                              </Fragment>}
                              {direction === DIRECTIONS.LTR && <Fragment>
                              {data.name}
                              </Fragment>}
                          </NavLink>
                        </Menu.Item>
                      );
                    })}
                </Menu.ItemGroup>
                {/* 
								<Menu.ItemGroup key="Category" title="CATEGORIES">
									{CATEGORY && CATEGORY.map((data, i) => {
										return (

											<Menu.Item key={data._id}>{data.name}</Menu.Item>

										);
									})}
								</Menu.ItemGroup>
							 */}
              </Menu>
            </Col>
            <Col xs={24} sm={24} lg={19} className="animated fadeIn faster">
              <Row className="store-table">
              <DirectionProvider direction={direction}>
                <FilteredAppsRoute
                  match={match}
                  Applications={Applications}
                  searchApplication={searchApplication}
                />
                </DirectionProvider>
              </Row>
              <Row>
                <Col
                  xs={24}
                  style={{ marginBottom: "30px", marginTop: "30px" }}
                />
              </Row>
            </Col>
          </Row>
        </Content>
      </QueueAnim>
    </Fragment>
  );
};

const RFC = Form.create({ name: "Request_for_customize" })(Store);

RFC.propTypes = {
  loadApplication: PropTypes.func.isRequired,
  requestForCustomisedApp: PropTypes.func.isRequired,
  getLocation: PropTypes.func.isRequired,
  GetCategory: PropTypes.func.isRequired,
  Category: PropTypes.array.isRequired,
  Applications: PropTypes.array.isRequired,
  loading: PropTypes.bool
};
const mapStateToProps = state => ({
  Applications: state.application.Applications,
  userlocation: state.userlocation,
  Category: state.categoryManagement.Category,
  loading: state.application.loading,
  direction: withDirectionPropTypes.direction,
});
export default connect(mapStateToProps, {
  loadApplication,
  GetCategory,
  getLocation,
  requestForCustomisedApp,
  setAlert
})(withDirection(RFC));
