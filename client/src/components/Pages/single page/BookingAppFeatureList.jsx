import React, { Component, Fragment } from 'react'
import { Row, Col, Typography, List, Icon, Tooltip, Spin, Select } from 'antd';
import withDirection, { withDirectionPropTypes, DIRECTIONS } from 'react-with-direction';
import { useTranslation} from 'react-i18next';
import i18next from 'i18next';
import { connect } from "react-redux";



const BookingAppFeatureList = ({direction}) => {
	
	 const { t } = useTranslation();

		const upAppSpecialData = [
			{
				
				title: `${t('ecommerceSpecialFeature.1.title')}`,
				desc: `${t('ecommerceSpecialFeature.1.desc')}`
			},
			{
				
				title: `${t('ecommerceSpecialFeature.2.title')}`,
				desc: `${t('ecommerceSpecialFeature.2.title')}`,
			},
			{
				
				title: `${t('ecommerceSpecialFeature.15.title')}`,
				desc: `${t('ecommerceSpecialFeature.15.desc')}`
			},
			{
				
				title: `${t('ecommerceSpecialFeature.5.title')}`,
				desc:`${t('ecommerceSpecialFeature.5.desc')}`
			},
			{
				
				title:`${t('ecommerceSpecialFeature.6.title')}`,
				desc:`${t('ecommerceSpecialFeature.6.desc')}`
			},
			{
				
				title: `${t('ecommerceSpecialFeature.7.title')}`,
				desc: `${t('ecommerceSpecialFeature.7.desc')}`
			},
			{
				
				title: `${t('ecommerceSpecialFeature.9.title')}`,
				desc:  `${t('ecommerceSpecialFeature.9.title')}`
			},
			{
				
				title:  `${t('ecommerceSpecialFeature.11.title')}`,
				desc:  `${t('ecommerceSpecialFeature.11.desc')}`,
			},
			{
			
				title: `${t('ecommerceSpecialFeature.12.title')}`,
				desc: `${t('ecommerceSpecialFeature.12.title')}`,
			},
			{
			
				title: `${t('ecommerceSpecialFeature.13.title')}`,
				desc: `${t('ecommerceSpecialFeature.13.desc')}`,
			},
			{
			
				title: `${t('ecommerceSpecialFeature.14.title')}`,
				desc: `${t('ecommerceSpecialFeature.14.desc')}`,
			},
			// {
				
			// 	title: `${t('Pricing.ecommerce.COMPLIMENTARY.15.title')}`,
			// 	desc: 'Unlimited transactions from your app and system by your customers'
			// }
		];
		
		const UPappUserFeature = [
			{
				
				title: `${t('Pricing.ecommerce.USER.1.title')}`,
				desc: `${t('Pricing.ecommerce.USER.1.desc')}`,
			},
			{
				title: `${t('Pricing.ecommerce.USER.2.title')}`,
				desc:`${t('Pricing.ecommerce.USER.2.desc')}`,
			},
			{
				title: `${t('Pricing.ecommerce.USER.3.title')}`,
				desc:  `${t('Pricing.ecommerce.USER.3.desc')}`,
			},
			{
				title:`${t('Pricing.ecommerce.USER.4.title')}`,
				desc:  `${t('Pricing.ecommerce.USER.3.desc')}`,
			},
			{
				title: `${t('Pricing.ecommerce.USER.5.title')}`,
				desc:  `${t('Pricing.ecommerce.USER.5.desc')}`,
			},
			{
				title: `${t('Pricing.ecommerce.USER.6.title')}`,
				desc: `${t('Pricing.ecommerce.USER.6.desc')}`,
			},
			{
				title: `${t('Pricing.ecommerce.USER.7.title')}`,
				desc: `${t('Pricing.ecommerce.USER.7.desc')}`,
			},
			{
				title: `${t('Pricing.ecommerce.USER.8.title')}`,
				desc: `${t('Pricing.ecommerce.USER.8.desc')}`,
			},
			{
				title: `${t('Pricing.ecommerce.USER.9.title')}`,
				desc: `${t('Pricing.ecommerce.USER.9.desc')}`,
			},
			{
				title: `${t('Pricing.ecommerce.USER.10.title')}`,
				desc: `${t('Pricing.ecommerce.USER.10.desc')}`,
			},
			// {
			// 	title: 'View Cart',
			// 	desc: 'Your customers can view, edit and add/delete their created cart for shopping'
			// },
			{
				title:`${t('Pricing.ecommerce.USER.11.title')}`,
				desc: `${t('Pricing.ecommerce.USER.11.desc')}`,
			},
			{
				title: `${t('Pricing.ecommerce.USER.12.title')}`,
				desc: `${t('Pricing.ecommerce.USER.12.desc')}`,
			},
			{
				title: `${t('Pricing.ecommerce.USER.13.title')}`,
				desc: `${t('Pricing.ecommerce.USER.13.desc')}`,
			},
			{
				title: `${t('Pricing.ecommerce.USER.14.title')}`,
				desc: `${t('Pricing.ecommerce.USER.14.desc')}`,
			},
			{
				title: `${t('Pricing.ecommerce.USER.15.title')}`,
				desc: `${t('Pricing.ecommerce.USER.15.desc')}`,
			},
			{
				title: `${t('Pricing.ecommerce.USER.16.title')}`,
				desc: `${t('Pricing.ecommerce.USER.16.desc')}`,
			},
			{
				title: `${t('Pricing.ecommerce.USER.17.title')}`,
				desc:`${t('Pricing.ecommerce.USER.17.desc')}`,
			},
			{
				title: `${t('Pricing.ecommerce.USER.18.title')}`,
				desc: `${t('Pricing.ecommerce.USER.18.desc')}`,
			}
			
		]
		
		const UPappAdminFeature = [
			{
				title: `${t('Pricing.ecommerce.ADMIN.1.title')}`,
				desc: `${t('Pricing.ecommerce.ADMIN.1.desc')}`,
			},
			{
				title: `${t('Pricing.ecommerce.ADMIN.2.title')}`,
				desc: `${t('Pricing.ecommerce.ADMIN.2.desc')}`,
			},
			{
				title: `${t('Pricing.ecommerce.ADMIN.3.title')}`,
				desc: `${t('Pricing.ecommerce.ADMIN.3.desc')}`,
			},
			{
				title: `${t('Pricing.ecommerce.ADMIN.4.title')}`,
				desc: `${t('Pricing.ecommerce.ADMIN.4.desc')}`,
			},
			{
				title: `${t('Pricing.ecommerce.ADMIN.5.title')}`,
				desc: `${t('Pricing.ecommerce.ADMIN.5.desc')}`,
			},
			{
				title: `${t('Pricing.ecommerce.ADMIN.6.title')}`,
				desc:`${t('Pricing.ecommerce.ADMIN.6.desc')}`,
			},
			{
				title: `${t('Pricing.ecommerce.ADMIN.7.title')}`,
				desc: `${t('Pricing.ecommerce.ADMIN.7.desc')}`,
			},
			{
				title: `${t('Pricing.ecommerce.ADMIN.8.title')}`,
				desc: `${t('Pricing.ecommerce.ADMIN.8.desc')}`,
			},
			{
				title: `${t('Pricing.ecommerce.ADMIN.9.title')}`,
				desc: `${t('Pricing.ecommerce.ADMIN.9.desc')}`,
			},
			{
				title:`${t('Pricing.ecommerce.ADMIN.10.title')}`,
				desc: `${t('Pricing.ecommerce.ADMIN.10.desc')}`,
			},
			{
				title: `${t('Pricing.ecommerce.ADMIN.11.title')}`,
				desc: `${t('Pricing.ecommerce.ADMIN.11.desc')}`,
			},
			{
				title: `${t('Pricing.ecommerce.ADMIN.12.title')}`,
				desc: `${t('Pricing.ecommerce.ADMIN.12.desc')}`,
			},
			{
				title: `${t('Pricing.ecommerce.ADMIN.13.title')}`,
				desc: `${t('Pricing.ecommerce.ADMIN.13.desc')}`,
			},
			{
				title: `${t('Pricing.ecommerce.ADMIN.14.title')}`,
				desc: `${t('Pricing.ecommerce.ADMIN.14.desc')}`,
			},
			{
				title: `${t('Pricing.ecommerce.ADMIN.15.title')}`,
				desc: `${t('Pricing.ecommerce.ADMIN.15.desc')}`,
			},
			{
				title: `${t('Pricing.ecommerce.ADMIN.16.title')}`,
				desc:`${t('Pricing.ecommerce.ADMIN.16.desc')}`,
			},
			{
				title: `${t('Pricing.ecommerce.ADMIN.17.title')}`,
				desc:`${t('Pricing.ecommerce.ADMIN.17.desc')}`,
			}
		]

        return (
            <Fragment>
                    
                    <Row
					gutter={16}
					style={{
						marginTop: '50px',
						marginBottom: '50px',
						marginLeft: '0px',
						marginRight: '0px'
					}}
					// type="flex"
					justify="center"
				>
					<div style={{ margin: "50px auto", maxWidth: "1240px" }}>
					<Col xs={24} md={8} lg={8} >
						<div style={{ textAlign: 'center' }} className="feature-title-shape user-feature-title">
							{t("Pricing.ecommerce.UPAPPCOMPLIMENTARY")}
					</div>

						<List
							size="large"
							className="feature-lists"
							dataSource={upAppSpecialData}
							renderItem={item => (
								<List.Item className="subs-feature-list">
									<Icon className="green-6" type="check" /> {item.title}&nbsp;
								<Tooltip title={item.desc}>
										<Icon
											style={{
												fontSize: '12px',
												color: '#607D8B',
												cursor: 'pointer'
											}}
											type="info-circle"
										/>
									</Tooltip>
								</List.Item>
							)}
						/>
					</Col>
					<Col xs={24} md={8} lg={8} >
						<div className="feature-title-shape user-feature-title" style={{ textAlign: 'center' }}>
							{t("Pricing.ecommerce.USERFEATURES")}
					</div>

						<List
							size="large"
							className="feature-lists"
							dataSource={UPappUserFeature}
							renderItem={item => (
								<List.Item className="subs-feature-list">
									<Icon className="green-6" type="check" /> {item.title}&nbsp;
								<Tooltip title={item.desc}>
										<Icon
											style={{
												fontSize: '12px',
												color: '#607D8B',
												cursor: 'pointer'
											}}
											type="info-circle"
										/>
									</Tooltip>
								</List.Item>
							)}
						/>
					</Col>
					<Col xs={24} md={8} lg={8} >
						<div style={{ textAlign: 'center' }} className="feature-title-shape admin-feature-title">
						{t("Pricing.ecommerce.ADMINFEATURES")}
					</div>
						<List
							size="large"
							className="feature-lists"
							dataSource={UPappAdminFeature}
							renderItem={item => (
								<List.Item className="subs-feature-list">
									<Icon className="green-6" type="check" /> {item.title}&nbsp;
								<Tooltip title={item.desc}>
										<Icon
											style={{
												fontSize: '12px',
												color: '#607D8B',
												cursor: 'pointer'
											}}
											type="info-circle"
										/>
									</Tooltip>
								</List.Item>
							)}
						/>
					</Col>
					</div>
				</Row>
            
            </Fragment>
        )
}

const mapStateToProps = state => ({
  });
  export default connect(mapStateToProps, {
	// getLocation
  })(BookingAppFeatureList);
