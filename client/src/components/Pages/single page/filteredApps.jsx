import React, { Fragment, useEffect, useState } from "react";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import { Link } from "react-router-dom";
import ScrollTop from "../../ScrollTop";
import { Helmet } from "react-helmet";
import {
  List,
  Typography,
  Card,
  Layout,
  Input,
  Row,
  Col,
  Menu,
  Spin
} from "antd";
import Title from "antd/lib/typography/Title";
import withDirection, { withDirectionPropTypes, DIRECTIONS } from 'react-with-direction';
import { useTranslation } from 'react-i18next';

const { Text } = Typography;
const { Meta } = Card;

const AppType = ({direction, match, searchApplication, Applications }) => {
  //   console.log(match.params.industry, match.params.type, Applications);
  const [applicationData, setApplicationData] = useState({
    apps: [],
    searchApplication: ""
  });

  const { apps } = applicationData;
  // console.log("match >>>", match);
  if (Applications === null) {
    // console.log("if");
  }
  useEffect(() => {
    if (match.params.type === "all" || match.params.type === undefined) {
      // setApplicationData({
      //   ...applicationData,
      //   apps: Applications
      // });

      setApplicationData({
        ...applicationData,
        apps:
          Applications &&
          Applications.filter(myapp => {
            return (
              (myapp.fields &&
                myapp.fields.name
                  .toLowerCase()
                  .includes(
                    searchApplication && searchApplication.toLocaleLowerCase()
                  )) ||
              (myapp.fields &&
                myapp.fields.type
                  .toLowerCase()
                  .includes(
                    searchApplication && searchApplication.toLocaleLowerCase()
                  )) ||
              (myapp.fields &&
                myapp.fields.industry
                  .toLowerCase()
                  .includes(
                    searchApplication && searchApplication.toLocaleLowerCase()
                  )) ||
              (myapp.fields &&
                myapp.fields.category
                  .toLowerCase()
                  .includes(
                    searchApplication && searchApplication.toLocaleLowerCase()
                  ))
            );
          })
      });
    } else {
      if (searchApplication === "" || searchApplication === null) {
        setApplicationData({
          ...applicationData,
          apps:
            Applications &&
            Applications.filter(arg => {
              return (
                arg.fields.type.toLowerCase() ===
                  match.params.type.toLowerCase() ||
                arg.fields.industry.toLowerCase() ===
                  match.params.type.toLowerCase()
              );
            })
        });
      } else {
        setApplicationData({
          ...applicationData,
          apps:
            Applications &&
            Applications.filter(myapp => {
              if (
                myapp.fields.type.toLowerCase() ===
                  match.params.type.toLowerCase() ||
                myapp.fields.industry.toLowerCase() ===
                  match.params.type.toLowerCase()
              ) {
                return (
                  (myapp.fields &&
                    myapp.fields.name
                      .toLowerCase()
                      .includes(
                        searchApplication &&
                          searchApplication.toLocaleLowerCase()
                      )) ||
                  (myapp.fields &&
                    myapp.fields.type
                      .toLowerCase()
                      .includes(
                        searchApplication &&
                          searchApplication.toLocaleLowerCase()
                      )) ||
                  (myapp.fields &&
                    myapp.fields.industry
                      .toLowerCase()
                      .includes(
                        searchApplication &&
                          searchApplication.toLocaleLowerCase()
                      )) ||
                  (myapp.fields &&
                    myapp.fields.category
                      .toLowerCase()
                      .includes(
                        searchApplication &&
                          searchApplication.toLocaleLowerCase()
                      ))
                );
              }
            })
        });
      }
    }

    // console.log(match);
  }, [match, searchApplication, Applications]);

  // console.log(searchApplication);

  return (
    <Fragment>
      <ScrollTop />
      <div
        style={{
          fontSize: "30px",
          textAlign: "center",
          marginTop: "30px"
        }}
      >
        <Title
          level={1}
          style={{ fontSize: "30px", fontWeight: "500" }}
        ></Title>
      </div>

      <div style={{ maxWidth: "1280px", margin: "0 auto" }}>
        <Row style={{ marginTop: "30px" }}>
          <Col xs={24} sm={24} lg={24}>
            <Row className="store-table">
              <List
                loading={!apps}
                grid={{
                  gutter: 10,
                  xs: 1,
                  sm: 2,
                  md: 2,
                  lg: 2,
                  xl: 3,
                  xxl: 3
                }}
                
                dataSource={apps && apps}
                footer={<div />}
                renderItem={item => (
                  <List.Item>
                    <Card
                      hoverable
                      className="store-card"
                      cover={
                        <Link
                          to={`/readymade-apps/${item.fields.slug}/${item.UID}`}
                        >
                          <img
                            alt="example"
                            src={item.fields.images[0].thumb.url}
                          />
                        </Link>
                      }
                    >
                       {direction === DIRECTIONS.LTR && <Fragment>
                        <Meta
                        title={item.fields.name}
                        description={item.fields.desc}
                      />
                       </Fragment>
                }
                {direction === DIRECTIONS.RTL && <Fragment>
                        <Meta
                        title={item.fields.ar && item.fields.ar.name}
                        description={item.fields.ar && item.fields.ar.desc}
                      />
                       </Fragment>
                }
                     
                    </Card>
                  </List.Item>
                )}
              />
            </Row>
            <Row>
              <Col
                xs={24}
                style={{ marginBottom: "30px", marginTop: "30px" }}
              />
            </Row>
          </Col>
        </Row>
      </div>
    </Fragment>
  );
};

AppType.propTypes = {
  Applications: PropTypes.array.isRequired,
  loading: PropTypes.bool
};
const mapStateToProps = state => ({
  Applications: state.application.Applications,
  loading: state.application.loading,
  direction: withDirectionPropTypes.direction,
});
export default connect(mapStateToProps, null)(withDirection(AppType));
