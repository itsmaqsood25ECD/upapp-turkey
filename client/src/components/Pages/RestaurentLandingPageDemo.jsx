/* eslint react/prop-types: 0 */
import React, { useState, useEffect, Fragment } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { Button, Typography, Row, Col, Spin, Form } from "antd";
import "rc-banner-anim/assets/index.css";
import ScrollTop from "../ScrollTop";
import { Link } from "react-router-dom";
import "antd/dist/antd.css";
import "../../assets/css/custom.css";
import "../../assets/css/home.css";
import "../../assets/css/homeNew.css";
import "react-animated-slider/build/horizontal.css";
import SSsliderFacility from "./single page/SSsliderFacility";
import WebsiteDemo from "./RestaurantAppPages/websitedemo"

// images
import AndroidIosAppIncluded from "../../assets/img/eCommerce Page/Android&IosAppIncluded.svg";

import PackagePriceVerticleRestaurant from "./PackagePriceVerticleRestaurant";
import { Helmet } from "react-helmet";

const { Title } = Typography;

const RestaurentLandingPage = ({ loading }) => {
  const [appDetails, setAppData] = useState({
    apps: [],
    selectApplication: "",
    currency: "",

    // EcommcercenewBDSPM: '',
    newADSPM: "",
    newBDSPY: "",
    newADSPY: "",
    newMobileBDSPM: "",
    newMobileADSPM: "",
    newMobileBDSPY: "",
    newMobileADSPY: "",
    country: "",
    MSC: "",
    // Plan B
    MFPP: ""
  });

  const {
    country,
    currency,
    newMobileBDSPM,
    newMobileADSPM,
    newMobileBDSPY,
    newMobileADSPY,
    MSC,
    MFPP
  } = appDetails;

  // const Restaurant_Screenshot = [
  //   {
  //     img:
  //       "https://upappfactory-static-images.s3.us-east-2.amazonaws.com/RestaurentApp/Screens/Home+Page.webp",
  //     name: "Home Page"
  //   },
  //   {
  //     img:
  //       "https://upappfactory-static-images.s3.us-east-2.amazonaws.com/RestaurentApp/Screens/Restaurant+Outlet.webp",
  //     name: "Restaurant Outlet"
  //   },
  //   {
  //     img:
  //       "https://upappfactory-static-images.s3.us-east-2.amazonaws.com/RestaurentApp/Screens/Store+Locator+Page.webp",
  //     name: "Store Locator Page"
  //   },
  //   {
  //     img:
  //       "https://upappfactory-static-images.s3.us-east-2.amazonaws.com/RestaurentApp/Screens/Store+Page.webp",
  //     name: "Store Page"
  //   },
  //   {
  //     img:
  //       "https://upappfactory-static-images.s3.us-east-2.amazonaws.com/RestaurentApp/Screens/Menu+Page.webp",
  //     name: "Menu Page"
  //   },
  //   {
  //     img:
  //       "https://upappfactory-static-images.s3.us-east-2.amazonaws.com/RestaurentApp/Screens/Categories+Menu.webp",
  //     name: "Category Menu Page"
  //   },
  //   {
  //     img:
  //       "https://upappfactory-static-images.s3.us-east-2.amazonaws.com/RestaurentApp/Screens/Detail+Menu.webp",
  //     name: "Detailed Menu Page"
  //   },
  //   {
  //     img:
  //       "https://upappfactory-static-images.s3.us-east-2.amazonaws.com/RestaurentApp/Screens/Menu+Options.webp",
  //     name: "Menu Options"
  //   },
  //   {
  //     img:
  //       "https://upappfactory-static-images.s3.us-east-2.amazonaws.com/RestaurentApp/Screens/Menu+Overview.webp",
  //     name: "Menu Overview"
  //   },
  //   {
  //     img:
  //       "https://upappfactory-static-images.s3.us-east-2.amazonaws.com/RestaurentApp/Screens/My+Cart+Page.webp",
  //     name: "My Cart Page"
  //   },
  //   {
  //     img:
  //       "https://upappfactory-static-images.s3.us-east-2.amazonaws.com/RestaurentApp/Screens/Order+Summery+Page.webp",
  //     name: "Order Summary Page"
  //   },
  //   {
  //     img:
  //       "https://upappfactory-static-images.s3.us-east-2.amazonaws.com/RestaurentApp/Screens/Delivery+Address+Page.webp",
  //     name: "Delivery Address Page"
  //   },
  //   {
  //     img:
  //       "https://upappfactory-static-images.s3.us-east-2.amazonaws.com/RestaurentApp/Screens/Current+Order+Page.webp",
  //     name: "Current Order Page"
  //   },
  //   {
  //     img:
  //       "https://upappfactory-static-images.s3.us-east-2.amazonaws.com/RestaurentApp/Screens/loyalty+Points.webp",
  //     name: "Loyalty Points Page"
  //   }
  // ];


  const Restaurant_Screenshot = [
    {
      img: "https://upappfactory-static-images.s3.us-east-2.amazonaws.com/RestaurentApp/Mobile+Demo+Screens/iPhone+XS+%E2%80%93+1%402x.png",
      name: "Home Page"
    },
    {
      img: "https://upappfactory-static-images.s3.us-east-2.amazonaws.com/RestaurentApp/Mobile+Demo+Screens/iPhone+XS+1%402x.png",
      name: "Navigation Menu"
    },
    {
      img: "https://upappfactory-static-images.s3.us-east-2.amazonaws.com/RestaurentApp/Mobile+Demo+Screens/iPhone+XS+2%402x.png",
      name: "Menu Page"
    },
    {
      img: "https://upappfactory-static-images.s3.us-east-2.amazonaws.com/RestaurentApp/Mobile+Demo+Screens/iPhone+XS+14%402x.png",
      name: "Detailed Menu Page"
    },
    {
      img: "https://upappfactory-static-images.s3.us-east-2.amazonaws.com/RestaurentApp/Mobile+Demo+Screens/iPhone+XS+15%402x.png",
      name: "My Cart Page"
    },
    {
      img: "https://upappfactory-static-images.s3.us-east-2.amazonaws.com/RestaurentApp/Mobile+Demo+Screens/iPhone+XS+4%402x.png",
      name: "Registration Page"
    },
    {
      img: "https://upappfactory-static-images.s3.us-east-2.amazonaws.com/RestaurentApp/Mobile+Demo+Screens/iPhone+XS+5%402x.png",
      name: "Verification Page"
    },
    {
      img: "https://upappfactory-static-images.s3.us-east-2.amazonaws.com/RestaurentApp/Mobile+Demo+Screens/iPhone+XS+6%402x.png",
      name: "Feedback Page"
    },
    {
      img: "https://upappfactory-static-images.s3.us-east-2.amazonaws.com/RestaurentApp/Mobile+Demo+Screens/iPhone+XS+7%402x.png",
      name: "Feedback Submit Page"
    },
    {
      img: "https://upappfactory-static-images.s3.us-east-2.amazonaws.com/RestaurentApp/Mobile+Demo+Screens/iPhone+XS+8%402x.png",
      name: "Loyalty Page"
    },
    {
      img: "https://upappfactory-static-images.s3.us-east-2.amazonaws.com/RestaurentApp/Mobile+Demo+Screens/iPhone+XS+9%402x.png",
      name: "Promotion Page"
    },
    {
      img: "https://upappfactory-static-images.s3.us-east-2.amazonaws.com/RestaurentApp/Mobile+Demo+Screens/iPhone+XS+10%402x.png",
      name: "Near Me Page"
    },
    {
      img: "https://upappfactory-static-images.s3.us-east-2.amazonaws.com/RestaurentApp/Mobile+Demo+Screens/iPhone+XS+12%402x.png",
      name: "Reservation Page"
    },
    {
      img: "https://upappfactory-static-images.s3.us-east-2.amazonaws.com/RestaurentApp/Mobile+Demo+Screens/iPhone+XS+13%402x.png",
      name: "Reservation Confirmed Page"
    },

    {
      img: "https://upappfactory-static-images.s3.us-east-2.amazonaws.com/RestaurentApp/Mobile+Demo+Screens/iPhone+XS+18%402x.png",
      name: "Notification Page"
    },
    {
      img: "https://upappfactory-static-images.s3.us-east-2.amazonaws.com/RestaurentApp/Mobile+Demo+Screens/iPhone+XS+16%402x.png",
      name: "My Order Page"
    },
    {
      img: "https://upappfactory-static-images.s3.us-east-2.amazonaws.com/RestaurentApp/Mobile+Demo+Screens/iPhone+XS+17%402x.png",
      name: "Order Confirmed Page"
    },
  ];

  const keyFeatures = [
    {
      icon:
        "https://upappfactory-static-images.s3.us-east-2.amazonaws.com/RestaurentApp/Icon/OrderManagement.svg",
      title: "Order Management"
    },
    {
      icon:
        "https://upappfactory-static-images.s3.us-east-2.amazonaws.com/RestaurentApp/Icon/TableReservation.svg",
      title: "Online Table Reservation"
    },
    {
      icon:
        "https://upappfactory-static-images.s3.us-east-2.amazonaws.com/RestaurentApp/Icon/OnlineReview.svg",
      title: "Online Review"
    },
    {
      icon:
        "https://upappfactory-static-images.s3.us-east-2.amazonaws.com/RestaurentApp/Icon/DigitalMenu.svg",
      title: "Online Digital Menu"
    },
    {
      icon:
        "https://upappfactory-static-images.s3.us-east-2.amazonaws.com/RestaurentApp/Icon/coupons.svg",
      title: "Coupon & Promotional"
    },
    {
      icon:
        "https://upappfactory-static-images.s3.us-east-2.amazonaws.com/RestaurentApp/Icon/OnlinePayments.svg",
      title: "Online Payments"
    },
    {
      icon:
        "https://upappfactory-static-images.s3.us-east-2.amazonaws.com/RestaurentApp/Icon/LoyaltyManagement.svg",
      title: "Loyalty Points"
    }
  ];


  useEffect(() => {
    if (sessionStorage.getItem("currency") === "KWD") {
      setAppData({
        ...appDetails,
        country: sessionStorage.getItem("country"),
        currency: "KWD",
        // Mobile
        newMobileBDSPM: Math.round(3.08),
        newMobileADSPM: Math.round(3.08),
        newMobileBDSPY: Math.round(3.08),
        newMobileADSPY: Math.round(3.08),
        MSC: Math.round(6.22),
        // Plan B
        MFPP: Math.round(6.22)
      });
    } else if (sessionStorage.getItem("currency") === "AED") {
      setAppData({
        ...appDetails,
        country: sessionStorage.getItem("country"),
        currency: "AED",
        // Mobile
        newMobileBDSPM: Math.round(36.36),
        newMobileADSPM: Math.round(36.36),
        newMobileBDSPY: Math.round(36.36),
        newMobileADSPY: Math.round(36.36),
        // setup charge
        MSC: Math.round(73.09),
        // Plan B
        MFPP: Math.round(73.09)
      });
    } else if (sessionStorage.getItem("currency") === "SGD") {
      setAppData({
        ...appDetails,
        country: sessionStorage.getItem("country"),
        currency: "SGD",
        // Mobile
        newMobileBDSPM: Math.round(14.32),
        newMobileADSPM: Math.round(14.32),
        newMobileBDSPY: Math.round(14.32),
        newMobileADSPY: Math.round(14.32),
        MSC: Math.round(28.76),
        // Plan B
        MFPP: Math.round(28.76)
      });
    } else if (sessionStorage.getItem("currency") === "OMR") {
      setAppData({
        ...appDetails,
        country: sessionStorage.getItem("country"),
        currency: "OMR",
        // Mobile
        newMobileBDSPM: Math.round(3.81),
        newMobileADSPM: Math.round(3.81),
        newMobileBDSPY: Math.round(3.81),
        newMobileADSPY: Math.round(3.81),
        MSC: Math.round(7.65),
        //  Plan B
        MFPP: Math.round(7.65)
      });
    } else if (sessionStorage.getItem("currency") === "QAR") {
      setAppData({
        ...appDetails,
        country: sessionStorage.getItem("country"),
        currency: "QAR",
        // Mobile
        newMobileBDSPM: Math.round(36.05),
        newMobileADSPM: Math.round(36.05),
        newMobileBDSPY: Math.round(36.05),
        newMobileADSPY: Math.round(36.05),
        MSC: Math.round(72.46),
        //  Plan B
        MFPP: Math.round(72.46)
      });
    } else if (sessionStorage.getItem("currency") === "BHD") {
      setAppData({
        ...appDetails,
        country: sessionStorage.getItem("country"),
        currency: "BD",

        // Mobile
        newMobileBDSPM: Math.round(3.74),
        newMobileADSPM: Math.round(3.74),
        newMobileBDSPY: Math.round(3.74),
        newMobileADSPY: Math.round(3.74),
        MSC: Math.round(7.50),
        //  Plan B
        MFPP: Math.round(7.50)
      });
    } else if (sessionStorage.getItem("currency") === "MYR") {
      setAppData({
        ...appDetails,
        country: sessionStorage.getItem("country"),
        currency: "MYR",
        // Mobile
        newMobileBDSPM: Math.round(43.86),
        newMobileADSPM: Math.round(43.86),
        newMobileBDSPY: Math.round(43.86),
        newMobileADSPY: Math.round(43.86),
        MSC: Math.round(87.35),
        //  Plan B
        MFPP: Math.round(87.35)
      });
    } else if (sessionStorage.getItem("currency") === "LKR") {
      setAppData({
        ...appDetails,
        country: sessionStorage.getItem("country"),
        currency: "LKR",

        // Mobile
        newMobileBDSPM: Math.round(1857.55),
        newMobileADSPM: Math.round(1857.55),
        newMobileBDSPY: Math.round(1857.55),
        newMobileADSPY: Math.round(1857.55),
        MSC: Math.round(3741.86),
        //  Plan B
        MFPP: Math.round(3741.86)
      });
    } else if (sessionStorage.getItem("currency") === "EGP") {
      setAppData({
        ...appDetails,
        country: sessionStorage.getItem("country"),
        currency: "EGP",

        // Mobile
        newMobileBDSPM: Math.round(155.93),
        newMobileADSPM: Math.round(155.93),
        newMobileBDSPY: Math.round(155.93),
        newMobileADSPY: Math.round(155.93),
        MSC: Math.round(313.51),
        //  Plan B
        MFPP: Math.round(313.51)
      });
    } else if (sessionStorage.getItem("currency") === "SAR") {
      setAppData({
        ...appDetails,
        country: sessionStorage.getItem("country"),
        currency: "SAR",
        // Mobile
        newMobileBDSPM: Math.round(37.19),
        newMobileADSPM: Math.round(37.19),
        newMobileBDSPY: Math.round(37.19),
        newMobileADSPY: Math.round(37.19),
        MSC: Math.round(74.76),
        //  Plan B
        MFPP: Math.round(74.76)
      });
    } else if (sessionStorage.getItem("currency") === "JOD") {
      setAppData({
        ...appDetails,
        country: sessionStorage.getItem("country"),
        currency: "JOD",

        // Mobile
        newMobileBDSPM: Math.round(7.02),
        newMobileADSPM: Math.round(7.02),
        newMobileBDSPY: Math.round(7.02),
        newMobileADSPY: Math.round(7.02),
        MSC: Math.round(14.11),
        //  Plan B
        MFPP: Math.round(14.11)
      });
    } else if (sessionStorage.getItem("currency") === "KYD") {
      setAppData({
        ...appDetails,
        country: sessionStorage.getItem("country"),
        currency: "KYD",
        // Mobile
        newMobileBDSPM: Math.round(9.16),
        newMobileADSPM: Math.round(9.16),
        newMobileBDSPY: Math.round(9.16),
        newMobileADSPY: Math.round(9.16),
        MSC: Math.round(18.39),
        //  Plan B
        MFPP: Math.round(18.39)
      });
    } else if (sessionStorage.getItem("currency") === "ZAR") {
      setAppData({
        ...appDetails,
        country: sessionStorage.getItem("country"),
        currency: "ZAR",
        // Mobile
        newMobileBDSPM: Math.round(174.04),
        newMobileADSPM: Math.round(174.04),
        newMobileBDSPY: Math.round(174.04),
        newMobileADSPY: Math.round(174.04),
        MSC: Math.round(347.78),
        //  Plan B
        MFPP: Math.round(347.78)
      });
    } else if (sessionStorage.getItem("currency") === "USD") {
      setAppData({
        ...appDetails,
        country: sessionStorage.getItem("country"),
        currency: "USD",
        // Mobile
        newMobileBDSPM: 9.9,
        newMobileADSPM: 9.9,
        newMobileBDSPY: 9.9,
        newMobileADSPY: 9.9,

        MSC: Math.round(19.9),
        //  Plan B
        MFPP: 19.9
      });
    } else {
      setAppData({
        ...appDetails,
        country: sessionStorage.getItem("country"),
        currency: sessionStorage.getItem("currency_sign"),

        // Mobile
        newMobileBDSPM: Math.round(
          9.9 * sessionStorage.getItem("currency_rate")
        ),
        newMobileADSPM: Math.round(
          9.9 * sessionStorage.getItem("currency_rate")
        ),
        newMobileBDSPY: Math.round(
          9.9 * sessionStorage.getItem("currency_rate")
        ),
        newMobileADSPY: Math.round(
          9.9 * sessionStorage.getItem("currency_rate")
        ),
        MSC: Math.round(19.9 * sessionStorage.getItem("currency_rate")),
        //  Plan B
        MFPP: Math.round(19.9 * sessionStorage.getItem("currency_rate"))
      });
    }
  }, [country, currency]);


  return (
    <div>
      <ScrollTop />
      <Helmet>
        <title>UPapp factory - Restaurant Application</title>
        <meta
          name="Readymade Restaurant Mobile Apps for Android and iOS Platforms | UPapp factory"
          content="Build native mobile Restaurant Application for your Restaurant. Readymade ecommerce app on all platforms. we help you by developing a feature-rich mobile app solution depends on your business requirements."
        />
      </Helmet>
      <section
        className="UAF-container-fluid"
        style={{
          background: "#fff",
          padding: 0,
          maxWidth: "1200px",
          margin: "100px auto 180px auto"
        }}
      >
        <Row type="flex" gutter={50}>
          <Col lg={{ span: 12, order: 1 }} xs={{ span: 24, order: 2 }}>
            <div className="tagline-heading" style={{ padding: "auto 15px" }}>
              <Title level={1} className="texty-demo">
                Deliver a Delicious <br /> Experience With Your Own <br />{" "}
                Restaurant App.
              </Title>
              <Link to="/factory">
                <Button size={"large"} className="slider-get-started-btn uaf_allPageBtn_bg_Color">
                  Get Started
                </Button>
              </Link>
            </div>
          </Col>
          <Col lg={{ span: 12, order: 2 }} xs={{ span: 24, order: 1 }}>
            <div className="home-feature-img main">
              <img
                style={{ marginTop: "-50px" }}
                src="https://upappfactory-static-images.s3.us-east-2.amazonaws.com/RestaurentApp/FeaturedIMG.png"
                alt="Restaurant Application"
              />
            </div>
          </Col>
        </Row>
      </section>
      <section
        className="UAF-container-fluid"
        style={{
          background: "#fff",
          padding: 0,
          maxWidth: "1200px",
          margin: "0 auto 80px auto",
          textAlign: "center"
        }}
      >
        <p style={{ fontSize: "45px", marginBottom: "30px" }}>Key Features</p>
        <Row type="flex" justify="space-around" align="middle">
          {keyFeatures.map(index => {
            return (
              <Col xs={12} sm={6} lg={3}>
                <div
                  style={{
                    width: "100%",
                    maxWidth: "180px",
                    height: "130px",
                    margin: "30px auto"
                  }}
                >
                  <img
                    style={{ width: "100px", height: "100px" }}
                    src={index.icon}
                    alt=""
                  />
                  <p>{index.title}</p>
                </div>
              </Col>
            );
          })}
        </Row>
      </section>

      <section
        className="UAF-container-fluid"
        style={{
          margin: "80px auto 0px auto"
        }}
      >
        <div
          style={{
            maxWidth: "1200px",
            margin: "0 auto",
            textAlign: "center",
            padding: "25px"
          }}
        >
          <Row type="flex" gutter={30}>
            <Col lg={{ span: 12, order: 2 }} xs={{ span: 24, order: 2 }}>
              <div
                className="tagline-heading tagline-body sub-points lg-rtl"
                style={{ padding: "auto 15px" }}
              >
                <Title level={1} className="texty-demo lg-rtl">
                  Readymade Android and iOS Apps
                </Title>
                <Title
                  level={1}
                  className="text-demo-sub-text lg-rtl lg-float-right"
                  style={{
                    fontSize: "18px",
                    lineHeight: "20px",
                    fontWeight: 400,
                    maxWidth: "100%"
                  }}
                >
                  Get native mobile apps for Android & iOS, easy to launch in
                  the App stores & play store which gives you freedom to reach
                  everyone around earth.
                </Title>

                <Link to="/factory">
                  <Button size={"large"} className="slider-get-started-btn lg-float-right uaf_allPageBtn_bg_Color">
                    Get Started
                  </Button>
                </Link>
              </div>
            </Col>
            <Col lg={{ span: 12, order: 1 }} xs={{ span: 24, order: 1 }}>
              <div className="home-feature-img feature-img-body">
                <img
                  style={{ maxWidth: "200px" }}
                  src={AndroidIosAppIncluded}
                  alt="image with animation for upapp"
                />
              </div>
            </Col>
          </Row>
        </div>
      </section>

      <section
        className="UAF-container-fluid"
        style={{
          margin: "0px auto 60px auto"
        }}
      >
        <div
          style={{
            maxWidth: "1200px",
            margin: "0 auto",
            textAlign: "center",
            padding: "25px"
          }}
        >
          <p
            style={{
              fontSize: "45px",
              marginTop: "100px",
              marginBottom: "80px",
              lineHeight: "1"
            }}
          >
            A ready-made app to cater to
            <br />
            all your restaurant's needs
          </p>
          <Row type="flex" justify="center" align="top">
            <Col lg={{ span: 12, order: 1 }} xs={{ span: 24, order: 2 }}>
              <div
                className="tagline-heading tagline-body sub-points"
                style={{ padding: "auto 15px" }}
              >
                <Title level={1} className="texty-demo lg-ltr">
                  Multiple Mode of Service
                  <br />
                </Title>
                <p
                  className="text-demo-sub-text lg-ltr"
                  style={{
                    fontSize: "18px",
                    lineHeight: "20px",
                    fontWeight: 400,
                    maxWidth: "100%"
                  }}
                >
                  <ul className="m-dir-ltr m-align-left facility-lists">
                    <li>
                      <p>
                        {" "}
                        Order Online - Order Online & Get Delivered your order
                        on Time
                      </p>
                    </li>
                    <li>
                      <p>
                        Take Away - Order Online & Collect your Order from
                        restaurant
                      </p>
                    </li>
                    <li>
                      <p>
                        {" "}
                        Serve at Outlet -Visit Restaurant & Get Served The
                        Delicious Food
                      </p>
                    </li>
                  </ul>
                </p>

                <Link to="/factory/booking">
                  <Button
                    // type="dashed"
                    size={"large"}
                    style={{
                      background: "#4FBFDD",
                      color: "#fff",
                      marginTop: "20px"
                    }}
                    className="slider-get-started-btn lg-float-left"
                  >
                    Get Started
                  </Button>
                </Link>
              </div>
            </Col>
            <Col lg={{ span: 12, order: 2 }} xs={{ span: 24, order: 1 }}>
              <div className="home-feature-img feature-img-body">
                <img
                  style={{ maxWidth: "200px" }}
                  src="https://upappfactory-static-images.s3.us-east-2.amazonaws.com/RestaurentApp/multipleMode+OfService.svg"
                  alt="image with animation for upapp"
                />
              </div>
            </Col>
          </Row>
        </div>
      </section>

      <section
        className="UAF-container-fluid"
        style={{
          margin: "0px auto 60px auto"
        }}
      >
        <div
          style={{
            maxWidth: "1200px",
            margin: "0 auto",
            textAlign: "center",
            padding: "25px"
          }}
        >
          <Row type="flex" justify="center" align="top" gutter={30}>
            <Col lg={{ span: 12, order: 2 }} xs={{ span: 24, order: 2 }}>
              <div
                className="tagline-heading tagline-body sub-points lg-rtl"
                style={{ padding: "auto 15px" }}
              >
                <Title level={1} className="texty-demo lg-rtl">
                  Deliver Orders Faster
                  <br />
                </Title>
                <p
                  className="text-demo-sub-text lg-rtl lg-float-right"
                  style={{
                    fontSize: "18px",
                    lineHeight: "20px",
                    fontWeight: 400,
                    maxWidth: "100%"
                  }}
                >
                  <div
                    dir="rtl"
                    className="m-dir-ltr m-align-left facility-lists facility-lists-right"
                  >
                    <p>
                      Your staff can track and update the status of each order
                      as opened, served, billed, or closed with the click of a
                      button. Delivery and kitchen staff can fulfill orders
                      faster without having to run between the back and front of
                      the house.{" "}
                    </p>
                  </div>
                </p>

                <Link to="/factory/booking">
                  <Button
                    // type="dashed"
                    size={"large"}
                    style={{
                      background: "#4FBFDD",
                      color: "#fff",
                      marginTop: "0px"
                    }}
                    className="slider-get-started-btn lg-float-right"
                  >
                    Get Started
                  </Button>
                </Link>
              </div>
            </Col>
            <Col lg={{ span: 12, order: 1 }} xs={{ span: 24, order: 1 }}>
              <div className="home-feature-img feature-img-body">
                <img
                  style={{ maxWidth: "200px" }}
                  src="https://upappfactory-static-images.s3.us-east-2.amazonaws.com/RestaurentApp/DeliverOrders+FasterFlip.svg"
                  alt="Deliver food faster"
                />
              </div>
            </Col>
          </Row>
        </div>
      </section>

      <section
        className="UAF-container-fluid"
        style={{
          margin: "0px auto 60px auto"
        }}
      >
        <div
          style={{
            maxWidth: "1200px",
            margin: "0 auto",
            textAlign: "center",
            padding: "25px"
          }}
        >
          <Row type="flex" justify="center" align="top">
            <Col lg={{ span: 12, order: 1 }} xs={{ span: 24, order: 2 }}>
              <div
                className="tagline-heading tagline-body sub-points"
                style={{ padding: "auto 15px" }}
              >
                <Title level={1} className="texty-demo lg-ltr">
                  Make The Reservation
                  <br />
                  Process Painless.
                </Title>
                <p
                  className="text-demo-sub-text lg-ltr"
                  style={{
                    fontSize: "18px",
                    lineHeight: "20px",
                    fontWeight: 400,
                    maxWidth: "100%"
                  }}
                >
                  Spare your customers the long wait and offer the option to
                  reserve a table in advance. Use the interactive table
                  selection module of the app to easily allocate tables and send
                  instant booking confirmation to customers via email or push
                  notification.
                </p>

                <Link to="/factory/booking">
                  <Button
                    // type="dashed"
                    size={"large"}
                    style={{
                      background: "#4FBFDD",
                      color: "#fff",
                      marginTop: "20px"
                    }}
                    className="slider-get-started-btn lg-float-left"
                  >
                    Get Started
                  </Button>
                </Link>
              </div>
            </Col>
            <Col lg={{ span: 12, order: 2 }} xs={{ span: 24, order: 1 }}>
              <div className="home-feature-img feature-img-body">
                <img
                  style={{ maxWidth: "200px" }}
                  src="https://upappfactory-static-images.s3.us-east-2.amazonaws.com/RestaurentApp/TableReservation.svg"
                  alt="image with animation for upapp"
                />
              </div>
            </Col>
          </Row>
        </div>
      </section>
      <section
        className="UAF-container-fluid"
        style={{
          margin: "0px auto 60px auto"
        }}
      >
        <div
          style={{
            maxWidth: "1200px",
            margin: "0 auto",
            textAlign: "center",
            padding: "25px"
          }}
        >
          <Row type="flex" justify="center" align="middle" gutter={30}>
            <Col lg={{ span: 12, order: 2 }} xs={{ span: 24, order: 2 }}>
              <div
                className="tagline-heading tagline-body sub-points lg-rtl"
                style={{ padding: "auto 15px" }}
              >
                <Title level={1} className="texty-demo lg-rtl">
                  Say Goodbye To Paperback
                  <br />
                  Feedback Forms
                  <br />
                </Title>
                <p
                  className="text-demo-sub-text lg-rtl lg-float-right"
                  style={{
                    fontSize: "18px",
                    lineHeight: "20px",
                    fontWeight: 400,
                    maxWidth: "100%"
                  }}
                >
                  Your staff can track and update the status of each order as
                  opened, served, billed, or closed with the click of a button.
                  Delivery and kitchen staff can fulfill orders faster without
                  having to run between the back and front of the house.
                </p>

                <Link to="/factory/booking">
                  <Button
                    // type="dashed"
                    size={"large"}
                    style={{
                      background: "#4FBFDD",
                      color: "#fff",
                      marginTop: "0px"
                    }}
                    className="slider-get-started-btn lg-float-right"
                  >
                    Get Started
                  </Button>
                </Link>
              </div>
            </Col>
            <Col lg={{ span: 12, order: 1 }} xs={{ span: 24, order: 1 }}>
              <div className="home-feature-img feature-img-body">
                <img
                  style={{ maxWidth: "200px" }}
                  src="https://upappfactory-static-images.s3.us-east-2.amazonaws.com/RestaurentApp/RatingReviewSm.svg"
                  alt="Rating Reviews"
                />
              </div>
            </Col>
          </Row>
        </div>
      </section>

      {/* <div className="UAF-container-fluid">
        <div className="single-app-heading-text">
          <br />
        How Your Website Will Look To Your Customers
      </div>
        <WebsiteDemo />
      </div> */}
      <div className="single-app-heading-text">
        <br />
        How Your App Will Look To Your Customers
      </div>
      <SSsliderFacility screenshots={Restaurant_Screenshot} />
      <br />
      <section className="" style={{ marginTop: "80px", marginBottom: "80px" }}>
        <PackagePriceVerticleRestaurant
          currency={currency}
          MBDSPM={newMobileBDSPM}
          MADSPM={newMobileADSPM}
          MBDSPY={newMobileBDSPY}
          MADSPY={newMobileADSPY}
          duration="Mo"
          PackType="Annually"
          MSC={MSC}
          MFPP={MFPP}
        />
      </section>
    </div>
  );
};

const HomeRFC = Form.create({ name: "Request_for_customize" })(
  RestaurentLandingPage
);

HomeRFC.propTypes = {
  loading: PropTypes.bool
};

const mapStateToProps = state => ({
  loading: state.application.loading
});

export default connect(mapStateToProps, {})(HomeRFC);
