import React, { useState, useEffect, Fragment } from "react";
import ReactDOM from "react-dom";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import "antd/dist/antd.css";
import { Link } from "react-router-dom";
import { List, Avatar, Button, Skeleton, Card, Icon } from "antd";
import withDirection, { withDirectionPropTypes, DIRECTIONS } from 'react-with-direction';
import { useTranslation } from 'react-i18next';
import i18next from 'i18next';
import Slider from "react-slick";
import "slick-carousel/slick/slick.css";



let count;
const { Meta } = Card;

const WebsiteThemes = ({ }) => {

  const { t } = useTranslation();

  const [state, setState] = useState({
    initLoading: true,
    loading: false,
    data: [],
    list: []
  })

  const { initLoading, loading, list } = state;


  useEffect(() => {

    setState({
      initLoading: false,
      data: Newdata,
      list: []
    });


  }, [])

  function NextArrow(props) {
    const { onClick } = props;
    return (
      <Button
        type="primary"
        onClick={onClick}
        className="ss-next-btn"
        shape="circle"
      >
        <Icon type="arrow-right" />
      </Button>
    );
  }

  function PrevArrow(props) {
    const { onClick } = props;
    return (
      <Button
        type="primary"
        onClick={onClick}
        className="ss-prev-btn"
        shape="circle"
      >
        <Icon type="arrow-left" />
      </Button>
    );
  }


  var settings = {
    prevArrow: <PrevArrow />,
    nextArrow: <NextArrow />,
    dots: false,
    infinite: true,
    centerMode: true,
    speed: 200,
    swipeToSlide: true,
    slidesToShow: 4,
    slidesToScroll: 1,
    autoplay: true,
    autoplaySpeed: 2000,
    lazyLoad: true,
    initialSlide: 0,

    responsive: [
      {
        breakpoint: 1024,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 1,
          infinite: true,
          dots: false
        }
      },
      {
        breakpoint: 600,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 1,
          initialSlide: 2
        }
      },
      {
        breakpoint: 480,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1
        }
      }
    ]
  };


  const Newdata = [
    // {
    //   title: `${t("ecommercePage.Demo")} 1`,
    //   url: "https://ecomdemo1.upappfactory.app/",
    //   img: "https://upappfactory-static-images.s3.us-east-2.amazonaws.com/EcommDemos/Demo-1.png"
    // },
    // {
    //   title: `${t("ecommercePage.Demo")} 2`,
    //   url: "https://ecomdemo2.upappfactory.app/",
    //   img: "https://upappfactory-static-images.s3.us-east-2.amazonaws.com/EcommDemos/Demo-2.png"

    // },
    {
      title: `${t("ecommercePage.Demo")} 1`,
      url: "https://ecomdemo3.upappfactory.app/",
      img: "https://upappfactory-static-images.s3.us-east-2.amazonaws.com/EcommDemos/Demo-3.png"
    },
    // {
    //   title: `${t("ecommercePage.Demo")} 4`,
    //   url: "https://ecomdemo4.upappfactory.app/",
    //   img: "https://upappfactory-static-images.s3.us-east-2.amazonaws.com/EcommDemos/Demo-4.png"
    // },
    // {
    //   title: `${t("ecommercePage.Demo")} 5`,
    //   url: "https://ecomdemo5.upappfactory.app/",
    //   img: "https://upappfactory-static-images.s3.us-east-2.amazonaws.com/EcommDemos/Demo-5.png"
    // },
    // {
    //   title: `${t("ecommercePage.Demo")} 6`,
    //   url: "https://ecomdemo6.upappfactory.app/",
    //   img: "https://upappfactory-static-images.s3.us-east-2.amazonaws.com/EcommDemos/Demo-6.png"
    // },
    // {
    //   title: `${t("ecommercePage.Demo")} 7`,
    //   url: "https://ecomdemo7.upappfactory.app/",
    //   img: "https://upappfactory-static-images.s3.us-east-2.amazonaws.com/EcommDemos/Demo-7.png"
    // },
    // {
    //   title: `${t("ecommercePage.Demo")} 8`,
    //   url: "https://ecomdemo8.upappfactory.app/",
    //   img: "https://upappfactory-static-images.s3.us-east-2.amazonaws.com/EcommDemos/Demo-8.png"
    // },
    // {
    //   title: `${t("ecommercePage.Demo")} 9`,
    //   url: "https://ecomdemo9.upappfactory.app/",
    //   img: "https://upappfactory-static-images.s3.us-east-2.amazonaws.com/EcommDemos/Demo-9.png"
    // },
    {
      title: `${t("ecommercePage.Demo")} 2`,
      url: "https://ecomdemo10.upappfactory.app/",
      img: "https://upappfactory-static-images.s3.us-east-2.amazonaws.com/EcommDemos/Demo-10.png"
    },
    // {
    //   title: `${t("ecommercePage.Demo")} 11`,
    //   url: "https://ecomdemo11.upappfactory.app/",
    //   img: "https://upappfactory-static-images.s3.us-east-2.amazonaws.com/EcommDemos/Demo-11.png"
    // },
    // {
    //   title: `${t("ecommercePage.Demo")} 12`,
    //   url: "https://ecomdemo12.upappfactory.app/",
    //   img: "https://upappfactory-static-images.s3.us-east-2.amazonaws.com/EcommDemos/Demo-12.png"
    // },
    // {
    //   title: `${t("ecommercePage.Demo")} 13`,
    //   url: "https://ecomdemo13.upappfactory.app/",
    //   img: "https://upappfactory-static-images.s3.us-east-2.amazonaws.com/EcommDemos/Demo-13.png"
    // },
    // {
    //   title: `${t("ecommercePage.Demo")} 14`,
    //   url: "https://ecomdemo14.upappfactory.app/",
    //   img: "https://upappfactory-static-images.s3.us-east-2.amazonaws.com/EcommDemos/Demo-14.png"
    // },
    // {
    //   title: `${t("ecommercePage.Demo")} 15`,
    //   url: "https://ecomdemo15.upappfactory.app/",
    //   img: "https://upappfactory-static-images.s3.us-east-2.amazonaws.com/EcommDemos/Demo-15.png"
    // },
    // {
    //   title: `${t("ecommercePage.Demo")} 16`,
    //   url: "https://ecomdemo16.upappfactory.app/",
    //   img: "https://upappfactory-static-images.s3.us-east-2.amazonaws.com/EcommDemos/Demo-16.png"
    // },
    {
      title: `${t("ecommercePage.Demo")} 3`,
      url: "https://ecomdemo17.upappfactory.app/",
      img: "https://upappfactory-static-images.s3.us-east-2.amazonaws.com/EcommDemos/Demo-17.png"
    },
    // {
    //   title: `${t("ecommercePage.Demo")} 18`,
    //   url: "https://ecomdemo18.upappfactory.app/",
    //   img: "https://upappfactory-static-images.s3.us-east-2.amazonaws.com/EcommDemos/Demo-18.png"
    // },
    // {
    //   title: `${t("ecommercePage.Demo")} 19`,
    //   url: "https://ecomdemo19.upappfactory.app/",
    //   img: "https://upappfactory-static-images.s3.us-east-2.amazonaws.com/EcommDemos/Demo-19.png"
    // },
    // {
    //   title: `${t("ecommercePage.Demo")} 20`,
    //   url: "https://ecomdemo20.upappfactory.app/",
    //   img: "https://upappfactory-static-images.s3.us-east-2.amazonaws.com/EcommDemos/Demo-20.png"
    // },
    {
      title: `${t("ecommercePage.Demo")} 4`,
      url: "https://ecomdemo21.upappfactory.app/",
      img: "https://upappfactory-static-images.s3.us-east-2.amazonaws.com/EcommDemos/Demo-21.png"
    },
    // {
    //   title: `${t("ecommercePage.Demo")} 22`,
    //   url: "https://ecomdemo22.upappfactory.app/",
    //   img: "https://upappfactory-static-images.s3.us-east-2.amazonaws.com/EcommDemos/Demo-22.png"
    // },
    {
      title: `${t("ecommercePage.Demo")} 5`,
      url: "https://ecomdemo23.upappfactory.app/",
      img: "https://upappfactory-static-images.s3.us-east-2.amazonaws.com/EcommDemos/Demo-23.png"
    },
    // {
    //   title: `${t("ecommercePage.Demo")} 24`,
    //   url: "https://ecomdemo24.upappfactory.app/",
    //   img: "https://upappfactory-static-images.s3.us-east-2.amazonaws.com/EcommDemos/Demo-24.png"
    // },
    // {
    //   title: `${t("ecommercePage.Demo")} 26`,
    //   url: "https://ecomdemo26.upappfactory.app/",
    //   img: "https://upappfactory-static-images.s3.us-east-2.amazonaws.com/EcommDemos/Demo-26.png"
    // },
    // {
    //   title: `${t("ecommercePage.Demo")} 27`,
    //   url: "https://ecomdemo27.upappfactory.app/",
    //   img: "https://upappfactory-static-images.s3.us-east-2.amazonaws.com/EcommDemos/Demo-27.png"
    // },
    // {
    //   title: `${t("ecommercePage.Demo")} 28`,
    //   url: "https://ecomdemo28.upappfactory.app/",
    //   img: "https://upappfactory-static-images.s3.us-east-2.amazonaws.com/EcommDemos/Demo-28.png"
    // },
    // {
    //   title: `${t("ecommercePage.Demo")} 30`,
    //   url: "https://ecomdemo30.upappfactory.app/",
    //   img: "https://upappfactory-static-images.s3.us-east-2.amazonaws.com/EcommDemos/Demo-30.png"
    // },
    // {
    //   title: `${t("ecommercePage.Demo")} 31`,
    //   url: "https://ecomdemo31.upappfactory.app/",
    //   img: "https://upappfactory-static-images.s3.us-east-2.amazonaws.com/EcommDemos/Demo-31.png"
    // },
    // {
    //   title: `${t("ecommercePage.Demo")} 32`,
    //   url: "https://ecomdemo32.upappfactory.app/",
    //   img: "https://upappfactory-static-images.s3.us-east-2.amazonaws.com/EcommDemos/Demo-32.png"
    // },
    // {
    //   title: `${t("ecommercePage.Demo")} 33`,
    //   url: "https://ecomdemo33.upappfactory.app/",
    //   img: "https://upappfactory-static-images.s3.us-east-2.amazonaws.com/EcommDemos/Demo-33.png"
    // }
  ];


  const dummyData = [
    {
      title: `${t("ecommercePage.Demo")} 7`,
      url: "https://ecomdemo7.upappfactory.app/",
      img: "https://upappfactory-static-images.s3.us-east-2.amazonaws.com/EcommDemos/Demo-7.png"
    },
    {
      title: `${t("ecommercePage.Demo")} 8`,
      url: "https://ecomdemo8.upappfactory.app/",
      img: "https://upappfactory-static-images.s3.us-east-2.amazonaws.com/EcommDemos/Demo-8.png"
    },
    {
      title: `${t("ecommercePage.Demo")} 9`,
      url: "https://ecomdemo9.upappfactory.app/",
      img: "https://upappfactory-static-images.s3.us-east-2.amazonaws.com/EcommDemos/Demo-9.png"
    },
    {
      title: `${t("ecommercePage.Demo")} 10`,
      url: "https://ecomdemo10.upappfactory.app/",
      img: "https://upappfactory-static-images.s3.us-east-2.amazonaws.com/EcommDemos/Demo-10.png"
    },
    {
      title: `${t("ecommercePage.Demo")} 11`,
      url: "https://ecomdemo11.upappfactory.app/",
      img: "https://upappfactory-static-images.s3.us-east-2.amazonaws.com/EcommDemos/Demo-11.png"
    },
    {
      title: `${t("ecommercePage.Demo")} 12`,
      url: "https://ecomdemo12.upappfactory.app/",
      img: "https://upappfactory-static-images.s3.us-east-2.amazonaws.com/EcommDemos/Demo-12.png"
    },
    {
      title: `${t("ecommercePage.Demo")} 13`,
      url: "https://ecomdemo13.upappfactory.app/",
      img: "https://upappfactory-static-images.s3.us-east-2.amazonaws.com/EcommDemos/Demo-13.png"
    },
    {
      title: `${t("ecommercePage.Demo")} 14`,
      url: "https://ecomdemo14.upappfactory.app/",
      img: "https://upappfactory-static-images.s3.us-east-2.amazonaws.com/EcommDemos/Demo-14.png"
    },
    {
      title: `${t("ecommercePage.Demo")} 15`,
      url: "https://ecomdemo15.upappfactory.app/",
      img: "https://upappfactory-static-images.s3.us-east-2.amazonaws.com/EcommDemos/Demo-15.png"
    },
    {
      title: `${t("ecommercePage.Demo")} 16`,
      url: "https://ecomdemo16.upappfactory.app/",
      img: "https://upappfactory-static-images.s3.us-east-2.amazonaws.com/EcommDemos/Demo-16.png"
    },
    {
      title: `${t("ecommercePage.Demo")} 17`,
      url: "https://ecomdemo17.upappfactory.app/",
      img: "https://upappfactory-static-images.s3.us-east-2.amazonaws.com/EcommDemos/Demo-17.png"
    },
    {
      title: `${t("ecommercePage.Demo")} 18`,
      url: "https://ecomdemo18.upappfactory.app/",
      img: "https://upappfactory-static-images.s3.us-east-2.amazonaws.com/EcommDemos/Demo-18.png"
    },
    {
      title: `${t("ecommercePage.Demo")} 19`,
      url: "https://ecomdemo19.upappfactory.app/",
      img: "https://upappfactory-static-images.s3.us-east-2.amazonaws.com/EcommDemos/Demo-19.png"
    },
    {
      title: `${t("ecommercePage.Demo")} 20`,
      url: "https://ecomdemo20.upappfactory.app/",
      img: "https://upappfactory-static-images.s3.us-east-2.amazonaws.com/EcommDemos/Demo-20.png"
    },
    {
      title: `${t("ecommercePage.Demo")} 21`,
      url: "https://ecomdemo21.upappfactory.app/",
      img: "https://upappfactory-static-images.s3.us-east-2.amazonaws.com/EcommDemos/Demo-21.png"
    },
    {
      title: `${t("ecommercePage.Demo")} 22`,
      url: "https://ecomdemo22.upappfactory.app/",
      img: "https://upappfactory-static-images.s3.us-east-2.amazonaws.com/EcommDemos/Demo-22.png"
    },
    {
      title: `${t("ecommercePage.Demo")} 23`,
      url: "https://ecomdemo23.upappfactory.app/",
      img: "https://upappfactory-static-images.s3.us-east-2.amazonaws.com/EcommDemos/Demo-23.png"
    },
    // {
    //   title: `${t("ecommercePage.Demo")} 24`,
    //   url: "https://ecomdemo24.upappfactory.app/",
    //   img: "https://upappfactory-static-images.s3.us-east-2.amazonaws.com/EcommDemos/Demo-24.png"
    // },
    // {
    //   title: `${t("ecommercePage.Demo")} 26`,
    //   url: "https://ecomdemo26.upappfactory.app/",
    //   img: "https://upappfactory-static-images.s3.us-east-2.amazonaws.com/EcommDemos/Demo-26.png"
    // },
    // {
    //   title: `${t("ecommercePage.Demo")} 27`,
    //   url: "https://ecomdemo27.upappfactory.app/",
    //   img: "https://upappfactory-static-images.s3.us-east-2.amazonaws.com/EcommDemos/Demo-27.png"
    // },
    // {
    //   title: `${t("ecommercePage.Demo")} 28`,
    //   url: "https://ecomdemo28.upappfactory.app/",
    //   img: "https://upappfactory-static-images.s3.us-east-2.amazonaws.com/EcommDemos/Demo-28.png"
    // },
    // {
    //   title: `${t("ecommercePage.Demo")} 30`,
    //   url: "https://ecomdemo30.upappfactory.app/",
    //   img: "https://upappfactory-static-images.s3.us-east-2.amazonaws.com/EcommDemos/Demo-30.png"
    // },
    // {
    //   title: `${t("ecommercePage.Demo")} 31`,
    //   url: "https://ecomdemo31.upappfactory.app/",
    //   img: "https://upappfactory-static-images.s3.us-east-2.amazonaws.com/EcommDemos/Demo-31.png"
    // },
    // {
    //   title: `${t("ecommercePage.Demo")} 32`,
    //   url: "https://ecomdemo32.upappfactory.app/",
    //   img: "https://upappfactory-static-images.s3.us-east-2.amazonaws.com/EcommDemos/Demo-32.png"
    // },
    // {
    //   title: `${t("ecommercePage.Demo")} 33`,
    //   url: "https://ecomdemo33.upappfactory.app/",
    //   img: "https://upappfactory-static-images.s3.us-east-2.amazonaws.com/EcommDemos/Demo-33.png"
    // }
  ];
  const getData = () => {
    let count = 3;
    while (count > 0) {
      if (dummyData.length === 0) {
        return console.log("No Data To loop Through!!");
      }
      console.log(dummyData.length);
      const t = dummyData.shift();
      Newdata.push(t);
      console.log(t);
      console.log(dummyData.length);
      console.log(Newdata);
      count = count - 1;
    }
  };

  const onLoadMore = () => {
    setState({
      loading: true,
      list: this.state.data.concat(
        [...new Array(count)].map(() => ({ loading: true, title: {} }))
      )
    });

    getData(res => {
      const data = this.state.data.concat();
      setState({
        data,
        list: data,
        loading: false
      });
    });

  };

  
  return (
    <div style={{ maxWidth: "1760px", margin: "0 auto" }}>
<Slider {...settings}>
          {Newdata &&
            Newdata.map((item, i) => {
              return (
                <div key={i} className="Website-Theme-Slider">
                    <Card
              hoverable
              className="store-card theme-card Website-Theme-Slide-Card"
              cover={
                <a href={item.url} target="_blank">
                  <img className="Website-Theme-Slider-Img" alt="example" src={item.img} />
                </a>
              }
            >
              <Meta title={item.title} />
            </Card>
                </div>
              );
            })}
        </Slider>
      
    </div>
  );
}

const mapStateToProps = state => ({

});

export default connect(mapStateToProps, {})(WebsiteThemes);
