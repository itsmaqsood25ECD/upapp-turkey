import React, { useEffect, useState, Fragment } from "react";
import PropTypes from "prop-types";
import { CountryDropdown } from "react-country-region-selector";
import PhoneInput from "react-phone-number-input";
import "react-phone-number-input/style.css";
import SmartInput from "react-phone-number-input/smart-input";
import { Redirect } from "react-router-dom";
import { connect } from "react-redux";
import moment from "moment";
import ReactGA from "react-ga";
import * as Ionicons from "react-icons/io";
import demoRequestBG from "../../assets/img/demoRequest.svg";
import SrollTop from "../ScrollTop";
import { Helmet } from "react-helmet";
import ReCAPTCHA from "react-google-recaptcha";
import { setAlert } from "../../actions/alert.action.jsx";

import {
  Icon,
  Button,
  Typography,
  Modal,
  Input,
  Row,
  Col,
  Form,
  Select,
  DatePicker,
  Menu
} from "antd";
import { requestDemo } from "../../actions/contact.action";
import { loadApplication } from "../../actions/application.action";

const { Text } = Typography;
const { Option, OptGroup } = Select;
const dateFormatList = ["DD/MM/YYYY", "DD/MM/YY"];
const recaptchaRef = React.createRef();

const DemoRequests = ({
  requestDemo,
  form: { getFieldDecorator, setFieldsValue, validateFieldsAndScroll },
  Applications,
  setAlert
}) => {
  const [appDetails, setAppData] = useState({
    apps: [],
    selectApplication: "",
    HumanVarification: false,
    isSubmitted: false
  });

  const { HumanVarification, isSubmitted } = appDetails;

  useEffect(() => {
    if (Applications) {
      setAppData({
        ...appDetails,
        apps:
          Applications &&
          Applications.map(arg => {
            return arg;
          })
      });
    }
  }, [Applications]);
  const { apps } = appDetails;

  const handleSelect = value => {
    setFormData({ ...formData, interested_in: value });
  };

  const handleSelectCsize = value => {
    setFormData({ ...formData, csize: value });
  };

  const ecommApps =
    apps &&
    apps.filter(myapp => {
      return myapp.fields && myapp.fields.type.toLowerCase() === "ecommerce";
    });

  const bookingApps =
    apps &&
    apps.filter(myapp => {
      return myapp.fields && myapp.fields.type.toLowerCase() === "booking";
    });

  //  Request For Demo

  const selectCountry = val => {
    setFormData({
      ...formData,
      country: val
    });
  };

  const [modal, setModal] = useState({
    visible: false
  });

  const [formData, setFormData] = useState({
    type: "demo"
  });
  const demoRequest = () => {
    showModal();
  };

  const showModal = () => {
    setModal({
      visible: true
    });
  };
  window.dataLayer = window.dataLayer || [];
  function gtag() {
    window.dataLayer.push(arguments);
  }
  gtag("js", new Date());
  gtag("config", "UA-146764719-1");
  gtag("config", "AW-697783018");
  function gtag_report_conversion(url) {
    var callback = function () {
      if (typeof url != "undefined") {
        window.location = url;
      }
    };
    gtag("event", "conversion", {
      send_to: "AW-697783018/HQSvCLzz_LUBEOql3cwC",
      event_callback: callback
    });
    return false;
  }

  const handleOk = e => {
    validateFieldsAndScroll((err, values) => {
      if (!HumanVarification) {
        console.log("You are BOT");
        setAlert("Please Verify reCAPTCHA", "danger");
      } else if (!err) {
        // setModal({
        // 	visible: false
        // });
        console.log(formData);
        ReactGA.event({
          category: "Contact",
          label: "Request_Demo",
          action: "Request Demo Submit",
          value: 10
        });
        requestDemo(formData);
        setAppData({
          ...formData,
          isSubmitted: true
        });
        return gtag_report_conversion(
          "https://upappfactory.com/demo-requests/success"
        );

        // Modal.success({
        // 	title: 'Demo Request Successful',
        // 	content:
        // 		'Your request to have a demo with UP app factory’s demo team is now received and our pre-sales consultant will be in touch with you soon.'
        // });
      }
    });
  };

  const onCapchaChange = e => {
    console.log(e);
    setAppData({
      ...appDetails,
      HumanVarification: true
    });
  };

  const onExpired = () => {
    console.log("expired");
    setAppData({
      ...appDetails,
      HumanVarification: false
    });
  };

  const handleCancel = e => {
    setModal({
      visible: false
    });
  };

  const onPhoneChange = e => {
    setFormData({
      ...formData,
      mobile: e
    });
  };
  const onChange = e =>
    setFormData({ ...formData, [e.target.name]: e.target.value });

  const onDateChange = e => {
    setFormData({ ...formData, prefered_date: e._d });
  };

  const disabledDate = current => {
    // Can not select days before today and today
    return current && current < moment().endOf("day");
  };

  if (isSubmitted) {
    return <Redirect to="/demo-requests/success"></Redirect>;
  }

  // Request for Demo

  return (
    <Fragment>
      <SrollTop />
      <Helmet></Helmet>
      <div
        style={{
          marginTop: "80px",
          minHeight: "580px",
          height: "100%",
          background:
            "linear-gradient(to right, rgb(33, 147, 176), rgb(109, 213, 237))"
        }}
      >
        <Row>
          <Col lg={16} xs={24} style={{ paddingTop: "60px" }}>
            <img className="demoRequestBG" src={demoRequestBG} alt="" />
          </Col>
          <Col lg={8} xs={24}>
            <Form
              // style={{
              // marginTop: '80px',
              // boxShadow: '0px 0px 10px #ccc',
              // padding: '40px 20px',
              // marginLeft: '30px',
              // marginRight: '30px',
              // background: '#fff'
              // }}

              className="demo-request-form-single"
            >
              <Row gutter={16}>
                <Col xs={24} lg={24}>
                  <Form.Item>
                    {getFieldDecorator("name", {
                      rules: [
                        {
                          required: true,
                          message: "Please type your name"
                        }
                      ]
                    })(
                      <Input
                        placeholder="Name"
                        name="name"
                        onChange={e => onChange(e)}
                        prefix={
                          <Icon
                            type="user"
                            style={{ color: "rgba(0,0,0,.25)" }}
                          />
                        }
                      />
                    )}
                  </Form.Item>
                </Col>
                <Col xs={24} lg={24}>
                  <Form.Item>
                    {getFieldDecorator("email", {
                      rules: [
                        {
                          required: true,
                          message: "Please type your email"
                        }
                      ]
                    })(
                      <Input
                        placeholder="Email"
                        name="email"
                        onChange={e => onChange(e)}
                        prefix={
                          <Icon
                            type="mail"
                            style={{ color: "rgba(0,0,0,.25)" }}
                          />
                        }
                      />
                    )}
                  </Form.Item>
                </Col>
                <Col xs={24} lg={24}>
                  <Form.Item>
                    {getFieldDecorator("country", {
                      rules: [
                        {
                          required: true,
                          message: "Please type your country"
                        }
                      ]
                    })(
                      <CountryDropdown
                        blacklist={[
                          "AF",
                          "AO",
                          "DJ",
                          "GQ",
                          "ER",
                          "GA",
                          "IR",
                          "KG",
                          "LY",
                          "MD",
                          "NP",
                          "ST",
                          "SL",
                          "SD",
                          "SY",
                          "SR",
                          "TM",
                          "VE",
                          "ZW",
                          "IL"
                        ]}
                        name="country"
                        valueType="full"
                        style={{ marginTop: "12px" }}
                        onChange={val => selectCountry(val)}
                      />
                    )}
                  </Form.Item>
                </Col>
                <Col xs={24} lg={24}>
                  <Form.Item>
                    {getFieldDecorator("mobile", {
                      rules: [
                        {
                          required: true,
                          message: "Please input your mobile number!"
                        }
                      ]
                    })(
                      <PhoneInput
                        style={{ marginTop: "10px" }}
                        name="mobile"
                        inputComponent={SmartInput}
                        placeholder="Enter phone number"
                        onChange={e => onPhoneChange(e)}
                      />
                    )}
                  </Form.Item>
                </Col>
                <Col xs={24} lg={24}>
                  <Form.Item hasFeedback>
                    {getFieldDecorator("interested_in", {
                      rules: [
                        {
                          required: true,
                          message:
                            "Please select the app you are interested in "
                        }
                      ]
                    })(
                      <Select
                        placeholder="Choose the Category you are intrested in"
                        style={{ marginTop: "10px" }}
                        onChange={handleSelect}
                      >
                        <Option key="ecommerce" value="eCommerce App">
                          eCommerce Application
                        </Option>
                        <Option key="booking" value="Booking App">
                          Booking Application
                        </Option>
                      </Select>
                    )}
                  </Form.Item>
                </Col>
                <Col xs={24} lg={24}>
                  <Form.Item>
                    <ReCAPTCHA
                      ref={recaptchaRef}
                      onExpired={onExpired}
                      sitekey="6LcG18EUAAAAAGweYgM2TTt89iBNGtyYlsP_rfKd"
                      onChange={e => onCapchaChange(e)}
                    />
                  </Form.Item>
                </Col>
                {/* <Col xs={24}>
            <div style={{ marginBottom: '10px' }}>Prefered Date</div>
            <Form.Item>
            {getFieldDecorator('prefered_date', {
            rules: [
            {
            required: true,
            message: 'Please choose your prefered date for demo'
            }
            ]
            })(
            <DatePicker
            showTime
            onChange={e => onDateChange(e)}
            format={dateFormatList}
            disabledDate={disabledDate}
            name="prefered_date"
            style={{ width: '100%' }}
            />
            )}
            </Form.Item>
            </Col>*/}
              </Row>

              <div style={{ textAlign: "center" }}>
                <Button className="UAFprimaryButton1" type="primary" onClick={handleOk} >Submit</Button>
              </div>
            </Form>
          </Col>
        </Row>
      </div>
    </Fragment>
  );
};

const RFD = Form.create({ name: "Demo_Request" })(DemoRequests);
RFD.propTypes = {
  requestDemo: PropTypes.func.isRequired,
  loadApplication: PropTypes.func.isRequired,
  Applications: PropTypes.array.isRequired,
  loading: PropTypes.bool
};

const mapStateToProps = state => ({
  Applications: state.application.Applications,
  loading: state.application.loading
});

const mapDispatchToProps = {};

export default connect(mapStateToProps, {
  requestDemo,
  loadApplication,
  setAlert
})(RFD);
