/* eslint react/prop-types: 0 */
import React, { useState, useEffect, Fragment } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { Icon, Card, Button, Typography, Row, Col, List, Spin, Input, Form, Anchor } from "antd";
import CurrencyFormat from "react-currency-format";
import BannerAnim from "rc-banner-anim";
import { Element } from "rc-banner-anim";
import "rc-banner-anim/assets/index.css";
import ScrollTop from "../ScrollTop";
import { NavLink } from "react-router-dom";
import Clientele from "./Clientele";
import "antd/dist/antd.css";
import "../../assets/css/custom.css";
import "../../assets/css/home.css";
import "../../assets/css/homeNew.css";
import "react-animated-slider/build/horizontal.css";
import { getRecentBlog } from "../../actions/admin/blog.management.action";
import GetYourOwn from "../../assets/img/Home Page images/GetYourOwn.svg";
import Support247 from "../../assets/img/Home Page images/24-7.svg";
import DeliverIcon from "../../assets/img/Home Page images/calendar.svg";
import BudgetFriendlyPackagesIcon from "../../assets/img/Home Page images/BudgetFriendlyPackagesIcon.svg";
import OneStopeCommerceShop from "../../assets/img/Home Page images/OneStopeCommerceShop.svg";
import OnlineBookingSystem from "../../assets/img/Home Page images/OnlineBookingSystem.svg";
import GoToMarketIcon from "../../assets/img/Home Page images/GoToMarketIcon.svg";
import SaveMoneyAndTime from "../../assets/img/Home Page images/SaveMoneyAndTime.svg";
import ConvertShopersIntoBuyers from "../../assets/img/Home Page images/ConvertShopersIntoBuyers.svg";
import IncreaseRevenue from "../../assets/img/Home Page images/IncreaseRevenue.svg";
import eCommerce from "../../assets/img/Home Page images/eCommerce.svg"
import singlevendor from "../../assets/img/Home Page images/singlevendor.svg"
import multivendor from "../../assets/img/Home Page images/multivendor.svg"
import restaurant from "../../assets/img/Home Page images/restaurant.svg"
// end Home

// Banners
import StartyourEcommerceStorein7Days from "../../assets/img/Home Page images/Home Page Banners/StartyourEcommerceStorein7Days.svg";
import StartYourAppointmentManagementAppIn7Days from "../../assets/img/Home Page images/Home Page Banners/StartYourAppointmentManagementAppIn7Days.svg";
import eCommerceWebsite from "../../assets/img/Home Page images/Home Page Banners/eCommerceWebsite.svg";
import eCommerceMobileApp from "../../assets/img/Home Page images/Home Page Banners/eCommerceMobileApp.svg";
import eCommerceWebApp from "../../assets/img/Home Page images/Home Page Banners/eCommerceWebApp.svg";

import ClienteleSection from './ClienteleSection'

// end Banners

// Process Flow

import Consultation from "../../assets/img/Home Page images/process flow/Consultation.svg";
import PlanningProcess from "../../assets/img/Home Page images/process flow/PlanningProcess.svg";
import Training from "../../assets/img/Home Page images/process flow/Training.svg";
import Implementation from "../../assets/img/Home Page images/process flow/Implementation.svg";
import Launch from "../../assets/img/Home Page images/process flow/Launch.svg";
import Support from "../../assets/img/Home Page images/process flow/Support.svg";
// Process Flow End

//features
import PaymentGatewayIntegrationIcon from "../../assets/img/Icons/UpAppSpecial/PaymentGatewayIntegration.svg";
import ServerIcon from "../../assets/img/Icons/UpAppSpecial/server.svg";
import EnglishArabicIcon from "../../assets/img/Icons/UpAppSpecial/EnglishandArabic.svg";
import contentMangementIcon from "../../assets/img/Icons/UpAppSpecial/contentMangement.svg";
import SPMarketingIcon from "../../assets/img/Icons/UpAppSpecial/SPmarketing.svg";
import AppAndplaystoreIcon from "../../assets/img/Icons/UpAppSpecial/AppAndplaystore.svg";
import TrainingIcon from "../../assets/img/Icons/UpAppSpecial/Training.svg";
import SupportIcon from "../../assets/img/Icons/UpAppSpecial/support.svg";
import TechnicalUpgradesIcon from "../../assets/img/Icons/UpAppSpecial/TechnicalUpgrades.svg";
import AnalyticsToolIcon from "../../assets/img/Icons/UpAppSpecial/AnalyticsTool.svg";
import SocialMediaIntegrationsIcon from "../../assets/img/Icons/UpAppSpecial/SocialMediaIntegration.svg";
import SSLIcon from "../../assets/img/Icons/UpAppSpecial/ssl.svg";
import CDNIcon from "../../assets/img/Icons/UpAppSpecial/cdn.svg";
import MaliciousAppScanningIcon from "../../assets/img/Icons/UpAppSpecial/MaliciousAppScanning.svg";
import UnlimitedTransactionsIcon from "../../assets/img/Icons/UpAppSpecial/unlimitedTransaction.svg";
import FacilityManagementIcon from "../../assets/img/Home Page images/Facility_Management_icon.svg";

// end features

import HomePricing from './HomePricing';
import PackagePriceVerticleEcommerce from "../Pages/PackagePriceVerticleEcommerce";
import { store } from "../../store/store";
import { requestForCustomisedApp } from "../../actions/contact.action";
import { loadApplication } from "../../actions/application.action";
import { setAlert } from "../../actions/alert.action.jsx";
import { Helmet } from "react-helmet";
import moment from 'moment';

import Slider from "react-slick";
import "slick-carousel/slick/slick.css";
import ContactUsForm from "../Pages/ContactUsForm";
import IndustriesWeServe from "./IndustriesWeServe";
import withDirection, { withDirectionPropTypes, DIRECTIONS } from 'react-with-direction';
import { useTranslation } from 'react-i18next';
import i18next from 'i18next';

const { Meta } = Card;
const BgElement = Element.BgElement;
const { TextArea } = Input;
const { Title, Text } = Typography;
let filteredApps;
const recaptchaRef = React.createRef();

const { Link } = Anchor;

const HomeNew = ({
  loadApplication,
  Applications,
  loading,
  requestForCustomisedApp,
  setAlert,
  form: { getFieldDecorator, validateFields, setFieldsValue },
  direction,
  getRecentBlog,
  recentBlog,

}) => {

  const { t } = useTranslation();


  const [appDetails, setAppData] = useState({
    apps: [],
    selectApplication: "",
    currency: "",
    // Booking
    bookingMobileBDSPM: "",
    bookingMobileADSPM: "",
    bookingMobileBDSPY: "",
    bookingMobileADSPY: "",
    bookingWebsiteBDSPM: "",
    bookingWebsiteADSPM: "",
    bookingWebsiteBDSPY: "",
    bookingWebsiteADSPY: "",
    bookingWebAppBDSPM: "",
    bookingWebAppADSPM: "",
    bookingWebAppBDSPY: "",
    bookingWebAppADSPY: "",
    // EcommcercenewBDSPM: '',
    newADSPM: "",
    newBDSPY: "",
    newADSPY: "",
    newMobileBDSPM: "",
    newMobileADSPM: "",
    newMobileBDSPY: "",
    newMobileADSPY: "",
    newWebsiteBDSPM: "",
    newWebsiteADSPM: "",
    newWebsiteBDSPY: "",
    newWebsiteADSPY: "",
    newWebAppBDSPM: "",
    newWebAppADSPM: "",
    newWebAppBDSPY: "",
    newWebAppADSPY: "",
    country: "",

    // Plan B
    WFPP: "",
    WAFPP: "",
    MFPP: "",
    // setup charge
    WSC: "",
    MSC: "",
    WMSC: ""
  });

  const {
    apps,
    selectApplication,
    country,
    currency,
  } = appDetails;

  let ecommApps, bookingApps;

  // store.dispatch(loadUser());
  // store.dispatch(loadApplication());
  // store.dispatch(getLocation());

  useEffect(() => {
    getRecentBlog()

    console.log('rerun', sessionStorage.getItem("currency_rate"))
    setAppData({
      ...appDetails,
      country: sessionStorage.getItem("country"),
      currency: sessionStorage.getItem("currency_sign"),
      // Website
      newWebsiteBDSPM: Math.round(
        29.9 * sessionStorage.getItem("currency_rate")
      ),
      newWebsiteADSPM: Math.round(
        19.9 * sessionStorage.getItem("currency_rate")
      ),
      newWebsiteBDSPY: Math.round(
        29.9 * sessionStorage.getItem("currency_rate")
      ),
      newWebsiteADSPY: Math.round(
        9.9 * sessionStorage.getItem("currency_rate")
      ),
      // Web App
      newWebAppBDSPM: Math.round(
        49.9 * sessionStorage.getItem("currency_rate")
      ),
      newWebAppADSPM: Math.round(
        39.9 * sessionStorage.getItem("currency_rate")
      ),
      newWebAppBDSPY: Math.round(
        49.9 * sessionStorage.getItem("currency_rate")
      ),
      newWebAppADSPY: Math.round(
        29.9 * sessionStorage.getItem("currency_rate")
      ),
      // Mobile
      newMobileBDSPM: Math.round(
        39.9 * sessionStorage.getItem("currency_rate")
      ),
      newMobileADSPM: Math.round(
        29.9 * sessionStorage.getItem("currency_rate")
      ),
      newMobileBDSPY: Math.round(
        39.9 * sessionStorage.getItem("currency_rate")
      ),
      newMobileADSPY: Math.round(
        19.9 * sessionStorage.getItem("currency_rate")
      ),
      //  Plan B

      WFPP: Math.round(59.9 * sessionStorage.getItem("currency_rate")),
      MFPP: Math.round(89.9 * sessionStorage.getItem("currency_rate")),
      WAFPP: Math.round(119.9 * sessionStorage.getItem("currency_rate")),
      // setup charge
      WSC: Math.round(500 * sessionStorage.getItem("currency_rate")),
      WMSC: Math.round(1200 * sessionStorage.getItem("currency_rate")),
      MSC: Math.round(800 * sessionStorage.getItem("currency_rate")),

      // Restaurant App

      // Mobile
      Res_newMobileBDSPM: Math.round(
        9.9 * sessionStorage.getItem("currency_rate")
      ),
      Res_newMobileADSPM: Math.round(
        9.9 * sessionStorage.getItem("currency_rate")
      ),
      Res_newMobileBDSPY: Math.round(
        9.9 * sessionStorage.getItem("currency_rate")
      ),
      Res_newMobileADSPY: Math.round(
        9.9 * sessionStorage.getItem("currency_rate")
      ),
      Res_MSC: Math.round(49.9 * sessionStorage.getItem("currency_rate")),
      //  Plan B
      Res_MFPP: Math.round(49.9 * sessionStorage.getItem("currency_rate"))
    });

  }, [sessionStorage.getItem("currency")]);

  function NextArrow(props) {
    const { onClick } = props;
    return (
      <Button
        type="primary"
        onClick={onClick}
        className="ss-next-btn"
        shape="circle"
      >
        <Icon type="arrow-right" />
      </Button>
    );
  }

  function PrevArrow(props) {
    const { onClick } = props;
    return (
      <Button
        type="primary"
        onClick={onClick}
        className="ss-prev-btn"
        shape="circle"
      >
        <Icon type="arrow-left" />
      </Button>
    );
  }

  var settings = {
    prevArrow: <PrevArrow />,
    nextArrow: <NextArrow />,
    dots: false,
    // infinite: true,
    centerMode: true,
    speed: 200,
    swipeToSlide: true,
    slidesToShow: 3,
    slidesToScroll: 1,
    autoplay: true,
    autoplaySpeed: 2000,
    lazyLoad: true,
    initialSlide: 0,

    responsive: [
      {
        breakpoint: 1024,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 1,
          // infinite: true,
          dots: false
        }
      },
      {
        breakpoint: 600,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 1,
          initialSlide: 2
        }
      },
      {
        breakpoint: 480,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1
        }
      }
    ]
  };

  ecommApps =
    Applications &&
    Applications.filter(myapp => {
      return myapp.fields && myapp.fields.type.toLowerCase() === "ecommerce";
    });

  bookingApps =
    Applications &&
    Applications.filter(myapp => {
      return myapp.fields && myapp.fields.type.toLowerCase() === "booking";
    });

  if (selectApplication === "" || selectApplication === undefined) {
    filteredApps = apps && apps[0];
  } else {
    let data =
      apps &&
      apps.filter(myapp => {
        return (
          (myapp.fields &&
            myapp.fields.name
              .toLowerCase()
              .includes(
                selectApplication && selectApplication.toLocaleLowerCase()
              )) ||
          (myapp.fields &&
            myapp.fields.type
              .toLowerCase()
              .includes(
                selectApplication && selectApplication.toLocaleLowerCase()
              ))
        );
      });
    filteredApps = data && data[0];
  }

  const upAppSpecialData = [
    {
      icon: PaymentGatewayIntegrationIcon,
      title: "Payment Gateway Integration",
      desc:
        "Your desired payment gateway can be integrated, details and integration files to be provided. Additional cost will be charged to setup the payment gateway"
    },
    {
      icon: ServerIcon,
      title: "Server",
      desc: "Your system and mobile apps will be hosted with us."
    },
    {
      icon: contentMangementIcon,
      title: "Content Management",
      desc: `initial content management is provided with the first 100 product's upload`
    },
    {
      icon: SPMarketingIcon,
      title: "Sales, Promotions & Marketing",
      desc:
        "Two 1 hour training sessions are provided to assist you with the initial promotions, offers, and marketing activities set up"
    },
    {
      icon: AppAndplaystoreIcon,
      title: "App Store Fee & Space",
      desc: "No additional costs are necessary for iOS & Google Play store"
    },
    {
      icon: TrainingIcon,
      title: "Training",
      desc: "Upto 5 training sessions can be provided to your team"
    },
    {
      icon: SupportIcon,
      title: "Support",
      // desc: "Free Online sessions, chats and calls for all customer support queries"
      desc: "Email Support"
    },
    {
      icon: TechnicalUpgradesIcon,
      title: "Technical Upgrades",
      desc: "Automatic app system upgrade"
    },
    {
      icon: AnalyticsToolIcon,
      title: "Analytics Tool",
      desc: "Free app analytics tools to all customers"
    },
    {
      icon: SocialMediaIntegrationsIcon,
      title: "Social Media Integrations",
      desc: "Connect your social media accounts to your mobile app"
    },
    {
      icon: SSLIcon,
      title: "SSL",
      desc: "Security enabled on the domain of your system"
    },
    {
      icon: CDNIcon,
      title: "CDN",
      desc: "For the best speed of your app, CDN enabled"
    },
    {
      icon: MaliciousAppScanningIcon,
      title: "Malicious App Scanning",
      desc: "Free regular malicious app scanning for all customers"
    },
    {
      icon: UnlimitedTransactionsIcon,
      title: "Unlimited Transactions",
      desc: "Unlimited transactions from your app and system by your customers"
    }
  ];

  const processFlow = [
    {
      no: "1",
      icon: Consultation,
      title: `${t("Home.layer9.p1.title")}`,
      desc: `${t("Home.layer9.p1.text")}`
    },
    {
      no: "2",
      icon: PlanningProcess,
      title: `${t("Home.layer9.p2.title")}`,
      desc: `${t("Home.layer9.p2.text")}`
    },
    {
      no: "3",
      icon: Training,
      title: `${t("Home.layer9.p3.title")}`,
      desc: `${t("Home.layer9.p3.text")}`
    },
    {
      no: "4",
      icon: Implementation,
      title: `${t("Home.layer9.p4.title")}`,
      desc: `${t("Home.layer9.p4.text")}`
    },
    {
      no: "5",
      icon: Launch,
      title: `${t("Home.layer9.p5.title")}`,
      desc: `${t("Home.layer9.p5.text")}`
    },
    {
      no: "6",
      icon: Support,
      title: `${t("Home.layer9.p6.title")}`,
      desc: `${t("Home.layer9.p6.text")}`
    }
  ];

  return Applications === null || loading ? (
    <Fragment>
      <Spin></Spin>
    </Fragment>
  ) : (
      <div>
        <ScrollTop />
        <Helmet>
          {/* <title>UPapp factory - Buy Ready Made Apps</title> */}
          <title>Buy Readymade Solutions | Website & App | UPapp factory</title>
          <meta
            name="Subscribe your ready made apps for your business. Start your e-Commerce business in a minute. Just choose the product which suits best to your work and you are all done."
            content="Buy Ready Made e-Commerce App Business Application Work Purchase"
          />
          <meta name="description" content="Ready to Launch Website and Applications for eCommerce, Restaurants and more." />
          <meta name="keywords" content="Ready to Launch Website, Readymade Apps for Business, eCommerce Website Development, Software Solutions, Create Online Store" />
        </Helmet>
        <BannerAnim prefixCls="banner-user" type="across">
          <Element prefixCls="banner-user-elem" key="0">
            <section
              className="UAF-container-fluid"
              style={{
                background: "#fff",
                padding: 0,
                maxWidth: "1200px",
                margin: "150px auto 80px auto"
              }}
            >
              <Row type="flex">
                <Col
                  sm={{ span: 24, order: 2 }}
                  md={{ span: 12, order: 1 }}
                  lg={{ span: 12, order: 1 }}
                  xs={{ span: 24, order: 2 }}
                >
                  {direction === DIRECTIONS.RTL && <Fragment>
                    <div
                      className="tagline-heading new-banner lg-rtl"
                      style={{ padding: "auto 15px" }}
                    >
                      <Title level={1} className="texty-demo lg-rtl">
                        {t('Home.Slider.1.Title.1')} <br /> {t('Home.Slider.1.Title.2')}
                        <br/>
                        <p className="special-slider-tag">
                        Single Vendor Ecommerce App
                        </p>
                      </Title>
                      <Anchor>
                        <Link
                          href="#select-plan-homepage"
                          title={
                            <Button size={"large"} className="slider-get-started-btn main lg-float-right uaf_allPageBtn_bg_Color">
                              {t('General.GetStarted')}
                            </Button>
                          }
                        ></Link>
                      </Anchor>
                    </div>

                  </Fragment>
                  }

                  {direction === DIRECTIONS.LTR && <Fragment>
                    <div
                      className="tagline-heading new-banner lg-ltr"
                      style={{ padding: "auto 15px" }}
                    >
                      <Title level={1} className="texty-demo lg-ltr">
                        {t('Home.Slider.1.Title.1')} <br /> {t('Home.Slider.1.Title.2')}
                        <br/>
                        <p className="special-slider-tag">
                        Single Vendor Ecommerce App
                        </p>
                    
                      </Title>
                      <Anchor>
                        <Link
                          href="#select-plan-homepage"
                          title={
                            <Button size={"large"} className="slider-get-started-btn main lg-float-left uaf_allPageBtn_bg_Color">
                              {t('General.GetStarted')}
                            </Button>
                          }
                        ></Link>
                      </Anchor>
                    </div>

                  </Fragment>
                  }

                </Col>
                <Col
                  sm={{ span: 24, order: 1 }}
                  md={{ span: 12, order: 2 }}
                  lg={{ span: 12, order: 2 }}
                  xs={{ span: 24, order: 1 }}
                >
                  <div className="home-feature-img main new-banner">
                    <img
                      style={{ maxWidth: "200px" }}
                      src={eCommerce}
                      alt="image with animation for upapp"
                    />
                  </div>
                </Col>
              </Row>
            </section>
          </Element>
{/*           
          <Element prefixCls="banner-user-elem" key="1">
            <section
              className="UAF-container-fluid"
              style={{
                background: "#fff",
                padding: 0,
                maxWidth: "1200px",
                margin: "150px auto 80px auto"
              }}
            >
              <Row type="flex" justify="space-around" align="middle">
                <Col
                  sm={{ span: 24, order: 2 }}
                  md={{ span: 12, order: 1 }}
                  lg={{ span: 10, order: 1 }}
                  xs={{ span: 24, order: 2 }}
                >
                  {direction === DIRECTIONS.LTR && <Fragment>
                    <div
                      className="tagline-heading new-banner"
                      style={{ padding: "auto 15px" }}
                    >
                      <Title level={1} className="texty-demo">
                        {t('Home.Slider.2.Title.1')} <br /> {t('Home.Slider.2.Title.2')}
                      </Title>
                      <NavLink to="/eCommerce-Single-Vendor-Platform">
                        <Button size={"large"} className="slider-get-started-btn uaf_allPageBtn_bg_Color">{t('General.GetStarted')}</Button>
                      </NavLink>
                    </div>

                  </Fragment>}
                  {direction === DIRECTIONS.RTL && <Fragment>
                    <div
                      className="tagline-heading new-banner lg-rtl"
                      style={{ padding: "auto 15px" }}
                    >
                      <Title level={1} className="texty-demo lg-rtl">
                        {t('Home.Slider.2.Title.1')} <br /> {t('Home.Slider.2.Title.2')}
                      </Title>
                      <NavLink to="/eCommerce-Single-Vendor-Platform">
                        <Button size={"large"} className="slider-get-started-btn main lg-float-right uaf_allPageBtn_bg_Color">
                          {t('General.GetStarted')}
                        </Button>
                      </NavLink>
                    </div>

                  </Fragment>}

                </Col>
                <Col
                  sm={{ span: 24, order: 1 }}
                  md={{ span: 12, order: 1 }}
                  lg={{ span: 14, order: 2 }}
                  xs={{ span: 24, order: 1 }}
                >
                  <div className="home-feature-img main new-banner">
                    <img
                      style={{
                        maxWidth: "700px",
                        top: "50px"
                      }}
                      src={singlevendor}
                      alt="image with animation for upapp"
                    />
                  </div>
                </Col>
              </Row>
            </section>
          </Element>
           */}
          <Element prefixCls="banner-user-elem" key="3">
            <section
              className="UAF-container-fluid"
              style={{
                background: "#fff",
                padding: 0,
                maxWidth: "1200px",
                margin: "150px auto 80px auto"
              }}
            >
              <Row type="flex" justify="space-around" align="middle">
                <Col
                  sm={{ span: 24, order: 2 }}
                  md={{ span: 12, order: 1 }}
                  lg={{ span: 14, order: 1 }}
                  xs={{ span: 24, order: 2 }}
                >
                  {direction === DIRECTIONS.LTR && <Fragment>
                    <div
                      className="tagline-heading new-banner"
                      style={{ padding: "auto 15px" }}
                    >
                      <Title level={1} className="texty-demo">
                        {t('Home.Slider.4.Title.1')}
                        <br /> {t('Home.Slider.4.Title.2')} <br /> {t('Home.Slider.4.Title.3')} <br></br>
                        <p className="special-slider-tag">
                        Multi Vendor Ecommerce App
                        </p>
                      </Title>
                      <NavLink to="/eCommerce-Multi-Vendor-Platform">
                        <Button size={"large"} className="slider-get-started-btn uaf_allPageBtn_bg_Color">{t('General.GetStarted')}</Button>
                      </NavLink>
                    </div>

                  </Fragment>
                  }
                  {direction === DIRECTIONS.RTL && <Fragment>
                    <div
                      className="tagline-heading new-banner lg-rtl"
                      style={{ padding: "auto 15px" }}
                    >
                      <Title level={1} className="texty-demo lg-rtl">
                        {t('Home.Slider.4.Title.1')}
                        <br/>
                        <p className="special-slider-tag">
                        Multi Vendor Ecommerce App
                        </p>

                      </Title>
                      <NavLink to="/eCommerce-Multi-Vendor-Platform">
                        <Button size={"large"} className="slider-get-started-btn main lg-float-right uaf_allPageBtn_bg_Color">
                          {t('General.GetStarted')}
                        </Button>
                      </NavLink>
                    </div>

                  </Fragment>
                  }

                </Col>
                <Col
                  sm={{ span: 24, order: 1 }}
                  md={{ span: 12, order: 1 }}
                  lg={{ span: 10, order: 2 }}
                  xs={{ span: 24, order: 1 }}
                >
                  <div className="home-feature-img main new-banner">
                    <img
                      style={{
                        maxWidth: "700px",
                        top: "50px"
                      }}
                      src={multivendor}
                      alt="image with animation for upapp"
                    />
                  </div>
                </Col>
              </Row>
            </section>
          </Element>
          <Element prefixCls="banner-user-elem" key="4">
            <section
              className="UAF-container-fluid"
              style={{
                background: "#fff",
                padding: 0,
                maxWidth: "1200px",
                margin: "150px auto 80px auto"
              }}
            >
              <Row type="flex" justify="space-around" align="middle">
                <Col
                  sm={{ span: 24, order: 2 }}
                  md={{ span: 12, order: 1 }}
                  lg={{ span: 14, order: 1 }}
                  xs={{ span: 24, order: 2 }}
                >
                  {direction === DIRECTIONS.LTR && <Fragment>
                    <div
                      className="tagline-heading new-banner"
                      style={{ padding: "auto 15px" }}
                    >
                      <Title level={1} className="texty-demo">
                        {t('Home.Slider.5.Title.1')}
                        <br /> {t('Home.Slider.5.Title.2')} <br /> {t('Home.Slider.5.Title.3')} 
                        <p className="special-slider-tag">
                        Online Restaurant App
                        </p>
                      </Title>
                      <NavLink to="/restaurant-application">
                        <Button size={"large"} className="slider-get-started-btn uaf_allPageBtn_bg_Color">{t('General.GetStarted')}</Button>
                      </NavLink>
                    </div>

                  </Fragment>
                  }
                  {direction === DIRECTIONS.RTL && <Fragment>
                    <div
                      className="tagline-heading new-banner lg-rtl"
                      style={{ padding: "auto 15px" }}
                    >
                      <Title level={1} className="texty-demo lg-rtl">
                        {t('Home.Slider.5.Title.1')}
                         <br></br>
                         <p className="special-slider-tag">
                        Online Restaurant App
                        </p>

                      </Title>
                      <NavLink to="/restaurant-application">
                        <Button size={"large"} className="slider-get-started-btn main lg-float-right uaf_allPageBtn_bg_Color">
                          {t('General.GetStarted')}
                        </Button>
                      </NavLink>
                    </div>

                  </Fragment>
                  }
                </Col>
                <Col
                  sm={{ span: 24, order: 1 }}
                  md={{ span: 12, order: 1 }}
                  lg={{ span: 10, order: 2 }}
                  xs={{ span: 24, order: 1 }}
                >
                  <div className="home-feature-img main new-banner">
                    <img
                      style={{
                        maxWidth: "700px",
                        top: "50px"
                      }}
                      src={restaurant}
                      alt="image with animation for upapp"
                    />
                  </div>
                </Col>
              </Row>
            </section>
          </Element>
        </BannerAnim>

        <section
          className="homepage-section-2"
          style={{
            margin: "80px 0px 0px 0px"
          }}
        >
          <div
            className="UAF-container-fluid"
            style={{
              padding: 0,
              maxWidth: "1200px",
              margin: "0 auto 0 auto"
            }}
          >
            {direction === DIRECTIONS.LTR && <Fragment>
              <Row gutter={16}>
                <Col xs={24} sm={8} lg={8}>
                  <Row>
                    <Col style={{ textAlign: "center" }} lg={8} xs={24}>
                      <img
                        style={{ width: "80px", height: "80px" }}
                        src={Support247}
                        alt="24/7 support"
                      />
                    </Col>
                    <Col lg={16} xs={24}>
                      <Title level={5} style={{ fontSize: "1.1rem" }}>
                        {t('Home.layer2.1.title')}
                      </Title>
                      <p>
                        {t('Home.layer2.1.subtitle')}
                      </p>
                    </Col>
                  </Row>
                </Col>
                <Col xs={24} sm={8} lg={8}>
                  <Row>
                    <Col style={{ textAlign: "center" }} lg={8} xs={24}>
                      <img
                        style={{ width: "80px", height: "80px" }}
                        src={DeliverIcon}
                        alt="Delivering 7-12 Business day"
                      />
                    </Col>
                    <Col lg={16} xs={24}>
                      <Title level={5} style={{ fontSize: "1.1rem" }}>
                        {t('Home.layer2.2.title')}
                      </Title>
                      <p>
                        {t('Home.layer2.2.subtitle')}</p>
                    </Col>
                  </Row>
                </Col>
                <Col xs={24} sm={8} lg={8}>
                  <Row>
                    <Col style={{ textAlign: "center" }} lg={8} xs={24}>
                      <img
                        style={{ width: "80px", height: "80px" }}
                        src={BudgetFriendlyPackagesIcon}
                        alt="Budget Friendly Packages"
                      />
                    </Col>
                    <Col lg={16} xs={24}>
                      <Title level={5} style={{ fontSize: "1.1rem" }}>
                        {t('Home.layer2.3.title')}
                      </Title>
                      <p>
                        {t('Home.layer2.3.subtitle')} {sessionStorage.getItem("currency_sign")} <CurrencyFormat value={(9.99 * sessionStorage.getItem("currency_rate")).toFixed(0)} displayType={"text"} thousandSeparator={true} /> {t('Home.layer2.3.PerMonth')}
                      </p>
                    </Col>
                  </Row>
                </Col>
              </Row>
            </Fragment>
            }

            {direction === DIRECTIONS.RTL && <Fragment>
              <Row gutter={16}>
                <Col xs={24} sm={8} lg={8}>
                  <Row type="flex">
                    <Col xs={{ span: 24, order: 2 }} lg={{ span: 16, order: 2 }}>
                      <Title level={5} style={{ fontSize: "1.1rem" }}>
                        {t('Home.layer2.1.title')}
                      </Title>
                      <p>
                        {t('Home.layer2.1.subtitle')}
                      </p>
                    </Col>
                    <Col style={{ textAlign: "center" }} xs={{ span: 24, order: 1 }} lg={{ span: 8, order: 1 }}>
                      <img
                        style={{ width: "80px", height: "80px" }}
                        src={Support247}
                        alt="24/7 support"
                      />
                    </Col>

                  </Row>
                </Col>
                <Col xs={24} sm={8} lg={8}>
                  <Row type="flex">
                    <Col xs={{ span: 24, order: 2 }} lg={{ span: 16, order: 2 }}>
                      <Title level={5} style={{ fontSize: "1.1rem" }}>
                        {t('Home.layer2.2.title')}
                      </Title>
                      <p>
                        {t('Home.layer2.2.subtitle')}</p>
                    </Col>
                    <Col style={{ textAlign: "center" }} xs={{ span: 24, order: 1 }} lg={{ span: 8, order: 1 }}>
                      <img
                        style={{ width: "80px", height: "80px" }}
                        src={DeliverIcon}
                        alt="Delivering 7-12 Business day"
                      />
                    </Col>

                  </Row>
                </Col>
                <Col xs={24} sm={8} lg={8}>
                  <Row type="flex">
                    <Col xs={{ span: 24, order: 2 }} lg={{ span: 16, order: 2 }}>
                      <Title level={5} style={{ fontSize: "1.1rem" }}>
                        {t('Home.layer2.3.title')}
                      </Title>
                      <p>
                        {t('Home.layer2.3.subtitle')}
                      </p>
                    </Col>
                    <Col style={{ textAlign: "center" }} xs={{ span: 24, order: 1 }} lg={{ span: 8, order: 1 }}>
                      <img
                        style={{ width: "80px", height: "80px" }}
                        src={BudgetFriendlyPackagesIcon}
                        alt="Budget Friendly Packages"
                      />
                    </Col>

                  </Row>
                </Col>
              </Row>
            </Fragment>
            }
          </div>
        </section>
        <section
          style={{
            margin: "80px auto 0px auto"
          }}
        >
          <div
            className="UAF-container-fluid"
            style={{
              maxWidth: "1200px",
              margin: "0 auto",
              textAlign: "center",
              padding: "25px"
            }}
          >
            <p style={{ fontSize: "45px", marginBottom: "0px" }}>
              {t('Home.industry.industryWeServe')}
            </p>
            <p style={{ fontSize: "1.1rem", marginBottom: "60px" }}>
              {t('Home.industry.androidIos')}
            </p>
            <br />
            <IndustriesWeServe />
          </div>
        </section>

        <section
          className="UAF-container-fluid"
          style={{
            background: "#fff",
            padding: 0,
            maxWidth: "1200px",
            margin: "0 auto 80px auto"
          }}
        >
          <Row type="flex">
            <Col lg={{ span: 12, order: 1 }} md={{ span: 12, order: 1 }} xs={{ span: 24, order: 1 }}>
              <div className="home-feature-img">
                <img
                  style={{ maxWidth: "450px" }}
                  src={OneStopeCommerceShop}
                  alt="image with animation for upapp"
                />
              </div>
            </Col>
            <Col lg={{ span: 12, order: 3 }} md={{ span: 12, order: 3 }} xs={{ span: 24, order: 3 }}>
              {direction === DIRECTIONS.LTR && <Fragment>
                <div className="tagline-heading" style={{ padding: "auto 15px" }}>
                  <Title level={1} className="texty-demo lg-rtl">
                    {t('Home.layer3.1')} <br /> {t('Home.layer3.2')}
                  </Title>
                  <Title
                    level={1}
                    className="text-demo-sub-text lg-rtl"
                    style={{
                      fontSize: "14px",
                      lineHeight: "20px",
                      fontWeight: 400
                    }}
                  >
                    {t('Home.layer3.text')}
                  </Title>

                  {/* <NavLink to="/ecommerce-platform"> */}
                  <NavLink to="/eCommerce-Single-Vendor-Platform">
                    <Button size={"large"} className="slider-get-started-btn lg-float-right uaf_allPageBtn_bg_Color">
                      {t('General.GetStarted')}
                    </Button>
                  </NavLink>
                </div>
              </Fragment>
              }
              {direction === DIRECTIONS.RTL && <Fragment>
                <div className="tagline-heading lg-rtl lg-p-rtl-15">
                  <Title level={1} className="texty-demo lg-rtl">
                    {t('Home.layer3.1')} <br /> {t('Home.layer3.2')}
                  </Title>
                  <Title
                    level={1}
                    className="text-demo-sub-text lg-rtl"
                    style={{
                      fontSize: "14px",
                      lineHeight: "20px",
                      fontWeight: 400
                    }}
                  >
                    {t('Home.layer3.text')}
                  </Title>

                  {/* <NavLink to="/ecommerce-platform"> */}
                  <NavLink to="/eCommerce-Single-Vendor-Platform">
                    <Button size={"large"} className="slider-get-started-btn main lg-float-rtl uaf_allPageBtn_bg_Color">
                      {t('General.GetStarted')}
                    </Button>
                  </NavLink>
                </div>
              </Fragment>
              }
            </Col>
          </Row>
        </section>
        {/* 
      <section
        className="UAF-container-fluid"
        style={{
          background: "#fff",
          padding: 0,
          maxWidth: "1200px",
          margin: "0 auto 80px auto"
        }}
      >
        <Row type="flex">
          <Col lg={{ span: 12, order: 1 }} xs={{ span: 24, order: 3 }}>
          {direction === DIRECTIONS.LTR && <Fragment>
            <div className="tagline-heading" style={{ padding: "auto 15px" }}>
              <Title level={1} className="texty-demo lg-ltr">
              {t('Home.layer4.1')} <br /> {t('Home.layer4.2')}
              </Title>
              <Title
                level={1}
                className="text-demo-sub-text lg-ltr"
                style={{
                  fontSize: "14px",
                  lineHeight: "20px",
                  fontWeight: 400
                }}
              >
                 {t('Home.layer4.text')}
              </Title>

              <NavLink
                to="/factory/booking
              
              "
              >
                <Button size={"large"} className="slider-get-started-btn lg-float-left uaf_allPageBtn_bg_Color">
                  {t('General.GetStarted')}
                </Button>
              </NavLink>
            </div>
          </Fragment>
          }
           {direction === DIRECTIONS.RTL && <Fragment>
            <div className="tagline-heading lg-rtl" style={{ padding: "auto 15px" }}>
              <Title level={1} className="texty-demo lg-rtl">
              {t('Home.layer4.1')} <br /> {t('Home.layer4.2')}
              </Title>
              <Title
                level={1}
                className="text-demo-sub-text lg-rtl"
                style={{
                  fontSize: "14px",
                  lineHeight: "20px",
                  fontWeight: 400
                }}
              >
                {t('Home.layer4.text')}
              </Title>

              <NavLink
                to="/factory/booking
              
              "
              >
                <Button size={"large"} className="slider-get-started-btn main lg-float-right uaf_allPageBtn_bg_Color">
                  {t('General.GetStarted')}
                </Button>
              </NavLink>
            </div>
          </Fragment>
          }
          </Col>
          <Col lg={{ span: 12, order: 3 }} xs={{ span: 24, order: 1 }}>
            <div className="home-feature-img">
              <img
                style={{ maxWidth: "200px" }}
                src={OnlineBookingSystem}
                alt="image with animation for upapp"
              />
            </div>
          </Col>
        </Row>
      </section>

        */}
        {/* 
      <section
        className="UAF-container-fluid"
        style={{
          background: "#fff",
          padding: 0,
          maxWidth: "1200px",
          margin: "0 auto 80px auto"
        }}
      >
        <Row type="flex">
          <Col lg={{ span: 12, order: 1 }} xs={{ span: 24, order: 1 }}>
            <div className="home-feature-img">
              <img
                style={{ Width: "450px !important" }}
                src={FacilityManagementIcon}
                alt="image with animation for upapp"
              />
            </div>
          </Col>
          <Col lg={{ span: 12, order: 3 }} xs={{ span: 24, order: 3 }}>
          {direction === DIRECTIONS.LTR && <Fragment>
            <div className="tagline-heading" style={{ padding: "auto 15px" }}>
              <Title level={1} className="texty-demo lg-rtl">
              {t('Home.layer5.1')}<br /> {t('Home.layer5.2')}
              </Title>
              <Title
                level={1}
                className="text-demo-sub-text lg-rtl"
                style={{
                  fontSize: "14px",
                  lineHeight: "20px",
                  fontWeight: 400
                }}
              >
                  {t('Home.layer5.text')}
              </Title>

              <NavLink to="/facility-management-application">
                <Button size={"large"} className="slider-get-started-btn lg-float-right uaf_allPageBtn_bg_Color">
                  {t('General.GetStarted')}
                </Button>
              </NavLink>
            </div>
          </Fragment>
}
{direction === DIRECTIONS.RTL && <Fragment>
            <div className="tagline-heading lg-p-rtl-15" style={{ padding: "auto 15px"}}>
              <Title level={1} className="texty-demo lg-rtl">
              {t('Home.layer5.1')}<br /> {t('Home.layer5.2')}
              </Title>
              <Title
                level={1}
                className="text-demo-sub-text lg-rtl"
                style={{
                  fontSize: "14px",
                  lineHeight: "20px",
                  fontWeight: 400,
                  
                }}
              >
                {t('Home.layer5.text')}
              </Title>

              <NavLink to="/facility-management-application">
                <Button
                  // type="dashed"
                  size={"large"}
                  style={{ background: "#4FBFDD", color: "#fff"}}
                  className="slider-get-started-btn main lg-float-right"
                >
                  {t('General.GetStarted')}
                </Button>
              </NavLink>
            </div>
          </Fragment>
}
          </Col>
        </Row>
      </section>
       */}
        <section
          className="UAF-container-fluid"
          style={{
            background: "#fff",
            padding: 0,
            maxWidth: "1200px",
            margin: "0 auto 180px auto"
          }}
        >
          <Row type="flex">
            <Col lg={{ span: 12, order: 1 }} md={{ span: 12, order: 1 }} xs={{ span: 24, order: 3 }}>
              {direction === DIRECTIONS.LTR && <Fragment>
                <div className="tagline-heading lg-ltr" style={{ padding: "auto 15px" }}>
                  <Title level={1} className="texty-demo lg-ltr">
                    {t('Home.layer6.1')} <br /> {t('Home.layer6.2')}
                  </Title>
                  <Title
                    level={1}
                    className="text-demo-sub-text lg-ltr"
                    style={{
                      fontSize: "14px",
                      lineHeight: "20px",
                      fontWeight: 400
                    }}
                  >
                    Launch your online Restaurant Application system with mobile app
                    (android & iOS) in 30 business days
              </Title>

                  <NavLink
                    to="/restaurant-application"
                  >
                    <Button size={"large"} className="slider-get-started-btn lg-ltr uaf_allPageBtn_bg_Color" >
                      {t('General.GetStarted')}
                    </Button>
                  </NavLink>
                </div>
              </Fragment>
              }
              {direction === DIRECTIONS.RTL && <Fragment>
                <div className="tagline-heading lg-p-ltr-15">
                  <Title level={1} className="texty-demo lg-rtl ">
                    {t('Home.layer6.1')} <br /> {t('Home.layer6.2')}
                  </Title>
                  <Title
                    level={1}
                    className="text-demo-sub-text"
                    style={{
                      fontSize: "14px",
                      lineHeight: "20px",
                      fontWeight: 400,
                      textAlign: "center"
                    }}
                  >
                    {t('Home.layer6.text')}
                  </Title>

                  <NavLink to="/restaurant-application">
                    <Button size={"large"} className="slider-get-started-btn lg-float-right uaf_allPageBtn_bg_Color">
                      {t('General.GetStarted')}
                    </Button>
                  </NavLink>
                </div>
              </Fragment>
              }
            </Col>
            <Col lg={{ span: 12, order: 2 }} md={{ span: 12, order: 2 }} xs={{ span: 24, order: 1 }}>
              <div className="home-feature-img">
                <img
                  style={{ width: "90%" }}
                  src="https://upappfactory-static-images.s3.us-east-2.amazonaws.com/RestaurentApp/multipleMode+OfService.svg"
                  alt="image with animation for upapp"
                />
              </div>
            </Col>
          </Row>
        </section>

        {/* clientele Section */}

        <section
          style={{
            background: "rgb(243, 243, 243)",
            padding: "10px"
          }}
        >
          <ClienteleSection />
        </section>

        {/* <Clientele /> */}

        <section
          className="UAF-container-fluid"
          style={{
            background: "#fff",
            padding: 0,
            maxWidth: "1200px",
            margin: "100px auto 0px auto"
          }}
        >
          <Row type="flex">
            <Col xs={24} sm={6} lg={6}>
              <div className="UAF-power-icon-container">
                <img src={GoToMarketIcon} alt="Go to Market Faster" />
                <p>{t("Home.layer8.market")}</p>
              </div>
            </Col>
            <Col xs={24} sm={6} lg={6}>
              <div className="UAF-power-icon-container">
                <img src={SaveMoneyAndTime} alt="Go to Market Faster" />
                <p>{t("Home.layer8.invest")}</p>
              </div>
            </Col>
            <Col xs={24} sm={6} lg={6}>
              <div className="UAF-power-icon-container">
                <img src={ConvertShopersIntoBuyers} alt="Go to Market Faster" />
                <p>{t("Home.layer8.buyers")}</p>
              </div>
            </Col>
            <Col xs={24} sm={6} lg={6}>
              <div className="UAF-power-icon-container">
                <img src={IncreaseRevenue} alt="Go to Market Faster" />
                <p>{t("Home.layer8.revenue")}</p>
              </div>
            </Col>
          </Row>
        </section>

        {direction === DIRECTIONS.RTL && <Fragment>
          <section
            style={{
              // background: "#474848"
            }}
          >
            <div
              className="UAF-container-fluid"
              style={{
                // background: "#474848",
                padding: 0,
                maxWidth: "1200px",
                margin: "0px auto 0px auto",
                padding: "40px 15px"
              }}
            >
              <p
                style={{
                  fontSize: "45px",
                  marginTop: "20px",
                  marginBottom: "50px",
                  textAlign: "center"
                }}
              >
                {t("Home.layer9.heading")}
              </p>
              <Row gutter={16} type="flex" dir="rtl">
                {processFlow.map(flow => {
                  return (
                    <Col xs={24} md={12} lg={12} dir="rtl">
                      <Row gutter={16} type="flex" className="m-center" dir="rtl">
                        <Col xs={{ span: 24, order: 1 }} md={{ span: 20, order: 2 }} lg={{ span: 20, order: 2 }} direction="rtl">
                          <div
                            className="flow-card lg-rtl"
                            style={{ marginBottom: "100px", paddingLeft: "20px", paddingRight: "20px" }}
                          >
                            <p style={{ fontSize: "20px" }}>
                              {flow.no} &nbsp; {flow.title}
                            </p>

                            <p style={{ marginTop: "1.1rem" }}>
                              {flow.desc}
                            </p>
                          </div>
                        </Col>
                        <Col xs={{ span: 24, order: 2 }} md={{ span: 4, order: 1 }} lg={{ span: 4, order: 1 }}>
                          <img
                            style={{ width: "80px", height: "80px" }}
                            src={flow.icon}
                            alt={flow.title}
                          />
                        </Col>
                      </Row>
                    </Col>
                  );
                })}
              </Row>
            </div>
          </section>

        </Fragment>
        }

        {direction === DIRECTIONS.LTR && <Fragment>
          <section
            style={{
              // background: "#474848"
            }}
          >
            <div
              className="UAF-container-fluid"
              style={{
                // background: "#474848",
                padding: 0,
                maxWidth: "1200px",
                margin: "0px auto 0px auto",
                padding: "40px 15px"
              }}
            >
              <p
                style={{
                  fontSize: "45px",
                  marginTop: "50px",
                  marginBottom: "100px",
                  textAlign: "center",
                }}
              >
                {t("Home.layer9.heading")}
              </p>
              <Row gutter={16} type="flex" >
                {processFlow.map(flow => {
                  return (
                    <Col xs={24} md={12} lg={12} >
                      <Row gutter={16} className="m-center" >
                        <Col xs={24} md={4} lg={4}>
                          <img
                            style={{ width: "80px", height: "80px" }}
                            src={flow.icon}
                            alt={flow.title}
                          />
                        </Col>

                        <Col xs={24} md={20} lg={20} >
                          <div
                            className="flow-card "
                            style={{ marginBottom: "100px", paddingLeft: "20px", paddingRight: "20px" }}
                          >
                            <p style={{ fontSize: "1.1rem" }}>
                              {flow.no} &nbsp; {flow.title}
                            </p>

                            <p style={{ marginTop: "15px", }}>
                              {flow.desc}
                            </p>
                          </div>
                        </Col>


                      </Row>
                    </Col>
                  );
                })}
              </Row>
            </div>
          </section>

        </Fragment>
        }

        <section style={{ marginTop: "50px", marginBottom: "20px" }} id="select-plan-homepage">
          <HomePricing />
        </section>


        <section
          className="UAF-container-fluid"
          style={{
            background: "#fff",
            padding: 0,
            maxWidth: "1200px",
            margin: "0 auto 180px auto"
          }}
        >
          <p
            style={{
              fontSize: "45px",
              marginTop: "20px",
              marginBottom: "50px",
              textAlign: "center"
            }}
          >
            Our Blogs
              </p>
          <Row gutter={16} type="flex" justify="space-around" align="middle">
            <Col xs={{ span: 24 }} sm={{ span: 24 }} md={{ span: 24 }} lg={{ span: 24 }} xl={{ span: 24 }}>
              <Row gutter={16}>
                <Slider {...settings} style={{ padding: "20px 50px" }}>
                  {recentBlog && recentBlog.map((blog) =>
                    <Col xs={{ span: 24 }} sm={{ span: 12 }} md={{ span: 12 }} lg={{ span: 12 }} xl={{ span: 12 }}>
                      <NavLink to={`/blog/${blog.slug}`}>
                        <Card className="uaf_bp_card"
                          cover={<img style={{ height: "230px", width: "100%", objectFit: "cover" }} alt="example" src={blog.featured_image} />}
                        >
                          <Meta description={<div>
                            <p className="uaf_bp_card-desc"><span className="uaf_bp_card-desc-date">{moment(blog.date_addedd).format('ll')} | by</span><a href="javascript:void(0)" > {blog.author}</a></p>
                            <p className="uaf_bp_card-desc-heading">{blog.title}</p>
                            {/* <p className="uaf_bp_card-desc-data">{renderHTML(blog.content)}</p> */}
                          </div>} />
                        </Card>
                      </NavLink>
                    </Col>
                  )
                  }
                </Slider>
              </Row>
            </Col>
            <Col style={{ textAlign: "center" }} xs={{ span: 24 }} sm={{ span: 24 }} md={{ span: 24 }} lg={{ span: 24 }} xl={{ span: 24 }}>
              <NavLink to={`/blog`}>
                <Button type="primary" style={{ maxWidth: "320px", margin: "25px auto 0px auto" }} block>View All Blogs</Button>
              </NavLink>
            </Col>
          </Row>

        </section>


        <ContactUsForm />
      </div>
    );
};

const HomeRFC = Form.create({ name: "Request_for_customize" })(HomeNew);

HomeRFC.propTypes = {
  loadApplication: PropTypes.func.isRequired,
  requestForCustomisedApp: PropTypes.func.isRequired,
  Applications: PropTypes.array.isRequired,
  loading: PropTypes.bool,
  direction: withDirectionPropTypes.direction,
  getRecentBlog: PropTypes.func.isRequired,
};

const mapStateToProps = state => ({
  Applications: state.application.Applications,
  recentBlog: state.BlogManagement.recentBlog,
  loading: state.application.loading
});

export default connect(mapStateToProps, {
  loadApplication,
  requestForCustomisedApp,
  setAlert,
  getRecentBlog
})(withDirection(HomeRFC));
