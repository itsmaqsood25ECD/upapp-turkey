import React, { useState, useEffect, Fragment } from "react";
import { Typography, Layout, Input, Row, Col, Form, Spin } from "antd";
import ScrollTop from "../../ScrollTop";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import { Link, NavLink } from "react-router-dom";
import "antd/dist/antd.css";
import "../../../assets/css/custom.css";
import QueueAnim from "rc-queue-anim";
import { Helmet } from "react-helmet";
import BlogData from "./blogData";
import { useTranslation } from 'react-i18next';
import { getAllBlog } from '../../../actions/admin/blog.management.action'

const { Content } = Layout;
const { Title } = Typography;

const Store = ({
    loading,
    getAllBlog,
    Blogs
}) => {

    const [state, setState] = useState({
        blog: ''
    });

    if (Applications === null) {
        console.log(Applications, loading);
        loadApplication();
    }
    useEffect(() => {

    }, []);

    return (
        <Fragment>
            <Helmet>
                <title>Blogs</title>
                <meta
                    name="Subscribe your ready made apps for your business. Start your eCommerce business in a minute. Just choose the product which suits best to your work and you are all done."
                    content="Buy Ready Made eCommerce App Business Application Work Purchase"
                />
            </Helmet>
            <ScrollTop />
            <QueueAnim delay={300} type="left" key="a" className="demo-content">
                <Content style={{ margin: "24px 0px 0", overflow: "initial" }}>
                    <Row gutter={16} type="flex" style={{ margin: "30px auto" }}>
                        <Col xs={24} sm={24} lg={24} className="animated fadeIn faster">
                            <Row className="blog-table">
                                <BlogData match={match} Applications={Applications} searchApplication={searchApplication} />
                            </Row>
                            <Row>
                                <Col xs={24} style={{ marginBottom: "30px", marginTop: "30px" }} />
                            </Row>
                        </Col>
                    </Row>
                </Content>
            </QueueAnim>
        </Fragment>
    );
};

const RFC = Form.create({ name: "Request_for_customize" })(Store);

RFC.propTypes = {
    loadApplication: PropTypes.func.isRequired,
    requestForCustomisedApp: PropTypes.func.isRequired,
    getLocation: PropTypes.func.isRequired,
    GetCategory: PropTypes.func.isRequired,
    Category: PropTypes.array.isRequired,
    Applications: PropTypes.array.isRequired,
    loading: PropTypes.bool
};
const mapStateToProps = state => ({
    Applications: state.application.Applications,
    userlocation: state.userlocation,
    Category: state.categoryManagement.Category,
    loading: state.application.loading,
    direction: withDirectionPropTypes.direction,
});
export default connect(mapStateToProps, {
    loadApplication,
    GetCategory,
    getLocation,
    requestForCustomisedApp,
    setAlert
})(withDirection(RFC));
