import React, { Fragment, useEffect, useState } from "react";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import { Link } from "react-router-dom";
import ScrollTop from "../../ScrollTop";
import { Helmet } from "react-helmet";
import { List, Typography, Card, Layout, Input, Row, Col, Menu, Avatar, Icon } from "antd";
import Title from "antd/lib/typography/Title";
import '../../../assets/css/blog.css';

const { Text } = Typography;
const { Meta } = Card;

const AppType = ({ getAllBlog,
  Blogs }) => {
  //   console.log(match.params.industry, match.params.type, Applications);
  const [applicationData, setApplicationData] = useState({
    apps: [],
    searchApplication: ""
  });

  const { apps } = applicationData;
  console.log("match >>>", match);
  if (Applications === null) {
    console.log("if");
  }
  useEffect(() => {
    if (match.params.type === "all" || match.params.type === undefined) {
      // setApplicationData({
      //   ...applicationData,
      //   apps: Applications
      // });

      setApplicationData({
        ...applicationData,
        apps:
          Applications &&
          Applications.filter(myapp => {
            return (
              (myapp.fields &&
                myapp.fields.name
                  .toLowerCase()
                  .includes(
                    searchApplication && searchApplication.toLocaleLowerCase()
                  )) ||
              (myapp.fields &&
                myapp.fields.type
                  .toLowerCase()
                  .includes(
                    searchApplication && searchApplication.toLocaleLowerCase()
                  )) ||
              (myapp.fields &&
                myapp.fields.industry
                  .toLowerCase()
                  .includes(
                    searchApplication && searchApplication.toLocaleLowerCase()
                  )) ||
              (myapp.fields &&
                myapp.fields.category
                  .toLowerCase()
                  .includes(
                    searchApplication && searchApplication.toLocaleLowerCase()
                  ))
            );
          })
      });
    } else {
      if (searchApplication === "" || searchApplication === null) {
        setApplicationData({
          ...applicationData,
          apps:
            Applications &&
            Applications.filter(arg => {
              return (
                arg.fields.type.toLowerCase() ===
                match.params.type.toLowerCase() ||
                arg.fields.industry.toLowerCase() ===
                match.params.type.toLowerCase()
              );
            })
        });
      } else {
        setApplicationData({
          ...applicationData,
          apps:
            Applications &&
            Applications.filter(myapp => {
              if (
                myapp.fields.type.toLowerCase() ===
                match.params.type.toLowerCase() ||
                myapp.fields.industry.toLowerCase() ===
                match.params.type.toLowerCase()
              ) {
                return (
                  (myapp.fields &&
                    myapp.fields.name
                      .toLowerCase()
                      .includes(
                        searchApplication &&
                        searchApplication.toLocaleLowerCase()
                      )) ||
                  (myapp.fields &&
                    myapp.fields.type
                      .toLowerCase()
                      .includes(
                        searchApplication &&
                        searchApplication.toLocaleLowerCase()
                      )) ||
                  (myapp.fields &&
                    myapp.fields.industry
                      .toLowerCase()
                      .includes(
                        searchApplication &&
                        searchApplication.toLocaleLowerCase()
                      )) ||
                  (myapp.fields &&
                    myapp.fields.category
                      .toLowerCase()
                      .includes(
                        searchApplication &&
                        searchApplication.toLocaleLowerCase()
                      ))
                );
              }
            })
        });
      }
    }

    console.log(match);
  }, [match, searchApplication, Applications]);

  console.log(searchApplication);

  return (
    <Fragment>
      <ScrollTop />
      <div style={{ fontSize: "30px", textAlign: "center", marginTop: "30px" }}>
        <Title level={1} style={{ fontSize: "30px", fontWeight: "500" }}></Title>
      </div>

      <div style={{ margin: "0 auto" }}>
        <Row style={{ marginTop: "30px" }}>
          <Col xs={24} sm={24} lg={24}>
            <Row className="store-table">
              <List loading={!apps} grid={{ gutter: 10, xs: 1, sm: 2, md: 2, lg: 3, xl: 4, xxl: 4 }} dataSource={apps && apps} footer={<div />}
                renderItem={item => (
                  <List.Item>
                    <Card hoverable className="store-card blog-list-card" style={{ margin: "30px auto" }}
                      cover={
                        <Link
                        // to={`/readymade-apps/${item.fields.slug}/${item.UID}`}
                        >
                          <img
                            alt="example"
                            src={item.fields.images[0].thumb.url}
                          />
                        </Link>
                      }
                    >
                      <div className="hover-item">
                        <span className="btn-like btn-hover-item btn-hover-default">
                          <Icon type="heart" />
                          <span className="btn-hover-item-number">02</span>
                        </span>
                        <strong className="btn-collect btn-hover-item btn-hover-default">Collect</strong>
                      </div>
                      <Meta
                        // title={item.fields.name}
                        description={item.fields.desc}
                        style={{ textAlign: "left", padding: "15px 0px" }}
                      />



                      <Row>
                        <Col sm={12} xs={12}>
                          <span>Added to <Link>Web Design</Link> </span>
                        </Col>
                        <Col sm={12} xs={12} style={{ textAlign: "right" }}>
                          <span>April 09, 2020</span>
                        </Col>
                      </Row>
                      <hr />
                      <Row>
                        <Col sm={24} style={{ display: 'flex', alignItems: 'center' }}>
                          <Avatar src={item.fields.images[0].thumb.url} />
                          <div className="by">
                            By <Link>UPapp Factory</Link>
                          </div>
                        </Col>
                      </Row>
                    </Card>
                  </List.Item>
                )}
              />
            </Row>
            <Row>
              <Col
                xs={24}
                style={{ marginBottom: "30px", marginTop: "30px" }}
              />
            </Row>
          </Col>
        </Row>
      </div>
    </Fragment>
  );
};

AppType.propTypes = {
  Applications: PropTypes.array.isRequired,
  loading: PropTypes.bool
};
const mapStateToProps = state => ({
  Applications: state.application.Applications,
  loading: state.application.loading
});
export default connect(mapStateToProps, null)(AppType);
