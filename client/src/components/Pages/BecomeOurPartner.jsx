import React, { useEffect, useState, Fragment } from "react";
import PropTypes from "prop-types";
import { CountryDropdown } from "react-country-region-selector";
import PhoneInput from "react-phone-number-input";
import "react-phone-number-input/style.css";
import SmartInput from "react-phone-number-input/smart-input";
import { connect } from "react-redux";
import { BecomePartner } from "../../actions/partner.action";
import { Redirect } from "react-router-dom";
import moment from "moment";
import ReactGA from "react-ga";
import SrollTop from "../ScrollTop";
import {
  Icon,
  Button,
  Typography,
  Modal,
  Input,
  Row,
  Col,
  Form,
  Select,
  DatePicker,
  Menu
} from "antd";

const { Title, Text } = Typography;
const { Option, OptGroup } = Select;
const dateFormatList = ["DD/MM/YYYY", "DD/MM/YY"];
const recaptchaRef = React.createRef();

const BecomeOurPartner = ({
  form: { getFieldDecorator, validateFields, setFieldsValue },
  BecomePartner
}) => {
  const [state, setState] = useState({});

  const [formData, setFormData] = useState({
    isSubmitted: false
  });

  useEffect(() => {}, []);

  const onChange = e => {
    setFormData({ ...formData, [e.target.name]: e.target.value });
  };

  const handleSelect = e => {
    setFormData({ ...formData, company_size: e });
  };

  const onPhoneChange = e => {
    setFormData({ ...formData, mobile: e });
  };

  const selectCountry = val => {
    setFormData({ ...formData, country: val });
  };

  const onSubmit = e => {
    e.preventDefault();
    validateFields((err, values) => {
      if (err) {
        console.log(err);
      } else if (!err) {
        setFormData({
          ...formData
        });
        BecomePartner(formData);
        setFormData({
          ...formData,
          isSubmitted: true
        });
      }
    });
  };

  if (formData.isSubmitted) {
    return <Redirect to="/become-our-partner/sumbitted" />;
  }

  return (
    <Fragment>
      <Row type="flex" className="partner-section-bg">
        <Col lg={16} xs={24} style={{ paddingTop: "60px" }}>
          <div className="partner-tagline">
            <Title level={1} className="partner-tag">
              <span className="bold-tag">Partners</span>
            </Title>
            <Title level={4} className="single-app-description light-text">
              A key part of the UPappfactory story is our partnerships: for
              capacity of human talent, cloud and ultimately go-to-market.
            </Title>
          </div>
          <div className="partner-tagline">
            <Title level={1} className="partner-tag">
              Go-To Market <span className="bold-tag">Partners</span>
            </Title>
            <Title level={4} className="single-app-description light-text">
              Partner with us, and we'll provide you with lifetime revenue share
              and the ability to build deeper relationships with your customers.
            </Title>
          </div>
          <div className="partner-tagline">
            <Title level={1} className="partner-tag">
              UPappfactory Tech <span className="bold-tag">Partners</span>
            </Title>
            <Title level={4} className="single-app-description light-text">
              Partner with us, and we'll provide you with lifetime revenue share
              and the ability to build deeper relationships with your customers.
            </Title>
          </div>
        </Col>
        <Col lg={8} xs={24}>
          <div className="Become-Our-Partner-form">
            <div
              class="logo"
              style={{ width: "100%", marginTop: "40px", marginLeft: "0px" }}
            ></div>
            <Form className="partnership-form">
              <Row gutter={16}>
                <Col xs={24} lg={24}>
                  <Form.Item>
                    {getFieldDecorator("name", {
                      rules: [
                        {
                          required: true,
                          message: "Please type your name"
                        }
                      ]
                    })(
                      <Input
                        placeholder="Name"
                        name="name"
                        onChange={e => onChange(e)}
                        prefix={
                          <Icon
                            type="user"
                            style={{ color: "rgba(0,0,0,.25)" }}
                          />
                        }
                      />
                    )}
                  </Form.Item>
                </Col>
                <Col xs={24} lg={24}>
                  <Form.Item>
                    {getFieldDecorator("email", {
                      rules: [
                        {
                          required: true,
                          message: "Please type your email"
                        }
                      ]
                    })(
                      <Input
                        placeholder="Email"
                        name="email"
                        onChange={e => onChange(e)}
                        prefix={
                          <Icon
                            type="mail"
                            style={{ color: "rgba(0,0,0,.25)" }}
                          />
                        }
                      />
                    )}
                  </Form.Item>
                </Col>

                <Col xs={24} lg={24}>
                  <Form.Item>
                    {getFieldDecorator("country", {
                      rules: [
                        {
                          required: true,
                          message: "Please type your country"
                        }
                      ]
                    })(
                      <CountryDropdown
                        blacklist={[
                          "AF",
                          "AO",
                          "DJ",
                          "GQ",
                          "ER",
                          "GA",
                          "IR",
                          "KG",
                          "LY",
                          "MD",
                          "NP",
                          "ST",
                          "SL",
                          "SD",
                          "SY",
                          "SR",
                          "TM",
                          "VE",
                          "ZW",
                          "IL"
                        ]}
                        name="country"
                        valueType="full"
                        style={{ marginTop: "12px" }}
                        onChange={val => selectCountry(val)}
                      />
                    )}
                  </Form.Item>
                </Col>
                <Col xs={24} lg={24}>
                  <Form.Item>
                    {getFieldDecorator("mobile", {
                      rules: [
                        {
                          required: true,
                          message: "Please input your mobile number!"
                        }
                      ]
                    })(
                      <PhoneInput
                        style={{ marginTop: "10px" }}
                        name="mobile"
                        inputComponent={SmartInput}
                        placeholder="Enter phone number"
                        onChange={e => onPhoneChange(e)}
                      />
                    )}
                  </Form.Item>
                </Col>
                <Col xs={24} lg={24}>
                  <Form.Item hasFeedback>
                    {getFieldDecorator("company_size", {
                      rules: [
                        {
                          required: true,
                          message: "Please select your company size "
                        }
                      ]
                    })(
                      <Select
                        placeholder="Company Size"
                        style={{ marginTop: "10px" }}
                        onChange={handleSelect}
                      >
                        <Option key="ecommerce" value="0-10">
                          0 - 10
                        </Option>
                        <Option key="booking" value="10-50">
                          10 - 50
                        </Option>
                        <Option key="booking" value="50-100">
                          50 - 100
                        </Option>
                        <Option key="booking" value="100+">
                          100+
                        </Option>
                      </Select>
                    )}
                  </Form.Item>
                </Col>
              </Row>

              <div style={{ textAlign: "center" }}>
                <Button
                  style={{
                    width: "100%",
                    height: "40px",
                    fontSize: "18px"
                  }}
                  type="primary"
                  onClick={onSubmit}
                >
                  Submit
                </Button>
              </div>
            </Form>
          </div>
        </Col>
      </Row>
    </Fragment>
  );
};

const BOP = Form.create({ name: "Become_our_partner" })(BecomeOurPartner);
BOP.propTypes = {};

const mapStateToProps = state => ({
  partner: PropTypes.func.isRequired
});

const mapDispatchToProps = {};

export default connect(mapStateToProps, {
  BecomePartner
})(BOP);
