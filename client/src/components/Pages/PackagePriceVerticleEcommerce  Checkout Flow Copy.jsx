/* eslint react/prop-types: 0 */
import React, { Component } from "react";
import { connect } from "react-redux";
import { Row, Col, Typography, Radio, Button, Icon, Tabs, Anchor } from "antd";
import Walkthrugh from "./Walkthrugh";
import { Link, NavLink } from 'react-router-dom'
import CurrencyFormat from "react-currency-format";
import base64 from "base-64";
import MobileApp from "../../assets/img/PackIcon/mobileApp.svg";
import WebApp from "../../assets/img/PackIcon/WebApp.svg";
import Website from "../../assets/img/PackIcon/Website.svg";
import "../../assets/css/packagePrice.css";
import withDirection, { withDirectionPropTypes, DIRECTIONS } from 'react-with-direction';
import { withTranslation } from 'react-i18next';
import i18next from 'i18next';


const { TabPane } = Tabs;
const { Title } = Typography;

class PackagePriceVerticleEcommerce extends Component {
  constructor(props) {
    super(props);
    this.state = {
      //plan B
      WFPP: this.props.WFPP,
      WAFPP: this.props.WAFPP,
      MFPP: this.props.MFPP,

      // Plan A
      MBDSPM: this.props.MBDSPM,
      MADSPM: this.props.MADSPM,
      MBDSPY: this.props.MBDSPY,
      MADSPY: this.props.MADSPY,
      WBDSPM: this.props.WBDSPM,
      WADSPM: this.props.WADSPM,
      WBDSPY: this.props.WBDSPY,
      WADSPY: this.props.WADSPY,
      WABDSPM: this.props.WABDSPM,
      WAADSPM: this.props.WAADSPM,
      WABDSPY: this.props.WABDSPY,
      WAADSPY: this.props.WAADSPY,
      Duration: this.props.duration,
      currency: this.props.currency,
      MBDSP: this.props.MBDSPY,
      MADSP: this.props.MADSPY,
      WBDSP: this.props.WBDSPY,
      WADSP: this.props.WADSPY,
      WABDSP: this.props.WABDSPY,
      WAADSP: this.props.WAADSPY,
      PackType: this.props.PackType,
      SLUG: this.props.SLUG,
      ID: this.props.ID,

      // Setup Charge
      WSC: this.props.WSC,
      MSC: this.props.MSC,
      WMSC: this.props.WMSC,

      // User 
      USER: this.props.USER


    };

    console.log("USER >> ", this.state.USER)
  }

  // componentDidMount(){
  // 	if(localStorage.getItem('lng') === 'AR'){
  // 		  // layoutDirection = 'rtl';
  // 		  i18next.changeLanguage('AR')
  // 		}else if(localStorage.getItem('lng') === 'EN'){
  // 		  // layoutDirection = 'ltr';
  // 		  i18next.changeLanguage('EN')
  // 		}
  //   } 

  componentWillReceiveProps(nextApp) {
    this.setState({
      // Plan B
      WFPP: nextApp.WFPP,
      WAFPP: nextApp.WAFPP,
      MFPP: nextApp.MFPP,
      // Plan A
      MBDSPM: nextApp.MBDSPM,
      MADSPM: nextApp.MADSPM,
      MBDSPY: nextApp.MBDSPY,
      MADSPY: nextApp.MADSPY,
      WBDSPM: nextApp.WBDSPM,
      WADSPM: nextApp.WADSPM,
      WBDSPY: nextApp.WBDSPY,
      WADSPY: nextApp.WADSPY,
      WABDSPM: nextApp.WABDSPM,
      WAADSPM: nextApp.WAADSPM,
      WABDSPY: nextApp.WABDSPY,
      WAADSPY: nextApp.WAADSPY,
      Duration: nextApp.duration,
      currency: nextApp.currency,
      MBDSP: nextApp.MBDSPY,
      MADSP: nextApp.MADSPY,
      WBDSP: nextApp.WBDSPY,
      WADSP: nextApp.WADSPY,
      WABDSP: nextApp.WABDSPY,
      WAADSP: nextApp.WAADSPY,
      PackType: nextApp.PackType,
      SLUG: nextApp.SLUG,
      ID: nextApp.ID,
      // Setup Charge
      WSC: nextApp.WSC,
      MSC: nextApp.MSC,
      WMSC: nextApp.WMSC,
      USER: nextApp.USER,
    });
  }

  handleChange = e => {
    if (e.target.value === "Year") {
      this.setState({
        MBDSP: this.state.MBDSPY,
        MADSP: this.state.MADSPY,
        WBDSP: this.state.WBDSPY,
        WADSP: this.state.WADSPY,
        WABDSP: this.state.WABDSPY,
        WAADSP: this.state.WAADSPY,
        PackType: "Annually"
      });
    } else if (e.target.value === "Month") {
      this.setState({
        MBDSP: this.state.MBDSPM,
        MADSP: this.state.MADSPM,
        WBDSP: this.state.WBDSPM,
        WADSP: this.state.WADSPM,
        WABDSP: this.state.WABDSPM,
        WAADSP: this.state.WAADSPM,
        PackType: "Monthly"
      });
    }
  };

  render() {
    const currency = this.props.currency;
    const WSC = this.props.WSC;
    const WMSC = this.props.WMSC;
    const MSC = this.props.MSC;
    // const USER = this.props.USER;
    const { t } = this.props;

    return (

      <div className="package-bg" dir="ltr">
        {/* <RequestForDemo /> */}

        <div style={{ textAlign: "center" }}>
          <Title level="3">
            {t('pricing.Boost')}
          </Title>

          <div className="price-verticle-tagline">
            {t('pricing.noCommitment')}
            <br />
            {t('pricing.changeCancel')}
          </div>
        </div>
        <Row
          gutter={16}
          type="flex"
          justify="center"
          align="middle"
          style={{ margin: "0px 0px", border: "0px solid #eee" }}
        >
          <Col lg={24} xs={24} className="">
            <div
              className="V-Price-Switcher"
              style={{
                textAlign: "center",
                marginTop: "15px",
                marginBottom: "15px"
              }}
            >

              <Radio.Group
                defaultValue="Year"
                buttonStyle="solid"
                onChange={this.handleChange}
                defaultValue="Year"
                size="large"
                className="Pack-chooser-container"
                shape="circle"
              >
                <Radio.Button className="year" value="Year">
                  {t('pricing.yearly')}
                </Radio.Button>
                <Radio.Button className="month" value="Month">
                  {t('pricing.Monthly')}
                </Radio.Button>
              </Radio.Group>
            </div>
          </Col>
          <Col xs={24}>

            <Row gutter={16}>
              <div style={{ margin: "100px auto", maxWidth: "1080px" }}>
                {/* Website Package */}
                <Col xs={24} md={8} lg={8}>
                  <div className="pack-card Basic">
                    <div className="Pack-identifier Basic">
                      <p>
                        {/* <span>Only Website </span> */}
                        <span>{t("pricing.onlyweb")}</span>
                        {/* <br />{" "} */}
                      </p>
                    </div>
                    <div className="package-img-cont">
                      <img
                        className="package-img"
                        src={Website}
                        alt=""
                        style={{ width: "150px" }}
                      />
                    </div>
                    <div style={{ textAlign: "center" }}>
                      <Title className="package-title" level={3}>
                        {t("pricing.basic")}
                      </Title>
                    </div>
                    <div
                      style={{
                        textAlign: "center",
                        margin: "10px auto",
                        fontSize: "14px"
                      }}
                    >
                      <div
                        style={{
                          maxWidth: "230px",
                          borderBottom: "0.5px solid #ddd",
                          margin: "0px auto",
                          lineHeight: "2"
                        }}
                      >
                        {t("pricing.ecommWebsite")}
                      </div>
                      <div
                        style={{
                          maxWidth: "150px",
                          margin: "0px auto",
                          lineHeight: "2"
                        }}
                      >
                        {t("pricing.allFeatures")}
                      </div>

                      <div></div>
                    </div>
                    <div>
                          <Title
                            level={2}
                            class="light-text"
                            style={{
                              marginTop: "10px",
                              marginBottom: "30px",
                              textAlign: "center"
                            }}
                          >
                            <div className="before-discount-price">
                              {currency}&nbsp;
                  <CurrencyFormat
                                value={Number(this.state.WBDSP)}
                                displayType={"text"}
                                thousandSeparator={true}
                              />
                            </div>
                &nbsp;
                <span className="PriceNum">
                              <span className="currency">{currency}</span>
                              <CurrencyFormat
                                value={Number(this.state.WADSP)}
                                displayType={"text"}
                                thousandSeparator={true}
                              />
                              <sup className="sup-star">*</sup>
                              <span></span>
                              <span className="per-month-tag">/Mo</span>
                            </span>
                          </Title>
                          <div style={{ textAlign: "center" }}>
                            {this.state.USER != null ? <Link
                              to="/eCommerce-Single-Vendor-Platform/Basic/Standard"
                            >
                              <Button className="UAFprimaryButton1" type="primary">{t('General.GetStarted')}</Button>
                            </Link> : <Link
                              to="/login"
                            >
                                <Button className="UAFprimaryButton1" type="primary">{t('General.GetStarted')}</Button>
                              </Link>
                            }
                            <div className="one-time-setup-tag">
                              *{t("pricing.oneTimeSetup")} {currency} {" "}
                              <CurrencyFormat
                                value={WSC}
                                displayType={"text"}
                                thousandSeparator={true}
                              /> {" "} {t("pricing.willApply")}
                            </div>
                          </div>
                        </div>
                      

                    {/* <Tabs className="package-tabs" defaultActiveKey="1">
                      <TabPane tab={t("pricing.StandardPlan")} key="1">
                        <div>
                          <Title
                            level={2}
                            class="light-text"
                            style={{
                              marginTop: "10px",
                              marginBottom: "30px",
                              textAlign: "center"
                            }}
                          >
                            <div className="before-discount-price">
                              {currency}&nbsp;
                  <CurrencyFormat
                                value={Number(this.state.WBDSP)}
                                displayType={"text"}
                                thousandSeparator={true}
                              />
                            </div>
                &nbsp;
                <span className="PriceNum">
                              <span className="currency">{currency}</span>
                              <CurrencyFormat
                                value={Number(this.state.WADSP)}
                                displayType={"text"}
                                thousandSeparator={true}
                              />
                              <sup className="sup-star">*</sup>
                              <span></span>
                              <span className="per-month-tag">/Mo</span>
                            </span>
                          </Title>
                          <div style={{ textAlign: "center" }}>
                            {this.state.USER != null ? <Link
                              to="/eCommerce-Single-Vendor-Platform/Basic/Standard"
                            >
                              <Button className="UAFprimaryButton1" type="primary">{t('General.GetStarted')}</Button>
                            </Link> : <Link
                              to="/login"
                            >
                                <Button className="UAFprimaryButton1" type="primary">{t('General.GetStarted')}</Button>
                              </Link>
                            }
                            <div className="one-time-setup-tag">
                              *{t("pricing.oneTimeSetup")} {currency} {" "}
                              <CurrencyFormat
                                value={WSC}
                                displayType={"text"}
                                thousandSeparator={true}
                              /> {" "} {t("pricing.willApply")}
                            </div>
                          </div>
                        </div>
                      </TabPane>
                      <TabPane tab={t("pricing.RentalPlan")} key="2">
                        <div>
                          <Title
                            level={2}
                            class="light-text"
                            style={{
                              marginTop: "30px",
                              marginBottom: "38px",
                              textAlign: "center"
                            }}
                          >
                            &nbsp;
                <span className="PriceNum">
                              <span className="currency">{currency}</span>
                              {console.log("WFPP", this.state.WFPP)}
                              <CurrencyFormat
                                value={Number(this.state.WFPP)}
                                displayType={"text"}
                                thousandSeparator={true}
                              />
                              <sup className="sup-star">*</sup>
                              <span></span>
                              <span className="per-month-tag">/Mo</span>
                            </span>
                          </Title>
                          <div style={{ textAlign: "center" }}>
                            {this.state.USER != null ? <Link to="/eCommerce-Single-Vendor-Platform/Basic/Rental">
                              <Button className="UAFprimaryButton1" type="primary">{t('General.GetStarted')}</Button>
                            </Link> : <Link to="/login">
                                <Button className="UAFprimaryButton1" type="primary">{t('General.GetStarted')}</Button>
                              </Link>}
                            <div className="one-time-setup-tag">
                              *{t("pricing.NosetupCost")}
                            </div>
                          </div>
                        </div>
                      </TabPane>
                    </Tabs> */}
                  </div>
                </Col>
                {/* End Website Package */}
                {/* Web App Package */}
                <Col xs={24} md={8} lg={8}>
                  <div
                    className="pack-card ultra"
                    style={{ marginTop: "-10px" }}
                  >
                    <div className="Pack-identifier ultra">
                      <p>
                        {" "}
                        <span>{t("pricing.mobileweb")}</span>
                        <br />
                      </p>
                    </div>
                    <div className="package-img-cont">
                      <img className="package-img" src={WebApp} alt="" />
                    </div>

                    <div style={{ textAlign: "center" }}>
                      <Title className="package-title" level={3}>
                        {" "}
                        {t("pricing.ULTRA")}
                      </Title>
                    </div>
                    <div
                      style={{
                        textAlign: "center",
                        margin: "10px auto",
                        fontSize: "14px"
                      }}
                    >
                      <div
                        style={{
                          maxWidth: "230px",
                          borderBottom: "0.5px solid #ddd",
                          margin: "0px auto",
                          lineHeight: "2"
                        }}
                      >
                        {t("pricing.androidIos")}
                      </div>
                      <div
                        style={{
                          maxWidth: "230px",
                          borderBottom: "0.5px solid #ddd",
                          margin: "0px auto",
                          lineHeight: "2"
                        }}
                      >
                        {t("pricing.ecommWebsite")}
                      </div>
                      <div
                        style={{
                          maxWidth: "150px",
                          margin: "0px auto",
                          lineHeight: "2"
                        }}
                      >
                        {t("pricing.allFeatures")}
                      </div>

                      <div></div>
                    </div>

                    {/* New Tags */}

                    <div>
                          <Title level={2} class="light-text" style={{ marginTop: "10px", marginBottom: "30px", textAlign: "center" }} >
                            <div className="before-discount-price"> {currency}&nbsp;
                              <CurrencyFormat value={Number(this.state.WABDSP)} displayType={"text"} thousandSeparator={true} />
                            </div>&nbsp;
                            <span className="PriceNum">
                              <span className="currency">{currency}</span>
                              <CurrencyFormat value={Number(this.state.WAADSP)} displayType={"text"} thousandSeparator={true} />
                              <sup className="sup-star">*</sup>
                              <span></span>
                              <span className="per-month-tag">/Mo</span>
                            </span>
                          </Title>
                          <div style={{ textAlign: "center" }}>
                            {/* {console.log(this.state.USER)} */}
                            {this.state.USER != null ? <Link to="/eCommerce-Single-Vendor-Platform/Ultra/Standard" >
                              <Button className="UAFprimaryButton1" type="primary">{t('General.GetStarted')}</Button>
                            </Link> : <Link to="/login" >
                                <Button className="UAFprimaryButton1" type="primary">{t('General.GetStarted')}</Button>
                              </Link>
                            }
                            <div className="one-time-setup-tag">
                              *{t("pricing.oneTimeSetup")} {currency} {" "}
                              <CurrencyFormat value={WMSC} displayType={"text"} thousandSeparator={true} /> {" "} {t("pricing.willApply")}
                            </div>
                          </div>
                        </div>
                      

                    {/* <Tabs className="package-tabs" defaultActiveKey="1">
                      <TabPane tab={t("pricing.StandardPlan")} key="1">
                        <div>
                          <Title level={2} class="light-text" style={{ marginTop: "10px", marginBottom: "30px", textAlign: "center" }} >
                            <div className="before-discount-price"> {currency}&nbsp;
                              <CurrencyFormat value={Number(this.state.WABDSP)} displayType={"text"} thousandSeparator={true} />
                            </div>&nbsp;
                            <span className="PriceNum">
                              <span className="currency">{currency}</span>
                              <CurrencyFormat value={Number(this.state.WAADSP)} displayType={"text"} thousandSeparator={true} />
                              <sup className="sup-star">*</sup>
                              <span></span>
                              <span className="per-month-tag">/Mo</span>
                            </span>
                          </Title>
                          <div style={{ textAlign: "center" }}>
                  
                            {this.state.USER != null ? <Link to="/eCommerce-Single-Vendor-Platform/Ultra/Standard" >
                              <Button className="UAFprimaryButton1" type="primary">{t('General.GetStarted')}</Button>
                            </Link> : <Link to="/login" >
                                <Button className="UAFprimaryButton1" type="primary">{t('General.GetStarted')}</Button>
                              </Link>
                            }
                            <div className="one-time-setup-tag">
                              *{t("pricing.oneTimeSetup")} {currency} {" "}
                              <CurrencyFormat value={WMSC} displayType={"text"} thousandSeparator={true} /> {" "} {t("pricing.willApply")}
                            </div>
                          </div>
                        </div>
                      </TabPane>
                      <TabPane tab={t("pricing.RentalPlan")} key="2">
                        <div>
                          <Title level={2} class="light-text" style={{ marginTop: "30px", marginBottom: "38px", textAlign: "center" }} > &nbsp;
                            <span className="PriceNum">
                              <span className="currency">{currency}</span>
                              <CurrencyFormat value={Number(this.state.WAFPP)} displayType={"text"} thousandSeparator={true} />
                              <sup className="sup-star">*</sup>
                              <span></span>
                              <span className="per-month-tag">/Mo</span>
                            </span>
                          </Title>
                          <div style={{ textAlign: "center" }}> {this.state.USER != null ? <Link to="/eCommerce-Single-Vendor-Platform/Ultra/Rental" >
                            <Button className="UAFprimaryButton1" type="primary">{t('General.GetStarted')}</Button>
                          </Link> : <Link to="/login" >
                              <Button className="UAFprimaryButton1" type="primary">{t('General.GetStarted')}</Button>
                            </Link>}

                            <div className="one-time-setup-tag">
                              *{t("pricing.NosetupCost")}
                            </div>
                          </div>
                        </div>
                      </TabPane>
                    </Tabs> */}

                    {/* New Tags end */}
                  </div>
                </Col>
                {/* End Web App Package */}
                {/* Mobile App Package */}
                <Col xs={24} md={8} lg={8}>
                  <div className="pack-card Premium">
                    <div className="Pack-identifier Premium">
                      <p>
                        {" "}
                        <span>{t("pricing.mobileapp")}</span>
                        {/* <br />{" "} */}
                      </p>
                    </div>
                    <div className="package-img-cont">
                      <img className="package-img" src={MobileApp} alt="" />
                    </div>
                    <div style={{ textAlign: "center" }}>
                      <Title className="package-title" level={3}>
                        {t("pricing.PREMIUM")}
                      </Title>
                    </div>
                    <div style={{ textAlign: "center", margin: "10px auto", fontSize: "14px" }} >
                      <div style={{ maxWidth: "230px", borderBottom: "0.5px solid #ddd", margin: "0px auto", lineHeight: "2" }} >
                        {t("pricing.androidIos")}
                      </div>
                      <div style={{ maxWidth: "150px", margin: "0px auto", lineHeight: "2" }} >
                        {t("pricing.allFeatures")}
                      </div>
                      <div></div>
                    </div>
                    {/* New Tags */}

                    <div>
                          <Title level={2} class="light-text" style={{ marginTop: "10px", marginBottom: "30px", textAlign: "center" }} >
                            <div className="before-discount-price"> {currency}&nbsp;
                              <CurrencyFormat value={Number(this.state.MBDSP)} displayType={"text"} thousandSeparator={true} />
                            </div> &nbsp;
                            <span className="PriceNum">
                              <span className="currency">{currency}</span>
                              <CurrencyFormat value={Number(this.state.MADSP)} displayType={"text"} thousandSeparator={true} />
                              <sup className="sup-star">*</sup>
                              <span></span>
                              <span className="per-month-tag">/Mo</span>
                            </span>
                          </Title>
                          <div style={{ textAlign: "center" }}> {this.state.USER != null ? <Link to="/eCommerce-Single-Vendor-Platform/Premium/Standard" >
                            <Button className="UAFprimaryButton1" type="primary">{t('General.GetStarted')}</Button>
                          </Link> : <Link to="/login" >
                              <Button className="UAFprimaryButton1" type="primary">{t('General.GetStarted')}</Button>
                            </Link>}
                            <div className="one-time-setup-tag"> *{t("pricing.oneTimeSetup")} {currency} {" "}
                              <CurrencyFormat value={MSC} displayType={"text"} thousandSeparator={true} /> {" "}
                              {t("pricing.willApply")}
                            </div>
                          </div>
                        </div>
                      
{/* 
                    <Tabs className="package-tabs" defaultActiveKey="1">
                      <TabPane tab={t("pricing.StandardPlan")} key="1">
                        <div>
                          <Title level={2} class="light-text" style={{ marginTop: "10px", marginBottom: "30px", textAlign: "center" }} >
                            <div className="before-discount-price"> {currency}&nbsp;
                              <CurrencyFormat value={Number(this.state.MBDSP)} displayType={"text"} thousandSeparator={true} />
                            </div> &nbsp;
                            <span className="PriceNum">
                              <span className="currency">{currency}</span>
                              <CurrencyFormat value={Number(this.state.MADSP)} displayType={"text"} thousandSeparator={true} />
                              <sup className="sup-star">*</sup>
                              <span></span>
                              <span className="per-month-tag">/Mo</span>
                            </span>
                          </Title>
                          <div style={{ textAlign: "center" }}> {this.state.USER != null ? <Link to="/eCommerce-Single-Vendor-Platform/Premium/Standard" >
                            <Button className="UAFprimaryButton1" type="primary">{t('General.GetStarted')}</Button>
                          </Link> : <Link to="/login" >
                              <Button className="UAFprimaryButton1" type="primary">{t('General.GetStarted')}</Button>
                            </Link>}
                            <div className="one-time-setup-tag"> *{t("pricing.oneTimeSetup")} {currency} {" "}
                              <CurrencyFormat value={MSC} displayType={"text"} thousandSeparator={true} /> {" "}
                              {t("pricing.willApply")}
                            </div>
                          </div>
                        </div>
                      </TabPane>
                      <TabPane tab={t("pricing.RentalPlan")} key="2">
                        <div>
                          <Title level={2} class="light-text" style={{ marginTop: "30px", marginBottom: "38px", textAlign: "center" }} >&nbsp;
                            <span className="PriceNum">
                              <span className="currency">{currency}</span>
                              <CurrencyFormat value={Number(this.state.MFPP)} displayType={"text"} thousandSeparator={true} />
                              <sup className="sup-star">*</sup>
                              <span></span>
                              <span className="per-month-tag">/Mo</span>
                            </span>
                          </Title>
                          <div style={{ textAlign: "center" }}>
                    

                            {this.state.USER != null ? <Link
                              to="/eCommerce-Single-Vendor-Platform/Premium/Rental"
                            >
                              <Button className="UAFprimaryButton1" type="primary">{t('General.GetStarted')}</Button>
                            </Link> : <Link
                              to="/login"
                            >
                                <Button className="UAFprimaryButton1" type="primary">{t('General.GetStarted')}</Button>
                              </Link>}
                            <div className="one-time-setup-tag">
                              *{t("pricing.NosetupCost")}
                            </div>
                          </div>
                        </div>
                      </TabPane>
                    </Tabs> */}

                    {/* New Tags end */}
                  </div>
                </Col>
                {/* End Mobile App Package */}
              </div>
            </Row>

          </Col>
        </Row>
      </div>
    );
  }
}
PackagePriceVerticleEcommerce.propTypes = {
  ...withDirectionPropTypes,
};

export default withTranslation()(withDirection(PackagePriceVerticleEcommerce));
