/* eslint react/prop-types: 0 */
import React, { Component } from "react";
import { connect } from "react-redux";
import "../../assets/css/packagePrice.css";
import { Row, Col, Typography, Radio, Button } from "antd";
import { Link } from "react-router-dom";
const { Title } = Typography;
export class PackagePrice extends Component {
  constructor(props) {
    super(props);
    // Don't call this.setState() here!
    this.state = {
      BDSPM: this.props.BDSPM,
      ADSPM: this.props.ADSPM,
      BDSPY: this.props.BDSPY,
      ADSPY: this.props.ADSPY,
      Duration: this.props.duration,
      currency: this.props.currency,
      BDSP: this.props.BDSPY,
      ADSP: this.props.ADSPY,
      MDP: "0",
      YDP: "0",
      DP: (
        ((+this.props.BDSPY - +this.props.ADSPY) / +this.props.BDSPY) *
        100
      ).toFixed(1)
    };
  }

  componentWillReceiveProps(nextApp) {
    this.setState({
      BDSPM: nextApp.BDSPM,
      ADSPM: nextApp.ADSPM,
      BDSPY: nextApp.BDSPY,
      ADSPY: nextApp.ADSPY,
      Duration: nextApp.duration,
      currency: nextApp.currency,
      BDSP: nextApp.BDSPY,
      ADSP: nextApp.ADSPY,
      MDP: "0",
      YDP: "0",
      DP: (((+nextApp.BDSPY - +nextApp.ADSPY) / +nextApp.BDSPY) * 100).toFixed(
        0
      )
    });
  }

  handleChange = e => {
    if (e.target.value === "Year") {
      this.setState({
        BDSP: this.state.BDSPY,
        ADSP: this.state.ADSPY,
        DP: (
          ((+this.state.BDSPY - +this.state.ADSPY) / +this.state.BDSPY) *
          100
        ).toFixed(1)
      });
    } else if (e.target.value === "Month") {
      this.setState({
        BDSP: this.state.BDSPM,
        ADSP: this.state.ADSPM,
        DP: (
          ((+this.state.BDSPM - +this.state.ADSPM) / +this.state.BDSPM) *
          100
        ).toFixed(1)
      });
    }
  };

  render() {
    const SLUG = this.props.SLUG;
    const ID = this.props.ID;
    const currency = this.props.currency;

    return (
      <div>
        <Row
          gutter={16}
          type="flex"
          justify="space-around"
          align="middle"
          style={{ margin: "0px 0px", border: "0.5px solid #eee" }}
        >
          <Col
            lg={6}
            xs={24}
            className="subs-bg-blue"
            style={{ margin: "0px -10px" }}
          >
            <div style={{ textAlign: "center", marginTop: "30px" }}>
              <Radio.Group
                defaultValue="Year"
                buttonStyle="solid"
                onChange={this.handleChange}
                defaultValue="Year"
                size="large"
                className="Pack-chooser-container"
                shape="circle"
              >
                <Radio.Button className="year" value="Year">
                  Yearly
                </Radio.Button>
                <Radio.Button className="month" value="Month">
                  Monthly
                </Radio.Button>
              </Radio.Group>
            </div>
          </Col>
          <Col lg={5} xs={24} style={{ textAlign: "center" }}>
            <Title level={4} class="light-text">
              Base Price
            </Title>
            <Title
              level={2}
              class="light-text"
              style={{ opacity: "0.5", textDecoration: "line-through" }}
            >
              {currency}&nbsp;{Number(this.state.BDSP)}
            </Title>
          </Col>
          <Col lg={5} xs={24} style={{ textAlign: "center" }}>
            <Title level={4} class="light-text">
              Price After Discount
            </Title>
            <Title level={2} class="light-text">
              {currency}&nbsp;{Number(this.state.ADSP)}
            </Title>
          </Col>
          <Col lg={2} xs={24} style={{ textAlign: "center" }}>
            <Title level={4} class="light-text">
              Discount
            </Title>
            <Title level={2} style={{ color: "#4CAF50" }}>
              {Number(this.state.DP)}%
            </Title>
          </Col>
          <Col lg={5} xs={24} style={{ textAlign: "center" }}>
            <Link
              to={{
                pathname: !this.props.user
                  ? `/Login`
                  : `/store/${SLUG}/${ID}/checkout/${Number(this.state.ADSP)}`
              }}
            >
              <Button type="primary" size="large">
                Get Started Now
              </Button>
            </Link>
          </Col>
        </Row>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  user: state.auth.user
});

const mapDispatchToProps = {};

export default connect(mapStateToProps, mapDispatchToProps)(PackagePrice);
