import React, { useState, useEffect, Fragment } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import BotPriceVertical from "./BotPriceVertical";
import {Typography, Select } from "antd";
import { loadApplication } from "../../actions/application.action";
import { useTranslation } from 'react-i18next';


const Subscription = ({ direction }) => {
    const [state, setState] = useState({
        selectApplication: "Ecommerce"
    });

    const { t } = useTranslation();

    const [appDetails, setAppData] = useState({
        apps: [],
        currency: "",
        ProfessionalPrice:"",
        BusinessPrice:""

    });
    const {
        country,
        currency,
        ProfessionalPrice,
        BusinessPrice

    } = appDetails;


    useEffect(() => {
        
            setAppData({
                ...appDetails,

                country: sessionStorage.getItem("country"),
                currency: sessionStorage.getItem("currency_sign"),
                // Website
                ProfessionalPrice: Math.round(
                    99 * sessionStorage.getItem("currency_rate")
                ),
                BusinessPrice: Math.round(
                    499 * sessionStorage.getItem("currency_rate")
                ),
                
            });

    }, [sessionStorage.getItem("currency")]);


    return (

        <Fragment>
            <div style={{ margin: "40px auto 20px auto" }}>
                <BotPriceVertical
                    currency={currency}
                    ProfessionalPrice={ProfessionalPrice}
                    BusinessPrice={BusinessPrice}
                />
            </div>
        </Fragment>
    );
};

Subscription.propTypes = {
    user: PropTypes.object.isRequired,
    loadApplication: PropTypes.func.isRequired,
    Applications: PropTypes.array.isRequired,
    loading: PropTypes.bool
};

const mapStateToProps = state => ({
    user: state.auth.user,
    Applications: state.application.Applications,
    loading: state.application.loading
});
export default connect(mapStateToProps, {
    loadApplication
    // getLocation
})(Subscription);
