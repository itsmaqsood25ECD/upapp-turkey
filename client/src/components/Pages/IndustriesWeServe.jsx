import React from "react";
import PropTypes from "prop-types";
import { Row, Col, Button } from "antd";
import { Link } from "react-router-dom";
import { useTranslation} from 'react-i18next';
import i18next from 'i18next';

// industry we searve Icons

import healthcare from "../../assets/img/Home Page images/Industries/healthcare.svg";
import Education from "../../assets/img/Home Page images/Industries/Education.svg";
import Entertainment from "../../assets/img/Home Page images/Industries/Entertainment.svg";
import Beauty from "../../assets/img/Home Page images/Industries/Beauty.svg";
import HomeDecoration from "../../assets/img/Home Page images/Industries/HomeDecoration.svg";
import Retail from "../../assets/img/Home Page images/Industries/Retail.svg";
import JobCareer from "../../assets/img/Home Page images/Industries/JobCareer.svg";
import Travel from "../../assets/img/Home Page images/Industries/Travel.svg";
import Transport from "../../assets/img/Home Page images/Industries/Transport.svg";
import Food from "../../assets/img/Home Page images/Industries/Food.svg";
import RealEstate from "../../assets/img/Home Page images/Industries/RealEstate.svg";
import Sports from "../../assets/img/Home Page images/Industries/Sports.svg";
import Dating from "../../assets/img/Home Page images/Industries/Dating.svg";
// Industries End



function IndustriesWeServe(props) {

  const { t } = useTranslation();

  const industries = [
    {
      icon: healthcare,
      name: `${t('Home.industry.Icons.Healthcare')}`,
      link: "/factory/healthcare"
    },
    ,
    {
      icon: Education,
      name: `${t('Home.industry.Icons.Education')}`,
      link: "/factory/service"
    },
    {
      icon: Entertainment,
      name: `${t('Home.industry.Icons.Entertainment')}`,
      link: "/factory"
    },
    {
      icon: Beauty,
      name: `${t('Home.industry.Icons.Beauty')}`,
      link: "/factory/beauty"
    },
    {
      icon: HomeDecoration,
      name: `${t('Home.industry.Icons.HomeDecoration')}`,
      link: "/factory/retail"
    },
    {
      icon: Travel,
      name: `${t('Home.industry.Icons.Travel')}`,
      link: "/factory"
    },
    {
      icon: Retail,
      name: `${t('Home.industry.Icons.Retail')}`,
      link: "/factory/retail"
    },
    {
      icon: Food,
      name: `${t('Home.industry.Icons.Food&Beverage')}`,
      link: "/factory/retail"
    },
    {
      icon: RealEstate,
      name: `${t('Home.industry.Icons.RealEstate')}`,
      link: "/facility-management-application"
    },
    {
      icon: Sports,
      name: `${t('Home.industry.Icons.Adventure&Sports')}`,
      link: "/factory/sports"
    }
  ];

  return (
    <div style={{ textAlign: "center" }}>
      <Row gutter={16} type="flex" justify="center">
        {industries.map(icon => {
          return (
            <Col xs={12} md={6} lg={4}>
              <Link to={icon.link}>
                <div className="industry-card" style={{ marginBottom: "15px" }}>
                  <img
                    style={{ width: "80px", height: "80px" }}
                    src={icon.icon}
                    alt={icon.name}
                  />
                  <p style={{ marginTop: "15px" }}>{icon.name}</p>
                </div>
              </Link>
            </Col>
          );
        })}
      </Row>
    </div>
  );
}

IndustriesWeServe.propTypes = {};

export default IndustriesWeServe;
