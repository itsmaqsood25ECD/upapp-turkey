import React, { Fragment, useState, useEffect } from "react";
import { connect } from "react-redux";
import { Link } from "react-router-dom";
import {
	Table,
	Button,
	Input,
	Icon,
	Divider,
	Row,
} from "antd";
import PropTypes from "prop-types";
import { GetCategory } from '../../../actions/admin/category.management.action'
let searchInput = React.createRef();

const List = ({ Category, GetCategory, refresh }) => {
	const [formData, setFormData] = useState({
		filteredInfo: null,
		sortedInfo: null,
		selectedRowKeys: [],
		searchText: "",
		coupon_code: ""
	});
	let { filteredInfo, sortedInfo, selectedRowKeys } = useState;
	const data = [];

	useEffect(() => {
		GetCategory()
	}, [refresh])

	Category &&
		Category.map(arg => {
			let Ids = {
				id: arg._id,
				name: arg.name,
				type: arg.type
			};
			data.push(Ids);
		});


	//  Selecting rows

	const onSelectChange = selectedRowKeys => {
		setFormData({ selectedRowKeys });
	};
	const rowSelection = {
		selectedRowKeys,
		onChange: onSelectChange,
		hideDefaultSelections: true,
		selections: [
			{
				key: "all-data",
				text: "Select All Data",
				onSelect: () => {
					setFormData({
						selectedRowKeys: [...Array(46).keys()] // 0...45
					});
				}
			},
			{
				key: "odd",
				text: "Select Odd Row",
				onSelect: changableRowKeys => {
					let newSelectedRowKeys = [];
					newSelectedRowKeys = changableRowKeys.filter((key, index) => {
						if (index % 2 !== 0) {
							return false;
						}
						return true;
					});
					setFormData({
						selectedRowKeys: newSelectedRowKeys
					});
				}
			},
			{
				key: "even",
				text: "Select Even Row",
				onSelect: changableRowKeys => {
					let newSelectedRowKeys = [];
					newSelectedRowKeys = changableRowKeys.filter((key, index) => {
						if (index % 2 !== 0) {
							return true;
						}
						return false;
					});
					setFormData({
						selectedRowKeys: newSelectedRowKeys
					});
				}
			}
		]
	};
	//   End Selecting Rows
	// Search column

	const getColumnSearchProps = dataIndex => ({
		filterDropdown: ({
			setSelectedKeys,
			selectedKeys,
			confirm,
			clearFilters
		}) => (
				<div style={{ padding: 8 }}>
					<Input
						ref={node => {
							searchInput = node;
						}}
						placeholder={`Search ${dataIndex}`}
						value={selectedKeys[0]}
						onChange={e =>
							setSelectedKeys(e.target.value ? [e.target.value] : [])
						}
						onPressEnter={() => handleSearch(selectedKeys, confirm)}
						style={{
							width: 188,
							marginBottom: 8,
							display: "block"
						}}
					/>
					<Button
						type="primary"
						onClick={() => handleSearch(selectedKeys, confirm)}
						icon="search"
						size="small"
						style={{
							width: 90,
							marginRight: 8
						}}
					>
						Search
				</Button>
					<Button
						onClick={() => handleReset(clearFilters)}
						size="small"
						style={{ width: 90 }}
					>
						Reset
				</Button>
				</div>
			),
		filterIcon: filtered => (
			<Icon
				type="search"
				style={{
					color: filtered ? "#1890ff" : undefined
				}}
			/>
		),
		onFilter: (value, record) =>
			record[dataIndex]
				.toString()
				.toLowerCase()
				.includes(value.toLowerCase()),
		onFilterDropdownVisibleChange: visible => {
			if (visible) {
				setTimeout(() => searchInput.select());
			}
		}
	});
	const handleSearch = (selectedKeys, confirm) => {
		confirm();
		setFormData({ searchText: selectedKeys[0] });
	};
	const handleReset = clearFilters => {
		clearFilters();
		setFormData({ searchText: "" });
	};

	// End Search column
	const handleChange = (pagination, filters, sorter) => {
		setFormData({
			filteredInfo: filters,
			sortedInfo: sorter
		});
	};

	sortedInfo = sortedInfo || {};
	filteredInfo = filteredInfo || {};

	const columns = [
		{
			title: "Name",
			dataIndex: "name",
			key: 1,
			...getColumnSearchProps("name"),
			sorter: (a, b) => a.name.length - b.name.length,
			sortOrder: sortedInfo.columnKey === 1 && sortedInfo.order
		},
		{
			title: "Type",

			dataIndex: "type",
			key: 2,
			...getColumnSearchProps("type"),
			sorter: (a, b) => a.type.length - b.type.length,
			sortOrder: sortedInfo.columnKey === 1 && sortedInfo.order
		},
		{
			title: "Action",
			dataIndex: "id",
			key: 3,
			render: id => (
				<span>
					<Link to={`/cms/admin/edit-category/${id}`}>
						<Button type="dashed" size="small">
							<Icon type="edit" />
						</Button>
					</Link>
					<Button type="dashed" size="small">
						<Icon type="delete" />
					</Button>
				</span>
			)
		}
	];
	return (
		<Fragment>
			<Row>
				<Link to="/cms/admin/category-management/new-category">
					<Button
						type="primary"
						size={"large"}
						style={{
							float: "right",
							marginTop: "10px"
						}}
					>
						ADD NEW
					</Button>
				</Link>
			</Row>
			<Divider orientation="left">Category Management</Divider>
			<Table
				rowSelection={rowSelection}
				columns={columns}
				dataSource={data}
				onChange={handleChange}
				bordered
				size="small"
			/>
		</Fragment>
	);
};

List.propTypes = {
	Category: PropTypes.array.isRequired,
	refresh: PropTypes.bool,
	GetCategory: PropTypes.func.isRequired,

};

const mapStateToProps = state => ({
	Category: state.categoryManagement.Category,
	refresh: state.categoryManagement.refresh
});

export default connect(
	mapStateToProps,
	{ GetCategory }
)(List);
