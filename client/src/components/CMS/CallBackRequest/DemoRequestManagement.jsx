import React, { Fragment, useState, useEffect } from "react";
import { connect } from "react-redux";
import moment from "moment";
import { Allcontact } from "../../../actions/admin/request.management.action";
import { Table, Button, Input, Icon, Tabs, Modal, Row, Col, Descriptions } from "antd";
import PropTypes from "prop-types";
import { CSVLink } from "react-csv";
import { Card } from "antd";
import { CountryDropdown } from "react-country-region-selector";

const { TabPane } = Tabs;

let searchInput = React.createRef();

const CallBackRequest = ({ Allcontact, allcontact }) => {
  const [Data, setData] = useState({
    filteredInfo: null,
    sortedInfo: null,
    selectedRowKeys: [],
    searchText: "",
    data:[],
    visible:false,
    details:'',
    country:''
  });

  const[abc, SetAbc] = useState(0);


  let { filteredInfo, sortedInfo, selectedRowKeys, data, visible, details, country } = Data;

  const filterDemoRequestData = () => {
    setData({
      ...Data,
      country:'',
      data: allcontact && allcontact.filter((allcontact, index) => {
       return allcontact.type === "demo"
      })
    })
  }


  useEffect(() => {
    Allcontact();
    filterDemoRequestData();
    console.log(data)
  }, [abc]);


const searchDataHandle = e => {
    console.log(e.target.value)
      if(e.target.value === ""){
        setData({
          ...Data,
          data: allcontact && allcontact.filter((allcontact, index) => {
           return allcontact.type === "demo"
          })
        })
      } else {
        const searchData =  data && data.filter(s => {
          return (s.ticket_no.toString().toLowerCase().includes((e.target.value).toLocaleLowerCase())) ||
          (s.name.toString().toLowerCase().includes((e.target.value).toLocaleLowerCase())) ||
          (s.email.toString().toLowerCase().includes((e.target.value).toLocaleLowerCase())) ||
            (s.country.toString().toLowerCase().includes((e.target.value).toLocaleLowerCase())) 
             }) 

              console.log(searchData)
          setData({
            ...Data,
            data:searchData
          })
      }
}

const selectCountry = val => {

  const CountryData =  data && data.filter((d, index) => {
    return d.country === val
  })

  setData({
    ...Data,
    country:val,
    data: CountryData
  })

}

  // allcontact &&
  //   allcontact.map((allcontact, index) => {
  //     if (allcontact.type === "demo") {
  //       let demoIds = {
  //         Submit_date: moment(allcontact.createdAt).format("LLL"),
  //         interested_in: allcontact.interested_in,
  //         country: allcontact.country,
  //         description: allcontact.description,
  //         designation: allcontact.designation,
  //         csize: allcontact.csize,
  //         email: allcontact.email,
  //         mobile: allcontact.mobile,
  //         name: allcontact.name,
  //         ticket_no: allcontact.UID
  //       };
  //       dataDemo.push(demoIds);
  //     }
  //   });


  const MoreDetails = (text, record) => {
    showModal(text, record);
  };
  
  const showModal = (text, record) => {
    setData({
      ...Data,
      visible: true,
      details: record,
  
    });
  };
  const handleOk = e => {
    setData({
      ...Data,
      visible: false
    });
  };
  
  const handleCancel = e => {
    setData({
      ...Data,
      visible: false
    });
  };

  const ClearFilter = e => {
    setData({
      ...Data,
      visible: false
    });
  };

  



  const handleChange = (pagination, filters, sorter) => {
    setData({
      ...Data,
      filteredInfo: filters,
      sortedInfo: sorter
    });
  };

  sortedInfo = sortedInfo || {};
  filteredInfo = filteredInfo || {};
  

  const DemoColumns = [
    {
      title: "Ticket No",
      // width: 150,
      dataIndex: "ticket_no",
      key: 1,
      sorter: (a, b) => a.ticket_no.length - b.ticket_no.length,
      sortOrder: sortedInfo.columnKey === 1 && sortedInfo.order
    },
    {
      title: "Name",
      dataIndex: "name",
      // width: 100,
      key: 2,
      sorter: (a, b) => a.name.length - b.name.length,
      sortOrder: sortedInfo.columnKey === 2 && sortedInfo.order
    },
    {
      title: "Email",
      dataIndex: "email",
      // width: 100,
      key: 3,
      sorter: (a, b) => a.email.length - b.email.length,
      sortOrder: sortedInfo.columnKey === 3 && sortedInfo.order
    },
    {
      title: "Mobile No",
      dataIndex: "mobile",
      // width: 50,
      key: 4,
      sorter: (a, b) => a.mobile.length - b.mobile.length,
      sortOrder: sortedInfo.columnKey === 4 && sortedInfo.order
    },
    {
      title: "Country",
      dataIndex: "country",
      // width: 50,
      key: 5,
      sorter: (a, b) => a.country.length - b.country.length,
      sortOrder: sortedInfo.columnKey === 4 && sortedInfo.order
    },
    {
      title: "Interested in",
      dataIndex: "interested_in",
      key: 6,
    },
    {
      title: "Date",
      dataIndex: "createdAt",
      key: 8,
      // width: 50,
      sorter: (a, b) => a.createdAt.length - b.createdAt.length,
      sortOrder: sortedInfo.columnKey === 5 && sortedInfo.order,
      render: (createdAt)  => (
        <div style={{width:"100px"}}>
          {moment(createdAt).format("ll")}
        </div>
      )
    },
    {
			title: "Action",
			dataIndex: "description",
			// fixed: "right",
			key: 5,
			// width: 120,

			render: (text, record)  => (
				<div style={{textAlign:"center",width:"100%"}}>
					<Button
						type="dashed"
						size="small"
						onClick={() => MoreDetails(text, record)}
					>
						<Icon type="info-circle" />
					</Button>
          </div>
      )
    },
  ];

 

  return (
    <Fragment>
     <Card
            title="Request For Demo"
            extra={
              <CSVLink data={data} filename={"demo-requests.csv"}>
                <Button type="dashed">Download CSV</Button>
              </CSVLink>
            }
          >
            <Row gutter={16}>
              <Col xs={24} lg={10}>
              <Input style={{width:"100%"}} onChange={searchDataHandle} placeholder="Search By Ticket No, Name, Email"  />
              </Col>
              <Col xs={24} lg={10}>
              <CountryDropdown
              blacklist={[
                "AF",
                "AO",
                "DJ",
                "GQ",
                "ER",
                "GA",
                "IR",
                "KG",
                "LY",
                "MD",
                "NP",
                "ST",
                "SL",
                "SD",
                "SY",
                "SR",
                "TM",
                "VE",
                "ZW",
                "IL"
              ]}
              name="country"
              value={country}
              valueType="full"
              style={{height: "32px" }}
              onChange={val => selectCountry(val)}
            />
              </Col>
              <Col xs={24} lg={4}>
              <Button onClick={filterDemoRequestData} >
          Clear Filter
        </Button>
              </Col>
            </Row>
            
            <br/>
            <br/>
            <Table
              columns={DemoColumns}
              dataSource={data}
              // pagination={{ pageSizeOptions: ['10', '20', '30', '40'] }}
              onChange={handleChange}
            />
          </Card>

          <Modal
				title="Details"
				visible={Data.visible}
				onOk={handleOk}
				onCancel={handleCancel}
			>
        {console.log(details)}

        <Descriptions
            title=""
            size="small"
            bordered
            column={{ xxl: 4, xl: 1, lg: 1, md: 1, sm: 1, xs: 1 }}
          >
					<Descriptions.Item label="UID">
          {details.UID}
					</Descriptions.Item>
          <Descriptions.Item label="Name">
          {details.name}
					</Descriptions.Item>
          <Descriptions.Item label="Email">
          {details.email}
					</Descriptions.Item>
          <Descriptions.Item label="Country">
          {details.country}
					</Descriptions.Item>
          <Descriptions.Item label="Intrested In">
          {details.interested_in}
					</Descriptions.Item>
          <Descriptions.Item label="Business Name">
          {details.business_name}
					</Descriptions.Item>
          <Descriptions.Item label="Mobile">
          {details.mobile}
					</Descriptions.Item>
          <Descriptions.Item label="Description">
          {details.description}
					</Descriptions.Item>
          <Descriptions.Item label="Posted In">
          {moment(details.createdAt).format("ll")}
					</Descriptions.Item>


          </Descriptions>

        </Modal>
        
    </Fragment>
  );
};

CallBackRequest.propTypes = {
  Allcontact: PropTypes.func.isRequired,
  allcontact: PropTypes.object.isRequired
};

const mapStateToProps = state => ({
  allcontact: state.requestManagement.requests
});

export default connect(mapStateToProps, { Allcontact })(CallBackRequest);
