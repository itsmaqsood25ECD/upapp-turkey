import React, { Fragment, useState, useEffect } from "react";
import { connect } from "react-redux";
import moment from "moment";
import { Allcontact } from "../../../actions/admin/request.management.action";
import {  Table, Button, Input, Icon, Tabs, Modal, Row, Col, Descriptions  } from "antd";
import PropTypes from "prop-types";
import { CSVLink } from "react-csv";
import { Card } from "antd";
import { CountryDropdown } from "react-country-region-selector";

const { TabPane } = Tabs;

let searchInput = React.createRef();
const CallBackRequest = ({ Allcontact, allcontact }) => {
  const [Data, setData] = useState({
    filteredInfo: null,
    sortedInfo: null,
    selectedRowKeys: [],
    searchText: "",
    data:[],
    visible:false,
    details:'',
    country:''
  });


  const[abc, SetAbc] = useState(0);

  let { filteredInfo, sortedInfo, selectedRowKeys, data, visible, details, country } = Data;
  

  const filterCustomRequestData = () => {
    setData({
      ...Data,
      country:'',
      data: allcontact && allcontact.filter((allcontact, index) => {
       return allcontact.type === "appCustomizationRequest"
      })
    })
  }

  useEffect(() => {
    Allcontact();
    filterCustomRequestData();
    console.log(data)
  }, [abc]);
  console.log(allcontact);


  const searchDataHandle = e => {
    console.log(e.target.value)
      if(e.target.value === ""){
        setData({
          ...Data,
          data: allcontact && allcontact.filter((allcontact, index) => {
           return allcontact.type === "appCustomizationRequest"
          })
        })
      } else {
        const searchData =  data && data.filter(s => {
          return (s.ticket_no.toString().toLowerCase().includes((e.target.value).toLocaleLowerCase())) ||
          (s.name.toString().toLowerCase().includes((e.target.value).toLocaleLowerCase())) ||
          (s.email.toString().toLowerCase().includes((e.target.value).toLocaleLowerCase())) ||
            (s.country.toString().toLowerCase().includes((e.target.value).toLocaleLowerCase())) 
             }) 

              console.log(searchData)
          setData({
            ...Data,
            data:searchData
          })
      }
}

const selectCountry = val => {

  const CountryData =  data && data.filter((d, index) => {
    return d.country === val
  })

  setData({
    ...Data,
    country:val,
    data: CountryData
  })

}


  //  Selecting rows
  const onSelectChange = selectedRowKeys => {
    setData({ ...Data, selectedRowKeys });
  };

  const rowSelection = {
    selectedRowKeys,
    onChange: onSelectChange,
    hideDefaultSelections: true,
    selections: [
      {
        key: "all-data",
        text: "Select All Data",
        onSelect: () => {
          setData({
            ...Data,
            selectedRowKeys: [...Array(46).keys()] // 0...45
          });
        }
      },
      {
        key: "odd",
        text: "Select Odd Row",
        onSelect: changableRowKeys => {
          let newSelectedRowKeys = [];
          newSelectedRowKeys = changableRowKeys.filter((key, index) => {
            if (index % 2 !== 0) {
              return false;
            }
            return true;
          });
          setData({  ...Data, selectedRowKeys: newSelectedRowKeys });
        }
      },
      {
        key: "even",
        text: "Select Even Row",
        onSelect: changableRowKeys => {
          let newSelectedRowKeys = [];
          newSelectedRowKeys = changableRowKeys.filter((key, index) => {
            if (index % 2 !== 0) {
              return true;
            }
            return false;
          });
          setData({  ...Data, selectedRowKeys: newSelectedRowKeys });
        }
      }
    ]
  };

  //   End Selecting Rows

  // Search column

  // End Search column

  const handleChange = (pagination, filters, sorter) => {
    setData({
      ...Data,
      filteredInfo: filters,
      sortedInfo: sorter
    });
  };

  sortedInfo = sortedInfo || {};
  filteredInfo = filteredInfo || {};
 
  const CustomAppColumns = [
    {
      title: "Ticket No",
      // width: 150,
      dataIndex: "ticket_no",
      key: 1,
   
      sorter: (a, b) => a.ticket_no.length - b.ticket_no.length,
      sortOrder: sortedInfo.columnKey === 1 && sortedInfo.order
    },
    {
      title: "name",
      dataIndex: "name",
      // width: 100,
      key: 2,
      sorter: (a, b) => a.name.length - b.name.length,
      sortOrder: sortedInfo.columnKey === 2 && sortedInfo.order
    },
    {
      title: "Email",
      dataIndex: "email",
      // width: 100,
      key: 3,
      sorter: (a, b) => a.email.length - b.email.length,
      sortOrder: sortedInfo.columnKey === 3 && sortedInfo.order
    },
    {
      title: "Mobile No",
      dataIndex: "mobile",
      // width: 50,
      key: 4,
      sorter: (a, b) => a.mobile.length - b.mobile.length,
      sortOrder: sortedInfo.columnKey === 4 && sortedInfo.order
    },
    ,
    {
      title: "Country",
      dataIndex: "country",
      key: 5,
      // width: 50,
      sorter: (a, b) => a.country.length - b.country.length,
      sortOrder: sortedInfo.columnKey === 5 && sortedInfo.order
    },
    // {
    // 	title: "Requested At",
    // 	dataIndex: "created_at",
    // 	key: 5,
    // 	// width: 50,
    // 	...getColumnSearchProps("created_at"),
    // 	sorter: (a, b) => a.created_at.length - b.created_at.length,
    // 	sortOrder: sortedInfo.columnKey === 5 && sortedInfo.order
    // },
    {
      title: "Subject",
      dataIndex: "subject",
      key: 6,
      // width: 50,
      sorter: (a, b) => a.subject.length - b.subject.length,
      sortOrder: sortedInfo.columnKey === 5 && sortedInfo.order
    },
    {
      title: "Description",
      dataIndex: "description",
      key: 7,
      sorter: (a, b) => a.description.length - b.description.length,
      sortOrder: sortedInfo.columnKey === 5 && sortedInfo.order
    }
  ];

 

  return (
    <Fragment>
       <Card
            title="App Customization Request"
            extra={
              <CSVLink data={data} filename={"custom-app.csv"}>
                <Button type="dashed">Download CSV</Button>
              </CSVLink>
            }
          >
             <Row gutter={16}>
              <Col xs={24} lg={10}>
              <Input style={{width:"100%"}} onChange={searchDataHandle} placeholder="Search By Ticket No, Name, Email"  />
              </Col>
              <Col xs={24} lg={10}>
              <CountryDropdown
              blacklist={[
                "AF",
                "AO",
                "DJ",
                "GQ",
                "ER",
                "GA",
                "IR",
                "KG",
                "LY",
                "MD",
                "NP",
                "ST",
                "SL",
                "SD",
                "SY",
                "SR",
                "TM",
                "VE",
                "ZW",
                "IL"
              ]}
              name="country"
              value={country}
              valueType="full"
              style={{height: "32px" }}
              onChange={val => selectCountry(val)}
            />
              </Col>
              <Col xs={24} lg={4}>
              <Button onClick={filterCustomRequestData} >
          Clear Filter
        </Button>
              </Col>
            </Row>
            
            <br/>
            <br/>

            <Table
              rowSelection={rowSelection}
              columns={CustomAppColumns}
              dataSource={data}
              onChange={handleChange}
            />
          </Card>
        
    </Fragment>
  );
};

CallBackRequest.propTypes = {
  Allcontact: PropTypes.func.isRequired,
  allcontact: PropTypes.object.isRequired
};

const mapStateToProps = state => ({
  allcontact: state.requestManagement.requests
});

export default connect(mapStateToProps, { Allcontact })(CallBackRequest);
