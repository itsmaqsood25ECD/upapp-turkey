import React, { Fragment, useState, useEffect } from "react";
import { Row, Col, Input, Form, Button, Select, Tabs, Upload, Icon, message } from "antd";
import { Link, Redirect } from "react-router-dom";
import { connect } from "react-redux";
import axios from 'axios'
import { PostBlog } from '../../../actions/admin/blog.management.action'
import PropTypes from "prop-types";
import reqwest from 'reqwest';
import { EditorState, convertToRaw } from 'draft-js';
import { Editor } from 'react-draft-wysiwyg';
import 'react-draft-wysiwyg/dist/react-draft-wysiwyg.css';
import draftToHtml from 'draftjs-to-html';


const { Option } = Select;

const CreateForm = ({ PostBlog, form: { getFieldDecorator, validateFields, setFieldsValue } }) => {
  const [formData, setFormData] = useState({
    content: "",
    title: "",
    featureImage: [],
    author: "",
    fileList: [],
    uploading: false,
    uploadDisable: false,
    editorState: EditorState.createEmpty()
  });
  const { title,author, featureImage, uploaded, uploading, fileList, content, editorState, uploadDisable } = formData;

  useEffect(() => {

  }, [])

  const handleUpload = async () => {

    setFormData({
      ...formData,
      uploading: true,
    });

    try {
      const data = new FormData();
      fileList.forEach(file => {
        data.append('BlogFeatureImg', file);
      });
      const response = await axios.post('https://upappfactory.com/upload/blogFeatured', data)
      setFormData({
        ...formData,
        featureImage: response.data.location,
        uploading: false,
        uploadDisable: true,
      })
      message.success('Featured Image Added Successfully.');

    } catch (error) {
      console.log(error, "error")
      message.success('Featured Image upload Failed.');
    }
  };


  const onChange = e => {
    setFormData({
      ...formData,
      [e.target.name]: e.target.value
    });
  };

  const onChangeEditor = evt => {

    setFormData({
      ...formData,
      content: draftToHtml(convertToRaw(evt.getCurrentContent())),
      editorState: evt
    });
  };

  const onSubmit = e => {

      e.preventDefault();

      validateFields((err, values) => {
        if (!err) {
          console.log(formData, "formdata")
          PostBlog(formData)
          setFormData({
            ...formData,
            uploaded: true
          });
          message.success('Post Added Successfully.');
        }
      });

  };

  if (uploaded) {
    return <Redirect to="/cms/admin/blog-list"></Redirect>;
  }

  const props = {
    onRemove: file => {
      setFormData(formData => {
        const index = formData.fileList.indexOf(file);
        const newFileList = formData.fileList.slice();
        newFileList.splice(index, 1);
        return {
          ...formData,
          fileList: newFileList,
          uploadDisable: false,
        };
      });
    },
    beforeUpload: file => {
    const isLt2M = file.size / 1024 / 1024 < 0.350;
  if (!isLt2M) {
    message.error('Image must smaller than 300KB!');
  } else {
    setFormData(formData => ({
      ...formData,
      fileList: [...formData.fileList, file],
    }));
    return false;
  }
     
    },
    fileList,
  };

  return (
    <Fragment>
      <Form
        // className="vacency-add-form"
        layout="vertical"
        onSubmit={e => onSubmit(e)}
      >
        <Row gutter={16} type="flex" justify="space-around">
          <Col xs={24}>
            <Row gutter={16} type="flex" justify="space-around">
              <Col xs={24}>
                <div style={{ marginBottom: "10px" }}>Title</div>
                <Form.Item>
                {getFieldDecorator("title", {
										rules: [
											{ required: true, message: "Please Enter Blog Title" }
										]
									})(
                  <Input
                    placeholder="Blog Title"
                    onChange={e => onChange(e)}
                    name="title"
                  />
                  )}
                </Form.Item>
              </Col>
              <Col xs={24}>
                <div style={{ marginBottom: "10px" }}>Blog</div>
                <Form.Item>
                {getFieldDecorator("content", {
										rules: [
											{ required: true, message: "Please Enter Blog Content" }
										]
									})(
                  <Editor
                    editorState={editorState}
                    toolbarClassName="toolbarClassName"
                    wrapperClassName="wrapperClassName"
                    editorClassName="editorClassName"
                    onEditorStateChange={onChangeEditor}
                  />
                  )}
                </Form.Item>
              </Col>
              <Col xs={24}>
                <div style={{ marginBottom: "10px" }}>Featured Image (Please upload Less than 300kb File)</div>
                <Form.Item>
                  <div>
                    <Upload {...props}>
                      <Button>
                        <Icon type="upload" /> Select File
                    </Button>
                    </Upload>
                    <Button
                      type="primary"
                      onClick={handleUpload}
                      disabled={uploadDisable}
                      loading={uploading}
                      
                      style={{ marginTop: 16 }}
                    >
                      {uploading ? 'Uploading' : 'Start Upload'}
                    </Button>

                  </div>
                </Form.Item>
              </Col>
              <Col xs={24}>
                <div style={{ marginBottom: "10px" }}>Author</div>
                <Form.Item>
                {getFieldDecorator("author", {
										rules: [
											{ required: true, message: "Please Enter Author Name" }
										]
									})(
                  <Input
                    placeholder="Enter Author Name"
                    onChange={e => onChange(e)}
                    name="author"
                  />
                  )}
                </Form.Item>
              </Col>
            </Row>

          </Col>
        </Row>
        <div className="vacency-submit-btn" style={{ textAlign: "center" }}>
          <Button
            type="primary"
            htmlType="submit"
            style={{ margin: "10px", width: "100px" }}
            disabled={!uploadDisable}
          >
            ADD
          </Button>
          <Link
            to="/cms/admin/blog-list"
            style={{ margin: "10px", width: "100px" }}
          >
            <Button type="primary" htmlType="submit">
              CANCEL
            </Button>
          </Link>
        </div>
      </Form>
    </Fragment>
  );
};

const NormalCreate = Form.create({ name: "normal_Create" })(CreateForm);

NormalCreate.propTypes = {
  PostBlog: PropTypes.func.isRequired
};
export default connect(null, { PostBlog })(NormalCreate);
