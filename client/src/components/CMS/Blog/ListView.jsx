import React, { Fragment, useEffect, useState } from "react";
import { Link, Redirect } from "react-router-dom";
import { connect } from "react-redux";
import { Row, Button, Table, Input, Icon, Spin, Modal } from "antd";
import PropTypes from "prop-types";
import { getAllBlog, DeleteBlog } from "../../../actions/admin/blog.management.action";

const { confirm } = Modal;

let searchInput = React.createRef();
const List = ({ getAllBlog, Blogs, DeleteBlog, refresh }) => {
  const [Data, setData] = useState({
    filteredInfo: null,
    sortedInfo: null,
    selectedRowKeys: [],
    searchText: "",
    visible: false
  });

  let { filteredInfo, sortedInfo, selectedRowKeys } = Data;
  const data = [];

  useEffect(() => {
    getAllBlog();
  }, [refresh]);
  console.log(Blogs, "Blogs", refresh);

  Blogs &&
    Blogs.map((blog, index) => {
      let Ids = {
        blog_title: blog && blog.title,
        blog_author: blog && blog.author,
        id: blog && blog.UID,
        slug: blog && blog.slug
      };
      // console.log("IDs", Ids);
      data.push(Ids);
    });
  console.log("DATA >>> ", data);

  //  Selecting rows
  const onSelectChange = selectedRowKeys => {
    setData({ selectedRowKeys });
  };

  function showDeleteConfirm(id) {
    confirm({
      title: 'Are you sure delete this task?',
      content: 'Some descriptions',
      okText: 'Yes',
      okType: 'danger',
      cancelText: 'No',
      onOk() {
        DeleteBlog(id)
      },
      onCancel() {
        console.log('Cancel');
      },
    });
  }


  const rowSelection = {
    selectedRowKeys,
    onChange: onSelectChange,
    hideDefaultSelections: true,
    selections: [
      {
        key: "all-data",
        text: "Select All Data",
        onSelect: () => {
          setData({
            selectedRowKeys: [...Array(46).keys()] // 0...45
          });
        }
      },
      {
        key: "odd",
        text: "Select Odd Row",
        onSelect: changableRowKeys => {
          let newSelectedRowKeys = [];
          newSelectedRowKeys = changableRowKeys.filter((key, index) => {
            if (index % 2 !== 0) {
              return false;
            }
            return true;
          });
          setData({ selectedRowKeys: newSelectedRowKeys });
        }
      },
      {
        key: "even",
        text: "Select Even Row",
        onSelect: changableRowKeys => {
          let newSelectedRowKeys = [];
          newSelectedRowKeys = changableRowKeys.filter((key, index) => {
            if (index % 2 !== 0) {
              return true;
            }
            return false;
          });
          setData({ selectedRowKeys: newSelectedRowKeys });
        }
      }
    ]
  };

  //   End Selecting Rows

  // Search column

  const getColumnSearchProps = dataIndex => ({
    filterDropdown: ({
      setSelectedKeys,
      selectedKeys,
      confirm,
      clearFilters
    }) => (
        <div style={{ padding: 8 }}>
          <Input
            ref={node => {
              searchInput = node;
            }}
            placeholder={`Search ${dataIndex}`}
            value={selectedKeys[0]}
            onChange={e =>
              setSelectedKeys(e.target.value ? [e.target.value] : [])
            }
            onPressEnter={() => handleSearch(selectedKeys, confirm)}
            style={{ width: 188, marginBottom: 8, display: "block" }}
          />
          <Button
            type="primary"
            onClick={() => handleSearch(selectedKeys, confirm)}
            icon="search"
            size="small"
            style={{ width: 90, marginRight: 8 }}
          >
            Search
        </Button>
          <Button
            onClick={() => handleReset(clearFilters)}
            size="small"
            style={{ width: 90 }}
          >
            Reset
        </Button>
        </div>
      ),
    filterIcon: filtered => (
      <Icon type="search" style={{ color: filtered ? "#1890ff" : undefined }} />
    ),
    onFilter: (value, record) =>
      record[dataIndex]
        .toString()
        .toLowerCase()
        .includes(value.toLowerCase()),
    onFilterDropdownVisibleChange: visible => {
      if (visible) {
        setTimeout(() => searchInput.select());
      }
    }
  });

  const handleSearch = (selectedKeys, confirm) => {
    confirm();
    setData({ searchText: selectedKeys[0] });
  };

  const handleReset = clearFilters => {
    clearFilters();
    setData({ searchText: "" });
  };

  // End Search column

  const handleChange = (pagination, filters, sorter) => {
    setData({
      filteredInfo: filters,
      sortedInfo: sorter
    });
  };

  sortedInfo = sortedInfo || {};
  filteredInfo = filteredInfo || {};

  const columns = [
    {
      title: "Blog Title",
      width: 150,
      dataIndex: "blog_title",
      key: 1,
      ...getColumnSearchProps("blog_title"),
      sorter: (a, b) => a.blog_title.length - b.blog_title.length,
      sortOrder: sortedInfo.columnKey === 1 && sortedInfo.order
    },
    {
      title: "Blog Author",
      dataIndex: "blog_author",
      width: 100,
      key: 2,
      sorter: (a, b) => a.blog_author.length - b.blog_author.length,
      sortOrder: sortedInfo.columnKey === 2 && sortedInfo.order
    },
    {
      title: "Action",
      dataIndex: "slug",
      // fixed: "right",
      key: 6,
      width: 60,

      render: slug => (
        <span>
          <Link to={`/cms/admin/edit-blog/${slug}`}>
            <Button type="dashed" size="small">
              <Icon type="edit" />
            </Button>
          </Link>
          &nbsp; &nbsp;
          <Button
            type="dashed"
            size="small"
            onClick={() => {
              showDeleteConfirm(slug)
            }}
          >
            <Icon type="delete" />
          </Button>
        </span>
      )
    }
  ];

  return (
    <Fragment>
      <Row>
        <Link to="/cms/admin/create-blog">
          <Button
            type="primary"
            size={"large"}
            style={{
              float: "right",
              marginTop: "10px",
              marginBottom: "20px"
            }}
          >
            ADD NEW
          </Button>
        </Link>
      </Row>

      <Table
        rowSelection={rowSelection}
        columns={columns}
        dataSource={data}
        onChange={handleChange}
        // scroll={{ x: 750 }}
        bordered
        size="small"
      />
    </Fragment>
  );
};

List.propTypes = {
  getAllBlog: PropTypes.func.isRequired,
  Blogs: PropTypes.array.isRequired,
  DeleteBlog: PropTypes.func.isRequired,
  loading: PropTypes.bool,
  refresh: PropTypes.bool,
};

const mapStateToProps = state => ({
  Blogs: state.BlogManagement.Blogs,
  refresh: state.BlogManagement.refresh,
  loading: state.BlogManagement.loading
});

export default connect(mapStateToProps, {
  getAllBlog,
  DeleteBlog
})(List);
