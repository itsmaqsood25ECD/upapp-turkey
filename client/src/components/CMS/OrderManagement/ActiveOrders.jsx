import React, { Fragment, useState, useEffect } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { Table, Button, Input, Icon, Tag, Divider } from 'antd';
import { Link } from 'react-router-dom';
import { getAllOrders } from '../../../actions/admin/orders.management.action';
import moment from 'moment';

let searchInput = React.createRef();

const ActiveOrders = ({ orders, loading, getAllOrders }) => {
	useEffect(() => {
		getAllOrders();
	}, [loading]);

	const [Data, setData] = useState({
		filteredInfo: null,
		sortedInfo: null,
		selectedRowKeys: [],
		searchText: ''
	});

	const data = [];

	orders.map((order, index) => {
		if (order && order.status && order.status.state == 'Active') {
			let Ids = {
				key: index + 1,
				orderid: order._id || order.PlanID,
				appname: order.appName,
				emailid: order.user.email,
				custname: `${order.user && order.user.firstName} ${order.user &&
					order.user.lastName}`,
				subs: order.packType + 'ly',
				invc: `/cms/admin/order-management/${order._id || order.PlanID}`,
				payment: `/cms/admin/order-management/${order._id || order.PlanID}`,
				start_date: moment(order.start_date).format('MMMM Do YYYY'),
				total_price: order.discount_price || order.total_price
			};
			data.push(Ids);
		}
		console.log(data);
	});

	console.log(data);
	let { filteredInfo, sortedInfo, selectedRowKeys } = Data;

	//  Selecting rows
	const onSelectChange = selectedRowKeys => {
		setData({ selectedRowKeys });
	};

	const rowSelection = {
		selectedRowKeys,
		onChange: onSelectChange,
		hideDefaultSelections: true,
		selections: [
			{
				key: 'all-data',
				text: 'Select All Data',
				onSelect: () => {
					setData({
						selectedRowKeys: [...Array(46).keys()] // 0...45
					});
				}
			},
			{
				key: 'odd',
				text: 'Select Odd Row',
				onSelect: changableRowKeys => {
					let newSelectedRowKeys = [];
					newSelectedRowKeys = changableRowKeys.filter((key, index) => {
						if (index % 2 !== 0) {
							return false;
						}
						return true;
					});
					setData({ selectedRowKeys: newSelectedRowKeys });
				}
			},
			{
				key: 'even',
				text: 'Select Even Row',
				onSelect: changableRowKeys => {
					let newSelectedRowKeys = [];
					newSelectedRowKeys = changableRowKeys.filter((key, index) => {
						if (index % 2 !== 0) {
							return true;
						}
						return false;
					});
					setData({ selectedRowKeys: newSelectedRowKeys });
				}
			}
		]
	};

	//   End Selecting Rows

	// Search column

	const getColumnSearchProps = dataIndex => ({
		filterDropdown: ({
			setSelectedKeys,
			selectedKeys,
			confirm,
			clearFilters
		}) => (
			<div style={{ padding: 8 }}>
				<Input
					ref={node => {
						searchInput = node;
					}}
					placeholder={`Search ${dataIndex}`}
					value={selectedKeys[0]}
					onChange={e =>
						setSelectedKeys(e.target.value ? [e.target.value] : [])
					}
					onPressEnter={() => handleSearch(selectedKeys, confirm)}
					style={{ width: 188, marginBottom: 8, display: 'block' }}
				/>
				<Button
					type="primary"
					onClick={() => handleSearch(selectedKeys, confirm)}
					icon="search"
					size="small"
					style={{ width: 90, marginRight: 8 }}
				>
					Search
				</Button>
				<Button
					onClick={() => handleReset(clearFilters)}
					size="small"
					style={{ width: 90 }}
				>
					Reset
				</Button>
			</div>
		),
		filterIcon: filtered => (
			<Icon type="search" style={{ color: filtered ? '#1890ff' : undefined }} />
		),
		onFilter: (value, record) =>
			record[dataIndex]
				.toString()
				.toLowerCase()
				.includes(value.toLowerCase()),
		onFilterDropdownVisibleChange: visible => {
			if (visible) {
				setTimeout(() => searchInput.select());
			}
		}
	});

	const handleSearch = (selectedKeys, confirm) => {
		confirm();
		setData({ searchText: selectedKeys[0] });
	};

	const handleReset = clearFilters => {
		clearFilters();
		setData({ searchText: '' });
	};

	// End Search column

	const handleChange = (pagination, filters, sorter) => {
		setData({
			filteredInfo: filters,
			sortedInfo: sorter
		});
	};

	sortedInfo = sortedInfo || {};
	filteredInfo = filteredInfo || {};
	const columns = [
		{
			title: 'Order ID',
			fixed: 'left',
			width: 250,
			dataIndex: 'orderid',
			key: 'orderid',
			...getColumnSearchProps('orderid'),
			sorter: (a, b) => a.orderid.length - b.orderid.length,
			sortOrder: sortedInfo.columnKey === 'orderid' && sortedInfo.order
		},
		{
			title: 'App Name',
			dataIndex: 'appname',
			width: 250,
			key: 'appname',
			...getColumnSearchProps('appname'),
			sorter: (a, b) => a.appname.length - b.appname.length,
			sortOrder: sortedInfo.columnKey === 'appname' && sortedInfo.order
		},
		{
			title: 'Cust Name',
			dataIndex: 'custname',
			width: 200,
			key: 'custname',
			...getColumnSearchProps('custname'),
			sorter: (a, b) => a.custname.length - b.custname.length,
			sortOrder: sortedInfo.columnKey === 'custname' && sortedInfo.order
		},
		{
			title: 'Email',
			dataIndex: 'emailid',
			key: 'emailid',
			width: 300,
			...getColumnSearchProps('emailid'),
			sorter: (a, b) => a.emailid.length - b.emailid.length,
			sortOrder: sortedInfo.columnKey === 'emailid' && sortedInfo.order
		},
		{
			title: 'Subscription',
			dataIndex: 'subs',
			key: 'subs',
			width: 150,
			sorter: (a, b) => a.subs.length - b.subs.length,
			sortOrder: sortedInfo.columnKey === 'subs' && sortedInfo.order
		},
		{
			title: 'Details',
			dataIndex: 'invc',
			key: 'invc',
			width: 100,

			render: invc => (
				<span>
					<Tag>
						<Link to={invc}>
							<span>VIEW</span>
						</Link>
					</Tag>
				</span>
			)
		}
	];

	return (
		<div>
			<Divider orientation="left">Order Details</Divider>

			<Table
				rowSelection={rowSelection}
				columns={columns}
				dataSource={data}
				onChange={handleChange}
				scroll={{ x: 1300 }}
				bordered
				size="small"
			/>
		</div>
	);
};

ActiveOrders.propTypes = {
	getAllOrders: PropTypes.func.isRequired,
	orders: PropTypes.object.isRequired,
	loading: PropTypes.bool
};

const mapStateToProps = state => ({
	orders: state.ordersManagement.orders,
	loading: state.ordersManagement.loading
});
export default connect(
	mapStateToProps,
	{ getAllOrders }
)(ActiveOrders);
