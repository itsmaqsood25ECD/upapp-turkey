import React, { Fragment, useState, useEffect } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { Table, Button, Input, Icon, Divider, Modal, DatePicker } from 'antd';
import { Link } from 'react-router-dom';
import {
	getAllOrders,
	verifyPayment
} from '../../../actions/admin/orders.management.action';
import moment from 'moment';

let searchInput = React.createRef();

let recieptIMG;

const PendingOrders = ({ orders, loading, getAllOrders, verifyPayment }) => {
	useEffect(() => {
		getAllOrders();
	}, [orders]);

	const [Data, setData] = useState({
		filteredInfo: null,
		sortedInfo: null,
		selectedRowKeys: [],
		searchText: '',
		imgModalVisible: false,
		dataModalVisible: false,
		recurring_date: '',
		end_date: '',
		start_date: "",
		id: ''
	});

	const data = [];

	orders.map((order, index) => {
		if (order && order.status.state == 'Pending') {
			let Ids = {
				key: index + 1,
				orderid: order._id,
				appname: order.appName,
				emailid: order.user && order.user.email,
				custname: `${order.user && order.user.firstName} ${order.user &&
					order.user.lastName}`,
				subs: order.packType + 'ly',
				invc: `/cms/admin/order-management/${order._id}`,
				payment: `/cms/admin/order-management/${order._id}`,
				payment_initated_date: moment(order.payment_initated_date).format('MMMM Do YYYY'),
				total_price: order.discount_price || order.total_price,
				receiptUrl: order.reciepts && order.reciepts[0] && order.reciepts[0].url
			};
			data.push(Ids);
		}
	});

	let { filteredInfo, sortedInfo, selectedRowKeys } = Data;

	//  Selecting rows
	const onSelectChange = selectedRowKeys => {
		setData({ selectedRowKeys });
	};

	const rowSelection = {
		selectedRowKeys,
		onChange: onSelectChange,
		hideDefaultSelections: true,
		selections: [
			{
				key: 'all-data',
				text: 'Select All Data',
				onSelect: () => {
					setData({
						selectedRowKeys: [...Array(46).keys()] // 0...45
					});
				}
			},
			{
				key: 'odd',
				text: 'Select Odd Row',
				onSelect: changableRowKeys => {
					let newSelectedRowKeys = [];
					newSelectedRowKeys = changableRowKeys.filter((key, index) => {
						if (index % 2 !== 0) {
							return false;
						}
						return true;
					});
					setData({ selectedRowKeys: newSelectedRowKeys });
				}
			},
			{
				key: 'even',
				text: 'Select Even Row',
				onSelect: changableRowKeys => {
					let newSelectedRowKeys = [];
					newSelectedRowKeys = changableRowKeys.filter((key, index) => {
						if (index % 2 !== 0) {
							return true;
						}
						return false;
					});
					setData({ selectedRowKeys: newSelectedRowKeys });
				}
			}
		]
	};

	//   End Selecting Rows

	// Search column

	const getColumnSearchProps = dataIndex => ({
		filterDropdown: ({
			setSelectedKeys,
			selectedKeys,
			confirm,
			clearFilters
		}) => (
				<div style={{ padding: 8 }}>
					<Input
						ref={node => {
							searchInput = node;
						}}
						placeholder={`Search ${dataIndex}`}
						value={selectedKeys[0]}
						onChange={e =>
							setSelectedKeys(e.target.value ? [e.target.value] : [])
						}
						onPressEnter={() => handleSearch(selectedKeys, confirm)}
						style={{ width: 188, marginBottom: 8, display: 'block' }}
					/>
					<Button
						type="primary"
						onClick={() => handleSearch(selectedKeys, confirm)}
						icon="search"
						size="small"
						style={{ width: 90, marginRight: 8 }}
					>
						Search
				</Button>
					<Button
						onClick={() => handleReset(clearFilters)}
						size="small"
						style={{ width: 90 }}
					>
						Reset
				</Button>
				</div>
			),
		filterIcon: filtered => (
			<Icon type="search" style={{ color: filtered ? '#1890ff' : undefined }} />
		),
		onFilter: (value, record) =>
			record[dataIndex]
				.toString()
				.toLowerCase()
				.includes(value.toLowerCase()),
		onFilterDropdownVisibleChange: visible => {
			if (visible) {
				setTimeout(() => searchInput.select());
			}
		}
	});

	const handleSearch = (selectedKeys, confirm) => {
		confirm();
		setData({ searchText: selectedKeys[0] });
	};

	const handleReset = clearFilters => {
		clearFilters();
		setData({ searchText: '' });
	};

	// End Search column

	const handleChange = (pagination, filters, sorter) => {
		setData({
			filteredInfo: filters,
			sortedInfo: sorter
		});
	};

	sortedInfo = sortedInfo || {};
	filteredInfo = filteredInfo || {};
	const columns = [
		{
			title: 'Order ID',
			fixed: 'left',
			// width: 100,
			dataIndex: 'orderid',
			key: 'orderid',
			...getColumnSearchProps('orderid'),
			sorter: (a, b) => a.orderid.length - b.orderid.length,
			sortOrder: sortedInfo.columnKey === 'orderid' && sortedInfo.order
		},
		{
			title: 'App Name',
			dataIndex: 'appname',
			// width: 150,
			key: 'appname',
			...getColumnSearchProps('appname'),
			sorter: (a, b) => a.appname.length - b.appname.length,
			sortOrder: sortedInfo.columnKey === 'appname' && sortedInfo.order
		},
		{
			title: 'Cust Name',
			dataIndex: 'custname',
			// width: 200,
			key: 'custname',
			...getColumnSearchProps('custname'),
			sorter: (a, b) => a.custname.length - b.custname.length,
			sortOrder: sortedInfo.columnKey === 'custname' && sortedInfo.order
		},
		{
			title: 'Email',
			dataIndex: 'emailid',
			key: 'emailid',
			// width: 300,
			...getColumnSearchProps('emailid'),
			sorter: (a, b) => a.emailid.length - b.emailid.length,
			sortOrder: sortedInfo.columnKey === 'emailid' && sortedInfo.order
		},
		{
			title: 'Subscription',
			dataIndex: 'subs',
			key: 'subs',
			// width: 150,
			sorter: (a, b) => a.subs.length - b.subs.length,
			sortOrder: sortedInfo.columnKey === 'subs' && sortedInfo.order
		},
		{
			title: 'Action',
			dataIndex: 'orderid',
			key: 'orderid',
			// width: 120,

			render: orderid => (
				<span>
					<Button type="dashed" shape="circle" onClick={() => viewReciept(orderid)}>
						<Icon type="eye" />
					</Button>
					&nbsp;
					<Button
						type="dashed"
						shape="circle"
						style={{ color: '#5cd65c' }}
						onClick={() => openDataModal(orderid)}
					>
						<Icon type="check" />
					</Button>
					&nbsp;
					<Button type="dashed" shape="circle" style={{ color: '#ff4d4d' }}>
						<Icon type="close" />
					</Button>
				</span>
			)
		}
	];

	const handleCancel = () => {
		setData({
			...Data,
			imgModalVisible: false,
			dataModalVisible: false
		});
	};

	const handleOk = () => {
		setData({
			...Data,
			imgModalVisible: false
		});
	};
	const viewReciept = orderid => {
		{
			data && data.map(data => {
				if (data.orderid == orderid) {
					return recieptIMG = data.receiptUrl
				}
			})
		}
		setData({
			...Data,
			imgModalVisible: true
		});
	};

	const openDataModal = orderid => {
		setData({
			...Data,
			dataModalVisible: true,
			id: orderid
		});
	};
	const handleSubmit = () => {
		setData({
			...Data,
			dataModalVisible: false
		});
		verifyPayment(Data);
		getAllOrders();
	};
	const onStartDateChange = e => {
		setData({
			...Data,
			start_date: e._d
		});
	};
	const onRecurringDateChange = e => {
		setData({
			...Data,
			recurring_date: e._d
		});
	};
	const onEndDateChange = e => {
		setData({
			...Data,
			end_date: e._d
		});
	};
	return (
		<Fragment>
			<Divider orientation="left">Pending Details</Divider>
			<Table
				rowSelection={rowSelection}
				columns={columns}
				dataSource={data}
				onChange={handleChange}
				// scroll={{ x: 1300 }}
				bordered
				size="small"
			/>
			{/* Img Modal */}

			<Modal
				visible={Data.imgModalVisible}
				title="Reciept Verification"
				onCancel={handleCancel}
				footer={[
					<Button key="submit" type="primary" onClick={handleOk}>
						Okay
					</Button>
				]}
			>
				{/* {data &&
					data.receiptUrl &&
					data.receiptUrl.map(arg => {
						return ( */}
				<div>
					<img style={{ width: "100%" }} src={recieptIMG} alt="reciept"></img>
				</div>
				{/* 						
						);
					})} */}
			</Modal>

			{/* Img Modal */}
			{/*	Data Modal */}

			<Modal
				visible={Data.dataModalVisible}
				title="Reciept Verification"
				onCancel={handleCancel}
				footer={[
					<Button key="back" onClick={handleCancel}>
						Cancel
					</Button>,
					<Button key="submit" type="primary" onClick={handleSubmit}>
						Submit
					</Button>
				]}
			>
				<div style={{ margin: '10px 0' }}>Start Date</div>
				<DatePicker
					onChange={e => onStartDateChange(e)}
					format={('DD/MM/YYYY', 'DD/MM/YY')}
					name="start_date"
					style={{ width: '100%' }}
				/>
				<div style={{ margin: '10px 0' }}>Recurring Date</div>
				<DatePicker
					onChange={e => onRecurringDateChange(e)}
					format={('DD/MM/YYYY', 'DD/MM/YY')}
					name="recurring_date"
					style={{ width: '100%' }}
				/>
				<div style={{ margin: '10px 0px' }}>End Date</div>
				<DatePicker
					onChange={e => onEndDateChange(e)}
					format={('DD/MM/YYYY', 'DD/MM/YY')}
					name="end_date"
					style={{ width: '100%' }}
				/>
			</Modal>

			{/* Data Modal */}
		</Fragment>
	);
};

PendingOrders.propTypes = {
	getAllOrders: PropTypes.func.isRequired,
	orders: PropTypes.object.isRequired,
	loading: PropTypes.bool,
	verifyPayment: PropTypes.func.isRequired
};

const mapStateToProps = state => ({
	orders: state.ordersManagement.orders,
	loading: state.ordersManagement.loading
});
export default connect(
	mapStateToProps,
	{ getAllOrders, verifyPayment }
)(PendingOrders);
