import React, { Fragment, useState, useEffect } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { Table, Button, Input, Icon, Tag, Divider, message, Modal } from 'antd';
import { Link } from 'react-router-dom';
import {
	getCancelledOrders,
	approveCancellation,
	declineCancellation
} from '../../../actions/admin/orders.management.action';
import moment from 'moment';

let searchInput = React.createRef();
const { confirm } = Modal;

const CancelledOrders = ({
	CancelledOrders,
	getCancelledOrders,
	approveCancellation,
	declineCancellation
}) => {
	useEffect(() => {
		getCancelledOrders();
	}, []);

	console.log(CancelledOrders)

	const cancellationAproved = id => {
		confirm({
			title: 'Approve Cancellation',
			content: 'Are you sure you want approve this cancellation request?',
			okText: 'Yes',
			okType: 'danger',
			cancelText: 'No',
			onOk() {
				approveCancellation(id);
				Modal.success({
					title: 'Approve Cancellation',
					content: `You have aprroved the cancellation request with id ${id}`
				});
			},
			onCancel() {}
		});
	};
	const cancellationDeclined = id => {
		confirm({
			title: 'Decline Cancellation',
			content: 'Are you sure you want decline this cancellation request?',
			okText: 'Yes',
			okType: 'danger',
			cancelText: 'No',
			onOk() {
				declineCancellation(id);
				Modal.success({
					title: 'Approve Cancellation',
					content: `You have declined the cancellation request with id ${id}`
				});
			},
			onCancel() {}
		});
	};

	const [Data, setData] = useState({
		filteredInfo: null,
		sortedInfo: null,
		selectedRowKeys: [],
		searchText: ''
	});

	const data = [];

	CancelledOrders &&
		CancelledOrders.map((order, index) => {
			if (order) {
				let Ids = {
					key: index + 1,
					orderid: order.order._id || order.order.PlanID,
					cancelledid: order._id,
					pending: order.pending,
					appname: order.order.appName,
					emailid: order.user && order.user.email,
					custname: `${order.user && order.user.firstName} ${order.user &&
						order.user.lastName}`,
					subs: order.order.packType,
					amt_to_be_charged: order.amt_to_be_charged,
					amt_to_be_returned: order.amt_to_be_returned,
					canelled_at: moment(order.cancelled_at).format('MMMM Do YYYY'),
					// invc: `/cms/admin/order-management/${order._id || order.PlanID}`,
					// payment: `/cms/admin/order-management/${order._id || order.PlanID}`,
					start_date: moment(order.order.start_date).format('MMMM Do YYYY'),
					total_price: order.order.discount_price || order.order.total_price
				};
				data.push(Ids);
			}
		});

	let { filteredInfo, sortedInfo, selectedRowKeys } = Data;

	//  Selecting rows
	const onSelectChange = selectedRowKeys => {
		setData({ selectedRowKeys });
	};

	const rowSelection = {
		selectedRowKeys,
		onChange: onSelectChange,
		hideDefaultSelections: true,
		selections: [
			{
				key: 'all-data',
				text: 'Select All Data',
				onSelect: () => {
					setData({
						selectedRowKeys: [...Array(46).keys()] // 0...45
					});
				}
			},
			{
				key: 'odd',
				text: 'Select Odd Row',
				onSelect: changableRowKeys => {
					let newSelectedRowKeys = [];
					newSelectedRowKeys = changableRowKeys.filter((key, index) => {
						if (index % 2 !== 0) {
							return false;
						}
						return true;
					});
					setData({ selectedRowKeys: newSelectedRowKeys });
				}
			},
			{
				key: 'even',
				text: 'Select Even Row',
				onSelect: changableRowKeys => {
					let newSelectedRowKeys = [];
					newSelectedRowKeys = changableRowKeys.filter((key, index) => {
						if (index % 2 !== 0) {
							return true;
						}
						return false;
					});
					setData({ selectedRowKeys: newSelectedRowKeys });
				}
			}
		]
	};

	//   End Selecting Rows

	// Search column

	const getColumnSearchProps = dataIndex => ({
		filterDropdown: ({
			setSelectedKeys,
			selectedKeys,
			confirm,
			clearFilters
		}) => (
			<div style={{ padding: 8 }}>
				<Input
					ref={node => {
						searchInput = node;
					}}
					placeholder={`Search ${dataIndex}`}
					value={selectedKeys[0]}
					onChange={e =>
						setSelectedKeys(e.target.value ? [e.target.value] : [])
					}
					onPressEnter={() => handleSearch(selectedKeys, confirm)}
					style={{ width: 188, marginBottom: 8, display: 'block' }}
				/>
				<Button
					type="primary"
					onClick={() => handleSearch(selectedKeys, confirm)}
					icon="search"
					size="small"
					style={{ width: 90, marginRight: 8 }}
				>
					Search
				</Button>
				<Button
					onClick={() => handleReset(clearFilters)}
					size="small"
					style={{ width: 90 }}
				>
					Reset
				</Button>
			</div>
		),
		filterIcon: filtered => (
			<Icon type="search" style={{ color: filtered ? '#1890ff' : undefined }} />
		),
		onFilter: (value, record) =>
			record[dataIndex]
				.toString()
				.toLowerCase()
				.includes(value.toLowerCase()),
		onFilterDropdownVisibleChange: visible => {
			if (visible) {
				setTimeout(() => searchInput.select());
			}
		}
	});

	const handleSearch = (selectedKeys, confirm) => {
		confirm();
		setData({ searchText: selectedKeys[0] });
	};

	const handleReset = clearFilters => {
		clearFilters();
		setData({ searchText: '' });
	};

	// End Search column

	const handleChange = (pagination, filters, sorter) => {
		setData({
			filteredInfo: filters,
			sortedInfo: sorter
		});
	};

	sortedInfo = sortedInfo || {};
	filteredInfo = filteredInfo || {};
	const columns = [
		{
			title: 'Cancel ID',
			dataIndex: 'cancelledid',
			key: 1,
			...getColumnSearchProps('cancelledid'),
			sorter: (a, b) => a.cancelledid.length - b.cancelledid.length,
			sortOrder: sortedInfo.columnKey === 'cancelledid' && sortedInfo.order
		},
		// {
		// 	title: 'Order ID',
		// 	dataIndex: 'orderid',
		// 	key: 2,
		// 	...getColumnSearchProps('orderid'),
		// 	sorter: (a, b) => a.orderid.length - b.orderid.length,
		// 	sortOrder: sortedInfo.columnKey === 'orderid' && sortedInfo.order
		// },
		{
			title: 'App Name',
			dataIndex: 'appname',
			key: 3,
			...getColumnSearchProps('appname'),
			sorter: (a, b) => a.appname.length - b.appname.length,
			sortOrder: sortedInfo.columnKey === 'appname' && sortedInfo.order
		},
		{
			title: 'Cust Name',
			dataIndex: 'custname',
			key: 4,
			...getColumnSearchProps('custname'),
			sorter: (a, b) => a.custname.length - b.custname.length,
			sortOrder: sortedInfo.columnKey === 'custname' && sortedInfo.order
		},
		{
			title: 'Email',
			dataIndex: 'emailid',
			key: 5,
			...getColumnSearchProps('emailid'),
			sorter: (a, b) => a.emailid.length - b.emailid.length,
			sortOrder: sortedInfo.columnKey === 'emailid' && sortedInfo.order
		},
		{
			title: 'Subscription',
			dataIndex: 'subs',
			key: 6,
			sorter: (a, b) => a.subs.length - b.subs.length,
			sortOrder: sortedInfo.columnKey === 'subs' && sortedInfo.order
		},
		{
			title: 'Status',
			dataIndex: 'pending',
			key: 7,
			render: (pending, order) => (
				<span>
					{console.log(order.pending)}
					{
	      order.pending == false ? <Tag color="volcano">Cancelled</Tag> : <span>
			yo
			</span>					
					}
				</span>
			)
			
		},
		{
			title: 'Actions',
			dataIndex: 'cancelledid',
			key: 8,
			render: (cancelledid, order) => (
				<span>
					{console.log(order.pending)}
					{
	order.pending == false ? '' : <span>
			<Button
						type="dashed"
						shape="circle"
						style={{ color: '#5cd65c' }}
						onClick={() => cancellationAproved(cancelledid)}
					>
						<Icon type="check" />
					</Button>
					&nbsp;
					<Button
						type="dashed"
						shape="circle"
						style={{ color: '#ff4d4d' }}
						onClick={() => cancellationDeclined(cancelledid)}
					>
						<Icon type="close" />
					</Button>
				</span>					
					}
				</span>
			)
		}
	];

	return (
		<div>
			<Divider orientation="left">Cancelled Order Details</Divider>

			<Table
				rowSelection={rowSelection}
				columns={columns}
				dataSource={data}
				onChange={handleChange}
				bordered
				size="small"
			/>
		</div>
	);
};

CancelledOrders.propTypes = {
	getCancelledOrders: PropTypes.func.isRequired,
	CancelledOrders: PropTypes.array.isRequired,
	approveCancellation: PropTypes.func.isRequired,
	declineCancellation: PropTypes.func.isRequired
};

const mapStateToProps = state => ({
	CancelledOrders: state.ordersManagement.cancelledOrders
});
export default connect(
	mapStateToProps,
	{ getCancelledOrders, approveCancellation, declineCancellation }
)(CancelledOrders);
