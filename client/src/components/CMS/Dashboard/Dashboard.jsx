import React, { Fragment, useState, useEffect } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";

import { loadUser } from "../../../actions/auth.action";
import { store } from "../../../store/store";
import { GetApplication } from "../../../actions/admin/application.management.action";
import { GetCategory } from "../../../actions/admin/category.management.action";

const Dashboard = ({ GetApplication, GetCategory }) => {
	useEffect(() => {
		store.dispatch(loadUser());
		GetApplication();
		GetCategory();
	}, []);

	return <Fragment></Fragment>;
};

Dashboard.propTypes = {
	GetApplication: PropTypes.func.isRequired,
	GetCategory: PropTypes.func.isRequired
};
export default connect(
	null,
	{ GetApplication, GetCategory }
)(Dashboard);
