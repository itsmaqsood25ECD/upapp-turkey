import React, { Fragment, useState, useEffect } from 'react'
import { Link } from "react-router-dom";
import { connect } from "react-redux";
import moment from "moment";
import PropTypes from 'prop-types'
import { GetAllCustomers, GetCustomers,  } from '../../../actions/admin/customer.management.action'
import {
	Modal,
	Table,
	Button,
	Input,
	Icon,
	Tooltip,
	Tag,
	Divider,
	Row,
	Spin,
	Descriptions
} from "antd";

let searchInput = React.createRef();
const List = ({ GetAllCustomers, Customers, GetCustomers, Customer, loading }) => {

	const [Data, setData] = useState({
		filteredInfo: null,
		sortedInfo: null,
		selectedRowKeys: [],
		searchText: "",
		visible: false,
		data:[],
		details:''
	});

	const[abc, SetAbc] = useState(0);


	let { filteredInfo, sortedInfo, selectedRowKeys, details, data, visible } = Data;

	useEffect(() => {
		GetAllCustomers()
		setData({
			...Data,
			data: Customers
		})
		console.log(Customers)
	}, [abc, loading])

	// Customers &&
	// 	Customers.map((cust, index) => {
	// 		let Ids = {
	// 			id: cust.UID,
	// 			name: cust.firstName + ' ' + cust.lastName,
	// 			gender: cust.gender,
	// 			phone: cust.phone,
	// 			country: cust.country,
	// 			age: cust.country,
	// 			created_at: moment(cust.createdAt).format('lll')

	// 		};
	// 		data.push(Ids);
	// 	});

	
	//   End Selecting Rows


	const handleChange = (pagination, filters, sorter) => {
		setData({ 
			...Data,
			filteredInfo: filters,
			sortedInfo: sorter
		});
	};

	sortedInfo = sortedInfo || {};
	filteredInfo = filteredInfo || {};

	// Get full information of User




	const showModal = (text, record) => {
		setData({
			...Data,
			visible: true,
			details: record
		});
	};

	const handleOk = e => {
		setData({
			...Data,
			visible: false
		});
	};

	const handleCancel = e => {
		setData({
			...Data,
			visible: false
		});
	};

	const columns = [
		{
			title: "ID",
			dataIndex: "UID",
			key: 1,
			sorter: (a, b) => a.UID.length - b.UID.length,
			sortOrder: sortedInfo.columnKey === 1 && sortedInfo.order
		},
		{
			title: "Name",
			dataIndex: "firstName",
			key: 2,
			sorter: (a, b) => a.firstName.length - b.firstName.length,
			sortOrder: sortedInfo.columnKey === 2 && sortedInfo.order,
			render: (text, record)  => (
				<div>
					{record.firstName} {record.lastName}
				</div>
			)
		},
		{
			title: "Gender",
			dataIndex: "gender",
			key: 3,
			sorter: (a, b) => a.gender.length - b.gender.length,
			sortOrder: sortedInfo.columnKey === 3 && sortedInfo.order
		},
		{
			title: "Phone",
			dataIndex: "phone",
			width: 50,
			key: 4,
			sorter: (a, b) => a.phone.length - b.phone.length,
			sortOrder: sortedInfo.columnKey === 4 && sortedInfo.order
		},
		{
			title: "Country",
			dataIndex: "country",
			key: 5,
			sorter: (a, b) => a.country.length - b.country.length,
			sortOrder: sortedInfo.columnKey === 5 && sortedInfo.order
		},
		{
			title: "Created At",
			dataIndex: "createdAt",
			key: 6,
			sorter: (a, b) => a.createdAt.length - b.createdAt.length,
			sortOrder: sortedInfo.columnKey === 5 && sortedInfo.order,
			render: (createdAt) => (
				<div>
					 {moment(createdAt).format("ll")}
				</div>
			)
		},
		{
			title: "Action",
			dataIndex: "UID",
			// fixed: "right",
			key: 7,
			width: 60,

			render: (text, record)  => (
				<span style={{display:"flex"}}>
					<Tooltip title="User Info">
						<Button
							type="dashed"
							size="small"
							onClick={() => showModal(text, record)}
						>
							<Icon type="info-circle" />
						</Button>
					</Tooltip>
					&nbsp;
					&nbsp;
					&nbsp;

					<Tooltip title="BAN user">
					<Button
						type="dashed"
						size="small"
						onClick={() => console.log("delted")}
					>
						<Icon type="delete" />
					</Button>
					</Tooltip>
				</span>
			)
		}
	];


	console.log(Customers, loading)
	return (
		<Fragment>
			<Table
				columns={columns}
				dataSource={data}
				onChange={handleChange}
			/>

			<Modal
				title="User Details"
				visible={visible}
				onOk={handleOk}
				onCancel={handleCancel}
			>

{console.log("details",details)}
				<Descriptions
					title=""
					size="small"
					bordered
					column={{ xxl: 4, xl: 1, lg: 1, md: 1, sm: 1, xs: 1 }}
				>
					<Descriptions.Item label="User ID">
						{details.UID}
					</Descriptions.Item>
					<Descriptions.Item label="Name">
						{details.firstName} {details.lastName}
					</Descriptions.Item>
					<Descriptions.Item label="Email">
						{details.email}
					</Descriptions.Item>
					<Descriptions.Item label="Phone">
						{details.phone}
					</Descriptions.Item>
					<Descriptions.Item label="age">
						{details.age}
					</Descriptions.Item>
					<Descriptions.Item label="Country">
						{details.country}
					</Descriptions.Item>
					<Descriptions.Item label="User Added">
						{moment(details.createdAt).format("ll")}
					</Descriptions.Item>	
				</Descriptions>
			</Modal>

		</Fragment>
	)
}

List.propTypes = {
	GetAllCustomers: PropTypes.func.isRequired,
	GetCustomers: PropTypes.func.isRequired,
	Customers: PropTypes.array.isRequired,
	Customer: PropTypes.object.isRequired,
	loading: PropTypes.bool,
}

const mapStateToProps = state => ({
	Customers: state.customerManagement.Customers,
	Customer: state.customerManagement.Customer,
	loading: state.customerManagement.loading
});

export default connect(mapStateToProps, { GetAllCustomers, GetCustomers })(List)

