import React, { Fragment, useState } from 'react'
import { Row, Col, Input, Form, Button, Select, Tabs } from 'antd';
import { createJob } from '../../../actions/admin/career.management.action';
import { Link, Redirect } from 'react-router-dom';
import { connect } from 'react-redux';

import PropTypes from 'prop-types'

const { Option } = Select;
const { TabPane } = Tabs;

const Edit = props => {
  const [formData, setFormData] = useState({
    job_location: '',
    job_locationAR: '',
    job_title: '',
    job_titleAR: '',
    job_vacancy: '',
    job_vacancyAR: '',
    job_exp: '',
    job_expAR: '',
    uploaded: false
  });
  const { uploaded } = formData
  const handleLocationSelect = val => {
    setFormData({
      ...formData,
      job_location: val
    });
  };
  const handleLocationSelecAR = val => {
    setFormData({
      ...formData,
      job_locationAR: val
    });
  };

  const onChange = e => {
    setFormData({
      ...formData,
      [e.target.name]: e.target.value
    });
  };

  const onSubmit = e => {
    e.preventDefault();
    createJob(formData);
    setFormData({
      ...formData,
      uploaded: true
    });
  };

  if (uploaded) {
    return <Redirect to="/cms/admin/career-management"></Redirect>;
  }

  return (
    <Fragment>
      <Form layout="vertical" onSubmit={e => onSubmit(e)}>
        <Row gutter={16} type="flex" justify="space-around">
          <Tabs defaultActiveKey="English">
            <TabPane tab="English" key="English">
              <Col xs={24}>
                <Form.Item>
                  <Select defaultValue="Select Type" onChange={handleLocationSelect}>
                    <Option key="1" value="INDIA">
                      India
                    </Option>
                    <Option key="2" value="SINGAPORE">
                      Singapore
                    </Option>
                    <Option key="3" value="OMAN">
                      Oman
                    </Option>
                    <Option key="3" value="SRILANKA">
                      Srilanka
                    </Option>
                  </Select>
                </Form.Item>
              </Col>
              <Col xs={24}>
                <div style={{ marginBottom: '10px' }}>Job Title</div>
                <Form.Item>
                  <Input
                    placeholder="Job Title"
                    onChange={e => onChange(e)}
                    name="job_title"
                  />
                </Form.Item>
              </Col>
              <Col xs={24}>
                <div style={{ marginBottom: '10px' }}>Job Vacancy</div>
                <Form.Item>
                  <Input
                    placeholder="Job Vacancy"
                    onChange={e => onChange(e)}
                    name="job_vacancy"
                  />
                </Form.Item>
              </Col>
              <Col xs={24}>
                <div style={{ marginBottom: '10px' }}>Job Exp</div>
                <Form.Item>
                  <Input
                    placeholder="Job Exp"
                    onChange={e => onChange(e)}
                    name="job_exp"
                  />
                </Form.Item>
              </Col>
            </TabPane>
            <TabPane tab="Arabic" key="Arabic">
              <Col xs={24}>
                <Form.Item>
                  <Select defaultValue="Select Type" onChange={handleLocationSelecAR}>
                    <Option key="1" value="الهند">
                      الهند
                    </Option>
                    <Option key="2" value="سنغافورة">
                      سنغافورة
                    </Option>
                    <Option key="3" value="سلطنة عمان">
                      سلطنة عمان
                    </Option>
                    <Option key="3" value="سريلانكا">
                      سريلانكا
                    </Option>
                  </Select>
                </Form.Item>
              </Col>
              <Col xs={24}>
                <div style={{ marginBottom: '10px' }}>Job Title</div>
                <Form.Item>
                  <Input
                    placeholder="Job Title"
                    onChange={e => onChange(e)}
                    name="job_titleAR"
                  />
                </Form.Item>
              </Col>
              <Col xs={24}>
                <div style={{ marginBottom: '10px' }}>Job Vacancy</div>
                <Form.Item>
                  <Input
                    placeholder="Job Vacancy"
                    onChange={e => onChange(e)}
                    name="job_vacancyAR"
                  />
                </Form.Item>
              </Col>
              <Col xs={24}>
                <div style={{ marginBottom: '10px' }}>Job Exp</div>
                <Form.Item>
                  <Input
                    placeholder="Job Exp"
                    onChange={e => onChange(e)}
                    name="job_expAR"
                  />
                </Form.Item>
              </Col>

            </TabPane>
          </Tabs>
        </Row>
        <Button type="primary" htmlType="submit">
          ADD
      </Button>
        <Link to="/cms/admin/career-management">
          <Button type="primary" htmlType="submit">
            CANCEL
        </Button>
        </Link>
      </Form>
    </Fragment>
  )
}

Edit.propTypes = {

}

export default Edit
