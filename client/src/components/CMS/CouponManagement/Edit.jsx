import React, { Fragment, useState, useEffect } from "react";
import { connect } from "react-redux";
import { Redirect, Link } from "react-router-dom";
import {
	Row,
	Col,
	Input,
	Form,
	Select,
	Divider,
	Tabs,
	Button,
	Spin,
	DatePicker
} from "antd";
import PropTypes from "prop-types";
import { getCouponCode } from "../../../actions/checkout.action";
import { updateCoupon } from "../../../actions/admin/coupons.action";
import { GetCategory } from "../../../actions/admin/category.management.action";
import { GetApplication } from "../../../actions/admin/application.management.action";
import moment from "moment";

import "./coupon.style.css";

const { TextArea } = Input;
const { TabPane } = Tabs;
const { Option } = Select;
const dateFormatList = ["DD/MM/YYYY", "DD/MM/YY"];

const Edit = ({
	getCouponCode,
	updateCoupon,
	isSubmitted,
	user,
	match,
	GetCategory,
	GetApplication
}) => {
	const [formData, setState] = useState({
		data: [],
		fetching: false,
		coupon_code: "",
		coupon_description: "",
		discount_type: "",
		coupon_amount: "",
		coupon_expiry_date: "",
		coupon_pack_type: "",
		min_spend: "",
		max_spend: "",
		prod: [],
		exclude_prod: [],
		include_categ: [],
		exclude_categ: [],
		allowed_emails: [],
		usage_limit_per_coupon: "",
		usage_limit_x_items: "",
		usage_limit_per_user: "",
		categorySource: [],
		productSource: []
	});

	const {
		fetching,
		data,
		discount_type,
		coupon_amount,
		coupon_expiry_date,
		coupon_pack_type,
		min_spend,
		max_spend,
		prod,
		exclude_prod,
		include_categ,
		exclude_categ,
		allowed_emails,
		usage_limit_per_coupon,
		usage_limit_x_items,
		usage_limit_per_user,
		coupon_code,
		coupon_description,
		categorySource,
		productSource
	} = formData;
	useEffect(() => {
		async function fetch() {
			const res = await getCouponCode(match.params.coupon);
			const result = await GetCategory();
			const response = await GetApplication();
			setState({
				...formData,
				coupon_code: res.coupon_code,
				coupon_description: res.coupon_description,
				discount_type: res.general.discount_type,
				coupon_pack_type: res.general.coupon_pack_type,
				coupon_amount: res.general.coupon_amount,
				coupon_expiry_date: res.general.coupon_expiry_date,
				min_spend: res.usage_restriction.min_spend,
				max_spend: res.usage_restriction.max_spend,
				prod: res.usage_restriction.products.map(arg => {
					return {
						key: arg,
						label: arg
					};
				}),
				exclude_prod: res.usage_restriction.exclude_products.map(arg => {
					return {
						key: arg,
						label: arg
					};
				}),
				include_categ: res.usage_restriction.include_category.map(arg => {
					return {
						key: arg,
						label: arg
					};
				}),
				exclude_categ: res.usage_restriction.exclude_category.map(arg => {
					return {
						key: arg,
						label: arg
					};
				}),
				usage_limit_per_coupon: res.usage_limits.usage_limit_per_coupon,
				usage_limit_per_user: res.usage_limits.usage_limit_per_user,
				usage_limit_x_items: res.usage_limits.usage_limit_x_items,
				allowed_emails: res.usage_restriction.allowed_emails,
				categorySource: result.map(arg => {
					return {
						text: arg.name,
						value: arg.name
					};
				}),
				productSource: response.map(arg => {
					return {
						text: arg.fields.name,
						value: arg.fields.name
					};
				})
			});
		}
		fetch();
	}, []);

	const fetchproduct = arg => {
		setState({ ...formData, data: [], fetching: true });
		const filteredData = productSource.filter(app => {
			if (app.text.toLowerCase().includes(arg.toLocaleLowerCase())) {
				return {
					text: app.text,
					value: app.value
				};
			}
		});
		const data = filteredData.map(res => ({
			text: res.text,
			value: res.value
		}));
		setState({ ...formData, data, fetching: false });
	};

	const fetchCategory = arg => {
		setState({ ...formData, data: [], fetching: true });
		const filteredData = categorySource.filter(app => {
			if (app.text.toLowerCase().includes(arg.toLocaleLowerCase())) {
				return {
					text: app.text,
					value: app.value
				};
			}
		});
		const data = filteredData.map(res => ({
			text: res.text,
			value: res.value
		}));
		setState({ ...formData, data, fetching: false });
	};

	const handleIncludeProductChange = prod => {
		setState({ ...formData, prod, data: [], fetching: false });
	};
	const handleExcludeProductChange = exclude_prod => {
		setState({ ...formData, exclude_prod, data: [], fetching: false });
	};
	const handleIncludecategChange = include_categ => {
		setState({ ...formData, include_categ, data: [], fetching: false });
	};
	const handleExcludecategChange = exclude_categ => {
		setState({ ...formData, exclude_categ, data: [], fetching: false });
	};
	const onChange = e =>
		setState({
			...formData,
			[e.target.name]: e.target.value
		});

	const onDiscountSelect = e => {
		setState({ ...formData, discount_type: e });
	};

	const onPackSelect = e => {
		setState({ ...formData, coupon_pack_type: e });
	};

	const onDateChange = e => {
		setState({ ...formData, coupon_expiry_date: e._d });
	};

	const onSubmit = e => {
		e.preventDefault();
		updateCoupon(formData);
	};

	// Redirect if submitted
	if (isSubmitted && user && user.role === "admin") {
		return <Redirect to="/cms/admin/coupon-management" />;
	}
	return (
		<Fragment>
			<Form onSubmit={e => onSubmit(e)}>
				<Row type="flex" justify="space-around">
					<Col xs={24}>
						<div style={{ marginBottom: "10px" }}>Coupon Code</div>
						<Form.Item>
							<Input
								name="coupon_code"
								value={coupon_code}
								onChange={e => onChange(e)}
								placeholder="Please enter coupon code"
							/>
						</Form.Item>
					</Col>
					<Col xs={24}>
						<div style={{ marginBottom: "10px" }}>Coupon Description</div>
						<Form.Item>
							<TextArea
								name="coupon_description"
								value={coupon_description}
								onChange={e => onChange(e)}
								placeholder="description"
								autosize={{ minRows: 5, maxRows: 5 }}
							/>
						</Form.Item>
					</Col>
					<Divider dashed />
					<Col xs={24}>
						<div className="card-container">
							<Tabs type="card">
								<TabPane tab="General" key="1">
									<div style={{ marginBottom: "10px" }}>General</div>
									<Divider dashed />
									<Row type="flex" justify="space-between" gutter={16}>
										<Col xs={24} lg={12}>
											<span>Choose Discount Type</span>
											<Form.Item>
												<Select
													onChange={e => onDiscountSelect(e)}
													name="discount_type"
													value={discount_type}
												>
													<Option value="Select App" disabled>
														Select Discount type
													</Option>
													<Option value="Amount">Amount</Option>
													<Option value="Percentage">Percentage</Option>
												</Select>
											</Form.Item>
										</Col>
										<Col xs={24} lg={12}>
											<span>Choose Pack Type</span>
											<Form.Item>
												<Select
													onChange={e => onPackSelect(e)}
													defaultValue="Select Pack type"
													value={coupon_pack_type}
												>
													<Option value="Select App" disabled>
														Select Pack type
													</Option>
													<Option value="Month">Monthly</Option>
													<Option value="Year">Yearly</Option>
													<Option value="None">None</Option>
												</Select>
											</Form.Item>
										</Col>
									</Row>
									<Row type="flex" justify="space-between" gutter={16}>
										<Col xs={24} lg={12}>
											<div style={{ marginBottom: "10px" }}>Coupon Amount</div>
											<Form.Item>
												<Input
													onChange={e => onChange(e)}
													name="coupon_amount"
													value={coupon_amount}
													placeholder="Please enter coupon amount"
												/>
											</Form.Item>
										</Col>
										<Col xs={24} lg={12}>
											<div style={{ marginBottom: "10px" }}>
												Coupon Expiry Date
											</div>
											<Form.Item>
												<DatePicker
													onChange={e => onDateChange(e)}
													format={dateFormatList}
													defaultValue={moment(coupon_expiry_date)}
													name="coupon_expiry_date"
													selectedValue={coupon_expiry_date}
													style={{ width: "100%" }}
												/>
											</Form.Item>
										</Col>
									</Row>
								</TabPane>
								<TabPane tab="Usage Restriction" key="2">
									<Row type="flex" justify="space-between" gutter={16}>
										<Col xs={24} lg={12}>
											<div style={{ marginBottom: "10px" }}>Minimum Spend</div>
											<Form.Item>
												<Input
													onChange={e => onChange(e)}
													name="min_spend"
													value={min_spend}
													placeholder="Minimun Amount"
												/>
											</Form.Item>
										</Col>
										<Col xs={24} lg={12}>
											<div style={{ marginBottom: "10px" }}>Maximum Spend</div>
											<Form.Item>
												<Input
													onChange={e => onChange(e)}
													name="max_spend"
													value={max_spend}
													placeholder="Maximun Amount"
												/>
											</Form.Item>
										</Col>
										<Divider dashed />
										<Col xs={24} lg={12}>
											<div style={{ marginBottom: "10px" }}>Product</div>
											<Select
												mode="multiple"
												labelInValue
												value={prod}
												placeholder="Select product"
												notFoundContent={
													fetching ? <Spin size="small" /> : null
												}
												filterOption={true}
												onSearch={fetchproduct}
												onChange={handleIncludeProductChange}
												style={{ width: "100%" }}
											>
												{data.map(d => (
													<Option key={d.value}>{d.text}</Option>
												))}
											</Select>
										</Col>
										<Col xs={24} lg={12}>
											<div style={{ marginBottom: "10px" }}>
												Exclude product
											</div>
											<Select
												mode="multiple"
												labelInValue
												value={exclude_prod}
												placeholder="Select product"
												notFoundContent={
													fetching ? <Spin size="small" /> : null
												}
												filterOption={true}
												onSearch={fetchproduct}
												onChange={handleExcludeProductChange}
												style={{ width: "100%" }}
											>
												{data.map(d => (
													<Option key={d.value}>{d.text}</Option>
												))}
											</Select>
										</Col>
										<Divider dashed />
										<Col xs={24} lg={12}>
											<div style={{ marginBottom: "10px" }}>
												Product Categories
											</div>
											<Select
												mode="multiple"
												labelInValue
												value={include_categ}
												placeholder="Select product"
												notFoundContent={
													fetching ? <Spin size="small" /> : null
												}
												filterOption={true}
												onSearch={fetchCategory}
												onChange={handleIncludecategChange}
												style={{ width: "100%" }}
											>
												{data.map(d => (
													<Option key={d.value}>{d.text}</Option>
												))}
											</Select>
										</Col>
										<Col xs={24} lg={12}>
											<div style={{ marginBottom: "10px" }}>
												Exclude Categories
											</div>
											<Select
												mode="multiple"
												labelInValue
												value={exclude_categ}
												placeholder="Select product"
												notFoundContent={
													fetching ? <Spin size="small" /> : null
												}
												filterOption={true}
												onSearch={fetchCategory}
												onChange={handleExcludecategChange}
												style={{ width: "100%" }}
											>
												{data.map(d => (
													<Option key={d.value}>{d.text}</Option>
												))}
											</Select>
										</Col>
										<Divider dashed />
									</Row>
									<Col xs={24} lg={24}>
										<div style={{ marginBottom: "10px" }}>Allowed Emails </div>
										<Form.Item>
											<Input
												name="allowed_emails"
												value={allowed_emails}
												onChange={e => onChange(e)}
												placeholder="Allowed Emails"
											/>
										</Form.Item>
									</Col>
								</TabPane>
								<TabPane tab="Uasge Limitation" key="3">
									<Col xs={24} lg={24}>
										<div style={{ marginBottom: "10px", marginTop: "0px" }}>
											Usage limit per Coupon
										</div>
										<Form.Item>
											<Input
												placeholder="
											Limit usage of"
												name="usage_limit_per_coupon"
												value={usage_limit_per_coupon}
												onChange={e => onChange(e)}
											/>
										</Form.Item>
									</Col>
									<Col xs={24} lg={24}>
										<div style={{ marginBottom: "10px" }}>
											Usage limit to x items
										</div>
										<Form.Item>
											<Input
												name="usage_limit_x_items"
												value={usage_limit_x_items}
												onChange={e => onChange(e)}
												placeholder="Apply to all qualifying items in cart"
											/>
										</Form.Item>
									</Col>
									<Col xs={24} lg={24}>
										<div style={{ marginBottom: "10px" }}>
											Usage limit per user
										</div>
										<Form.Item>
											<Input
												name="usage_limit_per_user"
												value={usage_limit_per_user}
												onChange={e => onChange(e)}
												placeholder="Limit usage to user"
											/>
										</Form.Item>
									</Col>
								</TabPane>
							</Tabs>
						</div>
					</Col>
					<Row type="flex" justify="space-around">
						<Col span={10}>
							<Button
								type="primary"
								size={"large"}
								htmlType="submit"
								style={{ textAlign: "center", marginTop: "10px" }}
							>
								UPDATE
							</Button>
						</Col>
						<Col span={10}>
							<Link to="/cms/admin/coupon-management">
								<Button
									type="default"
									size={"large"}
									style={{ textAlign: "center", marginTop: "10px" }}
								>
									CANCEL
								</Button>
							</Link>
						</Col>
					</Row>
				</Row>
			</Form>
		</Fragment>
	);
};

Edit.propTypes = {
	getCouponCode: PropTypes.func.isRequired,
	GetCategory: PropTypes.func.isRequired,
	GetApplication: PropTypes.func.isRequired,
	isSubmitted: PropTypes.bool,
	user: PropTypes.object.isRequired,
	updateCoupon: PropTypes.func.isRequired
};
const mapStateToProps = state => ({
	isSubmitted: state.allcoupons.isSubmitted,
	user: state.auth.user
});
export default connect(
	mapStateToProps,
	{ getCouponCode, updateCoupon, GetCategory, GetApplication }
)(Edit);
