import axios from 'axios';
import {
	REGISTER_SUCCESS,
	REGISTER_FAIL,
	LOGIN_FAIL,
	LOGIN_SUCCESS,
	LOGOUT,
	USER_LOADED,
	AUTH_ERROR,
	UPDATE_FAIL,
	UPDATE_SUCCESS,
	FORGOT_PASSWORD
} from './constants';
import { setAlert } from "./alert.action"
import setAuthToken from '../utils/setAuthToken';

//Load User
export const loadUser = () => async dispatch => {
	if (localStorage.token) {
		setAuthToken(localStorage.token);
	}
	try {
		const res = await axios.get('/users/profile');
		dispatch({
			type: USER_LOADED,
			payload: res.data
		});
	} catch (error) {
		console.log(error,"auth")
		dispatch({
			type: AUTH_ERROR
		});
	}
};

// Register User
export const register = ({
	firstName,
	lastName,
	email,
	phone,
	password,
	age,
	gender,
	country
}) => async dispatch => {
	const config = {
		headers: {
			'Content-Type': 'application/json'
		}
	};

	const body = JSON.stringify({
		firstName,
		lastName,
		email,
		password,
		phone,
		age,
		country,
		gender
	});

	try {
		const res = await axios.post('/signup', body, config);
		dispatch({
			type: REGISTER_SUCCESS,
			payload: res.data
		});
	} catch (error) {
		console.error(error);
		dispatch({
			type: REGISTER_FAIL
		});
	}
};

export const ChangeUserRole = ({ id, role }) => async dispatch => {
	console.log(id, role)
	try {
		const config = {
			headers: {
				'Content-Type': 'application/json'
			}
		}
		const body = JSON.stringify({ role })
		const res = await axios.put(`/user-role/${id}`, body, config)
		dispatch({
			type: UPDATE_SUCCESS,
			payload: res.data
		})
		console.log(res.data)
	} catch (error) {
		console.log(error)
		dispatch({
			type: UPDATE_FAIL,
		})
	}
}

// Login User
export const login = (email, password) => async dispatch => {
	const config = {
		headers: {
			'Content-Type': 'application/json'
		}
	};

	const body = JSON.stringify({ email, password });

	try {
		const res = await axios.post('/login', body, config);
		dispatch({
			type: LOGIN_SUCCESS,
			payload: res.data
		});
		console.log("User Action",res.data)
	} catch (error) {
		console.error(error);
		if (error.response && error.response.data.error) {
			// console.log(error.response && error.response.data.error)
			dispatch(setAlert(error.response.data.message, "danger"))

		}
		dispatch({
			type: LOGIN_FAIL
		});
	}
};

// Logout User
export const logout = () => async dispatch => {
	const config = {
		headers: {
			'Content-Type': 'application/json'
		}
	};

	try {
		const res = await axios.post('/logout', {}, config);
		dispatch({
			type: LOGOUT,
			payload: res.data.success
		});
	} catch (error) {
		console.error(error);
		// dispatch({
		// 	type: LOGOUT_FAIL
		// });
	}
};

// Update User
export const update = ({
	firstName,
	lastName,
	email,
	phone,
	age,
	gender,
	country,
	line1,
	line2,
	city,
	state,
	postal_code
}) => async dispatch => {
	const config = {
		headers: {
			'Content-Type': 'application/json'
		}
	};

	const body = JSON.stringify({
		firstName,
		lastName,
		email,
		phone,
		age,
		gender,
		country,
		line1,
		line2,
		city,
		state,
		postal_code
	});

	try {
		const res = await axios.put('/users/profile', body, config);
		dispatch({
			type: UPDATE_SUCCESS,
			payload: res.data,
			edit: true
		});
	} catch (error) {
		console.error(error);
		dispatch({
			type: UPDATE_FAIL
		});
	}
};

// forgot password
export const forgotPassword = email => async dispatch => {
	const config = {
		headers: {
			'Content-Type': 'application/json'
		}
	};

	const body = JSON.stringify({ email });

	try {
		await axios.post('/forgot-password', body, config);
		dispatch({
			type: FORGOT_PASSWORD
		});
	} catch (error) {
		console.error(error);
	}
};

// reset password
export const resetPassword = ({ token, password }) => async dispatch => {
	const config = {
		headers: {
			'Content-Type': 'application/json'
		}
	};

	const body = JSON.stringify({ password });

	try {
		const res = await axios.patch(`/reset-password/${token}`, body, config);
		dispatch({
			type: UPDATE_SUCCESS,
			payload: res.data,
			edit: true
		});
	} catch (error) {
		console.error(error);
		dispatch({
			type: UPDATE_FAIL
		});
	}
};
