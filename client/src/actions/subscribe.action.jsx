import axios from "axios";
import {GET_SUBS ,SUBS_SUCCESS, SUBS_FAIL } from "./constants";

export const subscribe = (
  email
) => async dispatch => {
  const config = {
    headers: {
      "Content-Type": "application/json"
    }
  };

  const body = JSON.stringify({
    email
  });
  console.log(email);
  try {
    const res = await axios.post("/subscription", body, config);
    dispatch({
      type: SUBS_SUCCESS,
      payload: res.data
    });
  } catch (error) {
    console.error(error);
    dispatch({
      type: SUBS_FAIL
    });
  }
};

export const getsubscribe = () => async dispatch => {
  try {
    const res = await axios.get("/subscription");
    dispatch({
      type: GET_SUBS,
      payload: res.data
    });
    console.log("subs data",res.data)
  } catch (error) {
    console.error(error);
    dispatch({
      type: SUBS_FAIL
    });
  }
};

