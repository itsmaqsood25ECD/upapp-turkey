/* eslint react/prop-types: 0 */
import axios from "axios";
import {
  GET_ALL_ORDERS,
  ALL_ORDERS_ERROR,
  GET_CANCELLED_ORDERS,
  CANCELLATION_APPROVED,
  CANCELLATION_ERROR
} from "../constants";

//Get Orders
export const getAllOrders = () => async dispatch => {
  try {
    const res = await axios.get("/get-all-orders");
    dispatch({
      type: GET_ALL_ORDERS,
      payload: res.data
    });
  } catch (error) {
    console.error(error);
    dispatch({
      type: ALL_ORDERS_ERROR
    });
  }
};

export const getCancelledOrders = () => async dispatch => {
  try {
    const res = await axios.get("/cancel-subscription");
    dispatch({
      type: GET_CANCELLED_ORDERS,
      payload: res.data
    });
  } catch (error) {
    console.error(error);
  }
};

export const approveCancellation = id => async dispatch => {
  try {
    const res = await axios.patch(`/cancel-subscription/${id}`);
    dispatch({
      type: CANCELLATION_APPROVED,
      payload: res.data
    });
  } catch (error) {
    console.error(error);
    dispatch({
      type: CANCELLATION_ERROR
    });
  }
};

export const declineCancellation = id => async dispatch => {
  try {
    const res = await axios.delete(`/cancel-subscription/${id}`);
  } catch (error) {
    console.error(error);
  }
};

export const verifyPayment = ({
  id,
  end_date,
  recurring_date,
  start_date
}) => async dispatch => {
  const config = {
    headers: {
      "Content-Type": "application/json"
    }
  };
  const body = JSON.stringify({
    end_date,
    recurring_date,
    status: {
      purpose: "Activation",
      state: "Active"
    },
    start_date
  });
  try {
    const res = await axios.patch(`/bank-transfer/${id}`, body, config);
  } catch (error) {
    console.error(error);
  }
};
