import axios from 'axios';
import {
  BLOG_FAIL,
  BLOG_SUCCESS,
  GET_BLOG,
  GET_BLOGS,
  GET_RECENT_BLOG,
  GET_BLOG_FAIL,
  BLOG_DELETE_SUCCESS,
  BLOG_DELETE_FAIL
} from '../constants';


//Post Blog
export const PostBlog = ({
  title,
  content,
  featureImage,
  author,
}) => async dispatch => {
  const config = {
    headers: {
      "Content-Type": "application/json"
    }
  };
  const body = JSON.stringify({
    title,
    content,
    author,
    slug:title.split(" ").join("-"),
    featured_image: featureImage
  });
  try {
    const res = await axios.post('/add-blog', body, config);
    dispatch({
      type: BLOG_SUCCESS,
      payload: res.data
    });
  } catch (error) {
    dispatch({
      type: BLOG_SUCCESS
    });
  }
};

// Get All Blog
export const getAllBlog = () => async dispatch => {
  try {
    const res = await axios.get("/get-blog");
    dispatch({
      type: GET_BLOGS,
      payload: res.data
    });
  } catch (error) {
    console.error(error);
    dispatch({
      type: BLOG_FAIL,
      payload: error
    });
  }
};

// Get Recent Blog
export const getRecentBlog = () => async dispatch => {
  try {
    const res = await axios.get("/get-recent-blog");
    dispatch({
      type: GET_RECENT_BLOG,
      payload: res.data
    });
  } catch (error) {
    console.error(error);
    dispatch({
      type: BLOG_FAIL,
      payload: error
    });
  }
};

// Get Blog
export const getBlog = id => async dispatch => {
  try {
    const res = await axios.get(`/get-blog/${id}`);
    dispatch({
      type: GET_BLOG,
      payload: res.data
    });
    return res.data.result
  } catch (error) {
    dispatch({
      type: GET_BLOG_FAIL,
      payload: error
    });
  }
};



//Post Blog
export const UpdateBlog = ({
  id,
  title,
  content,
  featureImage,
  author,
}) => async dispatch => {
  const config = {
    headers: {
      "Content-Type": "application/json"
    }
  };
  const body = JSON.stringify({
    title,
    content,
    author,
    slug:title.split(" ").join("-"),
    featured_image: featureImage
  });
  try {
    const res = await axios.put(`/update-blog/${id}`, body, config);
    dispatch({
      type: BLOG_SUCCESS,
      payload: res.data
    });
    dispatch({
      type: GET_BLOG_FAIL
    })
  } catch (error) {
    console.errro(error)
    dispatch({
      type: GET_BLOG_FAIL
    });
  }
};

//  Delete Blog 
export const DeleteBlog = (id) => async dispatch => {
  try {
    const res = await axios.delete(`/delete-blog/${id}`)
    dispatch({
      type: BLOG_DELETE_SUCCESS,
      payload: res.data
    });
  } catch (error) {
    dispatch({
      type: BLOG_DELETE_FAIL
    });
  }
}

