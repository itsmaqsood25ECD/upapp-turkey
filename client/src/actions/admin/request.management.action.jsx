import axios from 'axios'
import { GET_CONTACT, CONTACT_FAIL } from '../constants'

// All Request

export const Allcontact = () => async dispatch => {

  try {
    const res = await axios.get('/contact-us');
    dispatch({
      type: GET_CONTACT,
      payload: res.data
    });
    console.log(res.data);
  } catch (error) {
    console.error(error);
    dispatch({
      type: CONTACT_FAIL
    });
  }
};