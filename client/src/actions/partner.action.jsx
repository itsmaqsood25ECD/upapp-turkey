import axios from "axios";
import { PARTNER_SUCCESS, PARTNER_FAIL } from "./constants";

// Become our partner Form Submission
export const BecomePartner = ({
  name,
  company,
  email,
  mobile,
  country,
  company_size
}) => async dispatch => {
  const config = {
    headers: {
      "Content-Type": "application/json"
    }
  };

  const body = JSON.stringify({
    name,
    company,
    email,
    mobile,
    country,
    company_size
  });
  console.log(body);
  try {
    const res = await axios.post("/partners", body, config);
    dispatch({
      type: PARTNER_SUCCESS,
      payload: res.data
    });
  } catch (error) {
    console.error(error);
    dispatch({
      type: PARTNER_FAIL
    });
  }
};
