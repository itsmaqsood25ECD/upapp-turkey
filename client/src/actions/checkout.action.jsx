import axios from 'axios';
import { GET_PAYMENTDETAILS, PROCEED_TO_PAY, PAYMENT_FAILED, GET_COUPON_DETAILS, COUPON_FAILURE } from './constants';

export const proceedToPay = ({
	pack_type,
	plan_name,
	plan_type,
	product_name,
	theme_id,
	plan_price,
	business_category,
	business_email,
	business_logo,
	domain_provider,
	domain_username,
	domain_password,
	default_currency,
	primary_color,
	secondary_color,
	languages,
	own_domain,
	payment_gateway_provider,
	payment_gateway_country,
	payment_gateway_details,
	shipping_gateway_provider,
	shipping_gateway_country,
	shipping_gateway_details,
	training_hours,
	product_list_count,
	additional_costs,
	token,
	total_price,
	selfDomainAdd
}) => async dispatch => {
	const config = {
		headers: {
			"Content-Type": "application/json"
		}
	};

	const body = JSON.stringify({
		pack_type,
		plan_name,
		plan_type,
		product_name,
		theme_id,
		plan_price,
		business_category,
		business_email,
		business_logo,
		domain_provider,
		domain_username,
		domain_password,
		default_currency,
		primary_color,
		secondary_color,
		languages,
		own_domain,
		payment_gateway_provider,
		payment_gateway_country,
		payment_gateway_details,
		shipping_gateway_provider,
		shipping_gateway_country,
		shipping_gateway_details,
		training_hours,
		product_list_count,
		additional_costs,
		token,
		total_price,
		selfDomainAdd
	});
	console.log(body)
	try {
		const response = await axios.post("/proceed_to_pay", body, config);
		dispatch({
			type: PROCEED_TO_PAY,
			payload: response.data
		});

	} catch (error) {
		console.log(error);
		dispatch({
			type: PAYMENT_FAILED
		});
	}
};

export const getCouponCode = (coupon) => async (dispatch) => {
	try {
		let res;
		if (process.env.NODE_ENV === 'production') {
			res = await axios.get(`https://www.upappfactory.com/coupon-management/${coupon}`);
		} else res = await axios.get(`/coupon-management/${coupon}`);
		return res.data;
	} catch (error) {
		console.error(error);
		dispatch({
			type: COUPON_FAILURE
		});
	}
};

export const BankTransfer = ({
	appName,
	description,
	packType,
	taxPrice,
	quantity,
	totalPrice,
	discountPrice,
	coupon_code,
	firstName,
	lastName,
	phone,
	email,
	line1,
	line2,
	city,
	country,
	state,
	postal_code
}) => async (dispatch) => {
	const config = {
		headers: {
			'Content-Type': 'application/json'
		}
	};
	const body = JSON.stringify({
		appName,
		description,
		packType,
		taxPrice,
		quantity,
		total_price: totalPrice,
		discount_price: discountPrice,
		coupon_code,
		billing_info: {
			firstName,
			lastName,
			phone,
			email,
			address: {
				line1,
				line2,
				city,
				state,
				postal_code,
				country
			}
		},
		status: {
			purpose: 'Activation',
			state: 'Pending'
		}
	});
	console.log(body)
	try {
		const res = await axios.post('/bank-transfer', body, config);
		console.log(res.data)
		return res.data.result;
	} catch (error) {
		console.error(error);
	}
};

