import axios from "axios";
import { GET_PAYMENTS } from "./constants";

//Get Orders
export const getPayments = () => async dispatch => {
	try {
		const res = await axios.get("/get-orders-details");
		dispatch({
			type: GET_PAYMENTS,
			payload: res.data
		});
	} catch (error) {
		console.error(error);
		// dispatch({
		// 	type: ORDER_ERROR
		// });
	}
};
// Get payment Token
export const getPayemntTokenDetails = () => async dispatch => {
	console.log('called action')
	try {
		const res = await axios.get('/get_payment_token');
		console.log(res.data)
		return res.data.result
	} catch (error) {
		console.error(error)
	}
}
