import axios from 'axios';
import { GET_ORDERS, ORDER_ERROR, PROCEED_TO_PAY ,GET_ORDER_DETAILS} from './constants';

//Get User Orders
export const getUserOrders = () => async dispatch => {
	try {
		const res = await axios.get('/get_user_orders');
		dispatch({
			type: GET_ORDERS,
			payload: res.data
		});
		// console.log(res.data)
	} catch (error) {
		console.error(error);
		dispatch({
			type: ORDER_ERROR
		});
	}
};

//Get User Order details
export const getOrderDetails = (id) => async dispatch => {
	try {
		const res = await axios.get(`/get_user_order_details/${id}`);
		dispatch({
			type: GET_ORDER_DETAILS,
			payload: res.data
		});
	} catch (error) {
		console.error(error);
		dispatch({
			type: ORDER_ERROR
		});
	}
};

export const cancelSubscription = id => async dispatch => {
	try {
		await axios.post(`/cancel-subscription/${id}`);
	} catch (error) {
		console.error(error);
	}
};
export const upgradeSubscription = id => async dispatch => {
	const config = {
		headers: {
			'Content-Type': 'application/json'
		}
	};
	const body = JSON.stringify({
		upgraded: true
	});
	console.log(body);
	try {
		const res = await axios.post(`upgrade-subscription/${id}`, body, config);
		dispatch({
			type: PROCEED_TO_PAY,
			payload: res.data
		});
	} catch (error) {
		console.error(error);
	}
};

export const uploadReceipt = ({ id, reciepts }) => async dispatch => {
	const config = {
		headers: {
			'Content-Type': 'application/json'
		}
	};
	const body = JSON.stringify({
		reciepts
	});
	console.log(body);
	try {
		const res = await axios.patch(`/reciept-upload/${id}`, body, config);
		console.log(res);
	} catch (error) {
		console.log(error);
	}
};
