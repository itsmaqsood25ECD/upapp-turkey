const mongoose = require('mongoose')
const validator = require('validator')
const bcrypt = require('bcryptjs')
const moment = require('moment')


const Schema = mongoose.Schema;

const OrderSchema = new Schema({
	UID: {
		type: String
	},
	pack_type: {
		type: String,
		required: true
	},
	plan_name: {
		type: String,
		required: true
	},
	plan_type: {
		type: String,
		required: true
	},
	product_name: {
		type: String,
		required: true
	},
	theme_id: {
		type: String
	},
	plan_price: {
		type: Number,
		required: true
	},
	business_category: {
		type: String,
		required: true
	},
	business_email: {
		type: String,
		required: true,
		lowercase: true,
		validate(value) {
			if (!validator.isEmail(value)) {
				throw new Error('please type in a valid email')
			}
		}
	},
	business_logo: {
		type: String,
		required: true
	},
	domain_provider: {
		type: String
	},
	domain_username: {
		type: String
	},
	domain_password: {
		type: String
	},
	default_currency: {
		type: String
	},
	primary_color: {
		type: String
	},
	secondary_color: {
		type: String
	},
	languages: [{
		type: String
	}],
	payment_gateway_provider: {
		type: String
	},
	payment_gateway_country: {
		type: String
	},
	payment_gateway_details: {
		type: String
	},
	shipping_gateway_provider: {
		type: String
	},
	shipping_gateway_country: {
		type: String
	},
	shipping_gateway_details: {
		type: String
	},
	training_hours: {
		type: Number
	},
	product_listing: {
		type: Number
	},
	product_list_count: {
		type: Number
	},
	self_domain_add: {
		type: Boolean
	},
	additional_costs: {
		shipping_cost: {
			type: Number
		},
		training_cost: {
			type: Number
		},
		product_cost: {
			type: Number
		},
		payment_gateway_cost: {
			type: Number
		},
		shipping_gateway_cost: {
			type: Number
		},
		language_cost: {
			type: Number
		},
		one_time_setup_charge: {
			type: Number
		}
	},
	order_placed_at: {
		type: String,
		default: moment().format()
	},
	created_by: {
		type: mongoose.Schema.Types.ObjectId,
		ref: "User",
		required: true
	},
	total_price: {
		type: Number
	},
	upgraded: {
		type: Boolean,
		default: false
	},
	downgraded: {
		type: Boolean,
		default: false
	},
	deleted: {
		type: Boolean,
		default: false
	},
	own_domain: {
		type: String
	}
})


// find Orders by User
OrderSchema.statics.getOrderByUser = async (id) => {
	try {
		const orders = await Order.find({ created_by: id })
		return orders
	} catch (error) {
		console.error(error)
	}
}

// find User by Order
OrderSchema.statics.getUserIdByOrder = async (id) => {
	try {
		const res = await Order.find({ OrderID: id }, { user: 1, _id: 0 })
		return res[0].user
	} catch (error) {
		console.error(error)
	}
}

// hash password middleware
OrderSchema.pre('save', async function (next) {
	const order = this
	if (order.isModified('domain_password')) {
		order.domain_password = await bcrypt.hash(order.domain_password, 8)
	}
	next()
})

const Order = mongoose.model("Order", OrderSchema)
module.exports = Order