const mongoose = require('mongoose')
// const moment = require('moment')

const Schema = mongoose.Schema

const TestSchema = new Schema({
  token: {
    type: String
  },
  createdAt: {
    type: Date,
    default: Math.round((new Date()).getTime() / 1000)
  },
  enc_request: {
    type: String
  },
  acess_code: {
    type: String,
    default: "AVUA03GJ12AM29AUMA"
  }
})

const Test = mongoose.model('Test', TestSchema)
module.exports = Test