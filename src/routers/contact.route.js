const express = require('express')
const Contact = require('../models/contact.model')
const nodemailer = require('nodemailer')
const moment = require('moment')
const mysql = require('mysql');

let connection = mysql.createPool({
	host: '74.208.88.152',
	user: 'admin_crm',
	password: 'Admin@123',
	database: 'admin_crm'
});


const router = new express.Router()

// Send the email
const transporter = nodemailer.createTransport({
	host: 'smtp.gmail.com',
	port: 465,
	secure: true,
	auth: {
		user: process.env.EMAIL_USERNAME,
		pass: process.env.EMAIL_PASSWORD
	}
})
// Send the email
const personalTransporter = nodemailer.createTransport({
	host: 'smtp.gmail.com',
	port: 465,
	secure: true,
	auth: {
		user: 'harris@upapp.co',
		pass: 'Harris@1234!'
	}
})


const OmanTransporter = nodemailer.createTransport({
	host: 'smtp.gmail.com',
	port: 465,
	secure: true,
	auth: {
		user: 'zach@upapp.co',
		pass: 'UPapp^321'
	}
})


connection.getConnection(function (err) {
	if (err) {
		console.error('error connecting: ' + err.stack);
		// connection = reconnect(connection)
		return;
	}

	console.log('My sql connected ');
});

function reconnect(connection) {
	console.log("\n New connection tentative...");

	//- Destroy the current connection variable
	if (connection) connection.destroy();

	//- Create a new one
	connection = mysql.createPool({
		host: '74.208.88.152',
		user: 'admin_crm',
		password: 'Admin@123',
		database: 'admin_crm'
	});

	//- Try to reconnect
	connection.getConnection(function (err) {
		if (err) {
			//- Try to connect every 2 seconds.
			setTimeout(reconnect(connection), 2000);
		} else {
			console.log("\n\t *** New connection established with the database. ***")
			console.log('connected as id ' + connection.threadId);
			return connection;
		}
	});
}

//- Error listener
connection.on('error', function (err) {
	console.log(err, "error")
	//- The server close the connection.
	if (err.code === "PROTOCOL_CONNECTION_LOST") {
		console.log("/!\\ Cannot establish a connection with the database. /!\\ (" + err.code + ")");
		connection = reconnect(connection);
	}

	//- Connection in closing
	else if (err.code === "PROTOCOL_ENQUEUE_AFTER_QUIT") {
		console.log("/!\\ Cannot establish a connection with the database. /!\\ (" + err.code + ")");
		connection = reconnect(connection);
	}

	//- Fatal error : connection variable must be recreated
	else if (err.code === "PROTOCOL_ENQUEUE_AFTER_FATAL_ERROR") {
		console.log("/!\\ Cannot establish a connection with the database. /!\\ (" + err.code + ")");
		connection = reconnect(connection);
	}

	//- Error because a connection is already being established
	else if (err.code === "PROTOCOL_ENQUEUE_HANDSHAKE_TWICE") {
		console.log("/!\\ Cannot establish a connection with the database. /!\\ (" + err.code + ")");
	}

	//- Anything else
	else {
		console.log("/!\\ Cannot establish a connection with the database. /!\\ (" + err.code + ")");
		connection = reconnect(connection);
	}

});

// post contact form data
router.post('/contact-us', async (req, res) => {
	try {
		const prevContact = await Contact.find({}, { UID: 1, _id: 0 }).sort({ _id: -1 })
		let counter, UID, UAF
		if (prevContact.length === 0) {
			counter = 1
		} else {
			counter = prevContact[0].UID.slice(8)
			counter = +counter + 1
		}
		UAF = "UAF-CON-"
		if (counter < 10) {
			UID = `${UAF}0000000${counter}`
		} else if (counter < 100 && counter >= 10) {
			UID = `${UAF}0000000${counter}`
		} else if (counter < 1000 && counter >= 100) {
			UID = `${UAF}000000${counter}`
		} else if (counter < 10000 && counter >= 1000) {
			UID = `${UAF}00000${counter}`
		} else if (counter < 100000 && counter >= 10000) {
			UID = `${UAF}0000${counter}`
		} else if (counter < 1000000 && counter >= 100000) {
			UID = `${UAF}000${counter}`
		} else if (counter < 10000000 && counter >= 1000000) {
			UID = `${UAF}00${counter}`
		} else if (counter < 100000000 && counter >= 10000000) {
			UID = `${UAF}0${counter}`
		} else {
			UID = `${UAF}${counter}`
		}
		const contact = new Contact(req.body)
		contact.ticket_no = Date.now()
		contact.UID = UID
		if (!contact)
			return res.status(400).send({ error: true, success: false, message: "Bad Request", result: {} })
		await contact.save()
		let mailOptions, notify
		if (contact.type === 'ExistingUser' && contact.category === 'Billing') {
			mailOptions = {
				from: process.env.EMAIL_USERNAME,
				to: req.body.email,
				subject: `#${contact.UID} Call Back Request for ${contact.name}, ${moment().format('L')}`,
				bcc: "payments@upappfactory.com",
				html: `<!DOCTYPE html>
				<html lang="en">
					<head>
						<meta charset="UTF-8" />
						<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
						<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
						<title>
							Contact Us
						</title>
					</head>
	
					<body style="background: #fbfbfb;font-size:14px;font-family: Helvetica, sans-serif; line-height: 1.5">
						<table style="max-width: 500px;margin:0 auto;padding-bottom: 0px;background: #fff;border-spacing: 0px">
							<tbody>
								<tr style="display:inline-table;width: 100%;background: #f7f7f7;padding-top: 20px;padding-bottom: 20px;">
									<td style="padding-left: 15px;">
										<a href="">
											<img style="width: 150px" width="150" src="https://upappfactory-static-images.s3.us-east-2.amazonaws.com/logo.png" alt="" />
										</a>
									</td>
								</tr>
								<td style="font-size:14px; font-family: Helvetica, sans-serif; color: #333333;padding: 20px;">
									Dear ${contact.name},
									<br /><br />
									We understand that you are trying to reach out to the support team
									at UPappfactory.
									Since we have received your request with the ticket
									#${contact.UID}, our support team will be in touch with you
									very soon to assist you with your concerns.
									<br /><br />
									Regards,
									<br />
									Support Team
									<br />
									UPappfactory
								</td>
								<tr style="text-align: center;background: #f7f7f7">
									<td style="padding:0px 20px;text-align: center;background: #f7f7f7">
									</td>
								</tr>
								<tr style="text-align: center;background: #f7f7f7">
									<td style=" padding:0px 20px; text-align: center;padding-right: 15px;">
										<p style="margin: 10px auto 0px auto;line-height: 0.8;">
											<a href="mailto:contactus@upapp.co" style="text-decoration: none;">contactus@upapp.co</a>
											|
											<a style="text-decoration: none;" href="www.upappfactory.com">Upappfactory</a><br /><br />
										</p>
										<a href="https://www.facebook.com/UPappfactory/"><img style="width: 25px" width="25" src="https://upappfactory-static-images.s3.us-east-2.amazonaws.com/facebook-icon.png" /></a>
										<a href="https://www.linkedin.com/company/upappfactory/"><img style="width: 25px" width="25" src="https://upappfactory-static-images.s3.us-east-2.amazonaws.com/linkedin-icon.png" /></a>
										<a href="https://www.instagram.com/upappfactory/"><img style="width: 25px" width="25" src="https://upappfactory-static-images.s3.us-east-2.amazonaws.com/Instagram-Icon.png" /></a>
									</td>
								</tr>
								<tr style="padding:20px;text-align: center;background: #f7f7f7">
									<td style="padding: 5px 20px 20px 20px;text-align: center;background: #f7f7f7;">
										Address: 531A Upper Cross Street, #04-95, Hong Lim Complex 051531 Singapore, Ph: +6567146696
									</td>
								</tr>
							</tbody>
						</table>
					</body>
				</html>`
			}
			notify = {
				from: process.env.EMAIL_USERNAME,
				to: "payments@upappfactory.com",
				subject: `#${contact.UID} Call Back Request for ${contact.name}, ${moment().format('L')}`,
				bcc: "ali@upappfactory.com, aslam@upappfactory.com",
				html: `<!DOCTYPE html>
				<html lang="en">
	
				<head>
					<meta charset="UTF-8" />
					<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
					<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
					<title>Call Back Request</title>
					<style>
						.bank td,
						th {
							border: 1px solid #dddddd;
							text-align: left;
							padding: 8px;
						}
	
						.bank tr:nth-child(even) {
							background-color: #dddddd;
						}
					</style>
				</head>
	
				<body style="background: #fbfbfb;font-size:14px;font-family: Helvetica, sans-serif; line-height: 1.5">
					<table style="max-width: 500px;margin:0 auto;padding-bottom: 0px;background: #fff;border-spacing: 0px">
						<tbody>
							<tr style="display:inline-table;width: 100%;background: #f7f7f7;padding-top: 20px;padding-bottom: 20px;">
								<td style="
								 padding-left: 15px;
							">
									<a href="">
										<img style="width: 150px" width="150"
											src="https://upappfactory-static-images.s3.us-east-2.amazonaws.com/logo.png" alt="" />
									</a>
								</td>
	
							</tr>
							<tr>
								<td style="font-size:14px;font-family: Helvetica, sans-serif;color: #333333;padding: 20px;">
									Hi Sales Team,
									<br>
									We have received a new Call Back request with ticket no: ${contact.UID}, please find the details below:
									<br>
									<br>
									<table class="bank" style="width:100%;border-spacing: 0px;border:none">
	
										<tr>
											<td>Ticket No</td>
											<td>${contact.UID}</td>
										</tr>
										<tr>
											<td>Name</td>
											<td>${contact.name}</td>
										</tr>
										<tr>
											<td>Email Address</td>
											<td>${contact.email}</td>
										</tr>
										<tr>
											<td>Phone No</td>
											<td>${contact.mobile}</td>
										</tr>
										<tr>
											<td>Message</td>
											<td>${contact.description}</td>
										</tr>
									</table>
	
									<br />
									Thanks!
									<br>
									<br />
									Regards,
									<br />
									UPapp factory
								</td>
							</tr>
							<tr style="text-align: center;background: #f7f7f7">
	
								<td style=" padding:0px 20px; text-align: center;padding-right: 15px;">
									<p style="margin: 10px auto 0px auto;line-height: 0.8;">
										<a href="mailto:contactus@upapp.co" style="text-decoration: none;">contactus@upapp.co</a>
										|
										<a style="text-decoration: none;" href="www.upappfactory.com">UPappfactory</a><br /><br />
									</p>
									<a href="https://www.facebook.com/UPappfactory/"><img style="width: 25px" width="25"
											src="https://upappfactory-static-images.s3.us-east-2.amazonaws.com/facebook-icon.png" /></a>
									<a href="https://www.linkedin.com/company/upappfactory/"><img style="width: 25px" width="25"
											src="https://upappfactory-static-images.s3.us-east-2.amazonaws.com/linkedin-icon.png" /></a>
									<a href="https://www.instagram.com/upappfactory/"><img style="width: 25px" width="25"
											src="https://upappfactory-static-images.s3.us-east-2.amazonaws.com/Instagram-Icon.png" /></a>
								</td>
	
							</tr>
							<tr style="padding:20px;text-align: center;background: #f7f7f7">
								<td style="padding: 5px 20px 20px 20px;text-align: center;background: #f7f7f7;">
									Address: 531A Upper Cross Street, #04-95, Hong Lim Complex 051531 Singapore, Ph: +6567146696
								</td>
							</tr>
						</tbody>
					</table>
				</body>
				</html>`
			}
		} else if (contact.type === 'ExistingUser' && contact.category === 'technicalsupport') {
			mailOptions = {
				from: process.env.EMAIL_USERNAME,
				to: req.body.email,
				subject: `#${contact.UID} Call Back Request for ${contact.name}, ${moment().format('L')}`,
				bcc: "help@upappfactory.com",
				html: `<!DOCTYPE html>
				<html lang="en">
					<head>
						<meta charset="UTF-8" />
						<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
						<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
						<title>
							Contact Us
						</title>
					</head>
	
					<body style="background: #fbfbfb;font-size:14px;font-family: Helvetica, sans-serif; line-height: 1.5">
						<table style="max-width: 500px;margin:0 auto;padding-bottom: 0px;background: #fff;border-spacing: 0px">
							<tbody>
								<tr style="display:inline-table;width: 100%;background: #f7f7f7;padding-top: 20px;padding-bottom: 20px;">
									<td style="padding-left: 15px;">
										<a href="">
											<img style="width: 150px" width="150" src="https://upappfactory-static-images.s3.us-east-2.amazonaws.com/logo.png" alt="" />
										</a>
									</td>
								</tr>
								<td style="font-size:14px; font-family: Helvetica, sans-serif; color: #333333;padding: 20px;">
									Dear ${contact.name},
									<br /><br />
									We understand that you are trying to reach out to the support team
									at UPappfactory.
									Since we have received your request with the ticket
									#${contact.UID}, our support team will be in touch with you
									very soon to assist you with your concerns.
									<br /><br />
									Regards,
									<br />
									Support Team
									<br />
									UPappfactory
								</td>
								<tr style="text-align: center;background: #f7f7f7">
									<td style="padding:0px 20px;text-align: center;background: #f7f7f7">
									</td>
								</tr>
								<tr style="text-align: center;background: #f7f7f7">
									<td style=" padding:0px 20px; text-align: center;padding-right: 15px;">
										<p style="margin: 10px auto 0px auto;line-height: 0.8;">
											<a href="mailto:contactus@upapp.co" style="text-decoration: none;">contactus@upapp.co</a>
											|
											<a style="text-decoration: none;" href="www.upappfactory.com">UPappfactory</a><br /><br />
										</p>
										<a href="https://www.facebook.com/UPappfactory/"><img style="width: 25px" width="25" src="https://upappfactory-static-images.s3.us-east-2.amazonaws.com/facebook-icon.png" /></a>
										<a href="https://www.linkedin.com/company/upappfactory/"><img style="width: 25px" width="25" src="https://upappfactory-static-images.s3.us-east-2.amazonaws.com/linkedin-icon.png" /></a>
										<a href="https://www.instagram.com/upappfactory/"><img style="width: 25px" width="25" src="https://upappfactory-static-images.s3.us-east-2.amazonaws.com/Instagram-Icon.png" /></a>
									</td>
								</tr>
								<tr style="padding:20px;text-align: center;background: #f7f7f7">
									<td style="padding: 5px 20px 20px 20px;text-align: center;background: #f7f7f7;">
										Address: 531A Upper Cross Street, #04-95, Hong Lim Complex 051531 Singapore, Ph: +6567146696
									</td>
								</tr>
							</tbody>
						</table>
					</body>
				</html>`
			}
			notify = {
				from: process.env.EMAIL_USERNAME,
				to: "help@upappfactory.com",
				subject: `#${contact.UID} Call Back Request for ${contact.name}, ${moment().format('L')}`,
				bcc: "ali@upappfactory.com, aslam@upappfactory.com",
				html: `<!DOCTYPE html>
				<html lang="en">
	
				<head>
					<meta charset="UTF-8" />
					<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
					<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
					<title>Call Back Request</title>
					<style>
						.bank td,
						th {
							border: 1px solid #dddddd;
							text-align: left;
							padding: 8px;
						}
	
						.bank tr:nth-child(even) {
							background-color: #dddddd;
						}
					</style>
				</head>
	
				<body style="background: #fbfbfb;font-size:14px;font-family: Helvetica, sans-serif; line-height: 1.5">
					<table style="max-width: 500px;margin:0 auto;padding-bottom: 0px;background: #fff;border-spacing: 0px">
						<tbody>
							<tr style="display:inline-table;width: 100%;background: #f7f7f7;padding-top: 20px;padding-bottom: 20px;">
								<td style="
								 padding-left: 15px;
							">
									<a href="">
										<img style="width: 150px" width="150"
											src="https://upappfactory-static-images.s3.us-east-2.amazonaws.com/logo.png" alt="" />
									</a>
								</td>
	
							</tr>
							<tr>
								<td style="font-size:14px;font-family: Helvetica, sans-serif;color: #333333;padding: 20px;">
									Hi Sales Team,
									<br>
									We have received a new Call Back request with ticket no: ${contact.UID}, please find the details below:
									<br>
									<br>
									<table class="bank" style="width:100%;border-spacing: 0px;border:none">
	
										<tr>
											<td>Ticket No</td>
											<td>${contact.UID}</td>
										</tr>
										<tr>
											<td>Name</td>
											<td>${contact.name}</td>
										</tr>
										<tr>
											<td>Email Address</td>
											<td>${contact.email}</td>
										</tr>
										<tr>
											<td>Phone No</td>
											<td>${contact.mobile}</td>
										</tr>
										<tr>
											<td>Message</td>
											<td>${contact.description}</td>
										</tr>
									</table>
	
									<br />
									Thanks!
									<br>
									<br />
									Regards,
									<br />
									UPapp factory
								</td>
							</tr>
							<tr style="text-align: center;background: #f7f7f7">
	
								<td style=" padding:0px 20px; text-align: center;padding-right: 15px;">
									<p style="margin: 10px auto 0px auto;line-height: 0.8;">
										<a href="mailto:contactus@upapp.co" style="text-decoration: none;">contactus@upapp.co</a>
										|
										<a style="text-decoration: none;" href="www.upappfactory.com">UPappfactory</a><br /><br />
									</p>
									<a href="https://www.facebook.com/UPappfactory/"><img style="width: 25px" width="25"
											src="https://upappfactory-static-images.s3.us-east-2.amazonaws.com/facebook-icon.png" /></a>
									<a href="https://www.linkedin.com/company/upappfactory/"><img style="width: 25px" width="25"
											src="https://upappfactory-static-images.s3.us-east-2.amazonaws.com/linkedin-icon.png" /></a>
									<a href="https://www.instagram.com/upappfactory/"><img style="width: 25px" width="25"
											src="https://upappfactory-static-images.s3.us-east-2.amazonaws.com/Instagram-Icon.png" /></a>
								</td>
	
							</tr>
							<tr style="padding:20px;text-align: center;background: #f7f7f7">
								<td style="padding: 5px 20px 20px 20px;text-align: center;background: #f7f7f7;">
									Address: 531A Upper Cross Street, #04-95, Hong Lim Complex 051531 Singapore, Ph: +6567146696
								</td>
							</tr>
						</tbody>
					</table>
				</body>
				</html>`
			}
		} else {

			const message1 = `\n*****	New Customer *****\n Message: ${contact.description}\n`
			
			connection.query("INSERT INTO `fx_leads` (`name`,`email`,`country`,`mobile`,`token`,`stage_id`,`sales_rep`,`lead_score`,`due_date`,`next_followup`,`unsubscribed_at`,`created_at`,`updated_at`,`rating_status`,`source`,`message`) VALUES ('" + contact.name + "','" + contact.email + "','" + contact.country + "','" + contact.mobile + "','" + UID + "','42','25','10',DATE_ADD(UTC_TIMESTAMP(),INTERVAL 330 MINUTE),DATE_ADD(UTC_TIMESTAMP(),INTERVAL 330 MINUTE),DATE_ADD(UTC_TIMESTAMP(),INTERVAL 330 MINUTE),DATE_ADD(UTC_TIMESTAMP(),INTERVAL 330 MINUTE),DATE_ADD(UTC_TIMESTAMP(),INTERVAL 330 MINUTE),'cold','51','" + contact.description + "') ON DUPLICATE KEY UPDATE `message`  = (SELECT CONCAT(message,'" + message1 + "') as msg FROM fx_leads as leads WHERE email='" + contact.email + "')")

			mailOptions = {
				from: process.env.EMAIL_USERNAME,
				to: req.body.email,
				subject: `#${contact.UID} Call Back Request for ${contact.name}, ${moment().format('L')}`,
				bcc: "contactus@upapp.co",
				html: `<!DOCTYPE html>
					<html lang="en">
						<head>
							<meta charset="UTF-8" />
							<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
							<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
							<title>
								Contact Us
							</title>
						</head>
		
						<body style="background: #fbfbfb;font-size:14px;font-family: Helvetica, sans-serif; line-height: 1.5">
							<table style="max-width: 500px;margin:0 auto;padding-bottom: 0px;background: #fff;border-spacing: 0px">
								<tbody>
									<tr style="display:inline-table;width: 100%;background: #f7f7f7;padding-top: 20px;padding-bottom: 20px;">
										<td style="padding-left: 15px;">
											<a href="">
												<img style="width: 150px" width="150" src="https://upappfactory-static-images.s3.us-east-2.amazonaws.com/logo.png" alt="" />
											</a>
										</td>
									</tr>
									<td style="font-size:14px; font-family: Helvetica, sans-serif; color: #333333;padding: 20px;">
										Dear ${contact.name},
										<br /><br />
										We understand that you are trying to reach out to the support team
										at UPappfactory.
										Since we have received your request with the ticket
										#${contact.UID}, our support team will be in touch with you
										very soon to assist you with your concerns.
										<br /><br />
										Regards,
										<br />
										Support Team
										<br />
										UPappfactory
									</td>
									<tr style="text-align: center;background: #f7f7f7">
										<td style="padding:0px 20px;text-align: center;background: #f7f7f7">
										</td>
									</tr>
									<tr style="text-align: center;background: #f7f7f7">
										<td style=" padding:0px 20px; text-align: center;padding-right: 15px;">
											<p style="margin: 10px auto 0px auto;line-height: 0.8;">
												<a href="mailto:contactus@upapp.co" style="text-decoration: none;">contactus@upapp.co</a>
												|
												<a style="text-decoration: none;" href="www.upappfactory.com">UPappfactory</a><br /><br />
											</p>
											<a href="https://www.facebook.com/UPappfactory/"><img style="width: 25px" width="25" src="https://upappfactory-static-images.s3.us-east-2.amazonaws.com/facebook-icon.png" /></a>
											<a href="https://www.linkedin.com/company/upappfactory/"><img style="width: 25px" width="25" src="https://upappfactory-static-images.s3.us-east-2.amazonaws.com/linkedin-icon.png" /></a>
											<a href="https://www.instagram.com/upappfactory/"><img style="width: 25px" width="25" src="https://upappfactory-static-images.s3.us-east-2.amazonaws.com/Instagram-Icon.png" /></a>
										</td>
									</tr>
									<tr style="padding:20px;text-align: center;background: #f7f7f7">
										<td style="padding: 5px 20px 20px 20px;text-align: center;background: #f7f7f7;">
											Address: 531A Upper Cross Street, #04-95, Hong Lim Complex 051531 Singapore, Ph: +6567146696
										</td>
									</tr>
								</tbody>
							</table>
						</body>
					</html>`
			}
			notify = {
				from: process.env.EMAIL_USERNAME,
				to: "contactus@upapp.co",
				subject: `#${contact.UID} Call Back Request for ${contact.name}, ${moment().format('L')}`,
				bcc: "ali@upapp.co, aslam@upapp.co,zach@upapp.co, saad@upapp.co , vaishali.uttam@upapp.co",
				html: `<!DOCTYPE html>
					<html lang="en">
		
					<head>
						<meta charset="UTF-8" />
						<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
						<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
						<title>Call Back Request</title>
						<style>
							.bank td,
							th {
								border: 1px solid #dddddd;
								text-align: left;
								padding: 8px;
							}
		
							.bank tr:nth-child(even) {
								background-color: #dddddd;
							}
						</style>
					</head>
		
					<body style="background: #fbfbfb;font-size:14px;font-family: Helvetica, sans-serif; line-height: 1.5">
						<table style="max-width: 500px;margin:0 auto;padding-bottom: 0px;background: #fff;border-spacing: 0px">
							<tbody>
								<tr style="display:inline-table;width: 100%;background: #f7f7f7;padding-top: 20px;padding-bottom: 20px;">
									<td style="
									 padding-left: 15px;
								">
										<a href="">
											<img style="width: 150px" width="150"
												src="https://upappfactory-static-images.s3.us-east-2.amazonaws.com/logo.png" alt="" />
										</a>
									</td>
		
								</tr>
								<tr>
									<td style="font-size:14px;font-family: Helvetica, sans-serif;color: #333333;padding: 20px;">
										Hi Sales Team,
										<br>
										We have received a new Call Back request with ticket no: ${contact.UID}, please find the details below:
										<br>
										<br>
										<table class="bank" style="width:100%;border-spacing: 0px;border:none">
		
											<tr>
												<td>Ticket No</td>
												<td>${contact.UID}</td>
											</tr>
											<tr>
												<td>Name</td>
												<td>${contact.name}</td>
											</tr>
											<tr>
												<td>Email Address</td>
												<td>${contact.email}</td>
											</tr>
											<tr>
												<td>Phone No</td>
												<td>${contact.mobile}</td>
											</tr>
											<tr>
												<td>Message</td>
												<td>${contact.description}</td>
											</tr>
										</table>
		
										<br />
										Thanks!
										<br>
										<br />
										Regards,
										<br />
										UPapp factory
									</td>
								</tr>
								<tr style="text-align: center;background: #f7f7f7">
		
									<td style=" padding:0px 20px; text-align: center;padding-right: 15px;">
										<p style="margin: 10px auto 0px auto;line-height: 0.8;">
											<a href="mailto:contactus@upapp.co" style="text-decoration: none;">contactus@upapp.co</a>
											|
											<a style="text-decoration: none;" href="www.upappfactory.com">UPappfactory</a><br /><br />
										</p>
										<a href="https://www.facebook.com/UPappfactory/"><img style="width: 25px" width="25"
												src="https://upappfactory-static-images.s3.us-east-2.amazonaws.com/facebook-icon.png" /></a>
										<a href="https://www.linkedin.com/company/upappfactory/"><img style="width: 25px" width="25"
												src="https://upappfactory-static-images.s3.us-east-2.amazonaws.com/linkedin-icon.png" /></a>
										<a href="https://www.instagram.com/upappfactory/"><img style="width: 25px" width="25"
												src="https://upappfactory-static-images.s3.us-east-2.amazonaws.com/Instagram-Icon.png" /></a>
									</td>
		
								</tr>
								<tr style="padding:20px;text-align: center;background: #f7f7f7">
									<td style="padding: 5px 20px 20px 20px;text-align: center;background: #f7f7f7;">
										Address: 531A Upper Cross Street, #04-95, Hong Lim Complex 051531 Singapore, Ph: +6567146696
									</td>
								</tr>
							</tbody>
						</table>
					</body>
					</html>`
			}
		}
		await transporter.sendMail(mailOptions)
		await transporter.sendMail(notify)
		res.status(201).send({ error: false, success: true, message: "Contact request success", result: contact })
	} catch (e) {
		console.error(e)
		res.status(500).send({ error: true, success: false, message: "Server Error", result: e })
	}
})

router.post('/request-demo', async (req, res) => {
	try {
		const prevContact = await Contact.find({}, { UID: 1, _id: 0 }).sort({ _id: -1 })
		let counter, UID, UAF
		if (prevContact.length === 0) {
			counter = 1
		} else {
			counter = prevContact[0].UID.slice(8)
			counter = +counter + 1
		}
		UAF = "UAF-DEM-"
		if (counter < 10) {
			UID = `${UAF}0000000${counter}`
		} else if (counter < 100 && counter >= 10) {
			UID = `${UAF}0000000${counter}`
		} else if (counter < 1000 && counter >= 100) {
			UID = `${UAF}000000${counter}`
		} else if (counter < 10000 && counter >= 1000) {
			UID = `${UAF}00000${counter}`
		} else if (counter < 100000 && counter >= 10000) {
			UID = `${UAF}0000${counter}`
		} else if (counter < 1000000 && counter >= 100000) {
			UID = `${UAF}000${counter}`
		} else if (counter < 10000000 && counter >= 1000000) {
			UID = `${UAF}00${counter}`
		} else if (counter < 100000000 && counter >= 10000000) {
			UID = `${UAF}0${counter}`
		} else {
			UID = `${UAF}${counter}`
		}
		const contact = new Contact(req.body)
		contact.ticket_no = Date.now()
		contact.UID = UID
		const message1 = `\n*****Request For Demo *****\n App interested in : ${contact.interested_in}\n App Name : ${contact.app_name}\n Business Name : ${contact.business_name}\n Business Details: ${contact.description}\n`
	
		connection.query("INSERT INTO `fx_leads` (`name`,`email`,`country`,`mobile`,`token`,`stage_id`,`sales_rep`,`lead_score`,`due_date`,`next_followup`,`unsubscribed_at`,`created_at`,`updated_at`,`rating_status`,`source`,`message`) VALUES ('" + contact.name + "','" + contact.email + "','" + contact.country + "','" + contact.mobile + "','" + UID + "','42','25','10',DATE_ADD(UTC_TIMESTAMP(),INTERVAL 330 MINUTE),DATE_ADD(UTC_TIMESTAMP(),INTERVAL 330 MINUTE),DATE_ADD(UTC_TIMESTAMP(),INTERVAL 330 MINUTE),DATE_ADD(UTC_TIMESTAMP(),INTERVAL 330 MINUTE),DATE_ADD(UTC_TIMESTAMP(),INTERVAL 330 MINUTE),'cold','51','" + contact.interested_in + "') ON DUPLICATE KEY UPDATE `message`  = (SELECT CONCAT(message,'" + message1 + "') as msg FROM fx_leads as leads WHERE email='" + contact.email + "')")

		if (!contact)
			return res.status(400).send({ error: true, success: false, message: "Bad Request", result: {} })
		await contact.save()
	
		let mailOptions
	
			mailOptions = {
				from: 'zach@upapp.co',
				to: contact.email,
				subject: 'Greetings from UPapp!',
				bcc: "contactus@upapp.co, maqsood@upapp.co",
				html: `<!DOCTYPE html lang="en">

				<head>
					<meta charset="UTF-8">
					<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
					<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
					<title>Contact</title>
					<style>
						.bank td,
						th {
							border: 1px solid #dddddd;
							text-align: left;
							padding: 8px;
						}
				
						.bank tr:nth-child(even) {
							background-color: #dddddd;
						}
					</style>
				</head>
				
				<body style="background: #fbfbfb;font-size:14px;font-family: Calibri, sans-serif; line-height: 1.5">
					<table style="margin:0 auto;padding-bottom: 0px;background: #fff;border-spacing: 0px">
						<tbody>
							<tr>
								<td style="padding: 40px 40px 5px 40px">
									Hi ${contact.name},
									<br>
									<br>
									This is Zach Naji, Sales and Business Development Manager at UPapp. I will be your consultant and
									contact point at UPapp.<br><br>
				
									Thank you for visiting UPapp’s website and submitting your request to connect. We are glad! <br><br>
				
									UPapp is a Mobile Applications factory with our HQ in Singapore. We are a team of 80+ Mobile
									Applications Business Experts. <br><br>
				
									I understand that you’re looking for a Mobile App probably with a beautiful web portal along with a
									state-of-the-art system to manage. It would be great if we can set up a call with you so that we can
									share ideas and give you some insightful information regarding your requirements.
									<br><br>
									Appreciate it if you could send me an email and suggest a good time to connect. You also may add me
									on WhatsApp or Skype via below mentioned details in case that's easier.<br><br>
				
				
				
									You can go through our profile with case studies on this <a
										href="https://calendly.com/zachnaji/digital-consultation">link</a><br><br>
				
									Looking forward to hearing from you.
									<br>
									<br>
									Regards,<br><br>
								</td>
                            </tr>
                            <tr>
                                <td style="padding: 0px 10px 20px 10px">
                                    <div>
                                        <div dir="ltr" class="gmail_signature" data-smartmail="gmail_signature">
                                            <div dir="ltr">
                                                <table cellpadding="0" cellspacing="0"
                                                    style="border-spacing:0px;border-collapse:collapse;color:rgb(68,68,68);width:480px;font-size:10pt;font-family:Arial,sans-serif;line-height:normal">
                                                    <tbody>
                                                        <tr>
                                                            <td valign="top"
                                                                style="padding:10px 0px 12px;width:160px;vertical-align:top"><a
                                                                    href="https://www.upappfactory.com/"
                                                                    style="background-color:transparent;color:rgb(51,122,183)"
                                                                    target="_blank"><img border="0" alt="Logo" width="141"
                                                                        src="https://upappfactory-static-images.s3.us-east-2.amazonaws.com/UPtm_Logo.png"
                                                                        style="border:0px;vertical-align:middle;width:141px;height:auto"></a>
                                                            </td>
                                                            <td style="padding:6px 0px;width:320px">
                                                                <table cellpadding="0" cellspacing="0"
                                                                    style="border-spacing:0px;border-collapse:collapse;background-color:transparent">
                                                                    <tbody>
                                                                        <tr>
                                                                            <td
                                                                                style="padding:0px 0px 3px;font-size:12pt;font-family:Arial,sans-serif;font-weight:bold;color:#4fbfdd">
                                                                                <span>Zach Naji</span></td>
                                                                        </tr>
                
                
                                                                        <tr>
                                                                            <td
                                                                                style="padding:0px 0px 3px;font-size:10pt;font-family:Arial,sans-serif;color:rgb(61,60,63)">
                                                                                <span><span zeum4c1="PR_1_0" data-ddnwab="PR_1_0"
                                                                                        aria-invalid="spelling" class="LI ng">Manager -
                                                                                        Sales &amp; Business Development</span></span>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td
                                                                                style="padding:0px 0px 11px;font-size:12pt;font-family:Arial,sans-serif;font-weight:700;color:rgb(61,60,63)">
                                                                                <span>UPapp PTE LTD.</span></td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td
                                                                                style="padding: 3px 0px;font-size:10pt;font-family:Arial,sans-serif;color:rgb(155,155,155);">
                                                                                <span><span
                                                                                        style="color: #4fbfdd;padding-right: 5px;font-weight: 700;">Skype</span><a
                                                                                        href="https://join.skype.com/invite/majtRFQzZlRf">Chat
                                                                                        Now</a></span></td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td
                                                                                style="padding: 3px 0px;font-size:10pt;font-family:Arial,sans-serif;color:rgb(155,155,155);">
                                                                                <span><span
                                                                                        style="color: #4fbfdd;padding-right: 5px;font-weight: 700;">Whatsapp</span>
                                                                                    +968 9238 4957</span></td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td
                                                                                style="padding: 3px 0px;font-size:10pt;font-family:Arial,sans-serif;color:rgb(155,155,155);">
                                                                                <span><span
                                                                                        style="color: #4fbfdd;padding-right: 5px;font-weight: 700;">W</span>
                                                                                    <a href="www.upapp.co">www.upapp.co</a></span></td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td
                                                                                style="padding: 3px 0px;font-size:10pt;font-family:Arial,sans-serif;color:rgb(155,155,155);">
                                                                                <span><span
                                                                                        style="color: #4fbfdd;padding-right: 5px;font-weight: 700;">E</span>&nbsp;</span><span
                                                                                    style="color:rgb(23,147,210)"><a
                                                                                        href="mailto:zach@upapp.co"
                                                                                        target="_blank">zach@upapp.co</a></span></td>
                                                                        </tr>
                
                
                                                                        <tr>
                                                                            <td
                                                                                style="padding:0px;font-size:10pt;font-family:Arial,sans-serif;color:rgb(155,155,155)">
                                                                                <span>531A, Upper Cross St. #04-95, Hong Lim Complex
                                                                                    051531, Singapore.</span></td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td style="padding:6px 0px 0px"><span
                                                                                    style="display:inline-block;height:22px"><span><a
                                                                                            href="https://www.facebook.com/UPappfactory/"
                                                                                            style="background-color:transparent;color:rgb(51,122,183)"
                                                                                            target="_blank"><img alt="Facebook icon"
                                                                                                border="0" width="20" height="20"
                                                                                                src="https://codetwocdn.azureedge.net/images/mail-signatures/generator-dm/elegant-logo/fb.png"
                                                                                                style="border:0px;vertical-align:middle;height:20px;width:20px"></a>&nbsp;&nbsp;</span><span><a
                                                                                            href="https://www.linkedin.com/company/upappfactory/"
                                                                                            style="background-color:transparent;color:rgb(51,122,183)"
                                                                                            target="_blank"><img alt="LinkedIn icon"
                                                                                                border="0" width="20" height="20"
                                                                                                src="https://codetwocdn.azureedge.net/images/mail-signatures/generator-dm/elegant-logo/ln.png"
                                                                                                style="border:0px;vertical-align:middle;height:20px;width:20px"></a>&nbsp;&nbsp;</span><span><a
                                                                                            href="https://www.instagram.com/upappfactory/"
                                                                                            style="background-color:transparent;color:rgb(51,122,183)"
                                                                                            target="_blank"><img alt="Instagram icon"
                                                                                                border="0" width="20" height="20"
                                                                                                src="https://codetwocdn.azureedge.net/images/mail-signatures/generator-dm/elegant-logo/it.png"
                                                                                                style="border:0px;vertical-align:middle;height:20px;width:20px"></a>&nbsp;&nbsp;</span></span>
                                                                            </td>
                                                                        </tr>
                                                                    </tbody>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="2"
                                                                style="padding:8px 0px 0px;border-top:1px solid rgb(23,147,210);width:480px;font-family:Arial,sans-serif;color:rgb(155,155,155);text-align:justify">
                                                                <span>This <span zeum4c1="PR_2_0" data-ddnwab="PR_2_0"
                                                                        aria-invalid="spelling" class="LI ng">e-mail</span> may contain
                                                                    confidential/privileged information. If you
                                                                    are not the intended recipient, please notify the sender and destroy
                                                                    this e-mail. Any
                                                                    unauthorised copying, disclosure or distribution of the material in
                                                                    this <span zeum4c1="PR_3_0" data-ddnwab="PR_3_0"
                                                                        aria-invalid="spelling" class="LI ng">e-mail</span> is
                                                                    forbidden.</span></td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </td>
                            </tr>
						</tbody>
					</table>
				</body>
				
				</html>`
			// }
		}
		let notify
		notify = {
			from: process.env.EMAIL_USERNAME,
			to: "contactus@upapp.co",
			// to:"maqsood@upapp.co",
			subject: `#${contact.UID} Demo Request for ${contact.name}, country ${contact.country}`,
			bcc: "aslam@upapp.co, maqsood@upapp.co",
			subject: 'Greeting From UPapp factory!',
			html: `<!DOCTYPE html>
				<html lang="en">
				
				<head>
					<meta charset="UTF-8" />
					<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
					<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
					<title>Bank Transfer</title>
					<style>
						.bank td,
						th {
							border: 1px solid #dddddd;
							text-align: left;
							padding: 8px;
						}
				
						.bank tr:nth-child(even) {
							background-color: #dddddd;
						}
					</style>
				</head>
				
				<body style="background: #fbfbfb;font-size:14px;font-family: Helvetica, sans-serif; line-height: 1.5">
					<table style="max-width: 500px;margin:0 auto;padding-bottom: 0px;background: #fff;border-spacing: 0px">
						<tbody>
							<tr style="display:inline-table;width: 100%;background: #f7f7f7;padding-top: 20px;padding-bottom: 20px;">
								<td style="
								 padding-left: 15px;
							">
									<a href="">
										<img style="width: 150px" width="150"
											src="https://upappfactory-static-images.s3.us-east-2.amazonaws.com/logo.png" alt="" />
									</a>
								</td>
				
							</tr>
							<tr>
								<td style="font-size:14px;font-family: Helvetica, sans-serif;color: #333333;padding: 20px;">
									Hi Sales Team,
									<br>
									We have received a new demo request with ticket no: ${contact.UID}, please find the details below:
									<br>
									<br>
									<table class="bank" style="width:100%;border-spacing: 0px;border:none">
				
										<tr>
											<td>Ticket No</td>
											<td>${contact.UID}</td>
										</tr>
										<tr>
										<td>Name</td>
										<td>${contact.name}</td>
									</tr>
									<tr>
										<td>Email Address</td>
										<td>${contact.email}</td>
									</tr>
									<tr>
										<td>Phone No</td>
										<td>${contact.mobile}</td>
									</tr>
									<tr>
										<td>Country</td>
										<td>${contact.country}</td>
									</tr>
									<tr>
										<td>App Type</td>
										<td>${contact.interested_in}</td>
									</tr>
									<tr>
										<td>App Name</td>
										<td>${contact.app_name}</td>
									</tr>
									<tr>
										<td>Businesss Name</td>
										<td>${contact.business_name}</td>
									</tr>
									<tr>
										<td>Details of business</td>
										<td>${contact.description}</td>
									</tr>
									</table>
				
									<br />
									Thanks!
									<br>
									<br />
									Regards,
									<br />
									UPapp factory
								</td>
							</tr>
							<tr style="text-align: center;background: #f7f7f7">
				
								<td style=" padding:0px 20px; text-align: center;padding-right: 15px;">
									<p style="margin: 10px auto 0px auto;line-height: 0.8;">
										<a href="mailto:contactus@upapp.co" style="text-decoration: none;">contactus@upapp.co</a>
										|
										<a style="text-decoration: none;" href="www.upappfactory.com">UPappfactory</a><br /><br />
									</p>
									<a href="https://www.facebook.com/UPappfactory/"><img style="width: 25px" width="25"
											src="https://upappfactory-static-images.s3.us-east-2.amazonaws.com/facebook-icon.png" /></a>
									<a href="https://www.linkedin.com/company/upappfactory/"><img style="width: 25px" width="25"
											src="https://upappfactory-static-images.s3.us-east-2.amazonaws.com/linkedin-icon.png" /></a>
									<a href="https://www.instagram.com/upappfactory/"><img style="width: 25px" width="25"
											src="https://upappfactory-static-images.s3.us-east-2.amazonaws.com/Instagram-Icon.png" /></a>
								</td>
				
							</tr>
							<tr style="padding:20px;text-align: center;background: #f7f7f7">
								<td style="padding: 5px 20px 20px 20px;text-align: center;background: #f7f7f7;">
									Address: 531A Upper Cross Street, #04-95, Hong Lim Complex 051531 Singapore, Ph: +6567146696
								</td>
							</tr>
						</tbody>
					</table>
				</body>
				
				</html>`
		}
		// if (contact.country === "Oman" || contact.country == "Bahrain" || contact.country == "Kuwait" || contact.country == "Qatar" || contact.country == "United Arab Emirates") {
		// 	await OmanTransporter.sendMail(mailOptions)
		// }
		// else {
			await OmanTransporter.sendMail(mailOptions)
		// }
		await transporter.sendMail(notify)
		res.status(201).send({ error: false, success: true, message: "Contact request success", result: contact })
	} catch (error) {
		console.log(error)
		return res.status(500).send({ error: true, success: false, message: "Server error", result: error })
	}
})

router.get('/contact-us', async (req, res) => {
	try {
		const contacts = await Contact.find({}).sort({ _id: -1 })
		if (!contacts)
			return res.status(404).send({ error: true, success: false, message: "No requests found", result: {} })
		return res.status(200).send({ error: false, success: true, message: "Request found", result: contacts })
	} catch (e) {
		res.status(500).send({ error: true, success: false, message: "server error", result: e })
	}
})

router.post('/make_your_app', async (req, res) => {
	try {

		const prevContact = await Contact.find({}, { UID: 1, _id: 0 }).sort({ _id: -1 })
		let counter, UID, UAF
		if (prevContact.length === 0) {
			counter = 1
		} else {
			counter = prevContact[0].UID.slice(8)
			counter = +counter + 1
		}
		UAF = "UAF-CUS-"
		if (counter < 10) {
			UID = `${UAF}0000000${counter}`
		} else if (counter < 100 && counter >= 10) {
			UID = `${UAF}0000000${counter}`
		} else if (counter < 1000 && counter >= 100) {
			UID = `${UAF}000000${counter}`
		} else if (counter < 10000 && counter >= 1000) {
			UID = `${UAF}00000${counter}`
		} else if (counter < 100000 && counter >= 10000) {
			UID = `${UAF}0000${counter}`
		} else if (counter < 1000000 && counter >= 100000) {
			UID = `${UAF}000${counter}`
		} else if (counter < 10000000 && counter >= 1000000) {
			UID = `${UAF}00${counter}`
		} else if (counter < 100000000 && counter >= 10000000) {
			UID = `${UAF}0${counter}`
		} else {
			UID = `${UAF}${counter}`
		}
		const make_your_app = new Contact(req.body)
		make_your_app.ticket_no = Date.now()
		make_your_app.UID = UID
		const message1 = `\n *****Customised App*****\n Subject: ${make_your_app.subject}\n  message:${make_your_app.description}\n`

		connection.query("INSERT INTO `fx_leads` (`name`,`email`,`country`,`mobile`,`token`,`stage_id`,`sales_rep`,`lead_score`,`due_date`,`next_followup`,`unsubscribed_at`,`created_at`,`updated_at`,`rating_status`,`source`,`message`) VALUES ('" + make_your_app.name + "','" + make_your_app.email + "','" + make_your_app.country + "','" + make_your_app.mobile + "','" + UID + "','42','25','10',DATE_ADD(UTC_TIMESTAMP(),INTERVAL 330 MINUTE),DATE_ADD(UTC_TIMESTAMP(),INTERVAL 330 MINUTE),DATE_ADD(UTC_TIMESTAMP(),INTERVAL 330 MINUTE),DATE_ADD(UTC_TIMESTAMP(),INTERVAL 330 MINUTE),DATE_ADD(UTC_TIMESTAMP(),INTERVAL 330 MINUTE),'cold','51','" + message1 + "') ON DUPLICATE KEY UPDATE `message`  = (SELECT CONCAT(message,'" + message1 + "') as msg FROM fx_leads as leads WHERE email='" + make_your_app.email + "')")

		if (!make_your_app)
			return res.status(400).send({ error: true, success: false, message: "Bad Request", result: {} })
		await make_your_app.save()
		const mailOptions = {
			from: process.env.EMAIL_USERNAME,
			to: make_your_app.email,
			subject: `Request for customised app`,
			bcc: "contactus@upapp.co , maqsood@upapp.co",
			html: `<!DOCTYPE html>
			<html lang="en">
				<head>
					<meta charset="UTF-8" />
					<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
					<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
					<title>
						Contact Us
					</title>
				</head>
				
				<body style="background: #fbfbfb;font-size:14px;font-family: Helvetica, sans-serif; line-height: 1.5">
					<table style="max-width: 500px;margin:0 auto;padding-bottom: 0px;background: #fff;border-spacing: 0px">
						<tbody>
							<tr style="display:inline-table;width: 100%;background: #f7f7f7;padding-top: 20px;padding-bottom: 20px;">
								<td style="padding-left: 15px;">
									<a href="">
										<img style="width: 150px" width="150" src="https://upappfactory-static-images.s3.us-east-2.amazonaws.com/logo.png" alt="" />
									</a>
								</td>
							</tr>
							<td style="font-size:14px; font-family: Helvetica, sans-serif; color: #333333;padding: 20px;">
								Dear ${make_your_app.name},
								<br /><br />
								We understand that you are trying to reach out to the sales team
								at UPappfactory.
								Since we have received your request with the ticket
								#${make_your_app.UID}, our sales team will be in touch with you
								very soon to assist you with your concerns.
								<br /><br />
								Regards,
								<br />
								Support Team
								<br />
								UPappfactory
							</td>
							<tr style="text-align: center;background: #f7f7f7">
								<td style="padding:0px 20px;text-align: center;background: #f7f7f7">
								</td>
							</tr>
							<tr style="text-align: center;background: #f7f7f7">
								<td style=" padding:0px 20px; text-align: center;padding-right: 15px;">
									<p style="margin: 10px auto 0px auto;line-height: 0.8;">
										<a href="mailto:contactus@upapp.co" style="text-decoration: none;">contactus@upapp.co</a>
										|
										<a style="text-decoration: none;" href="www.upappfactory.com">UPappfactory</a><br /><br />
									</p>
									<a href="https://www.facebook.com/UPappfactory/"><img style="width: 25px" width="25" src="https://upappfactory-static-images.s3.us-east-2.amazonaws.com/facebook-icon.png" /></a>
									<a href="https://www.linkedin.com/company/upappfactory/"><img style="width: 25px" width="25" src="https://upappfactory-static-images.s3.us-east-2.amazonaws.com/linkedin-icon.png" /></a>
									<a href="https://www.instagram.com/upappfactory/"><img style="width: 25px" width="25" src="https://upappfactory-static-images.s3.us-east-2.amazonaws.com/Instagram-Icon.png" /></a>
								</td>
							</tr>
							<tr style="padding:20px;text-align: center;background: #f7f7f7">
								<td style="padding: 5px 20px 20px 20px;text-align: center;background: #f7f7f7;">
									Address: 531A Upper Cross Street, #04-95, Hong Lim Complex 051531 Singapore, Ph: +6567146696
								</td>
							</tr>
						</tbody>
					</table>
				</body>
			</html>`
		}
		await transporter.sendMail(mailOptions)
		const notify = {
			from: process.env.EMAIL_USERNAME,
			to: "contactus@upapp.co",
			subject: `Request for customised app`,
			bcc: "aslam@upapp.co, maqsood@upapp.co",
			html: `<!DOCTYPE html>
			<html lang="en">
			
			<head>
				<meta charset="UTF-8" />
				<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
				<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
				<title>Call Back Request</title>
				<style>
					.bank td,
					th {
						border: 1px solid #dddddd;
						text-align: left;
						padding: 8px;
					}
			
					.bank tr:nth-child(even) {
						background-color: #dddddd;
					}
				</style>
			</head>
			
			<body style="background: #fbfbfb;font-size:14px;font-family: Helvetica, sans-serif; line-height: 1.5">
				<table style="max-width: 500px;margin:0 auto;padding-bottom: 0px;background: #fff;border-spacing: 0px">
					<tbody>
						<tr style="display:inline-table;width: 100%;background: #f7f7f7;padding-top: 20px;padding-bottom: 20px;">
							<td style="
							 padding-left: 15px;
						">
								<a href="">
									<img style="width: 150px" width="150"
										src="https://upappfactory-static-images.s3.us-east-2.amazonaws.com/logo.png" alt="" />
								</a>
							</td>
			
						</tr>
						<tr>
							<td style="font-size:14px;font-family: Helvetica, sans-serif;color: #333333;padding: 20px;">
								Hi Sales Team,
								<br>
								We have received a new request for customised app with ticket no: ${make_your_app.UID}, please find the details below:
								<br>
								<br>
								<table class="bank" style="width:100%;border-spacing: 0px;border:none">
			
									<tr>
										<td>Ticket No</td>
										<td>${make_your_app.UID}</td>
									</tr>
									<tr>
										<td>Name</td>
										<td>${make_your_app.name}</td>
									</tr>
									<tr>
										<td>Email Address</td>
										<td>${make_your_app.email}</td>
									</tr>
									<tr>
										<td>Phone No</td>
										<td>${make_your_app.mobile}</td>
									</tr>
									<tr>
										<td>Subject</td>
										<td>${make_your_app.subject}</td>
									</tr>
									<tr>
										<td>Message</td>
										<td>${make_your_app.description}</td>
									</tr>
								</table>
			
								<br />
								Thanks!
								<br>
								<br />
								Regards,
								<br />
								UPapp factory
							</td>
						</tr>
						<tr style="text-align: center;background: #f7f7f7">
			
							<td style=" padding:0px 20px; text-align: center;padding-right: 15px;">
								<p style="margin: 10px auto 0px auto;line-height: 0.8;">
									<a href="mailto:contactus@upapp.co" style="text-decoration: none;">contactus@upapp.co</a>
									|
									<a style="text-decoration: none;" href="www.upappfactory.com">UPappfactory</a><br /><br />
								</p>
								<a href="https://www.facebook.com/UPappfactory/"><img style="width: 25px" width="25"
										src="https://upappfactory-static-images.s3.us-east-2.amazonaws.com/facebook-icon.png" /></a>
								<a href="https://www.linkedin.com/company/upappfactory/"><img style="width: 25px" width="25"
										src="https://upappfactory-static-images.s3.us-east-2.amazonaws.com/linkedin-icon.png" /></a>
								<a href="https://www.instagram.com/upappfactory/"><img style="width: 25px" width="25"
										src="https://upappfactory-static-images.s3.us-east-2.amazonaws.com/Instagram-Icon.png" /></a>
							</td>
			
						</tr>
						<tr style="padding:20px;text-align: center;background: #f7f7f7">
							<td style="padding: 5px 20px 20px 20px;text-align: center;background: #f7f7f7;">
								Address: 531A Upper Cross Street, #04-95, Hong Lim Complex 051531 Singapore, Ph: +6567146696
							</td>
						</tr>
					</tbody>
				</table>
			</body>
			</html>`
		}
		await transporter.sendMail(notify)
		return res.status(201).send({ error: false, success: true, message: "New custom app success", result: make_your_app })
	} catch (error) {
		console.error(error)
		return res.status(500).send({ error: true, success: false, message: "Server error", result: error })
	}
})

router.post('/upappdream', async (req, res) => {
	try {
		const prevContact = await Contact.find({}, { UID: 1, _id: 0 }).sort({ _id: -1 })
		let counter, UID, UAF
		if (prevContact.length === 0) {
			counter = 1
		} else {
			counter = prevContact[0].UID.slice(8)
			counter = +counter + 1
		}
		UAF = "UAF-DRE-"
		if (counter < 10) {
			UID = `${UAF}0000000${counter}`
		} else if (counter < 100 && counter >= 10) {
			UID = `${UAF}0000000${counter}`
		} else if (counter < 1000 && counter >= 100) {
			UID = `${UAF}000000${counter}`
		} else if (counter < 10000 && counter >= 1000) {
			UID = `${UAF}00000${counter}`
		} else if (counter < 100000 && counter >= 10000) {
			UID = `${UAF}0000${counter}`
		} else if (counter < 1000000 && counter >= 100000) {
			UID = `${UAF}000${counter}`
		} else if (counter < 10000000 && counter >= 1000000) {
			UID = `${UAF}00${counter}`
		} else if (counter < 100000000 && counter >= 10000000) {
			UID = `${UAF}0${counter}`
		} else {
			UID = `${UAF}${counter}`
		}
		const contact = new Contact(req.body)
		contact.mobile = `${req.body.countryCode}${req.body.tel}`
		contact.countryCode = req.body.countryCode
		contact.business_name = req.body.interested
		contact.languages = req.body.language
		contact.budget = req.body.budget
		contact.ticket_no = Date.now()
		contact.type = 'upappdream'
		contact.UID = UID
		console.log(contact, "contact>>>>>>>>>")

		const upapp_dream = new Contact(req.body)
		upapp_dream.ticket_no = Date.now()
		upapp_dream.UID = UID
		const message1 = `\n *****Customised App*****\n Subject: ${upapp_dream.subject}\n  message:${upapp_dream.description}\n`

		connection.query("INSERT INTO `fx_leads` (`name`,`email`,`country`,`mobile`,`token`,`stage_id`,`sales_rep`,`lead_score`,`due_date`,`next_followup`,`unsubscribed_at`,`created_at`,`updated_at`,`rating_status`,`source`,`message`) VALUES ('" + upapp_dream.name + "','" + upapp_dream.email + "','" + upapp_dream.country + "','" + upapp_dream.mobile + "','" + UID + "','42','25','10',DATE_ADD(UTC_TIMESTAMP(),INTERVAL 330 MINUTE),DATE_ADD(UTC_TIMESTAMP(),INTERVAL 330 MINUTE),DATE_ADD(UTC_TIMESTAMP(),INTERVAL 330 MINUTE),DATE_ADD(UTC_TIMESTAMP(),INTERVAL 330 MINUTE),DATE_ADD(UTC_TIMESTAMP(),INTERVAL 330 MINUTE),'cold','51','" + message1 + "') ON DUPLICATE KEY UPDATE `message`  = (SELECT CONCAT(message,'" + message1 + "') as msg FROM fx_leads as leads WHERE email='" + upapp_dream.email + "')")

		console.log(connection.query("INSERT INTO `fx_leads` (`name`,`email`,`country`,`mobile`,`token`,`stage_id`,`sales_rep`,`lead_score`,`due_date`,`next_followup`,`unsubscribed_at`,`created_at`,`updated_at`,`rating_status`,`source`,`message`) VALUES ('" + upapp_dream.name + "','" + upapp_dream.email + "','" + upapp_dream.country + "','" + upapp_dream.mobile + "','" + UID + "','42','25','10',DATE_ADD(UTC_TIMESTAMP(),INTERVAL 330 MINUTE),DATE_ADD(UTC_TIMESTAMP(),INTERVAL 330 MINUTE),DATE_ADD(UTC_TIMESTAMP(),INTERVAL 330 MINUTE),DATE_ADD(UTC_TIMESTAMP(),INTERVAL 330 MINUTE),DATE_ADD(UTC_TIMESTAMP(),INTERVAL 330 MINUTE),'cold','51','" + message1 + "') ON DUPLICATE KEY UPDATE `message`  = (SELECT CONCAT(message,'" + message1 + "') as msg FROM fx_leads as leads WHERE email='" + upapp_dream.email + "')"))

		if (!contact)
			return res.status(400).send({ error: true, success: false, message: "Bad Request", result: {} })
		await contact.save()
		const mailOptions = {
			from: 'harris@upapp.co',
			to: req.body.email,
			subject: `Turn Your Ideas Into Reality`,
			bcc: "contactus@upapp.co, maqsood@upapp.co",
			html: `<!DOCTYPE html>
			<html lang="en">
			
			<head>
					<meta charset="UTF-8" />
					<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
					<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
					<title>Bank Transfer</title>
					<style>
							.bank td,
							th {
									border: 1px solid #dddddd;
									text-align: left;
									padding: 8px;
							}
			
							.bank tr:nth-child(even) {
									background-color: #dddddd;
							}
					</style>
			</head>
			
			<body style="background: #fbfbfb;font-size:14px;font-family: Helvetica, sans-serif; line-height: 1.5">
					<table style="margin:0 auto;padding-bottom: 0px;background: #fff;border-spacing: 0px">
							<tbody>
							<tr>
							<td style="padding: 40px 40px 5px 40px">
							<p>
									Good day ${contact.name},<br>
</p>
									<p>Hope you're doing well.  <br></p>

									<p>Introducing myself as your business & digital transformation consultant at UPapp factory.<br> </p>
									<p>Thank you for expressing your interest in exploring the services & expertise that we've used to work with tens of brands globally.<br></p>
									<p>For us to work with you in transforming your business idea into a Mobile App, can we connect to discuss? Please suggest a suitable time for my team to connect over a 15-30 mins initial exploratory session to discuss your idea/project.<br></p>

									<p>We are excited about your business case and keenly looking forward to working with you.<br></p>

									<p>Thanks for your time.
									<br></p>
									<p>Regards<br></p>
								
							</td>
					</tr>
											<td style="padding: 0px 10px 20px 10px">
                                                <div>
                                                    <div dir="ltr" class="gmail_signature" data-smartmail="gmail_signature">
                                                        <div dir="ltr">
                                                            <table cellpadding="0" cellspacing="0"
                                                                style="border-spacing:0px;border-collapse:collapse;color:rgb(68,68,68);width:480px;font-size:10pt;font-family:Arial,sans-serif;line-height:normal">
                                                                <tbody>
                                                                    <tr>
                                                                        <td valign="top" style="padding:10px 0px 12px;width:160px;vertical-align:top"><a
                                                                                href="https://www.upappfactory.com/"
                                                                                style="background-color:transparent;color:rgb(51,122,183)" target="_blank"><img
                                                                                    border="0" alt="Logo" width="141" src="https://upappfactory-static-images.s3.us-east-2.amazonaws.com/UPtm_Logo.png"
                                                                                    style="border:0px;vertical-align:middle;width:141px;height:auto"></a></td>
                                                                        <td style="padding:6px 0px;width:320px">
                                                                            <table cellpadding="0" cellspacing="0"
                                                                                style="border-spacing:0px;border-collapse:collapse;background-color:transparent">
                                                                                <tbody>
                                                                                    <tr>
                                                                                        <td
                                                                                            style="padding:0px;font-size:12pt;font-family:Arial,sans-serif;font-weight:bold;color:rgb(61,60,63)">
                                                                                            <span>Harris A.</span></td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td
                                                                                            style="padding:0px 0px 11px;font-size:10pt;font-family:Arial,sans-serif;color:rgb(61,60,63)">
                                                                                            <span><span zeum4c1="PR_1_0" data-ddnwab="PR_1_0" aria-invalid="spelling"
                                                                                                    class="LI ng">Business & Digital Transformation Consultant</span></td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td
                                                                                            style="padding:0px;font-size:10pt;font-family:Arial,sans-serif;color:rgb(155,155,155)">
                                                                                            <span>phone: +65 6714 6696</span></td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td
                                                                                            style="padding:0px;font-size:10pt;font-family:Arial,sans-serif;color:rgb(155,155,155)">
                                                                                            <span>email:&nbsp;</span><span style="color:rgb(23,147,210)"><a
                                                                                                    href="mailto:harris@upapp.co"
                                                                                                    target="_blank">harris@upapp.co</a></span></td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td
                                                                                            style="padding:0px;font-size:10pt;font-family:Arial,sans-serif;color:rgb(155,155,155)">
                                                                                            <span>website:&nbsp;</span><span style="color:rgb(23,147,210)"><a
                                                                                                    href="http://upapp.co"
                                                                                                    target="_blank">upapp.co</a></span></td>
                                                                                    </tr>
                                                                                    
                                                                                    <tr>
                                                                                        <td
                                                                                            style="padding:0px;font-size:10pt;font-family:Arial,sans-serif;color:rgb(155,155,155)">
                                                                                            <span>UPapp PTE LTD, 531A, Upper Cross St. #04-95, Hong Lim Complex 051531,
                                                                                                Singapore.</span></td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td style="padding:6px 0px 0px"><span
                                                                                                style="display:inline-block;height:22px"><span><a
                                                                                                        href="https://www.facebook.com/UPappfactory/"
                                                                                                        style="background-color:transparent;color:rgb(51,122,183)"
                                                                                                        target="_blank"><img alt="Facebook icon" border="0" width="20"
                                                                                                            height="20"
                                                                                                            src="https://codetwocdn.azureedge.net/images/mail-signatures/generator-dm/elegant-logo/fb.png"
                                                                                                            style="border:0px;vertical-align:middle;height:20px;width:20px"></a>&nbsp;&nbsp;</span><span><a
                                                                                                        href="https://www.linkedin.com/company/upappfactory/"
                                                                                                        style="background-color:transparent;color:rgb(51,122,183)"
                                                                                                        target="_blank"><img alt="LinkedIn icon" border="0" width="20"
                                                                                                            height="20"
                                                                                                            src="https://codetwocdn.azureedge.net/images/mail-signatures/generator-dm/elegant-logo/ln.png"
                                                                                                            style="border:0px;vertical-align:middle;height:20px;width:20px"></a>&nbsp;&nbsp;</span><span><a
                                                                                                        href="https://www.instagram.com/upappfactory/"
                                                                                                        style="background-color:transparent;color:rgb(51,122,183)"
                                                                                                        target="_blank"><img alt="Instagram icon" border="0" width="20"
                                                                                                            height="20"
                                                                                                            src="https://codetwocdn.azureedge.net/images/mail-signatures/generator-dm/elegant-logo/it.png"
                                                                                                            style="border:0px;vertical-align:middle;height:20px;width:20px"></a>&nbsp;&nbsp;</span></span>
                                                                                        </td>
                                                                                    </tr>
                                                                                </tbody>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td colspan="2"
                                                                            style="padding:8px 0px 0px;border-top:1px solid rgb(23,147,210);width:480px;font-family:Arial,sans-serif;color:rgb(155,155,155);text-align:justify">
                                                                            <span>This <span zeum4c1="PR_2_0" data-ddnwab="PR_2_0" aria-invalid="spelling"
                                                                                    class="LI ng">e-mail</span> may contain confidential/privileged information. If you
                                                                                are not the intended recipient, please notify the sender and destroy this e-mail. Any
                                                                                unauthorised copying, disclosure or distribution of the material in this <span
                                                                                    zeum4c1="PR_3_0" data-ddnwab="PR_3_0" aria-invalid="spelling"
                                                                                    class="LI ng">e-mail</span> is forbidden.</span></td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                    </div>
                                                </div>
                                            </td>
									</tr>
							</tbody>
					</table>
            </body></html>
            `
		}
		const OmanMailOptions = {
			from: 'zach@upapp.co',
			to: req.body.email,
			subject: `Turn Your Ideas Into Reality`,
			bcc: "contactus@upapp.co, maqsood@upapp.co",
			html: `<html lang="en">

			<head>
				<meta charset="UTF-8">
				<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
				<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
				<title>Bank Transfer</title>
				<style>
					.bank td,
					th {
						border: 1px solid #dddddd;
						text-align: left;
						padding: 8px;
					}
			
					.bank tr:nth-child(even) {
						background-color: #dddddd;
					}
				</style>
			</head>
			
			<body style="background: #fbfbfb;font-size:14px;font-family: Helvetica, sans-serif; line-height: 1.5">
				<table style="margin:0 auto;padding-bottom: 0px;background: #fff;border-spacing: 0px">
					<tbody>
						<tr>
							<td style="padding: 40px 40px 5px 40px">
								<p>
									Good day ${contact.name},<br>
								</p>
								<p>Hope you're doing well. <br></p>
			
								<p>Introducing myself as your business &amp; digital transformation consultant at UPapp factory.<br>
								</p>
								<p>Thank you for expressing your interest in exploring the services &amp; expertise that we've used
									to work with tens of brands globally.<br></p>
								<p>For us to work with you in transforming your business idea into a Mobile App, can we connect to
									discuss? Please suggest a suitable time for my team to connect over a 15-30 mins initial
									exploratory session to discuss your idea/project.<br></p>
			
								<p>We are excited about your business case and keenly looking forward to working with you.<br></p>
			
								<p>Thanks for your time.
									<br></p>
								<p>Regards<br></p>
			
							</td>
						</tr>
						<tr>
							<td style="padding: 0px 10px 20px 10px">
								<div>
									<div dir="ltr" class="gmail_signature" data-smartmail="gmail_signature">
										<div dir="ltr">
											<table cellpadding="0" cellspacing="0"
												style="border-spacing:0px;border-collapse:collapse;color:rgb(68,68,68);width:480px;font-size:10pt;font-family:Arial,sans-serif;line-height:normal">
												<tbody>
													<tr>
														<td valign="top"
															style="padding:10px 0px 12px;width:160px;vertical-align:top"><a
																href="https://www.upappfactory.com/"
																style="background-color:transparent;color:rgb(51,122,183)"
																target="_blank"><img border="0" alt="Logo" width="141"
																	src="https://upappfactory-static-images.s3.us-east-2.amazonaws.com/UPtm_Logo.png"
																	style="border:0px;vertical-align:middle;width:141px;height:auto"></a>
														</td>
														<td style="padding:6px 0px;width:320px">
															<table cellpadding="0" cellspacing="0"
																style="border-spacing:0px;border-collapse:collapse;background-color:transparent">
																<tbody>
																	<tr>
																		<td
																			style="padding:0px 0px 3px;font-size:12pt;font-family:Arial,sans-serif;font-weight:bold;color:#4fbfdd">
																			<span>Zach Naji</span></td>
																	</tr>
			
			
																	<tr>
																		<td
																			style="padding:0px 0px 3px;font-size:10pt;font-family:Arial,sans-serif;color:rgb(61,60,63)">
																			<span><span zeum4c1="PR_1_0" data-ddnwab="PR_1_0"
																					aria-invalid="spelling" class="LI ng">Manager -
																					Sales &amp; Business Development</span></span>
																		</td>
																	</tr>
																	<tr>
																		<td
																			style="padding:0px 0px 11px;font-size:12pt;font-family:Arial,sans-serif;font-weight:700;color:rgb(61,60,63)">
																			<span>UPapp PTE LTD.</span></td>
																	</tr>
																	<tr>
																		<td
																			style="padding: 3px 0px;font-size:10pt;font-family:Arial,sans-serif;color:rgb(155,155,155);">
																			<span><span
																					style="color: #4fbfdd;padding-right: 5px;font-weight: 700;">Skype</span><a
																					href="https://join.skype.com/invite/majtRFQzZlRf">Chat
																					Now</a></span></td>
																	</tr>
																	<tr>
																		<td
																			style="padding: 3px 0px;font-size:10pt;font-family:Arial,sans-serif;color:rgb(155,155,155);">
																			<span><span
																					style="color: #4fbfdd;padding-right: 5px;font-weight: 700;">Whatsapp</span>
																				+968 9238 4957</span></td>
																	</tr>
																	<tr>
																		<td
																			style="padding: 3px 0px;font-size:10pt;font-family:Arial,sans-serif;color:rgb(155,155,155);">
																			<span><span
																					style="color: #4fbfdd;padding-right: 5px;font-weight: 700;">W</span>
																				<a href="www.upapp.co">www.upapp.co</a></span></td>
																	</tr>
																	<tr>
																		<td
																			style="padding: 3px 0px;font-size:10pt;font-family:Arial,sans-serif;color:rgb(155,155,155);">
																			<span><span
																					style="color: #4fbfdd;padding-right: 5px;font-weight: 700;">E</span>&nbsp;</span><span
																				style="color:rgb(23,147,210)"><a
																					href="mailto:zach@upapp.co"
																					target="_blank">zach@upapp.co</a></span></td>
																	</tr>
			
			
																	<tr>
																		<td
																			style="padding:0px;font-size:10pt;font-family:Arial,sans-serif;color:rgb(155,155,155)">
																			<span>531A, Upper Cross St. #04-95, Hong Lim Complex
																				051531, Singapore.</span></td>
																	</tr>
																	<tr>
																		<td style="padding:6px 0px 0px"><span
																				style="display:inline-block;height:22px"><span><a
																						href="https://www.facebook.com/UPappfactory/"
																						style="background-color:transparent;color:rgb(51,122,183)"
																						target="_blank"><img alt="Facebook icon"
																							border="0" width="20" height="20"
																							src="https://codetwocdn.azureedge.net/images/mail-signatures/generator-dm/elegant-logo/fb.png"
																							style="border:0px;vertical-align:middle;height:20px;width:20px"></a>&nbsp;&nbsp;</span><span><a
																						href="https://www.linkedin.com/company/upappfactory/"
																						style="background-color:transparent;color:rgb(51,122,183)"
																						target="_blank"><img alt="LinkedIn icon"
																							border="0" width="20" height="20"
																							src="https://codetwocdn.azureedge.net/images/mail-signatures/generator-dm/elegant-logo/ln.png"
																							style="border:0px;vertical-align:middle;height:20px;width:20px"></a>&nbsp;&nbsp;</span><span><a
																						href="https://www.instagram.com/upappfactory/"
																						style="background-color:transparent;color:rgb(51,122,183)"
																						target="_blank"><img alt="Instagram icon"
																							border="0" width="20" height="20"
																							src="https://codetwocdn.azureedge.net/images/mail-signatures/generator-dm/elegant-logo/it.png"
																							style="border:0px;vertical-align:middle;height:20px;width:20px"></a>&nbsp;&nbsp;</span></span>
																		</td>
																	</tr>
																</tbody>
															</table>
														</td>
													</tr>
													<tr>
														<td colspan="2"
															style="padding:8px 0px 0px;border-top:1px solid rgb(23,147,210);width:480px;font-family:Arial,sans-serif;color:rgb(155,155,155);text-align:justify">
															<span>This <span zeum4c1="PR_2_0" data-ddnwab="PR_2_0"
																	aria-invalid="spelling" class="LI ng">e-mail</span> may contain
																confidential/privileged information. If you
																are not the intended recipient, please notify the sender and destroy
																this e-mail. Any
																unauthorised copying, disclosure or distribution of the material in
																this <span zeum4c1="PR_3_0" data-ddnwab="PR_3_0"
																	aria-invalid="spelling" class="LI ng">e-mail</span> is
																forbidden.</span></td>
													</tr>
												</tbody>
											</table>
										</div>
									</div>
								</div>
							</td>
						</tr>
					</tbody>
				</table>
			</body>
			
			</html>`
		}

		const notify = {
			from: process.env.EMAIL_USERNAME,
			to: "contactus@upapp.co",
			bcc: "maqsood@upapp.co, aslam@upapp.co",
			subject: `UPapp Dream New Lead Request`,
			html: `<!DOCTYPE html>
			<html lang="en">
			<head>
				<meta charset="UTF-8" />
				<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
				<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
				<title>Call Back Request</title>
				<style>
					.bank td,
					th {
						border: 1px solid #dddddd;
						text-align: left;
						padding: 8px;
					}

					.bank tr:nth-child(even) {
						background-color: #dddddd;
					}
				</style>
			</head>

			<body style="background: #fbfbfb;font-size:14px;font-family: Helvetica, sans-serif; line-height: 1.5">
				<table style="max-width: 500px;margin:0 auto;padding-bottom: 0px;background: #fff;border-spacing: 0px">
					<tbody>
						<tr style="display:inline-table;width: 100%;background: #f7f7f7;padding-top: 20px;padding-bottom: 20px;">
							<td style="
							 padding-left: 15px;
						">
								<a href="">
									<img style="width: 150px" width="150"
										src="https://upappfactory-static-images.s3.us-east-2.amazonaws.com/logo.png" alt="" />
								</a>
							</td>

						</tr>
						<tr>
							<td style="font-size:14px;font-family: Helvetica, sans-serif;color: #333333;padding: 20px;">
								Hi ,
								<br>
								We have received a new business request with ticket no: ${contact.UID}, please find the details below:
								<br>
								<br>
								<table class="bank" style="width:100%;border-spacing: 0px;border:none">

									<tr>
										<td>Ticket No</td>
										<td>${contact.UID}</td>
									</tr>
									<tr>
										<td>Name</td>
										<td>${contact.name}</td>
									</tr>
									<tr>
										<td>Email Address</td>
										<td>${contact.email}</td>
									</tr>
									<tr>
										<td>Phone No</td>
										<td>${contact.mobile}</td>
									</tr>
									<tr>
										<td>Project title</td>
										<td>${contact.business_name}</td>
									</tr>
									<tr>
										<td>Languages needed in</td>
										<td>${contact.languages}</td>
									</tr>
									<tr>
									<td>Budget</td>
									<td>${contact.budget}</td>
								</tr>
									<tr>
										<td>Description</td>
										<td>${contact.description}</td>
									</tr>
								</table>

								<br />
								Thanks!
								<br>
								<br />
								Regards,
								<br />
								UPapp factory
							</td>
						</tr>
						<tr style="text-align: center;background: #f7f7f7">

							<td style=" padding:0px 20px; text-align: center;padding-right: 15px;">
								<p style="margin: 10px auto 0px auto;line-height: 0.8;">
									<a href="mailto:contactus@upapp.co" style="text-decoration: none;">contactus@upapp.co</a>
									|
									<a style="text-decoration: none;" href="www.upappfactory.com">UPappfactory</a><br /><br />
								</p>
								<a href="https://www.facebook.com/UPappfactory/"><img style="width: 25px" width="25"
										src="https://upappfactory-static-images.s3.us-east-2.amazonaws.com/facebook-icon.png" /></a>
								<a href="https://www.linkedin.com/company/upappfactory/"><img style="width: 25px" width="25"
										src="https://upappfactory-static-images.s3.us-east-2.amazonaws.com/linkedin-icon.png" /></a>
								<a href="https://www.instagram.com/upappfactory/"><img style="width: 25px" width="25"
										src="https://upappfactory-static-images.s3.us-east-2.amazonaws.com/Instagram-Icon.png" /></a>
							</td>

						</tr>
						<tr style="padding:20px;text-align: center;background: #f7f7f7">
							<td style="padding: 5px 20px 20px 20px;text-align: center;background: #f7f7f7;">
								Address: 531A Upper Cross Street, #04-95, Hong Lim Complex 051531 Singapore, Ph: +6567146696
							</td>
						</tr>
					</tbody>
				</table>
			</body>
			</html>`
		}
		
		await OmanTransporter.sendMail(OmanMailOptions)

	
		await transporter.sendMail(notify)
		// res.status(201).send({ error: false, success: true, message: "Contact request success", result: contact })
		res.redirect('https://upappfactory.com/upappdream/thank-you.html')
	} catch (error) {
		console.log(error)
		res.status(400).send({ error: true, success: false, message: "Bad Request", result: error })
	}

})

router.delete('contact-us/:id', async (req, res) => {
	try {
		const contact = await Application.findOneAndDelete({ UID: req.params.id })
		if (!contact)
			return res.status(400).send({ error: true, success: false, message: "Bad request", result: "" })
		return res.status(200).send({ error: false, success: true, message: "Deleted successfully", result: contact })
	} catch (e) {
		res.status(500).send({ error: false, success: false, message: "server error", result: e })
	}
})

module.exports = router