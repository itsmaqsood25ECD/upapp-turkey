require('dotenv').config()
const express = require('express')
const auth = require('../middleware/auth')
const nodemailer = require('nodemailer')
const Order = require('../models/order.model')
const router = new express.Router()
const stripe = require("stripe")("sk_live_Gk8K8f0mC2CL2gAiTT302D3e00fHjDTMsg");
// const stripe = require("stripe")("sk_test_DZ8EAXNzp6uX9w1cajLUT3Nh00NfIVsMFd");

const uuid = require("uuid/v4");
const Payment = require('../models/payment.model')
const moment = require('moment')


const transporter = nodemailer.createTransport({
	host: 'smtp.gmail.com',
	port: 465,
	secure: true,
	auth: {
		user: 'noreply@upapp.co',
		pass: "Ecommdesk@123"
	}
})

const domainTransporter = nodemailer.createTransport({
	host: 'smtp.gmail.com',
	port: 465,
	secure: true,
	auth: {
		user: 'support@upappfactory.com',
		pass: "$@2Support2@$,23"
	}
})

router.post('/proceed_to_pay', auth, async (req, res) => {
	try {
		const { token } = req.body
		const prevOrder = await Order.find({}, { UID: 1, _id: 0 }).sort({ _id: -1 })
		let counter, UID
		if (prevOrder.length === 0) {
			counter = 1
		} else {
			counter = prevOrder[0].UID.slice(8)
			counter = +counter + 1
		}
		if (counter < 10) {
			UID = `UAF-ORD-0000000${counter}`
		} else if (counter < 100 && counter >= 10) {
			UID = `UAF-ORD-0000000${counter}`
		} else if (counter < 1000 && counter >= 100) {
			UID = `UAF-ORD-000000${counter}`
		} else if (counter < 10000 && counter >= 1000) {
			UID = `UAF-ORD-00000${counter}`
		} else if (counter < 100000 && counter >= 10000) {
			UID = `UAF-ORD-0000${counter}`
		} else if (counter < 1000000 && counter >= 100000) {
			UID = `UAF-ORD-000${counter}`
		} else if (counter < 10000000 && counter >= 1000000) {
			UID = `UAF-ORD-00${counter}`
		} else if (counter < 100000000 && counter >= 10000000) {
			UID = `UAF-ORD-0${counter}`
		} else {
			UID = `UAF-ORD-${counter}`
		}

		const customer = await stripe.customers.create({
			email: token.email,
			source: token.id
		});

		const idempotency_key = uuid();
		const charge = await stripe.charges.create(
			{
				amount: parseInt(req.body.total_price * 100),
				currency: "sgd",
				customer: customer.id,
				receipt_email: token.email,
				description: `Payment for ${req.body.product_name}`,
				shipping: {
					name: token.card.name,
					address: {
						line1: token.card.address_line1,
						line2: token.card.address_line2,
						city: token.card.address_city,
						country: token.card.address_country,
						postal_code: token.card.address_zip
					}
				}
			},
			{
				idempotency_key
			}
		);
		const order = new Order(req.body)
		order.UID = UID
		order.created_by = req.user
		const response = await order.save()
		const payment = new Payment(req.body)
		payment.order = response._id
		payment.invoice_url = charge.receipt_url
		const billing_info = {
			line1: token.card.address_line1,
			line2: token.card.address_line2,
			city: token.card.address_city,
			country: token.card.address_country,
			postal_code: token.card.address_zip

		}
		payment.billing_info = billing_info
		payment.created_by = req.user
		await payment.save()
		const mailOptions = {
			from: 'noreply@upapp.co',
			to: req.user.email,
			subject: `Confirmation | ORDER ID : ${UID}`,
			bcc: "contactus@upapp.co, accounts.ind@upapp.co, support@upappc.co, maqsood@upapp.co",
			html: `<!DOCTYPE html>
			<html lang="en">
				<head>
					<meta charset="UTF-8" />
					<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
					<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
					<title>
						Order Confirmation
					</title>
				</head>

				<body style="background: #fbfbfb;font-size:14px;font-family: Helvetica, sans-serif; line-height: 1.5">
					<table style="max-width: 500px;margin:0 auto;padding-bottom: 0px;background: #fff;border-spacing: 0px">
						<tbody>
							<tr style="display:inline-table;width: 100%;background: #f7f7f7;padding-top: 20px;padding-bottom: 20px;">
								<td style="padding-left: 15px;">
									<a href="">
										<img style="width: 150px" width="150" src="https://upappfactory-static-images.s3.us-east-2.amazonaws.com/logo.png" alt="" />
									</a>
								</td>
							</tr>
							<td style="font-size:14px; font-family: Helvetica, sans-serif; color: #333333;padding: 20px;">
								Dear ${req.user.firstName} ${req.user.lastName},
								<br /><br />
								Thank you for your order.
								<br />
								Order ID : ${UID}
								<br />
								Product Name : ${order.product_name}
								<br />
								Package Type : ${order.pack_type}
								<br />
								Plan Name : ${order.plan_name} 
								<br />
								Plan Type : ${order.plan_type}
								<br />
								<br />
								Your invoice and Receipt will be sent to your registered email Id.
								<br /> Our support team will get in touch with you after your details are confirmed and verified.
								Regards,
								<br />
								Support Team
								<br />
								UPappfactory
							</td>
							<tr style="text-align: center;background: #f7f7f7">
								<td style="padding:0px 20px;text-align: center;background: #f7f7f7">
								</td>
							</tr>
							<tr style="text-align: center;background: #f7f7f7">
								<td style=" padding:0px 20px; text-align: center;padding-right: 15px;">
									<p style="margin: 10px auto 0px auto;line-height: 0.8;">
										<a href="mailto:contactus@upapp.co" style="text-decoration: none;">contactus@upapp.co</a>
										|
										<a style="text-decoration: none;" href="www.upappfactory.com">UPappfactory</a><br /><br />
									</p>
									<a href="https://www.facebook.com/UPappfactory/"><img style="width: 25px" width="25" src="https://upappfactory-static-images.s3.us-east-2.amazonaws.com/facebook-icon.png" /></a>
									<a href="https://www.linkedin.com/company/upappfactory/"><img style="width: 25px" width="25" src="https://upappfactory-static-images.s3.us-east-2.amazonaws.com/linkedin-icon.png" /></a>
									<a href="https://www.instagram.com/upappfactory/"><img style="width: 25px" width="25" src="https://upappfactory-static-images.s3.us-east-2.amazonaws.com/Instagram-Icon.png" /></a>
								</td>
							</tr>
							<tr style="padding:20px;text-align: center;background: #f7f7f7">
								<td style="padding: 5px 20px 20px 20px;text-align: center;background: #f7f7f7;">
									Address: 531A Upper Cross Street, #04-95, Hong Lim Complex 051531 Singapore, Ph: +6567146696
								</td>
							</tr>
						</tbody>
					</table>
				</body>
			</html>`
		}
		const notify = {
			from: 'noreply@upapp.co',
			to: "support@upapp.co",
			subject: `New order placed by ${req.user.firstName} ${req.user.lastName}`,
			bcc: "support@upapp.co, contactus@upapp.co, accounts.ind@upapp.co, maqsood@upapp.co",
			html: `<!DOCTYPE html>
			<html lang="en">
			
			<head>
				<meta charset="UTF-8" />
				<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
				<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
				<title>New Order Placed</title>
				<style>
					.bank td,
					th {
						border: 0px solid #dddddd;
						text-align: left;
						padding: 8px;
					}
			
					.bank tr:nth-child(even) {
						background-color: #dddddd;
					}
				</style>
			</head>
			
			<body style="background: #fbfbfb;font-size:14px;font-family: Helvetica, sans-serif; line-height: 1.5">
				<table
					style="border: 1px solid #ddd; max-width: 500px;margin:0 auto;padding-bottom: 0px;background: #fff;border-spacing: 0px">
					<tbody>
						<tr style="display:inline-table;width: 100%;background: #f7f7f7;padding-top: 20px;padding-bottom: 20px;">
							<td style="
							 padding-left: 15px;
						">
								<a href="">
									<img style="width: 150px" width="150"
										src="https://upappfactory-static-images.s3.us-east-2.amazonaws.com/logo.png" alt="" />
								</a>
							</td>
			
						</tr>
						<tr>
							<td style="font-size:14px;font-family: Helvetica, sans-serif;color: #333333;padding: 20px;">
								Hi Support Team,
								<br>
								We have received a new order ID ${order.UID}, please find the details below
								<br>
								<br>
								<table class="bank" style="width:100%;border-spacing: 0px;border:none">
			
									<tr>
										<td>Order ID</td>
										<td>${order.UID}</td>
									</tr>
									<tr>
										<td>Customer ID</td>
										<td>${req.user.UID}</td>
									</tr>
									<tr>
										<td>Name</td>
										<td>${req.user.firstName} ${req.user.lastName}</td>
									</tr>
									<tr>
										<td>Email Address</td>
										<td>${req.user.email}</td>
									</tr>
									<tr>
										<td>Phone No</td>
										<td>${req.user.phone}</td>
									</tr>
									<tr>
										<td>Country</td>
										<td>${req.user.country}</td>
									</tr>
									<tr>
										<td>Product Name</td>
										<td>${order.product_name}</td>
									</tr>
									<tr>
									<td>Theme </td>
									<td>${order.theme_id}</td>
									</tr>
									<tr>
									<td>Package Type</td>
									<td>${order.pack_type}</td>
									</tr>
									<tr>
									<td>Plan Name</td>
									<td>${order.plan_name}</td>
									</tr>
									<tr>
										<td>Plan Type</td>
										<td>${order.plan_type}</td>
									</tr>
									<tr>
									<td>Business Email</td>
									<td>${order.business_email}</td>
									</tr>
									<tr>
									<td>Business category</td>
									<td>${order.business_category}</td>
									</tr>
									<tr>
									<td>Business Logo</td>
									<td>${order.business_logo}</td>
									</tr>
									<tr>
									<td>Own Domain</td>
									<td>${order.own_domain}</td>
									</tr>
									<tr>
									<td>Domain Provider</td>
									<td>${order.domain_provider}</td>
									</tr>
									<tr>
									<td>Domain Username</td>
									<td>${order.domain_username}</td>
									</tr>
									<tr>
									<td>Domain Password</td>
									<td>${req.body.domain_password}</td>
									</tr>
									<tr>
									<td>Default Currency</td>
									<td>${order.default_currency}</td>
									</tr>
									<tr>
									<td>Primary Color</td>
									<td>${order.primary_color}</td>
									</tr>
									<tr>
									<td>Secondary Color</td>
									<td>${order.secondary_color}</td>
									</tr>
									<tr>
									<td>Additional Language</td>
									<td>${order.languages}</td>
									</tr>
									<tr>
									<td>Payment Gateway Selected</td>
									<td>${order.payment_gateway_Option_Selected}</td>
									</tr>
									<tr>
									<td>Payment Gateway Provider</td>
									<td>${order.payment_gateway_provider}</td>
									</tr>
									<tr>
									<td>Payment Gateway Country</td>
									<td>${order.payment_gateway_country}</td>
									</tr>
									<tr>
									<td>Payment Gateway Other Details</td>
									<td>${order.payment_gateway_details}</td>
									</tr>
									<tr>
									<td>Shipping Gateway Provider</td>
									<td>${order.shipping_gateway_provider}</td>
									</tr>
									<tr>
									<td>Shipping Gateway Provider</td>
									<td>${order.shipping_gateway_country}</td>
									</tr>
									<tr>
									<td>Shipping Gateway Other Details</td>
									<td>${order.shipping_gateway_details}</td>
									</tr>
									<tr>
									<td>Training Hours</td>
									<td>${+(order.training_hour)}</td>
									</tr>
									<tr>
									<td>Product List Count</td>
									<td>${+(order.product_list_count)}</td>
									</tr>
									<tr>
									<td>All Costs</td>
									</tr>
									<tr>
									<td>Shipping Gateway Cost</td>
									<td> SGD ${order.additional_costs.shipping_cost}</td>
									</tr>
									<tr>
									<td>Training Cost</td>
									<td> SGD ${order.additional_costs.training_cost}</td>
									</tr>
									<tr>
									<td>Product Listing Cost</td>
									<td> SGD ${order.additional_costs.product_cost}</td>
									</tr>
									<tr>
									<td>Payment Gateway Listing Cost</td>
									<td> SGD ${order.additional_costs.payment_gateway_cost}</td>
									</tr>
									<tr>
									<td>Additional Language Cost</td>
									<td> SGD ${order.additional_costs.language_cost}</td>
									</tr>
									<tr>
									<td>One Time Setup Cost</td>
									<td> SGD ${order.additional_costs.one_time_setup_charge}</td>
									</tr>
									<tr>
									<td>Total Cost Charged </td>
									<td> SGD ${(order.total_price).toFixed(1)}</td>
									</tr>
									
									<tr>
										<td>Received Order Date & Time</td>
										<td>${moment(order.order_placed_at).format('lll')}</td>
									</tr>
								</table>
			
								<br />
								Thanks!
								<br>
								<br />
								Regards,
								<br />
								UPapp factory
							</td>
						</tr>
						<tr style="text-align: center;background: #f7f7f7">
			
							<td style=" padding:0px 20px; text-align: center;padding-right: 15px;">
								<p style="margin: 10px auto 0px auto;line-height: 0.8;">
									<a href="mailto:contactus@upapp.co" style="text-decoration: none;">contactus@upapp.co</a>
									|
									<a style="text-decoration: none;" href="www.upappfactory.com">UPappfactory</a><br /><br />
								</p>
								<a href="https://www.facebook.com/UpAppFactory/"><img style="width: 25px" width="25"
										src="https://upappfactory-static-images.s3.us-east-2.amazonaws.com/facebook-icon.png" /></a>
								<a href="https://www.linkedin.com/company/upappfactory/"><img style="width: 25px" width="25"
										src="https://upappfactory-static-images.s3.us-east-2.amazonaws.com/linkedin-icon.png" /></a>
								<a href="https://www.instagram.com/upappfactory/"><img style="width: 25px" width="25"
										src="https://upappfactory-static-images.s3.us-east-2.amazonaws.com/Instagram-Icon.png" /></a>
							</td>
			
						</tr>
						<tr style="padding:20px;text-align: center;background: #f7f7f7">
							<td style="padding: 5px 20px 20px 20px;text-align: center;background: #f7f7f7;">
								Address: 531A Upper Cross Street, #04-95, Hong Lim Complex 051531 Singapore, Ph: +6567146696
							</td>
						</tr>
					</tbody>
				</table>
			</body>
			</html>`
		}
		await transporter.sendMail(mailOptions)
		await transporter.sendMail(notify)
		if (req.body.selfDomainAdd) {
			const Self_Domain_add = {
				from: 'support@upappfactory.com',
				to: req.user.email,
				subject: `Update the DNS and Name servers`,
				bcc: "payments@upappfactory.com,ali@upappfactory.com,tejas@upappfactory.com,support@upappfactory.com",
				html: `<!DOCTYPE html>
				<html lang="en">
					<head>
						<meta charset="UTF-8" />
						<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
						<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
						<title>
						 Update the DNS and Name servers
						</title>
					</head>
	
					<body style="background: #fbfbfb;font-size:14px;font-family: Helvetica, sans-serif; line-height: 1.5">
						<table style="max-width: 500px;margin:0 auto;padding-bottom: 0px;background: #fff;border-spacing: 0px">
							<tbody>
								<tr style="display:inline-table;width: 100%;background: #f7f7f7;padding-top: 20px;padding-bottom: 20px;">
									<td style="padding-left: 15px;">
										<a href="">
											<img style="width: 150px" width="150" src="https://upappfactory-static-images.s3.us-east-2.amazonaws.com/logo.png" alt="" />
										</a>
									</td>
								</tr>
								<td style="font-size:14px; font-family: Helvetica, sans-serif; color: #333333;padding: 20px;">
									Dear ${req.user.firstName} ${req.user.lastName},
									<br /><br />
									Thank you for choosing UPappfactory.com!
									<br /><br />
									In order to use your domain, you may have to update the DNS records and Name servers in your domain provider’s name server settings.
									<br /><br />
									All you have to do is enter these name servers in your domain provider's name server settings. The name servers are as follows:
									<br /><br />
									ns1045.ui-dns.org
									<br />
									ns1045.ui-dns.de
									<br />
									ns1045.ui-dns.biz
									<br />
									ns1045.ui-dns.com
									<br />
									<br />
									If you do not know how to configure the name servers with your domain provider, look in your provider's help center or contact the provider's customer service. We have already found the appropriate help center articles for a few major providers:
									<br />
									<br />
									<a href="https://www.godaddy.com/help/add-an-ns-record-19212">GoDaddy</a>
									<br />
									<a href="https://knowledge.web.com/subjects/article/KA-01114/en-us">Network Solutions</a>
									<br />
									<a href="https://help.enom.com/hc/en-us/articles/115000486451-How-to-Change-Nameservers-DNS-">eNom</a>
									<br />
									<a href="http://support.hostgator.com/articles/Article/how-do-i-change-my-dns-or-name-servers">Host Gator</a>
									<br />
									<a href="https://my.bluehost.com/cgi/help/transfer_client_start">Bluehost</a>
									<br />
									<br />
									Once you have changed the name servers with your domain provider, it can take up to 72 hours before our systems recognize the domain as fully set up.
									<br />
									<br />
									<br />
									<br />
									Best Regards,
									<br />
									UPapp factory support Team
									
								</td>
								<tr style="text-align: center;background: #f7f7f7">
									<td style="padding:0px 20px;text-align: center;background: #f7f7f7">
									</td>
								</tr>
								<tr style="text-align: center;background: #f7f7f7">
									<td style=" padding:0px 20px; text-align: center;padding-right: 15px;">
										<p style="margin: 10px auto 0px auto;line-height: 0.8;">
											<a href="mailto:contactus@upapp.co" style="text-decoration: none;">contactus@upapp.co</a>
											|
											<a style="text-decoration: none;" href="www.upappfactory.com">UPappfactory</a><br /><br />
										</p>
										<a href="https://www.facebook.com/UPappfactory/"><img style="width: 25px" width="25" src="https://upappfactory-static-images.s3.us-east-2.amazonaws.com/facebook-icon.png" /></a>
										<a href="https://www.linkedin.com/company/upappfactory/"><img style="width: 25px" width="25" src="https://upappfactory-static-images.s3.us-east-2.amazonaws.com/linkedin-icon.png" /></a>
										<a href="https://www.instagram.com/upappfactory/"><img style="width: 25px" width="25" src="https://upappfactory-static-images.s3.us-east-2.amazonaws.com/Instagram-Icon.png" /></a>
									</td>
								</tr>
								<tr style="padding:20px;text-align: center;background: #f7f7f7">
									<td style="padding: 5px 20px 20px 20px;text-align: center;background: #f7f7f7;">
										Address: 531A Upper Cross Street, #04-95, Hong Lim Complex 051531 Singapore, Ph: +6567146696
									</td>
								</tr>
							</tbody>
						</table>
					</body>
				</html>`
			}
			await domainTransporter.sendMail(Self_Domain_add)
		}
		return res.status(200).send({ error: false, success: true, message: "Order Placed successfully", result: order })
	} catch (error) {
		console.log(error)
		return res.status(500).send({ error: true, success: false, message: "Server Error", result: error })
	}
})




module.exports = router