const express = require('express')
const EventsRequest = require('../models/events.request.model')
const nodemailer = require('nodemailer')
const moment = require('moment')

const router = new express.Router()

// Send the email
const transporter = nodemailer.createTransport({
	host: 'smtp.zoho.com',
	port: 465,
	secure: true,
	auth: {
		user: process.env.EMAIL_USERNAME,
		pass: process.env.EMAIL_PASSWORD
	}
})


router.post('/event-request', async (req, res) => {
	try {
		const eventRequest = new EventsRequest(req.body)
		if (!eventRequest)
			return res.status(400).send({ error: true, success: false, message: "Bad Request", result: "" })
		await eventRequest.save()
		const mailOptions = {
			from: process.env.EMAIL_USERNAME,
			to: eventRequest.email,
			subject: `Thank You `,
			bcc: "contactus@upapp.co",
			html: `<!DOCTYPE html>
			<html lang="en">
				<head>
					<meta charset="UTF-8" />
					<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
					<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
					<title>
						Contact Us
					</title>
				</head>
				
				<body style="background: #fbfbfb;font-size:14px;font-family: Helvetica, sans-serif; line-height: 1.5">
					<table style="max-width: 500px;margin:0 auto;padding-bottom: 0px;background: #fff;border-spacing: 0px">
						<tbody>
							<tr style="display:inline-table;width: 100%;background: #f7f7f7;padding-top: 20px;padding-bottom: 20px;">
								<td style="padding-left: 15px;">
									<a href="">
										<img style="width: 150px" width="150" src="https://upappfactory-static-images.s3.us-east-2.amazonaws.com/logo.png" alt="" />
									</a>
								</td>
							</tr>
							<td style="font-size:14px; font-family: Helvetica, sans-serif; color: #333333;padding: 20px;">
								Dear ${eventRequest.name},
								<br /><br />
								Thank you for showing interest for the event. Hope to see you in the event.
								<br /><br />
								Regards,
								<br />
								Support Team
								<br />
								UPappfactory
							</td>
							<tr style="text-align: center;background: #f7f7f7">
								<td style="padding:0px 20px;text-align: center;background: #f7f7f7">
								</td>
							</tr>
							<tr style="text-align: center;background: #f7f7f7">
								<td style=" padding:0px 20px; text-align: center;padding-right: 15px;">
									<p style="margin: 10px auto 0px auto;line-height: 0.8;">
										<a href="mailto:contactus@upapp.co" style="text-decoration: none;">contactus@upapp.co</a>
										|
										<a style="text-decoration: none;" href="www.upappfactory.com">UPappfactory</a><br /><br />
									</p>
									<a href="https://www.facebook.com/UPappfactory/"><img style="width: 25px" width="25" src="https://upappfactory-static-images.s3.us-east-2.amazonaws.com/facebook-icon.png" /></a>
									<a href="https://www.linkedin.com/company/upappfactory/"><img style="width: 25px" width="25" src="https://upappfactory-static-images.s3.us-east-2.amazonaws.com/linkedin-icon.png" /></a>
									<a href="https://www.instagram.com/upappfactory/"><img style="width: 25px" width="25" src="https://upappfactory-static-images.s3.us-east-2.amazonaws.com/Instagram-Icon.png" /></a>
								</td>
							</tr>
							<tr style="padding:20px;text-align: center;background: #f7f7f7">
								<td style="padding: 5px 20px 20px 20px;text-align: center;background: #f7f7f7;">
									Address: 531A Upper Cross Street, #04-95, Hong Lim Complex 051531 Singapore, Ph: +6567146696
								</td>
							</tr>
						</tbody>
					</table>
				</body>
			</html>`
		}
		await transporter.sendMail(mailOptions)
		return res.status(200).send({ error: false, success: true, message: "Success", result: eventRequest })
	} catch (error) {
		console.error(error)
		return res.status(400).send({ error: true, success: false, message: "Bad Request", result: error })
	}
})

router.get('/event-request', async (req, res) => {
	try {
		const eventsRequest = await EventsRequest.find({})
		if (!eventsRequest)
			return res.status(404).send({ error: true, success: false, message: "Not Found", result: "" })
		return res.status(200).send({ error: false, success: true, message: "Found Data", result: eventsRequest })
	} catch (error) {
		console.error(error)
		return res.status(400).send({ error: true, success: false, message: "Bad Request", result: error })
	}
})


module.exports = router