const express = require('express')
const Category = require('../models/category.management.model')
const { ObjectID } = require('mongodb')


const router = new express.Router()

// post contact form data
router.post('/category-management', async (req, res) => {
    try {
        const category = new Category(req.body)
        if (!category)
            return res.status(400).send({ error: true, success: false, message: "Bad request", result: "" })
        await category.save()
        return res.status(201).send({ error: false, success: true, message: "Data posted successfully", result: category })
    } catch (e) {
        console.log(e)
        res.status(400).send({ error: true, success: false, message: "Bad Request", result: e })
    }
})

router.get('/category-management', async (req, res) => {
    try {
        const category = await Category.find({})
        if (!category)
            return res.status(404).send({ error: true, success: false, message: "Data not found", result: "" })
        return res.status(200).send({ error: false, success: true, message: "Data found", result: category })
    } catch (e) {
        console.log(e)
        return res.status(500).send({ error: false, success: true, message: "Bad request", result: e })
    }
})

router.get('/category-management/:id', async (req, res) => {
    try {
        const category = await Category.findById(req.params.id)
        if (!category)
            return res.status(404).send({ error: true, success: false, message: "Not found", result: "" })
        return res.status(200).send({ error: false, success: true, message: "Data found", result: category })
    } catch (e) {
        console.log(e)
        return res.status(500).send({ error: true, success: false, message: "Server error", result: e })
    }
})
router.put('/category-management/:id', async (req, res) => {
    try {
        const category = await Category.findByIdAndUpdate(req.params.id, { $set: req.body }, { new: true })
        if (!category)
            return res.status(400).send({ error: true, success: false, message: "Bad request", result: "" })
        return res.status(200).send({ error: false, success: "Updated successfully", result: category })
    } catch (e) {
        console.log(e)
        return res.status(500).send({ error: true, success: "Server error", result: e })
    }
})

module.exports = router